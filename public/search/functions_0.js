var searchData=
[
  ['_5f_5fcxa_5fatexit_0',['__cxa_atexit',['../OS_8cpp.html#a3259a7e7d5f86c3d46721e7d198bcdc8',1,'__cxa_atexit(void *, void(*)(void *), void *):&#160;OS.cpp'],['../OS_8hpp.html#a074a0bf6781862bd96105242d449d270',1,'__cxa_atexit(void *pThis, void(*pFunc)(void *pThis), void *pHandle):&#160;OS.hpp']]],
  ['_5f_5fcxa_5fpure_5fvirtual_1',['__cxa_pure_virtual',['../OS_8cpp.html#a4464d4205cf92370b8d5077d93bc48a6',1,'__cxa_pure_virtual(void):&#160;OS.cpp'],['../OS_8hpp.html#a4464d4205cf92370b8d5077d93bc48a6',1,'__cxa_pure_virtual(void):&#160;OS.cpp']]],
  ['_5f_5fthrow_5fbad_5ffunction_5fcall_2',['__throw_bad_function_call',['../namespacestd.html#a68007c73c66abf5fc76576c747a5539b',1,'std']]],
  ['_5fcpu_5fdisable_5fint_3',['_CPU_disable_int',['../CPU_8hpp.html#a3781b6f9329a7f8bfb1435c80325fd66',1,'CPU.hpp']]],
  ['_5fcpu_5fenable_5fint_4',['_CPU_enable_int',['../CPU_8hpp.html#a3d209096a98dcb2309b1f254c3456e21',1,'CPU.hpp']]],
  ['_5fcpu_5fhalt_5',['_CPU_halt',['../CPU_8hpp.html#ac141feec4b537e874b92965c2e7d6aa4',1,'CPU.hpp']]],
  ['_5fcpu_5fidle_6',['_CPU_idle',['../CPU_8hpp.html#aced4960bf71284313e91de89e90ac80d',1,'CPU.hpp']]],
  ['_5fcpu_5fwait_7',['_CPU_wait',['../CPU_8hpp.html#a4707b9e303902897448b8ca2b6a0cd8c',1,'CPU.hpp']]]
];
