var searchData=
[
  ['uart0_0',['UART0',['../classUART0.html#ae3a3d722c7a6ae0121f52c25cf713867',1,'UART0::UART0(const UART0 &amp;)=delete'],['../classUART0.html#ae64b8ef040f4a968ee955316488730db',1,'UART0::UART0()']]],
  ['uart1_1',['UART1',['../classUART1.html#a56e981c55f84af14cb453e25427de6f0',1,'UART1::UART1()'],['../classUART1.html#af76a378ee453f78df3415090c8c8d0d2',1,'UART1::UART1(const UART1 &amp;)=delete']]],
  ['uart3_2',['UART3',['../classUART3.html#a88257b941510b7796f802f85d3bf23f9',1,'UART3::UART3()'],['../classUART3.html#a31851ac866d586ac13ea4dba4ac5f5bb',1,'UART3::UART3(const UART3 &amp;)=delete']]],
  ['unassign_3',['unassign',['../namespaceCalls.html#a28e1b7ffd83f301045af606dafe15933',1,'Calls::unassign()'],['../namespaceDispatcher.html#a5e4a9f461dd016266302df1f9a6a7038',1,'Dispatcher::unassign()']]],
  ['unused_4',['unused',['../classMM.html#aa28be59dbc5d4c71c3654fffdda79d14',1,'MM::unused()'],['../classMMBitmask.html#a6f832bb3b3df7e963a39e891cba1ce2c',1,'MMBitmask::unused()'],['../classMMGready.html#a1d919f8b0c6d1854f8e4ca8af4e29aa5',1,'MMGready::unused()'],['../classMMLinkedList.html#a477d496e773b899b79280ab75ecc95dd',1,'MMLinkedList::unused()']]],
  ['up_5',['up',['../classVESA.html#afc04d2c807221f8336dd063bb58664cd',1,'VESA']]],
  ['usealt0_6',['useAlt0',['../namespaceGpio.html#aed5aef9cc7eaef89859835047c79d65e',1,'Gpio']]],
  ['usealt1_7',['useAlt1',['../namespaceGpio.html#ac4654838d2177784a8c5e5584253fa17',1,'Gpio']]],
  ['usealt2_8',['useAlt2',['../namespaceGpio.html#a30569a67206140372356459660246d2d',1,'Gpio']]],
  ['usealt3_9',['useAlt3',['../namespaceGpio.html#a1026f022adcc856e1c5617a61c94d55c',1,'Gpio']]],
  ['usealt4_10',['useAlt4',['../namespaceGpio.html#a609a45d55cb19005e4057e9c353ae40d',1,'Gpio']]],
  ['usealt5_11',['useAlt5',['../namespaceGpio.html#a20109377f545c6cd67a902e8b6bc4999',1,'Gpio']]],
  ['useinput_12',['useInput',['../namespaceGpio.html#a80297dfbf04c5c4ea84dfaaeb0df6d99',1,'Gpio']]],
  ['useoutput_13',['useOutput',['../namespaceGpio.html#a374260d568c1dbda189aba8402b5f597',1,'Gpio']]]
];
