var searchData=
[
  ['xhci_5freg_5fcap_5fhcsparams1_0',['XHCI_REG_CAP_HCSPARAMS1',['../Xhci_8cpp.html#afa09d884a866fd419e8488e4eb4fea5a',1,'Xhci.cpp']]],
  ['xhci_5freg_5fcap_5fhcsparams1_5fmax_5fports_5fmask_1',['XHCI_REG_CAP_HCSPARAMS1_MAX_PORTS_MASK',['../Xhci_8cpp.html#ad7b0c75f97d747ed9bc7773f185de693',1,'Xhci.cpp']]],
  ['xhci_5freg_5fcap_5fhcsparams1_5fmax_5fports_5fshift_2',['XHCI_REG_CAP_HCSPARAMS1_MAX_PORTS_SHIFT',['../Xhci_8cpp.html#a2d0c145c18dc90998f53d4330139cc0e',1,'Xhci.cpp']]],
  ['xhci_5freg_5fop_5fports_5fbase_3',['XHCI_REG_OP_PORTS_BASE',['../Xhci_8cpp.html#ac7e2c9a1919a3ecfe55948fae352ee1f',1,'Xhci.cpp']]],
  ['xhci_5freg_5fop_5fports_5fport_5f_5fsize_4',['XHCI_REG_OP_PORTS_PORT__SIZE',['../Xhci_8cpp.html#af235a5cf9f6869039bd19be680f3d9a7',1,'Xhci.cpp']]],
  ['xhci_5freg_5fop_5fports_5fportsc_5',['XHCI_REG_OP_PORTS_PORTSC',['../Xhci_8cpp.html#a6d1eebff00664fd73f2a7251a7877b79',1,'Xhci.cpp']]],
  ['xhci_5freg_5fop_5fports_5fportsc_5fccs_6',['XHCI_REG_OP_PORTS_PORTSC_CCS',['../Xhci_8cpp.html#ae7516030ec6d8165552eb8665c6e406e',1,'Xhci.cpp']]]
];
