var searchData=
[
  ['read_0',['read',['../classSerial.html#ad6dfc08d1e81bccb060477becb0dfe1d',1,'Serial::read()'],['../classUART0.html#ac56142abc9ea7594a4eef5a4fed02c7e',1,'UART0::read()'],['../classUART1.html#a3def6a3c6a81f7577c05d5e669f797c6',1,'UART1::read()'],['../classUART3.html#a7e8546f83f807d7ff9eacbf28d3e25cf',1,'UART3::read()'],['../classPS2Keyboard.html#a93831b3a2543c98f730daa000832fc60',1,'PS2Keyboard::read()']]],
  ['read16_1',['read16',['../namespacePeripherals.html#a615f57d1b97db7a1280797e6bb680359',1,'Peripherals']]],
  ['read32_2',['read32',['../namespacePeripherals.html#a8705adbce2b506e3482b35f788379705',1,'Peripherals']]],
  ['read8_3',['read8',['../namespacePeripherals.html#ac725178df1d0e6761201db25b0085a90',1,'Peripherals']]],
  ['readpixel_4',['readPixel',['../classGraphics.html#a0f9b76fc53071094c42d7bd3c78e94a8',1,'Graphics']]],
  ['readready_5',['readReady',['../classUART0.html#a856f02b867db7d96a5a5f0d2bfaf4112',1,'UART0::readReady()'],['../classUART1.html#a0ba044a8a275ee320fdd734b2485bf34',1,'UART1::readReady()'],['../classUART3.html#a107de2f578957214556beff96232e8de',1,'UART3::readReady()'],['../classSerial.html#aa4c382f5db2f53319ad602425ec51a50',1,'Serial::readReady()']]],
  ['readscancode_6',['readScanCode',['../classPS2Keyboard.html#a2ff7f0c62fc411b22765aef33dee2a5a',1,'PS2Keyboard']]],
  ['readunicode_7',['readUnicode',['../classPS2Keyboard.html#ae62598b23577744b4b8a84f30ba033e5',1,'PS2Keyboard']]],
  ['rect_8',['rect',['../classGraphics.html#a17da9b1eed23a3f9d3d06dac95bd3ea8',1,'Graphics']]],
  ['report_9',['report',['../namespaceDispatcher.html#a63150d08b786abe890b1531c057a5fc8',1,'Dispatcher::report()'],['../namespaceCalls.html#ae7555b0ed161d5dba409e0551abf6abf',1,'Calls::report()']]],
  ['reset_10',['reset',['../classXhci.html#a6eb2dc852500f6531b32f8c4101eaaa4',1,'Xhci::reset()'],['../namespacePower.html#a463636ce680ccfe87d69558cb8dfec65',1,'Power::reset()']]],
  ['right_11',['right',['../classVESA.html#a523b570150fa0f7bddebfb96b21df26a',1,'VESA']]],
  ['risingedge_12',['RisingEdge',['../classRisingEdge.html#ac39c35f68abab9637bb6eeb1c2b3658e',1,'RisingEdge']]],
  ['rotateleft_13',['rotateLeft',['../classAntApp.html#a2dc7ef2022938bd88bd4cb8228d4eeb1',1,'AntApp::rotateLeft()'],['../classWormApp.html#abd834ec3d5f7b1589b4e070fa681e95c',1,'WormApp::rotateLeft()'],['../classTurmiteApp.html#ac529fc0f65e144039493f6b64f1a02c2',1,'TurmiteApp::rotateLeft()']]],
  ['rotateright_14',['rotateRight',['../classWormApp.html#a682401a1769e0035ff0757924c42fc79',1,'WormApp::rotateRight()'],['../classTurmiteApp.html#a2cb092074ede5d7f8bc125d452e48f89',1,'TurmiteApp::rotateRight()'],['../classAntApp.html#ac4667780b809ed8ac1334914a6804543',1,'AntApp::rotateRight()']]],
  ['rotencoderapp_15',['RotEncoderApp',['../classRotEncoderApp.html#abc4f3960210b09448b1fc95416efb02f',1,'RotEncoderApp::RotEncoderApp()'],['../classRotEncoderApp.html#a82cb32e851f06d1d7be0b805d7dc6c13',1,'RotEncoderApp::RotEncoderApp(const RotEncoderApp &amp;copy)=delete']]],
  ['rows_16',['rows',['../classVESA.html#ae6d9a2703fa99f9138f82e2a3fc3d3e3',1,'VESA']]],
  ['run_17',['run',['../namespaceScheduler.html#a58fba108ce2748870a6288cd6f5fd1e3',1,'Scheduler']]]
];
