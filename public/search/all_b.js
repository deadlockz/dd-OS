var searchData=
[
  ['keymap_0',['keymap',['../classPS2Keyboard.html#a3a604f4e6b0b4f0deac817e99bc74c3c',1,'PS2Keyboard']]],
  ['keymap_5fgerman_1',['Keymap_German',['../classPS2Keyboard.html#a1830024017ae1e6154ced52ee963b03a',1,'PS2Keyboard']]],
  ['keymap_5fsize_2',['KEYMAP_SIZE',['../classPS2Keyboard.html#a041d441eb0465e15412ab9c77c3cbb4a',1,'PS2Keyboard']]],
  ['keymap_5fus_3',['Keymap_US',['../classPS2Keyboard.html#a408cc4d723fbd6047e540b07fcb38e40',1,'PS2Keyboard']]],
  ['kickoff_4',['kickoff',['../Scheduler_8cpp.html#a85b5b2ad4c1f2407a3cc5caccd810447',1,'kickoff(Thread *t):&#160;Scheduler.cpp'],['../Scheduler_8hpp.html#a85b5b2ad4c1f2407a3cc5caccd810447',1,'kickoff(Thread *t):&#160;Scheduler.cpp']]],
  ['kill_5',['kill',['../namespaceScheduler.html#aa6f27a7c07c84ad7389a561640b019d3',1,'Scheduler::kill(Thread *t)'],['../namespaceScheduler.html#ac3ade9594d2e497bbb1de326d2719436',1,'Scheduler::kill(unsigned long tid)']]],
  ['kill_5fbit_6',['KILL_BIT',['../namespaceClocks.html#a2b7c994275d9b1a4b90edf536c6e4e2da8f7ffc52232424080dfb2a5d7ccbe615',1,'Clocks']]],
  ['killed_7',['killed',['../classThread.html#a09ff9aab2f2699a1ae01bfbf86f79b9baedcba11835f9065d1589b8c7bd6f92b8',1,'Thread']]]
];
