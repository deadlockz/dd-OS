var indexSectionsWithContent =
{
  0: "_abcdefghijklmnopqrstuvwxy~",
  1: "abcdfgilmnopqrstuvwxy",
  2: "bcdgoprstu",
  3: "abcdfgilmopqrstuvwxy",
  4: "_abcdefghijklmnopqrstuvwxy~",
  5: "_abcdefghiklmnopqrstuwxy",
  6: "b",
  7: "acdefgimoprt",
  8: "abcdefghijklmnoprstuvwxy",
  9: "bcdefgilmprsux",
  10: "hst"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "enums",
  8: "enumvalues",
  9: "defines",
  10: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Enumerations",
  8: "Enumerator",
  9: "Macros",
  10: "Pages"
};

