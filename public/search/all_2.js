var searchData=
[
  ['b_0',['b',['../structBit32.html#a8415dc678931c5d1d2a2eb4fd52f5ba8',1,'Bit32::b()'],['../structBit24.html#ad546a11a3eb69addf120eff1b52dc2da',1,'Bit24::b()']]],
  ['backcol_1',['BACKCOL',['../classVClockApp.html#ae5ced83df27eef20cd4568c3846363a7',1,'VClockApp']]],
  ['base_2',['BASE',['../classUART0.html#a6776f785a3b8b8700a8d013053d5a4f9a52e79c8885788ac21eff16d2f99cfa13',1,'UART0::BASE()'],['../namespaceClocks.html#a0aeb38378c23371c07ae5d1ca52ce29bae8d8dd8139f828aca363f040285b7d0d',1,'Clocks::BASE()'],['../namespaceGpio.html#a898d1b4ced165ead386cfb92e94a6aa2a0b8438a27a5d44ffd02327b49c7eb861',1,'Gpio::BASE()'],['../namespaceGpuMbox.html#ab3f68d9bdb12df8c43c34c81835e22e4aa030c1e46868a508bbd188da6d1e3fe9',1,'GpuMbox::BASE()'],['../namespacePeripherals.html#ad677766494602ef9cf4b2f690cc69a3ea301b80d29f4475b6715ef4b07bf60044',1,'Peripherals::BASE()'],['../namespacePower.html#ab46f62bedd3619dd9e7284810f4104b7a3eac98870ab0261f2d2cd613682946d0',1,'Power::BASE()'],['../classXhci.html#abfe1dbadfcfd94a9bdc78b671f28dab0aa75c108929672ba451faa4f3f3816665',1,'Xhci::BASE()'],['../classUART3.html#ad1ca7ecc9808ac904a690b5bdcddcc57a96240759b09203dbf6c8d35447ab9eb4',1,'UART3::BASE()'],['../classUART1.html#adad0109963643eb1547e358969fae2dda3c724e1149cab9e42fe33f39f007ed9b',1,'UART1::BASE()'],['../classPIT.html#a244c92cbe735df7b2e7ba6e4a2c0e614ae87793cd098d5978becdff0a7f435a18',1,'PIT::BASE()'],['../classPCIe.html#aeb35e0f81ebffebdfcbd4cb7c467136daf5ee96f46c50693368169810c3f2cd53',1,'PCIe::BASE()'],['../namespaceUSB.html#a706f05b61fef0a2a9fb9a76831dde4f6abfbf1b3cfe6fe197df62fa27ce7eb6af',1,'USB::BASE()'],['../namespaceGIC.html#a086ce544f6b40a906b479f24b6459758aec0c3c02138d6365cd3ef767d5d77842',1,'GIC::BASE()'],['../classDMA.html#abd8c16425c5b477996937a269488ab88ab3cbd8cce8269fbc986267a38642dd9c',1,'DMA::BASE()']]],
  ['baud_3',['BAUD',['../classUART1.html#a2d1ee8a8604234b2c8986ce0b3938652adaaa3c0f866b72cc6f5986fae82043c7',1,'UART1']]],
  ['beep_4',['BEEP',['../classAudioControlApp.html#a61b5770027dc81f00c84b0d8565873a3acf731a13254c580a502fecd6025ccc08',1,'AudioControlApp']]],
  ['bit24_5',['Bit24',['../structBit24.html',1,'']]],
  ['bit2addr_6',['bit2addr',['../classMMBitmask.html#adf66b7b42f668a5303c0c445c4340a54',1,'MMBitmask']]],
  ['bit32_7',['Bit32',['../structBit32.html',1,'']]],
  ['bit64tostr_8',['bit64ToStr',['../namespaceCharTool.html#a9f75d84ef9179a1b8d9accea011b67e5',1,'CharTool']]],
  ['black_9',['black',['../classGraphics.html#adf238d97409355a79c42493b0fa20849a705694ef4f4e4a1d4fe32f007c0584ed',1,'Graphics']]],
  ['blend_10',['blend',['../classGraphics.html#a30b4cc3c77d12f11b3212bd2e7708908',1,'Graphics']]],
  ['blocksize_11',['Blocksize',['../classMMBitmask.html#afb9cb9f5224b875fddfb459f659cfe24ad7f07263f6231b3222ee28197817e335',1,'MMBitmask']]],
  ['blue_12',['blue',['../classGraphics.html#adf238d97409355a79c42493b0fa20849add3d7dc386b56232460342fc476afe6f',1,'Graphics']]],
  ['bluescreen_13',['bluescreen',['../Bluescreen_8cpp.html#a2e9f928c9bde062d6e3fde298ee71ef1',1,'bluescreen(unsigned long type, unsigned long spi):&#160;Bluescreen.cpp'],['../Bluescreen_8hpp.html#a2e9f928c9bde062d6e3fde298ee71ef1',1,'bluescreen(unsigned long type, unsigned long spi):&#160;Bluescreen.cpp']]],
  ['bluescreen_2ecpp_14',['Bluescreen.cpp',['../Bluescreen_8cpp.html',1,'']]],
  ['bluescreen_2ehpp_15',['Bluescreen.hpp',['../Bluescreen_8hpp.html',1,'']]],
  ['bmpimage_16',['BmpImage',['../namespaceBmpImage.html',1,'']]],
  ['bmpimage_2ehpp_17',['BmpImage.hpp',['../BmpImage_8hpp.html',1,'']]],
  ['bottom_18',['bottom',['../structLimits.html#ae77da3f38f7cc5b59e3274cdda8a9a37',1,'Limits']]],
  ['boundedbuffer_19',['BoundedBuffer',['../classBoundedBuffer.html#ac7bb75bbb378a077c717dd8e1771118a',1,'BoundedBuffer::BoundedBuffer()'],['../classBoundedBuffer.html',1,'BoundedBuffer&lt; T &gt;']]],
  ['boundedbuffer_2ehpp_20',['BoundedBuffer.hpp',['../BoundedBuffer_8hpp.html',1,'']]],
  ['brcm_5fmsi_5ftarget_5faddr_5fgt_5f4gb_21',['BRCM_MSI_TARGET_ADDR_GT_4GB',['../PCIe_8cpp.html#a225b2066a102695441c20dd897f115d8',1,'PCIe.cpp']]],
  ['brcm_5fmsi_5ftarget_5faddr_5flt_5f4gb_22',['BRCM_MSI_TARGET_ADDR_LT_4GB',['../PCIe_8cpp.html#ab9be5c7ac8bef8698fb34d38c9d9a715',1,'PCIe.cpp']]],
  ['brcm_5fpcie_5fcap_5fregs_23',['BRCM_PCIE_CAP_REGS',['../PCIe_8cpp.html#a0a5aa394eadc01724b5b36dbe0d67d83',1,'PCIe.cpp']]],
  ['brcm_5fpcie_5fhw_5frev_5f33_24',['BRCM_PCIE_HW_REV_33',['../PCIe_8cpp.html#a3833bc318e6f9b897ad5dc1689175c87',1,'PCIe.cpp']]],
  ['break_25',['BREAK',['../classPS2Keyboard.html#a80639a835bf7ef3734b0c827cce3ededadefa559239151393c4301e4586839ae9',1,'PS2Keyboard']]],
  ['breakpoint_26',['breakpoint',['../Calls_8hpp.html#ae24e375b5d152ab6073e662c9ab8c3ee',1,'Calls.hpp']]],
  ['brown_27',['brown',['../classGraphics.html#adf238d97409355a79c42493b0fa20849addd65162fc407869251826b952cf3f05',1,'Graphics']]],
  ['btns_28',['BTNS',['../namespaceGpuMbox.html#a116f6d1c9f4deb9d7a00b2f6a7cd09c7ab0b6a2a0227627e181177ddf601517b0',1,'GpuMbox']]],
  ['buffer_29',['buffer',['../classPS2Keyboard.html#a9756898629779be254c32c9e4b94122e',1,'PS2Keyboard']]],
  ['buffer_5fsize_30',['BUFFER_SIZE',['../classPS2Keyboard.html#ae233a20e9153b96a1ef519b249cacf6f',1,'PS2Keyboard']]],
  ['bufferedfactoryapp_31',['BufferedFactoryApp',['../classBufferedFactoryApp.html#a63deadc366d4563235fce7246c656193',1,'BufferedFactoryApp::BufferedFactoryApp()'],['../classBufferedFactoryApp.html',1,'BufferedFactoryApp']]],
  ['bufferedfactoryapp_2ehpp_32',['BufferedFactoryApp.hpp',['../BufferedFactoryApp_8hpp.html',1,'']]],
  ['buffersize_33',['BUFFERSIZE',['../classAudio.html#ac55cb0815eead3dff92f74373191ec26a3ff5ebeb8bb9a0f0cd57b2d5c220a0b1',1,'Audio']]],
  ['burst_5fsize_5f128_34',['BURST_SIZE_128',['../PCIe_8cpp.html#a9b441b446be67b7538c31318eea3e262',1,'PCIe.cpp']]],
  ['burst_5fsize_5f256_35',['BURST_SIZE_256',['../PCIe_8cpp.html#a3dfed1aee73ee13e06b5c421ea4bb731',1,'PCIe.cpp']]],
  ['burst_5fsize_5f512_36',['BURST_SIZE_512',['../PCIe_8cpp.html#afc160b94857cab725389e66fd1e38f2c',1,'PCIe.cpp']]],
  ['busy_5fbit_37',['BUSY_BIT',['../namespaceClocks.html#a2b7c994275d9b1a4b90edf536c6e4e2dafa74a38f2003a04cb04da5fee6a5c2a3',1,'Clocks']]],
  ['byte_38',['byte',['../Byte_8hpp.html#a0c8186d9b9b7880309c27230bbb5e69d',1,'Byte.hpp']]],
  ['byte_2ehpp_39',['Byte.hpp',['../Byte_8hpp.html',1,'']]],
  ['bytes_40',['BYTES',['../classMMBitmask.html#afb9cb9f5224b875fddfb459f659cfe24abd3b460fcbe2d56ffbd8dc8d70c4670c',1,'MMBitmask']]],
  ['bytes_5fper_5fpixel_41',['bytes_per_pixel',['../structSprite.html#a9bf9db064c7e5389f428e99f40482446',1,'Sprite']]]
];
