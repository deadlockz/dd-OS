var searchData=
[
  ['id_0',['ID',['../classPsDumpCall.html#a23eeda1dc812df172fbc9e27cc23f936a6306d183769f4e95da9f5c6aabde6dfd',1,'PsDumpCall::ID()'],['../classSysinfoCall.html#a7585c6f1bcf72bf8b6ebe558c16f459aa8e851a5e51e8f0572ebc3cefca558537',1,'SysinfoCall::ID()'],['../classOYieldCall.html#a86c0e4c1001c9617c123cbfbc71a6cc3a4ea8f08acb8fd38efd501e98d38567e9',1,'OYieldCall::ID()'],['../classPauseCall.html#ab4366ca9d9cf63eb7c0f2317bf0b54aeaf4253385f6c968a040d1525c2d23a9d3',1,'PauseCall::ID()'],['../classYieldCall.html#a613ac182e4810c38058709a22a18d4ddac5f658b287a53351ac8be2c681e694f4',1,'YieldCall::ID()']]],
  ['initial_5fhz_1',['INITIAL_HZ',['../classAudio.html#ac55cb0815eead3dff92f74373191ec26aab778cdf1e6d584f46b59ff0da28a70f',1,'Audio']]],
  ['integer_5fbaud_5frate_5fdivisor_2',['INTEGER_BAUD_RATE_DIVISOR',['../classUART0.html#a6776f785a3b8b8700a8d013053d5a4f9a1c61d1363cf8b5401df4339e200c9f25',1,'UART0::INTEGER_BAUD_RATE_DIVISOR()'],['../classUART3.html#ad1ca7ecc9808ac904a690b5bdcddcc57a5831d4069d51dfdc8fe8245a2876f7c1',1,'UART3::INTEGER_BAUD_RATE_DIVISOR()']]],
  ['integer_5fbit_3',['INTEGER_BIT',['../namespaceClocks.html#ad37d825892fecdd9813ccb721c16069fa74ddd0f252882c85f409b0475bfdf01e',1,'Clocks']]],
  ['intervall_4',['INTERVALL',['../classPIT.html#a86ecbe154912a22ca3b600946e88587da507ec1c71808287a4699fa71e47dc0e5',1,'PIT']]],
  ['irq_5',['IRQ',['../namespaceDispatcher.html#a669433d82da07e800a547f463c7dfb36ada12ea91456ea13247dd0916ad2ca2b6',1,'Dispatcher']]],
  ['irq_5fclear_6',['IRQ_CLEAR',['../classUART0.html#a6776f785a3b8b8700a8d013053d5a4f9af7bb447568aab2ebdf7345f98fc720d1',1,'UART0::IRQ_CLEAR()'],['../classUART3.html#ad1ca7ecc9808ac904a690b5bdcddcc57a86482e2d42dd64a5a479289ab4fa742c',1,'UART3::IRQ_CLEAR()']]],
  ['irq_5fstates_7',['IRQ_STATES',['../classDMA.html#abd8c16425c5b477996937a269488ab88a15627be41d6e77375a498d396c59e5d5',1,'DMA']]],
  ['irqpin_8',['IRQPIN',['../classPS2Keyboard.html#a0af060c1c3b51fe0db38b4c4da5a1cbda8da586e5e2abf29dab18a3493094897e',1,'PS2Keyboard']]],
  ['irqs_9',['IRQS',['../namespaceGIC.html#adb9baec061463850529a2728e10b8dfeaf2ed0e7b181a949704ca59e58bd9f8ce',1,'GIC']]]
];
