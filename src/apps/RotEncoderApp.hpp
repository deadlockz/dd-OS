#ifndef __RotEncoderApp_include__
#define __RotEncoderApp_include__ 1

#include "threads/Thread.hpp"
#include "devices/Gpio.hpp"
#include "devices/VESA.hpp"
#include "exceptions/RisingEdge.hpp"

class RotEncoderApp : public Thread {

private:
    /// we do not want copying instances
    RotEncoderApp (const RotEncoderApp &copy) = delete;
    void operator= (RotEncoderApp const &) = delete;

    int _event;
    RisingEdge * _edgeService;

public:

    enum CONFIG {
        pinA    = 20,
        pinB    = 21,
        press   = 16
    };
    
    enum EVENT : int {
        nothing = 0,
        left    = 1,
        right   = 2
    };

    RotEncoderApp () : Thread() {
        _event = false;
        Gpio::useInput(CONFIG::pinA, Gpio::PULL::UP);
        Gpio::useInput(CONFIG::pinB, Gpio::PULL::UP);
        Gpio::useInput(CONFIG::press, Gpio::PULL::UP);
    }

    ~RotEncoderApp () {
        delete _edgeService; // destructor has its own "plugout"
    }

    void main () {
        _edgeService = new RisingEdge(
            RotEncoderApp::CONFIG::pinA,
            &_event,
            [](int * para) {
                if (Gpio::get(RotEncoderApp::CONFIG::pinB)) {
                    *para = RotEncoderApp::EVENT::left;
                } else {
                    *para = RotEncoderApp::EVENT::right;
                }
            }
        );
        
        _edgeService->plugin();

        while (true) {
            if (_event != RotEncoderApp::EVENT::nothing) {
                if (_event == RotEncoderApp::EVENT::left) {
                    VESA::me().print(" left");
                } else {
                    VESA::me().print(" right");
                }
                _event = RotEncoderApp::EVENT::nothing;
            }
            
            if (Gpio::get(RotEncoderApp::CONFIG::press) == false) {
                VESA::me().print(" press");
            }
        }
    }

    char * name () { return "rotation encoder";}
};

#endif
