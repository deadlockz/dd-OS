#ifndef __ClockApp_include__
#define __ClockApp_include__ 1

#include <cstdint> // the uintX_t stuff is cool
#include "OS.hpp"
#include "threads/Thread.hpp"
#include "devices/PIT.hpp"
#include "devices/VESA.hpp"
#include "libs/Font.hpp"

/**
 * This App (Thread) displays a digital clock in the upper right corner.
 */
class ClockApp : public Thread {

private:
    /// we do not want copying instances
    ClockApp (const ClockApp &copy) = delete;
    void operator= (ClockApp const &) = delete;
    uint8_t _tick;
    
public:
    ClockApp () : Thread() {}
    
    char * name () {return "clock";}
    
    void main () {
        char rot[6] = "-\\|/o";
        char buffer[9];
        Font & fnt = VESA::me().getFont();
        _tick = 0;
        
        for (;;) {
            _tick++;
            char dummy[2] = {rot[_tick%5], '\0'};
            VESA::me().drawString(
                fnt,
                VESA::me()._xres - fnt.width() -1,
                1,
                VESA::RGB::sun,
                dummy,
                1,
                VESA::RGB::hhu
            );

            PIT::me()->stampToTime(OS::systime, buffer);
            VESA::me().drawString(
                fnt,
                VESA::me()._xres - fnt.width() * 9 -1,
                1,
                VESA::RGB::white,
                buffer,
                8,
                VESA::RGB::black
            );
        }
    }
};

#endif
