#include "OS.hpp"
#include "devices/Serial.hpp"
#include "devices/PS2Keyboard.hpp"
#include "devices/VESA.hpp"
#include "libs/Font.hpp"

#include "apps/Shell.hpp"
#include "apps/Interact.hpp"

void Interact::main () {
    
    int ticks = 0;                      // * cursor
    unsigned long now = OS::systime;    // *
    int x, y, fg, bg;                   // *
    Font font;
    
    for (;;) {
        
        if (OS::systime > (now+25)) {       // * cursor
            now = OS::systime;              // *
            ticks = (ticks+1)%50;           // *
        }

        if (PS2Keyboard::me()->available()) {
            // read the next key
            char ch = PS2Keyboard::me()->read();
            
            VESA::me().getpos(x, y);        // * cursor
            VESA::me().getColor(fg, bg);    // *
            font = VESA::me().getFont();    // *
            x = x * font.width();           // *
            y = y * font.height() +1;       // *
            VESA::me().drawString(font, x, y, bg, CURSOR, 1); // *

            // check for some of the special keys
            if (ch == PS2_ENTER) {
                OS::shell->addChar('\n');
                OS::serial->write('\n');
                VESA::me().print('\n');
            } else if (ch == PS2_PAGEUP) {
                VESA::me().offset(-1 * OS::shell->_font.height(), true);
            } else if (ch == PS2_PAGEDOWN) {
                VESA::me().offset(OS::shell->_font.height(), true);
            } else if (ch == PS2_LEFTARROW) {
                VESA::me().left();
            } else if (ch == PS2_RIGHTARROW) {
                VESA::me().right();
            } else if (ch == PS2_UPARROW) {
                VESA::me().up();
            } else if (ch == PS2_DOWNARROW) {
                VESA::me().down();
            } else if (ch == PS2_DELETE || ch == PS2_BACKSPACE) {
                OS::shell->del();
                OS::serial->write(ch);
            } else {
                OS::shell->addChar(ch);
                OS::serial->write(ch);
                VESA::me().print(ch);
            }
        }
        
        OS::serial->loadOutputFifo();
        
        if (OS::serial->readReady()) {
            unsigned char ch = OS::serial->read();
            if (ch == '\r') {
                ch = '\n'; // makes shell working on raspbootcom input!!!
                OS::serial->write('\r');
            }
            
            VESA::me().getpos(x, y);        // * cursor
            VESA::me().getColor(fg, bg);    // *
            font = VESA::me().getFont();    // *
            x = x * font.width();           // *
            y = y * font.height() +1;       // *
            VESA::me().drawString(font, x, y, bg, CURSOR, 1); // *
            
            /**
             * fill, clear or interpret escape sequence.
             * look at: https://vt100.net/docs/vt100-ug/chapter3.html
             * and: https://www.xfree86.org/current/ctlseqs.html
             */
            if (_cnt>0) {
                _esc[_cnt] = ch;
                _cnt++;
                if (_cnt==3) {
                    // actually without sending to shell
                    if        (_esc[1] == '[' && _esc[2] == 'A') {
                        VESA::me().up();
                        _cnt=-1; // done
                    } else if (_esc[1] == '[' && _esc[2] == 'B') {
                        VESA::me().down();
                        _cnt=-1; // done
                    } else if (_esc[1] == '[' && _esc[2] == 'C') {
                        VESA::me().right();
                        _cnt=-1; // done
                    } else if (_esc[1] == '[' && _esc[2] == 'D') {
                        VESA::me().left();
                        _cnt=-1; // done
                    }
                }
                if (_cnt>3) {
                    if        (_esc[1] == '[' && _esc[2] == '6' && _esc[3] == '~') {
                        // page down
                        VESA::me().offset(OS::shell->_font.height(), true);
                    } else if (_esc[1] == '[' && _esc[2] == '5' && _esc[3] == '~') {
                        // page up
                        VESA::me().offset(-1 * OS::shell->_font.height(), true);
                    }
                    _cnt=-1; // done
                }

            }
            
            // no escape sequence
            if (_cnt==0) {
                if (ch == 0x08 || ch == 0x7F) {
                    // remove input from shell
                    OS::shell->del();
                    OS::serial->write(ch);
                } else if (ch == 0x1B) {
                    // start escape sequence
                    _cnt = 0;
                    _esc[_cnt] = ch;
                    _cnt++;
                } else {
                    // add input
                    OS::shell->addChar(ch);
                    OS::serial->write(ch);
                    VESA::me().print(ch);
                }
            }
            
            // escape sequence done
            if (_cnt==-1) _cnt=0;
        }
        
        // * blink cursor improvement -------------------------------
        if (ticks%2 == 0) {
            // hide cursor must be at the same place, color, font where we place it!
            VESA::me().getpos(x, y);
            VESA::me().getColor(fg, bg);
            font = VESA::me().getFont();
            x = x * font.width();
            y = y * font.height() +1;
            VESA::me().drawString(font, x, y, fg, CURSOR, 1);
        }
        if (ticks%2 == 1) {
            VESA::me().getpos(x, y);
            VESA::me().getColor(fg, bg);
            font = VESA::me().getFont();
            x = x * font.width();
            y = y * font.height() +1;
            VESA::me().drawString(font, x, y, bg, CURSOR, 1);
        }
    }
}
