#ifndef __BufferedFactoryApp_include__
#define __BufferedFactoryApp_include__ 1

#include "threads/Thread.hpp"
#include "devices/VESA.hpp"
#include "devices/CPU.hpp"
#include "devices/PIT.hpp"
#include "libs/BoundedBuffer.hpp"
#include "threads/Scheduler.hpp"

/// with BoundedBuffer without a 2, you will use Spinlock Semaphores.
static BoundedBuffer<int> * lager;

/**
 * Strange Factory.
 * Every Machine produce a maximum of parts and want to drop it to the lager.
 * the lager has a Limit of 4 parts. The Sales Department Thread remove the
 * parts from the lager.
 */
class BufferedFactoryApp : public Thread {

public:

    BufferedFactoryApp () {
        // this is a very small lager! SalesDepartment has to do a good job!
        lager = new BoundedBuffer<int>(4);
    }
    
    void main() {
        Scheduler::add(new Maschine(5));
        Scheduler::add(new Maschine(3));
        Scheduler::add(new SalesDepartment());
        
        // endless, with wait it is a bit powersave
        while (true) CPU::me().wait();
    }

    char * name() {return "buffered factory";}

private:

    class Maschine : public Thread {
        int _step;
    public:
        Maschine(int stp) : _step(stp) {}
    
        void main() {
            int x = 30;
            int y = _step;
            char cls[] = "        ";
            char toLager[] = ".";
            while(true) {
                // the printing chars changes x and y to a new position. We do not need it here :-S
                x = 30;
                y = _step;
                PIT::me()->delay(200 + 10*_id);
                VESA::me().print(x, y, cls); // makes a       before production starts
                PIT::me()->delay(200 + 10*_id);
                x = 30;
                y = _step;
                // maschine produces new ones. now we try to put them to lager!
                VESA::me().print(x, y, _step, 10);
                x++;
                for(int parts=0;parts < _step; ++parts) {
                    PIT::me()->delay(200);
                    lager->push(1);
                     // added 1 to lager: print a dot!
                    VESA::me().print(x, y, toLager);
                }
            }
        }
        
        char * name() {return "maschine";}
    };

    class SalesDepartment : public Thread {
    public:
        SalesDepartment() {}

        ~SalesDepartment () {
            delete _stack;
        }
    
        void main() {
            int sales = 0;
            while(true) {
                sales += lager->pop();
                VESA::me().print(30, 10, sales, 10);
            }
        }
        
        char * name(){ return "sales department";}
    };

};

#endif
