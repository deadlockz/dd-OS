#ifndef __SemCountApp_include__
#define __SemCountApp_include__ 1

#include "threads/Thread.hpp"
#include "libs/Semaphore.hpp"

/** 3 preemptive SemFoo Threads counting up a value.
 * It is similar to CoFoo, but it uses a about 1000 times
 * slower code (Semaphore improvement).
 */
class SemFoo : public Thread {
    
    public:
        Semaphore * mu;
        int * cme;
        SemFoo () : Thread() {}

        char * name () { 
            return "Semaphore Count App (Foo)";
        }
        
        void main ();
};

class SemCountApp : public Thread {

    private:
        /// we do not want copying instances
        SemCountApp (const SemCountApp &copy) = delete;
        void operator= (SemCountApp const &) = delete;

        int countme;
    
    public:

        SemCountApp () : Thread() { countme = 0; }
        
        void main ();

        char * name () { return "Semaphore Count App"; }
};

#endif
