#include <cstdint> // the uintX_t stuff is cool

#include "apps/Shell.hpp"
#include "threads/Thread.hpp"
#include "threads/Scheduler.hpp"

#include "OS.hpp"
#include "heap/MM.hpp"
#include "devices/VESA.hpp"
#include "devices/CPU.hpp"

#include "exceptions/Dispatcher.hpp"
#include "exceptions/GIC.hpp"

#include "exceptions/Calls.hpp" // breakpoint() and "syscalls"
#include "libs/CharTool.hpp"
#include "libs/SimpleThread.hpp"

#include "libs/BmpImage.hpp"
#include "libs/Graphics.hpp"

#include "devices/PIT.hpp"
#include "devices/Power.hpp"
#include "devices/Temperature.hpp"
#include "devices/Revision.hpp"
#include "devices/Clocks.hpp"

#include "devices/PCIe.hpp"
#include "devices/Xhci.hpp"

#include "libs/Font.hpp"
#include "libs/fonts/Font_4x6.hpp"
#include "libs/fonts/Font_8x8.hpp"
#include "libs/fonts/Font_acorn_8x8.hpp"
#include "libs/fonts/Font_sun_12x22.hpp"

#include "libs/Sprite.hpp"
#include "apps/ClockApp.hpp"
#include "apps/VClockApp.hpp"
#include "apps/LOGO.hpp"
#include "apps/AntApp.hpp"
#include "apps/WormApp.hpp"
#include "apps/GlowFontApp.hpp"
#include "apps/TurmiteApp.hpp"
#include "apps/AudioControlApp.hpp"
#include "apps/RotEncoderApp.hpp"

#include "apps/CoCountApp.hpp"
#include "apps/SemCountApp.hpp"
#include "apps/BufferedFactoryApp.hpp"

void Shell::clean () {
    _line[0] = '\0';
    _count = 0;
}

Shell::Shell () : Thread() {
    _withBell = true;
    clean();
}

void Shell::main () {
    // initial beep
    Scheduler::add(new AudioControlApp(AudioControlApp::FLAG::BEEP));
    
    for (;;) {
        // endless loop tries to interpret _line inputs
        if (_count > 0 && _line[_count-1] == '\n') {
            _line[_count-1] = '\0';
            if(tryExecute() == true) {
                if(_withBell) {
                    Scheduler::add(new AudioControlApp(AudioControlApp::FLAG::BEEP));
                }
            }
            clean();
        }
    }
}

void Shell::del () {
    if (_count > 0) {
        _count--;
        _line[_count] = '\0';
    }
    VESA::me().del();
}

void Shell::addChar (char c) {
    _line[_count]   = c;
    _line[_count+1] = '\0';
    _count++;
    if (_count >= Shell_MAXLINE-1) clean();
}

bool Shell::tryExecute () {
    bool result = true;
    if (_count > 0 && _line[_count-1] == '\n') _line[_count-1] = '\0';
    
    if (CharTool::equal(_line, "help")) {
        VESA::me().clear();
        VESA::me().setpos(0, 0, 0);
        
        VESA::me().print(
            "ramdisk 1835008 set address of ramdisk to 0x1c0000\n"
            "new 123         allocate 123Bytes on heap\n"
            "del x           free Chunk with id x on heap\n"
            "mem             info about the heap\n\n"
        );
        VESA::me().print(
            "beep on|off     set shell beep after success on or off\n"
            "                (other audio commands switch beep to off)\n"
            "time 130912     set systime to 13:09 and 12sec\n"
            "rotenc          start rotation encoder App\n"
            "clock           start a clock App in the upper right on display\n"
            "vclock          start a vector based clock App\n\n"
        );
        VESA::me().print(
            "cls,clr        clear screen (+ set cursor on top left)\n"
            "vesa x         set 5 screen sizes 0-4 (320x240 - 1920x1080)\n"
            "font x         set one of 4 fonts (0-3)\n"
            "graph          example using Sprite, lines, boxes, alpha values\n"
            "bmp            draw a 32bit BMP file (must be a ramdisk)\n"
            "glow text      example App to display big text\n"
        );
        VESA::me().print(
            "ant            langstons ant app\n"
            "worm           pixel eating graphic app\n"
            "turmite        turning friendly graphic app\n"
            "double -123.45 using float operation. E.g. the -123.45 is doubled!\n"
            "scrollup       improvement/test virtual screensize\n\n"
        );
        VESA::me().print(
            "coco           3 cooperative counting test-app\n"
            "semco          3 preemptive counting via semaphore test-app\n"
            "factory        test slow semaphores with a bounded buffer\n"
            "ps             show process information\n"
            "kill x         kill Thread id x\n\n"
        );
        VESA::me().print(
            "call x         improvement/test to system calls as software interrupt\n"
            "break          test bluescreen and breakpoint\n"
            "fault          test bluescreen and memory access fault\n\n"
        );
        VESA::me().print(
            "halt,reset,off = change system state\n"
            "rev            print board revision number\n"
            "rate           print ARM clockrate in MHz\n"
            "temp           print CPU temperature\n\n"
        );
        VESA::me().print(
            "tone Hz        tone generator (20Hz - 6000Hz)\n"
            "load           load wav file for DMA sound (file must be a ramdisk)\n"
            "play,loop      play or endless loop a loaded file via DMA\n"
            "pause,stop     pause or stop a loaded file via DMA\n"
            "progress       displays how many DMA data is done\n"
            "wav            play wav file (=ramdisk) without DMA\n"
        );
    } else if (CharTool::startsWith(_line, "ramdisk ")) {
        OS::ramdiskAddr = CharTool::strToInt(&(_line[8]));

    } else if (CharTool::startsWith(_line, "new ")) {
        int sz = CharTool::strToInt(&(_line[4]));
        uint8_t * dummy = new uint8_t[sz];
        dummy[0] = 12;
        
    } else if (CharTool::equal(_line, "mem")) {
        Scheduler::add(new SimpleThread<int>(0, [](int &) {
            OS::mm->dump();
        }));
        
    } else if (CharTool::startsWith(_line, "del ")) {
        int sz = CharTool::strToInt(&(_line[4]));
        OS::mm->freeById(sz);
    
    } else if (CharTool::startsWith(_line, "time ")) {
        // time 130912 = 13:09 and 12 sec
        int sz = CharTool::strToInt(&(_line[5]));
        int HH = sz/10000;
        int MM = (sz - HH*10000)/100;
        int ss = (sz - HH*10000 - MM*100);
        OS::systime = (HH*3600 + MM*60 + ss)*100;
        
        // this was a wait for the PIT. "time 5" let the pit 5s
        //PIT::me()->intervalSet(10000, sz*1000000);
        
    } else if (CharTool::equal(_line, "clock")) {
        Scheduler::add(new ClockApp());
    } else if (CharTool::equal(_line, "vclock")) {
        Scheduler::add(new VClockApp());
        
    } else if (CharTool::equal(_line, "clr")) {
        VESA::me().clear();
        VESA::me().setpos(0, 0, 0);
    } else if (CharTool::equal(_line, "cls")) {
        VESA::me().clear();
    } else if (CharTool::equal(_line, "vesa 0")) {
        VESA::me().init(320, 240, 32);
    } else if (CharTool::equal(_line, "vesa 1")) {
        VESA::me().init(640, 480, 32);
    } else if (CharTool::equal(_line, "vesa 2")) {
        VESA::me().init(800, 600, 32);
    } else if (CharTool::equal(_line, "vesa 3")) {
        VESA::me().init(1024, 600, 32);
    } else if (CharTool::equal(_line, "vesa 4")) {
        VESA::me().init(1920, 1080, 32);
    
    } else if (CharTool::equal(_line, "ps")) {
        Calls::calling(Calls::ID::PS_DUMP);
        // alternative:
        
        //SimpleThread<int> * simpleThread;
        //simpleThread = new SimpleThread<int>(0, [](int &) {
        //    Scheduler::dump();
        //});
        //Scheduler::add(simpleThread);
        
    } else if (CharTool::startsWith(_line, "kill ")) {
        int sz = CharTool::strToInt(&(_line[5]));
        SimpleThread<int> * simpleThread;
        simpleThread = new SimpleThread<int>(sz, [](int & para) {
            Scheduler::kill(para);
        });
        Scheduler::add(simpleThread);

    } else if (CharTool::startsWith(_line, "double ")) {
        SimpleThread<char *> * simpleThread;
        simpleThread = new SimpleThread<char *>(
            &(_line[7]),
            [](char * para
        ) {
            double dbl = CharTool::strToDouble(para);
            dbl *= 2.0;
            VESA::me().dblOut(dbl, 8).print("\n");
        });
        Scheduler::add(simpleThread);

    } else if (CharTool::equal(_line, "coco")) {
        CoCountApp * coco = new CoCountApp();
        Scheduler::add(coco);
    } else if (CharTool::equal(_line, "semco")) {
        SemCountApp * semco = new SemCountApp();
        Scheduler::add(semco);
    } else if (CharTool::equal(_line, "factory")) {
        Scheduler::add(new BufferedFactoryApp());

    } else if (CharTool::equal(_line, "font 0")) {
        _font.set(fontdata_4x6, 4, 6);
        VESA::me().setFont(_font);
    } else if (CharTool::equal(_line, "font 1")) {
        _font.set(fontdata_acorn_8x8, 8, 8);
        VESA::me().setFont(_font);
    } else if (CharTool::equal(_line, "font 2")) {
        _font.set(fontdata_8x8, 8, 8);
        VESA::me().setFont(_font);
    } else if (CharTool::equal(_line, "font 3")) {
        _font.set(fontdata_sun_12x22, 12, 22);
        VESA::me().setFont(_font);
        
    } else if (CharTool::equal(_line, "graph")) {
        /// should be next to 0x40000000 
        VESA::me().print("Framepointer: ")
            .print((unsigned long long) VESA::me()._lfb, 16)
            .print("\n");
        VESA::me().rect(30,130, 80,400, RGB_24(255,255,255), RGB_24(0,120,190));
        VESA::me().rect(40,140, 90,300, RGB_32(100,255,255,255), RGB_32(70,0,120,190));
        VESA::me().circle(400,300, 40, RGB_24(0,255,255), RGB_32(0,255,0,100));
        VESA::me().line(5,200, 55,260, VESA::RGB::pink);
        VESA::me().drawSprite(489, 20, LOGO);

    } else if (CharTool::equal(_line, "rotenc")) {
        Scheduler::add(new RotEncoderApp());
        
    } else if (CharTool::equal(_line, "ant")) {
        Scheduler::add(new AntApp());
        
    } else if (CharTool::equal(_line, "worm")) {
        Scheduler::add(new WormApp());
        
    } else if (CharTool::equal(_line, "turmite")) {
        Scheduler::add(new TurmiteApp());
        
    } else if (CharTool::startsWith(_line, "glow ")) {
        Scheduler::add(new GlowFontApp(&(_line[5])));
        
    } else if (CharTool::equal(_line, "scrollup")) {
        VESA::me().offset(_font.height());
        
    } else if (CharTool::startsWith(_line, "call ")) {
        uint16_t sz = CharTool::strToInt(&(_line[5]));
        Calls::calling(sz);
        
    } else if (CharTool::equal(_line, "break")) {
        // test register save
        asm (
            "mov x0,#0x1100;"
            "mov x1,#0x1101;"
            "mov x2,#0x1102;"
            "mov x3,#0x1103;"
            //"mov x29,#0xaffe;"
            //"mov x30,#0xface;"
        );
        breakpoint(42); // CPU.hpp
        
    } else if (CharTool::equal(_line, "fault")) {
        // generate a Data Abort with a
        // bad address access Bluescreen
        unsigned int r;
        r=*((volatile unsigned int*)0xFFFFF0DEADBEEF);
        r++;
        
    } else if (CharTool::equal(_line, "halt")) {
        CPU::me().halt();
        
    } else if (CharTool::equal(_line, "reset")) {
        Power::reset();
        
    } else if (CharTool::equal(_line, "off")) {
        Power::poweroff();
        
    } else if (CharTool::equal(_line, "rev")) {
        
        VESA::me().print("Pi revision code: ")
            .print((long)Revision::get(), 16)
            .print("\n");

    } else if (CharTool::equal(_line, "rate")) {
        SimpleThread<int> * simpleThread;
        simpleThread = new SimpleThread<int>(0, [](int &) {
            VESA::me().print("ARM clock rate: ")
                .print((long)Clocks::getMessured(Clocks::Device::ARM)/1000000, 10)
                .print(" MHz\n");
        });
        Scheduler::add(simpleThread);
        /// Yes! change from 600MHz to 800 is possible by code!
        //Clocks::set(Clocks::Device::ARM, 800000000);
        
    } else if (CharTool::equal(_line, "temp")) {
        
        VESA::me().dblOut(Temperature::get(), 3)
            .print(" ")
            .print((char)248)
            .print("C\n");
    
    } else if (CharTool::equal(_line, "bmp")) {
        BmpImage::draw(OS::ramdiskAddr, 80, 10);

    } else if (CharTool::equal(_line, "beep off")) {
        _withBell = false;
    } else if (CharTool::equal(_line, "beep on")) {
        _withBell = true;
    } else if (CharTool::startsWith(_line, "tone ")) {
        _withBell = false;
        uint16_t sz = CharTool::strToInt(&(_line[5]));
        Scheduler::add(new AudioControlApp(AudioControlApp::FLAG::TONE, sz));
    } else if (CharTool::equal(_line, "load")) {
        _withBell = false;
        Scheduler::add(new AudioControlApp(AudioControlApp::FLAG::LOAD_RAMDISK));
    } else if (CharTool::equal(_line, "play")) {
        _withBell = false;
        Scheduler::add(new AudioControlApp(AudioControlApp::FLAG::PLAY));
    } else if (CharTool::equal(_line, "pause")) {
        _withBell = false;
        Scheduler::add(new AudioControlApp(AudioControlApp::FLAG::PAUSE));
    } else if (CharTool::equal(_line, "stop")) {
        _withBell = false;
        Scheduler::add(new AudioControlApp(AudioControlApp::FLAG::STOP));
    } else if (CharTool::equal(_line, "loop")) {
        _withBell = false;
        Scheduler::add(new AudioControlApp(AudioControlApp::FLAG::LOOP));
    } else if (CharTool::equal(_line, "progress")) {
        _withBell = false;
        Scheduler::add(new AudioControlApp(AudioControlApp::FLAG::PROGRESS));
    
    } else if (CharTool::equal(_line, "wav")) {
        Audio::me()->wav(OS::ramdiskAddr);

    // ----------------------------------------- pci usb msi irq testing
    
    } else if (CharTool::equal(_line, "usb")) {
        PCIe::me()->setup();
        PCIe::me()->plugin();
        Xhci::me()->plugin();
    } else if (CharTool::equal(_line, "usb scan")) {
        Xhci::me()->scanPorts();
    } else if (CharTool::equal(_line, "irq highs")) {
        for (int i=180; i < GIC::CONFIG::IRQS; ++i) {
            GIC::allow(i);
        }
    
    } else {
        result = false;
    }
    
    VESA::me().setColor(VESA::RGB::lightgray, VESA::RGB::black);
    clean();

    return result;
}
