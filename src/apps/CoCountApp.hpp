#ifndef __CoCountApp_include__
#define __CoCountApp_include__ 1

#include "threads/Thread.hpp"

class CoFoo : public Thread {
    
    public:

        int * cme;
        // Thread(true) = a cooperative Thread!
        CoFoo () : Thread(true) {}
        
        char * name () { 
            return "CoCount App (Foo)";
        }
        
        void main ();
};

class CoCountApp : public Thread {

    private:
        /// we do not want copying instances
        CoCountApp (const CoCountApp &copy) = delete;
        void operator= (CoCountApp const &) = delete;

        int countme;
    
    public:

        CoCountApp () : Thread() { countme = 0; }
        
        void main ();

        char * name () { return "CoCount App"; }
};

#endif
