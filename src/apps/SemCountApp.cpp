#include "apps/SemCountApp.hpp"

#include "devices/VESA.hpp"
#include "devices/CPU.hpp"
#include "libs/Semaphore.hpp"
#include "threads/Thread.hpp"
#include "threads/Scheduler.hpp"

void SemFoo::main () {
    int c = 0;
    int ga = 0;
    while (c < 300) {
        mu->P();
        ga = *cme;  // load
        ga++;       // count
        *cme = ga;  // save
        mu->V();
        c++;
    }
}

void SemCountApp::main () {
    Semaphore * mu = new Semaphore(1);
    
    SemFoo * fo1 = new SemFoo();
    SemFoo * fo2 = new SemFoo();
    SemFoo * fo3 = new SemFoo();
    
    // submit pointer-counter to the Foos
    fo1->cme = &countme;
    fo2->cme = &countme;
    fo3->cme = &countme;
    
    fo1->mu = mu;
    fo2->mu = mu;
    fo3->mu = mu;
    
    Scheduler::add(fo1);
    Scheduler::add(fo2);
    Scheduler::add(fo3);
    
    // join improvement
    while (
        Scheduler::exists(fo1) || 
        Scheduler::exists(fo2) ||
        Scheduler::exists(fo3)
    ) {
        CPU::me().idle();
    }
    //PIT::me()->delay(500);
    VESA::me().dblOut(countme, 0).print(" end\n");
    
    delete mu;
}

