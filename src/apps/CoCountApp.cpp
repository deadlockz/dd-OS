#include "apps/CoCountApp.hpp"

#include "devices/VESA.hpp"
#include "devices/CPU.hpp"
#include "exceptions/Calls.hpp" // breakpoint and "System" calls
#include "threads/Scheduler.hpp"

void CoFoo::main () {
    int c = 0;
    int ga = 0;
    while (c < 30000) {
        ga = *cme;  // load
        ga++;       // count
        *cme = ga;  // save
        // optional yield
        Calls::calling(Calls::ID::OYIELD);
        c++;
    }
}

void CoCountApp::main () {
    CoFoo * fo1 = new CoFoo();
    CoFoo * fo2 = new CoFoo();
    CoFoo * fo3 = new CoFoo();
    
    // submit pointer-counter to the Foos
    fo1->cme = &countme;
    fo2->cme = &countme;
    fo3->cme = &countme;
    
    Scheduler::add(fo1);
    Scheduler::add(fo2);
    Scheduler::add(fo3);
    
    // join improvement
    while (
        Scheduler::exists(fo1) || 
        Scheduler::exists(fo2) ||
        Scheduler::exists(fo3)
    ) {
        CPU::me().idle();
    }
    //PIT::me()->delay(500);
    VESA::me().dblOut(countme, 0).print(" end\n");
}

