#ifndef __Interact_include__
#define __Interact_include__ 1

#include "threads/Thread.hpp"

/**
 * Interact.
 * 
 * It gets single chars from UART and put it to:
 * 
 * - UART (echo)
 * - Shell-Thread (which tries to execute)
 * - VESA/Monitor
 * 
 * additionaly it creates a blinking cursor.
 */
class Interact : public Thread {

private:
    /// we do not want copying instances
    Interact (const Interact &copy) = delete;
    void operator= (Interact const &) = delete;
    const char * CURSOR = "_";
    char _esc[4];
    int _cnt;

public:
    Interact () : Thread() {
        _cnt = 0;
    }
    
    char * name () {return "Interact (UART, PS/2)";}
    void main ();
};

#endif
