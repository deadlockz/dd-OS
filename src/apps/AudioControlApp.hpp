#ifndef __AudioControlApp_include__
#define __AudioControlApp_include__ 1

#include <cstdint> // the uintX_t stuff is cool

#include "OS.hpp"
#include "threads/Thread.hpp"
#include "devices/VESA.hpp"
#include "devices/Audio.hpp"
#include "devices/PIT.hpp"

class AudioControlApp : public Thread {

private:
    /// we do not want copying instances
    AudioControlApp (const AudioControlApp &copy) = delete;
    void operator= (AudioControlApp const &) = delete;
    
    int _flag;
    int _hz;
    int _ms;
    
public:

    enum FLAG {
        LOAD_RAMDISK = 0x00,
        PLAY        = 0x01,
        LOOP        = 0x02,
        PAUSE       = 0x03,
        STOP        = 0x04,
        TONE        = 0x05,
        BEEP        = 0x06,
        PROGRESS    = 0x07
    };
    
    AudioControlApp (int flag, int hz=658, int ms=50) : Thread() {
        _flag = flag;
        _hz = hz;
        _ms = ms;
    }
    
    char * name () {return "audio control";}
    
    void main () {
        long proc,size;
        double duration;
        
        switch (_flag) {
            case FLAG::LOAD_RAMDISK:
                VESA::me().print("Loading for DMA transfer...\n");
                Audio::me()->load(OS::ramdiskAddr);
                VESA::me().print("Loading done!\n");
                break;
            
            case FLAG::PLAY:
                Audio::me()->play();
                break;
            
            case FLAG::LOOP:
                Audio::me()->play(true);
                break;
            
            case FLAG::PAUSE:
                Audio::me()->pause();
                break;
            
            case FLAG::TONE:
                if (Audio::me()->isPlaying()) Audio::me()->stop();
                Audio::me()->tone(_hz);
                Audio::me()->play(true);
                PIT::me()->delay(_ms);
                Audio::me()->stop();
                break;
            
            case FLAG::BEEP:
                if (Audio::me()->isPlaying()) break;
                
                Audio::me()->play(true);
                PIT::me()->delay(_ms);
                Audio::me()->stop();
                break;
            
            case FLAG::PROGRESS:
                proc = 100 * Audio::me()->progress();
                size = Audio::me()->getDuration();
                duration = size * 1.0/(double)(Audio::CONFIG::SAMPLING);
                VESA::me()
                    .print(proc)
                    .print("% done of ")
                    .print(size)
                    .print(" samples (")
                    .dblOut(duration, 3)
                    .print(" seconds)\n");

                if (Audio::me()->isPlaying()) {
                    VESA::me().print("Audio is playing.\n");
                } else {
                    VESA::me().print("Audio is not playing.\n");
                }
                break;
            
            default:
                Audio::me()->stop();
        }
    }
};

#endif
