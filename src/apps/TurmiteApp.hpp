#ifndef __TurmiteApp_include__
#define __TurmiteApp_include__ 1

#include "threads/Thread.hpp"
#include "devices/VESA.hpp"
#include "libs/Limits.hpp"
#include "libs/Byte.hpp"

/**
 * Turmite Ant.
 * Wikipedia: "TurNing machines" or "Well, they're Turing machines studied by Turk, so they should be tur-something."
 * This is a 2D turing maschine Thread.
 */
class TurmiteApp : public Thread {

private:
    /// we do not want copying instances
    TurmiteApp (const TurmiteApp &copy) = delete;
    void operator= (TurmiteApp const &) = delete;

	byte ori;
	Limits _lim;
	int    _col;
	bool   _bouncing;

	struct DI {
		enum {
			up=1,
			right=2,
			down=3,
			left=4
		};
	};

public:
	int x;
	int y;

	/**
	 * constructor.
	 * @param bouncing default is true. If it is set to false, the ant did jump to the other side of the playground.
	 */
	TurmiteApp (bool bouncing = false) : Thread() {
		_bouncing = bouncing;
		_col = VESA::_col[_id%16];
		if (_col == VESA::RGB::black) _col = VESA::RGB::white;
		_lim.left   = 0;
		_lim.right  = VESA::me()._xres -1;
		_lim.top    = 0;
		_lim.bottom = VESA::me()._yres -1;
		ori = DI::down;
		x = _lim.left + (_lim.right-_lim.left)/2;
		y = _lim.top  + (_lim.bottom-_lim.top)/2;
	}

	/**
	 * Set the Playground.
	 * Set the limits for the ant.
	 * @param left first x position
	 * @param right last x position
	 * @param top first y position
	 * @param bottom last y position
	 */
	void set (int left, int right, int top, int bottom) {
		_lim.left   = left;
		_lim.right  = right;
		_lim.top    = top;
		_lim.bottom = bottom;
		x = _lim.left + (_lim.right-_lim.left)/2;
		y = _lim.top  + (_lim.bottom-_lim.top)/2;
	}

	void rotateLeft () {
		ori--; if(ori==0) ori=4;
	}

	void rotateRight () {
		ori++; if(ori==5) ori=1;
	}

	/**
	 * forward.
	 * it sets the ant to the next field.
	 */
	void forward () {
		if (ori == DI::up) {
			y--;
		} else if (ori == DI::right) {
			x++;
		} else if (ori == DI::left) {
			x--;
		} else {
			y++;
		}
		if (_bouncing) {
			if (x < _lim.left) {
				ori = DI::right;
			} else if (x >= _lim.right) {
				ori = DI::left;
			}
			if (y < _lim.top) {
				ori = DI::down;
			} else if (y >= _lim.bottom) {
				ori = DI::up;
			}
			_lim.likeBorder(x,y);
		} else {
			_lim.likeWorld(x,y);
		}
	}

    void main() {
        byte r,g,b;
        int state = 0;

        while (true) {
            VESA::me().readPixel(x, y, r,g,b);
            
            if (state == 0) {
                if ((int)r+(int)g+(int)b > 0) { // is "white"
                    VESA::me().drawPixel(x, y, _col); // set turmite trek color
                    rotateRight();
                    state = 1;
                } else {
                    VESA::me().drawPixel(x, y, _col); // set turmite trek color
                    rotateRight();
                    state = 0;
                }
            } else {
                if ((int)r+(int)g+(int)b > 0) { // is "white"
                    VESA::me().drawPixel(x, y, VESA::RGB::black);
                    //rotateRight();
                    state = 1;
                } else {
                    VESA::me().drawPixel(x, y, VESA::RGB::black);
                    //rotateRight();
                    state = 0;
                }
            }
            forward();
        }
    }
	
	char * name () { return "turmite";}
};

#endif
