#ifndef __GlowFontApp_include__
#define __GlowFontApp_include__ 1

#include "threads/Thread.hpp"
#include "OS.hpp"
#include "devices/CPU.hpp"
#include "devices/VESA.hpp"
#include "libs/CharTool.hpp"
#include "libs/Font.hpp"
#include "libs/Byte.hpp"

class GlowFontApp : public Thread {

private:
    /// we do not want copying instances
    GlowFontApp (const GlowFontApp &copy) = delete;
    void operator= (GlowFontApp const &) = delete;
    
    char * _message;
    int _len;
    int _x;
    int _y;
    Font * _fnt;

    void draw (
        int x, 
        int y,
        unsigned int width,
        unsigned int height,
        unsigned char * bitmap, 
        int color,
        bool glow
    ) {
        // Breite in Bytes
        unsigned short width_byte = width/8 + ((width%8 != 0) ? 1 : 0);

        for (unsigned int yoff=0; yoff<(4*height); yoff+=4) {
            int xpos=x;
            int ypos=y+yoff;
            for (unsigned int xb=0; xb < width_byte; ++xb) {
                for (int src=7; src>=0; --src) {
                    if ((1 << src) & *bitmap) {
                        if (glow) {
                            Bit32 * c = (Bit32 *) &color;
                            Bit32 c2 = {c->b, c->g, c->r, 180};
                            int col = RGB_32(c2.a,c2.r,c2.g,c2.b);
                            
                            VESA::me().drawPixel(xpos-1, ypos, col);
                            VESA::me().drawPixel(xpos+1, ypos, col);
                            VESA::me().drawPixel(xpos-1, ypos-1, col);
                            VESA::me().drawPixel(xpos+1, ypos-1, col);
                            VESA::me().drawPixel(xpos-1, ypos+1, col);
                            VESA::me().drawPixel(xpos+1, ypos+1, col);
                            VESA::me().drawPixel(xpos, ypos-1, col);
                            VESA::me().drawPixel(xpos, ypos+1, col);
                            
                            //VESA::me().circle(xpos, ypos, 4, col, col);
                        }
                        VESA::me().drawPixel(xpos, ypos, color);
                    }
                    xpos+=4;
                }
                bitmap++;
            }
        }
    }

public:

    GlowFontApp (char * message) : Thread() {
        _len = CharTool::len(message);
        _message = new char[_len]();
        CharTool::strCopy(message, _message);
        VESA::me().getpos(_x, _y);
        _fnt = &(VESA::me().getFont());
        _x *= _fnt->width();
        _y *= _fnt->height();
        _y += _fnt->height();
    }
    
    ~GlowFontApp () {
        delete _message;
    }

    void main () {
        int j=0;
        for (int i=0; i<_len; ++i, ++j) {
            if (_message[i] == '\n' || (_x + j*4*(int)_fnt->width()) > VESA::me()._xres) {
                j = -1;
                i -= 2;
                _y += _fnt->height()*4;
            } else {
                draw(
                    _x + j*4*_fnt->width(),
                    _y,
                    _fnt->width(),
                    _fnt->height(),
                    _fnt->getChar( _message[i] ),
                    VESA::RGB::white,
                    false
                );
                PIT::me()->delay(10);
                
                draw(
                    _x + j*4*_fnt->width(),
                    _y,
                    _fnt->width(),
                    _fnt->height(),
                    _fnt->getChar( _message[i] ),
                    VESA::RGB::lightgreen,
                    true
                );
                PIT::me()->delay(50);
            }
        }
    }

    char * name () { return "Glow Font";}
};

#endif
