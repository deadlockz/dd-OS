#ifndef __Shell_include__
#define __Shell_include__ 1

#include "threads/Thread.hpp"
#include "libs/Font.hpp"

/// a limit for the max chars in a shell command
#define Shell_MAXLINE 100

/**
 * Command Shell.
 * This is a command shell to start Threads, see Memory Chunks and processes and more.
 * To make the memory dump more clean at the OS beginning, the Shell and the Stack
 * for this Thread is static and in {@link OS}
 * @see OS.hpp
 */
class Shell : public Thread {

private:
    /// we do not want copying instances
    Shell (const Shell &copy) = delete;
    void operator= (Shell const &) = delete;

    char _line[Shell_MAXLINE];
    int _count;
    bool _withBell;

    void clean ();

public:
    Font _font;
    Shell ();

    char * name () {return "SHELL";}
    void main ();
    
    /**
     * Add Character.
     * Method to add a charater to the Shell line.
     */
    void addChar (char c);
    
    void del ();

    /**
     * try Execute _line.
     * The Main loop of the Shell Thread tries to execute the Shell-Line. It
     * play a bell on success, if _withBell is true (command: bell on).
     * @return is true, if it was possible to execute the command in _line
     */
    bool tryExecute ();
};

#endif
