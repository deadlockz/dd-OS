#ifndef __WormApp_include__
#define __WormApp_include__ 1

#include "threads/Thread.hpp"
#include "devices/VESA.hpp"
#include "libs/Limits.hpp"
#include "libs/Byte.hpp"

/**
 * Langstons Ant as Worm.
 * This is a vesa 2D turing maschine Thread.
 */
class WormApp : public Thread {

private:
    /// we do not want copying instances
    WormApp (const WormApp &copy) = delete;
    void operator= (WormApp const &) = delete;

    static const int LEN = 200;
    
    byte ori;
    Limits _lim;
    int    _col;
    
    struct XY {
        int x;
        int y;
    };
    
    XY _wormBuff[LEN];
    int wormBuffId;

    struct DI {
        enum {
            up=1,
            right=2,
            down=3,
            left=4
        };
    };

public:
    int x;
    int y;

    WormApp () : Thread() {
        wormBuffId = 0;
        _col = VESA::_col[_id%16];
        if (_col == VESA::RGB::black) _col = VESA::RGB::white;
        _lim.left   = 0;
        _lim.right  = VESA::me()._xres -1;
        _lim.top    = 0;
        _lim.bottom = VESA::me()._yres -1;
        ori = DI::down;
        x = _lim.left + (_lim.right-_lim.left)/2;
        y = _lim.top  + (_lim.bottom-_lim.top)/2;
    }
    
    /**
     * Set the Playground.
     * Set the limits for the ant.
     * @param left first x position
     * @param right last x position
     * @param top first y position
     * @param bottom last y position
     */
    void set (int left, int right, int top, int bottom) {
        _lim.left   = left;
        _lim.right  = right;
        _lim.top    = top;
        _lim.bottom = bottom;
        x = _lim.left + (_lim.right-_lim.left)/2;
        y = _lim.top  + (_lim.bottom-_lim.top)/2;
    }

    void rotateLeft () {
        ori--; if(ori==0) ori=4;
    }

    void rotateRight () {
        ori++; if(ori==5) ori=1;
    }

    /**
     * forward.
     * it sets the ant to the next field.
     */
    void forward () {
        if (ori == DI::up) {
            y--;
        } else if (ori == DI::right) {
            x++;
        } else if (ori == DI::left) {
            x--;
        } else {
            y++;
        }
        _lim.likeWorld(x,y);
    }

    void main () {
        byte r,g,b;

        while (true) {
            VESA::me().readPixel(x, y, r,g,b);
            if ((int)r+(int)g+(int)b > 0) { // is white
                rotateRight();
                VESA::me().drawPixel(x, y, VESA::RGB::black);
            } else {
                rotateLeft();
                VESA::me().drawPixel(x, y, _col);
                _wormBuff[wormBuffId].x = x;
                _wormBuff[wormBuffId].y = y;
                
                int temp = (wormBuffId+2)%LEN;
                VESA::me().drawPixel(
                    _wormBuff[temp].x,
                    _wormBuff[temp].y,
                    VESA::RGB::black
                );
                wormBuffId = (wormBuffId+1)%LEN;
            }
            forward();
        }
    }

    char * name () { return "worm";}
};

#endif
