#ifndef __LOGO_include__
#define __LOGO_include__ 1

#include "libs/Sprite.hpp"

/// This is a small logo image. use VESA::me().drawSprite() to print it.
extern Sprite LOGO;

#endif
