#ifndef __VClockApp_include__
#define __VClockApp_include__ 1

#include "OS.hpp"
#include "threads/Thread.hpp"
#include "devices/CPU.hpp"
#include "devices/VESA.hpp"

/**
 * This App (Thread) displays a digital clock based on lines and circles.
 * Code Base: https://github.com/no-go/pi4-irq-SystemTimer/tree/someGraphics_highres
 */
class VClockApp : public Thread {

private:
    /// we do not want copying instances
    VClockApp (const VClockApp &copy) = delete;
    void operator= (VClockApp const &) = delete;

    static const int FONTPADDING = 8;
    static const int FONTSIZE = 64;
    static const int TOPP = 60;
    static const int WIDTH = 280;
    static const int LEFT = 20;
    static const int BACKCOL = 0x00303055;
    static const int FRONTCOL = 0x00FFFFFF;

    /*
     * print a number >=0 at position x,y with height (=he).
     * the number is a line/circle construct. you get the next pos
     * as return value.
     */
    int myFont (int x, int y, int b, int he, int col, int fillCol) {    
        if (b == 0) {
          VESA::me().circle(x+he/4, y+3*he/4, he/4, col, fillCol);
          return x+2+he/2;
        } else if (b == 1) {
          VESA::me().line(x,y+5,x+5,y, col);
          VESA::me().line(x+5,y,x+5,y+he, col);
          return x+8;
        } else if (b == 2) {
          VESA::me().circle(x+he/4, y-3+3*he/4, he/4, col, fillCol);
          VESA::me().rect(x,y,x+he/4,y+he, fillCol, fillCol);
          VESA::me().line(x+he/4,y+he-3,x+he/2,y+he, col);
          return x+2+he/2;
        } else if (b == 3) {
          VESA::me().circle(x+he/4, y+1*he/4, he/4, col, fillCol);
          VESA::me().circle(x+he/4, y+3*he/4, he/4, col, fillCol);
          VESA::me().rect(x,y,x+he/4,y+he, fillCol, fillCol);
          return x+2+he/2;
        } else if (b == 4) {
          VESA::me().line(x,y+he/2,x+he/2,y, col);
          VESA::me().line(x,y+he/2,x+he/2,y+he/2, col);
          VESA::me().line(x+he/2,y,x+he/2,y+he, col);
          return x+2+he/2;   
        } else if (b == 5) {
          VESA::me().line(x+he/4,y,x+he/2,y, col);
          VESA::me().line(x+he/4,y,x+he/4,y+he/2, col);
          VESA::me().circle(x+he/4, y+3*he/4, he/4, col, fillCol);
          VESA::me().rect(x,y,x-1+he/4,y+he, fillCol, fillCol);
          return x+2+he/2;   
        } else if (b == 6) {
          VESA::me().line(x,y-2+3*he/4,x+he/2,y, col);
          VESA::me().circle(x+he/4, y+3*he/4, he/4, col, fillCol);
          return x+2+he/2;   
        } else if (b == 7) {
          VESA::me().line(x,y+3,x+he/2,y, col);
          VESA::me().line(x+he/2,y,x,y+he, col);
          VESA::me().line(x+3,y+he/2,x+he/2-1,y+he/2, col);
          return x+2+5;  
        } else if (b == 8) {
          VESA::me().circle(x+he/4, y+1*he/4, he/4, col, fillCol);
          VESA::me().circle(x+he/4, y+3*he/4, he/4, col, fillCol);
          return x+2+he/2;
        } else if (b == 9) {
          VESA::me().circle(x+he/4, y+1*he/4, he/4, col, fillCol);
          VESA::me().line (x+he/2, y+1*he/4, x+he/2, y+he, col);
          return x+2+he/2;
        }
        
        int t = b/10;
        int pos = myFont(x, y, t, he, col, fillCol);
        t *= 10;
        b = b - t;
        return myFont(pos+FONTPADDING, y, b, he, col, fillCol);
    }

    int hours;
    int minutes;
    int sec;
    int old_sec;
    
public:
    VClockApp () : Thread() {}

    char * name () {return "vector clock";}

    void setHHMM (int d1, int d2) {
        hours = d1;
        minutes = d2;
        
        VESA::me().rect(LEFT,TOPP-2,LEFT+WIDTH,TOPP+FONTSIZE+4, BACKCOL, BACKCOL);
        
        if (d1<10) {
            myFont(LEFT+2*FONTPADDING+FONTSIZE/2, TOPP, d1, FONTSIZE, FRONTCOL, BACKCOL);
        } else {
            myFont(LEFT+FONTPADDING, TOPP, d1, FONTSIZE, FRONTCOL, BACKCOL);
        }
        if (d2<10) {
            myFont(LEFT+WIDTH/2 - FONTPADDING/2 - FONTSIZE/2, TOPP, 0, FONTSIZE, FRONTCOL, BACKCOL);
            myFont(LEFT+WIDTH/2 + FONTPADDING/2, TOPP, d2, FONTSIZE, FRONTCOL, BACKCOL);
        } else {
            myFont(LEFT+WIDTH/2 - FONTPADDING/2 - FONTSIZE/2, TOPP, d2, FONTSIZE, FRONTCOL, BACKCOL);
        }
        sec = 0;
        old_sec = 0;
    }

    void main () {
        int pos;
        
        hours = OS::systime / (unsigned long long) 360000;
        minutes = (OS::systime / 6000) - (hours*60);
        setHHMM(hours,minutes);
        
        for (;;) {
            sec = OS::systime/100 - hours*3600 - minutes*60;
            
            if (sec >= 60) {
                if (minutes < 10) {
                    myFont(LEFT+WIDTH/2 - FONTPADDING/2 - FONTSIZE/2, TOPP, 0, FONTSIZE, BACKCOL, BACKCOL);
                    myFont(LEFT+WIDTH/2 + FONTPADDING/2, TOPP, minutes, FONTSIZE, BACKCOL, BACKCOL);
                } else {
                    myFont(LEFT+WIDTH/2 - FONTPADDING/2 - FONTSIZE/2, TOPP, minutes, FONTSIZE, BACKCOL, BACKCOL);
                }

                minutes++;
                sec = 0;
                
                if (minutes < 10) {
                    myFont(LEFT+WIDTH/2 - FONTPADDING/2 - FONTSIZE/2, TOPP, 0, FONTSIZE, FRONTCOL, BACKCOL);
                    myFont(LEFT+WIDTH/2 + FONTPADDING/2, TOPP, minutes, FONTSIZE, FRONTCOL, BACKCOL);
                } else {
                    if (minutes < 60) myFont(LEFT+WIDTH/2 - FONTPADDING/2 - FONTSIZE/2, TOPP, minutes, FONTSIZE, FRONTCOL, BACKCOL);
                }
            }
            
            if (minutes == 60) {
                if (hours < 10) {
                    myFont(LEFT+2*FONTPADDING+FONTSIZE/2, TOPP, hours, FONTSIZE, BACKCOL, BACKCOL);
                } else {
                    myFont(LEFT+FONTPADDING, TOPP, hours, FONTSIZE, BACKCOL, BACKCOL);
                }
                myFont(LEFT+WIDTH/2 - FONTPADDING/2 - FONTSIZE/2, TOPP, 59, FONTSIZE, BACKCOL, BACKCOL);
                hours++;
                minutes = 0;
                myFont(LEFT+WIDTH/2 - FONTPADDING/2 - FONTSIZE/2, TOPP, 0, FONTSIZE, FRONTCOL, BACKCOL);
                myFont(LEFT+WIDTH/2 + FONTPADDING/2, TOPP, 0, FONTSIZE, FRONTCOL, BACKCOL);

                if (hours == 24) {
                    myFont(LEFT+FONTPADDING, TOPP, 23, FONTSIZE, BACKCOL, BACKCOL);
                    hours = 0;
                    myFont(LEFT+2*FONTPADDING+FONTSIZE/2, TOPP, hours, FONTSIZE, FRONTCOL, BACKCOL);
                } else {
                    if (hours < 10) {
                        myFont(LEFT+2*FONTPADDING+FONTSIZE/2, TOPP, hours, FONTSIZE, FRONTCOL, BACKCOL);
                    } else {
                        myFont(LEFT+FONTPADDING, TOPP, hours, FONTSIZE, FRONTCOL, BACKCOL);
                    }
                }
            }
            
            if (sec != old_sec) {
                pos = LEFT + WIDTH - 2*FONTPADDING - FONTSIZE;
                if (sec == 0) {
                    myFont(pos, TOPP, 59, FONTSIZE, BACKCOL, BACKCOL);
                } else if (sec < 10) {
                    pos = myFont(pos, TOPP, 0, FONTSIZE, FRONTCOL, BACKCOL);
                    pos += FONTPADDING;
                    myFont(pos, TOPP, old_sec, FONTSIZE, BACKCOL, BACKCOL);
                    myFont(pos, TOPP, sec, FONTSIZE, FRONTCOL, BACKCOL);
                } else if (sec == 10) {
                    pos = myFont(pos, TOPP, 0, FONTSIZE, BACKCOL, BACKCOL);
                    pos += FONTPADDING;
                    myFont(pos, TOPP, old_sec, FONTSIZE, BACKCOL, BACKCOL);
                    myFont(LEFT + WIDTH - 2*FONTPADDING - FONTSIZE, TOPP, sec, FONTSIZE, FRONTCOL, BACKCOL);
                } else {
                    myFont(pos, TOPP, old_sec, FONTSIZE, BACKCOL, BACKCOL);
                    myFont(pos, TOPP, sec, FONTSIZE, FRONTCOL, BACKCOL);
                }
                old_sec = sec;
            }
            
            CPU::me().wait();
        }
    }
};

#endif
