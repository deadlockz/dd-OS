#ifndef __GPIO_HPP__
#define __GPIO_HPP__ 1

#include <cstdint> // the uintX_t stuff is cool
#include "devices/Peripherals.hpp"

namespace Gpio {
    
    enum ADDR {
        BASE = Peripherals::ADDR::GPIO,
        GPFSEL0         = BASE,
        GPPUD           = BASE + 0x94, ///< not exists on Pi4: Controls actuation of pull up/down for ALL GPIO pins.
        GPPUDCLK0       = BASE + 0x98, ///< not exists on Pi4: Controls actuation of pull up/down for specific GPIO pin.
        GPPUDCLK1       = BASE + 0x9C, ///< not exists on Pi4: Controls actuation of pull up/down for specific GPIO pin.
        GPSET0          = BASE + 0x1C,
        GPCLR0          = BASE + 0x28,
        GPLEV0          = BASE + 0x34,
        EVENT_STATUS0   = BASE + 0x40, ///< GPIO 0-31 view event status (interrupt) or write 1 to clear
        EVENT_RISE0     = BASE + 0x4c, ///< generate interrupt on GPIO 0-31 (set the bit) if 0 goes to 1
        EVENT_FALL0     = BASE + 0x58, ///< like above, but 1 goes to 0
        // these registers are new on Pi4. 0 is the base. IO::call() select the relevant one
        GPPUPPDN0       = BASE + 0xE4
    };

    enum CONFIG {
        MAX_PIN       = 53, ///< Pi1  AND  Pi4 has 54 max gpio pins! Count the 0!
        FUNCTION_IN   = 0,
        FUNCTION_OUT  = 1,
        FUNCTION_ALT0 = 4, ///< 100
        FUNCTION_ALT1 = 5, ///< 101
        FUNCTION_ALT2 = 6, ///< 110
        FUNCTION_ALT5 = 2, ///< 010
        FUNCTION_ALT4 = 3, ///< 011
        FUNCTION_ALT3 = 7  ///< 111
    };

    enum PULL {
        NONE = 0,
        UP = 1,
        DOWN = 2
    };

    /// @todo this should be private ---------------------------
    unsigned int call (
        unsigned int pin, 
        unsigned int value, 
        uint64_t base,
        unsigned int field_size,
        unsigned int field_max
    );
    unsigned int set (unsigned int pin, unsigned int value);
    unsigned int clear (unsigned int pin, unsigned int value);
    unsigned int pull (unsigned int pin, unsigned int value);
    unsigned int function (unsigned int pin, unsigned int value);
    
    bool getFromRegister (unsigned int pin, uint64_t base);
    void setToRegister (unsigned int pin, uint64_t base, bool value);

    /// @todo this should be public ---------------------------
    void useAlt0 (unsigned int pin);
    void useAlt1 (unsigned int pin);
    void useAlt2 (unsigned int pin);
    void useAlt3 (unsigned int pin);
    void useAlt4 (unsigned int pin);
    void useAlt5 (unsigned int pin);
    void useOutput (unsigned int pin, unsigned int pull = Gpio::PULL::NONE);
    void useInput (unsigned int pin, unsigned int pull = Gpio::PULL::UP);
    
    void setPin (unsigned int pin, bool state);
    bool get (unsigned int pin);
    
    /// get a true, if there is an event on that pin
    bool getStatus (unsigned int pin);
    /// remove the event flag for a pin
    void clearStatus (unsigned int pin);
    /// if value is true: say the gpio system to make an interrupt (event) on falling edge for that pin
    void setFalling (unsigned int pin, bool value);
    /// if value is true: say the gpio system to make an interrupt (event) on rising edge for that pin
    void setRising (unsigned int pin, bool value);
    
}

#endif
