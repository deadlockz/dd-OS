#ifndef __UART0_HPP__
#define __UART0_HPP__ 1

#include "devices/Serial.hpp"
#include "devices/Peripherals.hpp"

/// hardware optimal UART on GPIO Pins 14 and 15

class UART0 : public Serial {

public:

    enum REGISTER {
        BASE = Peripherals::ADDR::UART0,
        DATA                        = BASE,
        FLAG                        = BASE + 0x18,
        INTEGER_BAUD_RATE_DIVISOR   = BASE + 0x24,
        FRACTAL_BAUD_RATE_DIVISOR   = BASE + 0x28,
        LINE_CONTROL                = BASE + 0x2c,
        CONTROL                     = BASE + 0x30,
        IRQ_CLEAR                   = BASE + 0x44
    };
    
    enum CONFIG {
        CLOCK = 3000000,
        PINA = 14,
        PINB = 15
    };

    void write (const char *buffer);
    void write (unsigned char ch);
    unsigned char read ();
    bool readReady ();
    bool writeReady ();

    /// This Serial connection is a singleton
    static UART0 & me () {
        static UART0 inst;
        return inst;
    }

private:

    /// singleton = constructor is private
    UART0 ();
    UART0 (const UART0 &) = delete;
    void operator= (UART0 const &) = delete;
};

#endif
