#include "devices/UART3.hpp"
#include "devices/Peripherals.hpp"
#include "devices/Clocks.hpp"
#include "devices/Gpio.hpp"

UART3::UART3 (void) {
    /// receive and transmit bits disabled
    Peripherals::write32(UART3::REGISTER::CONTROL, 0);
    
    /// set GPIO Pins 4 and 5 to UART3 function (it removes pullup and down, too)
    Gpio::useAlt4(UART3::CONFIG::PINA);
    Gpio::useAlt4(UART3::CONFIG::PINB); ///<GPIO4 and 5 ARE pin7 and pin29 !
    
    /// clear pending UART interrupts
    Peripherals::write32(UART3::REGISTER::IRQ_CLEAR, 0x7FF);

    /**
     * send a Mailbox message with set clock rate of PL011 (non mini UART)
     * to 3MHz
     */
    Clocks::set(Clocks::Device::UART_PL011, UART3::CONFIG::CLOCK);
    
    /**
     * @todo do this in code:
     * (PL011_CLOCK / (16 * 115200 baud) = 1.627
     * (0.627*64)+0.5 = 40
     * = int 1, frac 40
     */
    Peripherals::write32(UART3::REGISTER::INTEGER_BAUD_RATE_DIVISOR, 1);
    Peripherals::write32(UART3::REGISTER::FRACTAL_BAUD_RATE_DIVISOR, 40);

    /// Enable FIFO and 8 bit data transmission (1 stop bit, no parity)
    Peripherals::write32(
        UART3::REGISTER::LINE_CONTROL,
        (1 << 6) | (1 << 5) | (1 << 4)
    );
    
    /// receive, transmit and UART3 enabled
    Peripherals::write32(
        UART3::REGISTER::CONTROL,
        (1 << 9) | (1 << 8) | (1 << 0)
    );
}

bool UART3::readReady () { 
    /// if flag TX FF is set, UART FiFo is sending data and can not get
    return (Peripherals::read32(UART3::REGISTER::FLAG) & 0x10) == 0; 
}

bool UART3::writeReady () {
    /// if flag RX FF is set, UART FiFo is getting data and can not send
    return (Peripherals::read32(UART3::REGISTER::FLAG) & 0x20) == 0;
}

unsigned char UART3::read () {
    while (UART3::readReady() == false);
    return (unsigned char) Peripherals::read32(UART3::REGISTER::DATA);
}

void UART3::write (unsigned char ch) {
    while (UART3::writeReady() == false);
    Peripherals::write32(UART3::REGISTER::DATA, (unsigned int)ch);
}

void UART3::write (const char *buffer) {
    while (*buffer) {
       if (*buffer == '\n') UART3::write('\r');
       UART3::write(*buffer++);
    }
}

