#include <cstdint> // the uintX_t stuff is cool
#include "devices/Revision.hpp"
#include "devices/GpuMbox.hpp"

uint32_t Revision::get () {
    GpuMbox::_callBuffer[0] = 7*4; // Length of message in bytes
    GpuMbox::_callBuffer[1] = GpuMbox::FLAG::REQUEST;
    GpuMbox::_callBuffer[2] = GpuMbox::TAG::REVISION; // Tag identifier
    GpuMbox::_callBuffer[3] = 4; // request parameter size
    GpuMbox::_callBuffer[4] = 4; // result buffer size
    GpuMbox::_callBuffer[5] = 0; // return 1 value
    GpuMbox::_callBuffer[6] = GpuMbox::TAG::LAST;
    GpuMbox::call(GpuMbox::CHANNEL::PROP);
    uint32_t rev = GpuMbox::_callBuffer[5];
    return rev;
}

