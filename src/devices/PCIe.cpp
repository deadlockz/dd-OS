#include "threads/Scheduler.hpp"
#include "devices/PCIe.hpp"
#include "devices/Peripherals.hpp"
#include "devices/Revision.hpp"
#include "devices/PIT.hpp"
#include "exceptions/Dispatcher.hpp"
#include "exceptions/Calls.hpp" // breakpoint

#include <cstdint> // the uintX_t stuff is cool

#include "devices/VESA.hpp"

// Copyright (C) 2021 Jochen Peters
// Heinrich-Heine University
//
// I like to set this file to <http://unlicense.org> but the code
// comes primary from GNU-GPLv3 !!
//
// This file is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// This driver has been ported from Circle C++ bare metal environment
// for Raspberry Pi which is:
//
// lib/bcmpciehostbridge.cpp
// Copyright (C) 2019-2020  R. Stange <rsta2@o2online.de>
// Licensed under GPLv3
//
// ... and this driver has been ported from the Linux driver
// which is:
//
//  drivers/pci/controller/pcie-brcmstb.c
//  Copyright (C) 2009 - 2017 Broadcom
//  Licensed under GPL-2.0
//

#define PCIE_GEN    2

/* BRCM_PCIE_CAP_REGS - Offset for the mandatory capability config regs */
#define BRCM_PCIE_CAP_REGS  0x00ac

/*
 * Broadcom Settop Box PCIe Register Offsets. The names are from
 * the chip's RDB and we use them here so that a script can correlate
 * this code and the RDB to prevent discrepancies.
 */
#define PCIE_RC_CFG_VENDOR_VENDOR_SPECIFIC_REG1		0x0188
#define PCIE_RC_CFG_PRIV1_ID_VAL3			0x043c
#define PCIE_RC_DL_MDIO_ADDR				0x1100
#define PCIE_RC_DL_MDIO_WR_DATA				0x1104
#define PCIE_RC_DL_MDIO_RD_DATA				0x1108
#define PCIE_MISC_MISC_CTRL				0x4008
#define PCIE_MISC_CPU_2_PCIE_MEM_WIN0_LO		0x400c
#define PCIE_MISC_CPU_2_PCIE_MEM_WIN0_HI		0x4010
#define PCIE_MISC_RC_BAR1_CONFIG_LO			0x402c
#define PCIE_MISC_RC_BAR2_CONFIG_LO			0x4034
#define PCIE_MISC_RC_BAR2_CONFIG_HI			0x4038
#define PCIE_MISC_RC_BAR3_CONFIG_LO			0x403c
#define PCIE_MISC_MSI_BAR_CONFIG_LO			0x4044
#define PCIE_MISC_MSI_BAR_CONFIG_HI			0x4048
#define PCIE_MISC_MSI_DATA_CONFIG			0x404c
#define PCIE_MISC_PCIE_CTRL				0x4064
#define PCIE_MISC_PCIE_STATUS				0x4068
#define PCIE_MISC_REVISION				0x406c
#define PCIE_MISC_CPU_2_PCIE_MEM_WIN0_BASE_LIMIT	0x4070
#define PCIE_MISC_CPU_2_PCIE_MEM_WIN0_BASE_HI		0x4080
#define PCIE_MISC_CPU_2_PCIE_MEM_WIN0_LIMIT_HI		0x4084
#define PCIE_MISC_HARD_PCIE_HARD_DEBUG			0x4204
#define PCIE_INTR2_CPU_BASE				0x4300
#define PCIE_MSI_INTR2_BASE				0x4500

/*
 * Broadcom Settop Box PCIe Register Field shift and mask info. The
 * names are from the chip's RDB and we use them here so that a script
 * can correlate this code and the RDB to prevent discrepancies.
 */
#define PCIE_RC_CFG_VENDOR_VENDOR_SPECIFIC_REG1_ENDIAN_MODE_BAR2_MASK	0xc
#define PCIE_RC_CFG_VENDOR_VENDOR_SPECIFIC_REG1_ENDIAN_MODE_BAR2_SHIFT	0x2
#define PCIE_RC_CFG_PRIV1_ID_VAL3_CLASS_CODE_MASK		0xffffff
#define PCIE_RC_CFG_PRIV1_ID_VAL3_CLASS_CODE_SHIFT		0x0
#define PCIE_MISC_MISC_CTRL_SCB_ACCESS_EN_MASK			0x1000
#define PCIE_MISC_MISC_CTRL_SCB_ACCESS_EN_SHIFT			0xc
#define PCIE_MISC_MISC_CTRL_CFG_READ_UR_MODE_MASK		0x2000
#define PCIE_MISC_MISC_CTRL_CFG_READ_UR_MODE_SHIFT		0xd
#define PCIE_MISC_MISC_CTRL_MAX_BURST_SIZE_MASK			0x300000
#define PCIE_MISC_MISC_CTRL_MAX_BURST_SIZE_SHIFT		0x14
#define PCIE_MISC_MISC_CTRL_SCB0_SIZE_MASK			0xf8000000
#define PCIE_MISC_MISC_CTRL_SCB0_SIZE_SHIFT			0x1b
#define PCIE_MISC_MISC_CTRL_SCB1_SIZE_MASK			0x7c00000
#define PCIE_MISC_MISC_CTRL_SCB1_SIZE_SHIFT			0x16
#define PCIE_MISC_MISC_CTRL_SCB2_SIZE_MASK			0x1f
#define PCIE_MISC_MISC_CTRL_SCB2_SIZE_SHIFT			0x0
#define PCIE_MISC_RC_BAR1_CONFIG_LO_SIZE_MASK			0x1f
#define PCIE_MISC_RC_BAR1_CONFIG_LO_SIZE_SHIFT			0x0
#define PCIE_MISC_RC_BAR2_CONFIG_LO_SIZE_MASK			0x1f
#define PCIE_MISC_RC_BAR2_CONFIG_LO_SIZE_SHIFT			0x0
#define PCIE_MISC_RC_BAR3_CONFIG_LO_SIZE_MASK			0x1f
#define PCIE_MISC_RC_BAR3_CONFIG_LO_SIZE_SHIFT			0x0
#define PCIE_MISC_PCIE_CTRL_PCIE_PERSTB_MASK			0x4
#define PCIE_MISC_PCIE_CTRL_PCIE_PERSTB_SHIFT			0x2
#define PCIE_MISC_PCIE_CTRL_PCIE_L23_REQUEST_MASK		0x1
#define PCIE_MISC_PCIE_CTRL_PCIE_L23_REQUEST_SHIFT		0x0
#define PCIE_MISC_PCIE_STATUS_PCIE_PORT_MASK			0x80
#define PCIE_MISC_PCIE_STATUS_PCIE_PORT_SHIFT			0x7
#define PCIE_MISC_PCIE_STATUS_PCIE_DL_ACTIVE_MASK		0x20
#define PCIE_MISC_PCIE_STATUS_PCIE_DL_ACTIVE_SHIFT		0x5
#define PCIE_MISC_PCIE_STATUS_PCIE_PHYLINKUP_MASK		0x10
#define PCIE_MISC_PCIE_STATUS_PCIE_PHYLINKUP_SHIFT		0x4
#define PCIE_MISC_PCIE_STATUS_PCIE_LINK_IN_L23_MASK		0x40
#define PCIE_MISC_PCIE_STATUS_PCIE_LINK_IN_L23_SHIFT		0x6
#define PCIE_MISC_REVISION_MAJMIN_MASK				0xffff
#define PCIE_MISC_REVISION_MAJMIN_SHIFT				0
#define PCIE_MISC_CPU_2_PCIE_MEM_WIN0_BASE_LIMIT_LIMIT_MASK	0xfff00000
#define PCIE_MISC_CPU_2_PCIE_MEM_WIN0_BASE_LIMIT_LIMIT_SHIFT	0x14
#define PCIE_MISC_CPU_2_PCIE_MEM_WIN0_BASE_LIMIT_BASE_MASK	0xfff0
#define PCIE_MISC_CPU_2_PCIE_MEM_WIN0_BASE_LIMIT_BASE_SHIFT	0x4
#define PCIE_MISC_CPU_2_PCIE_MEM_WIN0_BASE_LIMIT_NUM_MASK_BITS	0xc
#define PCIE_MISC_CPU_2_PCIE_MEM_WIN0_BASE_HI_BASE_MASK		0xff
#define PCIE_MISC_CPU_2_PCIE_MEM_WIN0_BASE_HI_BASE_SHIFT	0x0
#define PCIE_MISC_CPU_2_PCIE_MEM_WIN0_LIMIT_HI_LIMIT_MASK	0xff
#define PCIE_MISC_CPU_2_PCIE_MEM_WIN0_LIMIT_HI_LIMIT_SHIFT	0x0
#define PCIE_MISC_HARD_PCIE_HARD_DEBUG_CLKREQ_DEBUG_ENABLE_MASK	0x2
#define PCIE_MISC_HARD_PCIE_HARD_DEBUG_CLKREQ_DEBUG_ENABLE_SHIFT 0x1
#define PCIE_MISC_HARD_PCIE_HARD_DEBUG_SERDES_IDDQ_MASK		0x08000000
#define PCIE_MISC_HARD_PCIE_HARD_DEBUG_SERDES_IDDQ_SHIFT	0x1b
#define PCIE_RGR1_SW_INIT_1_PERST_MASK				0x1
#define PCIE_RGR1_SW_INIT_1_PERST_SHIFT				0x0

// minimum
#define BRCM_PCIE_HW_REV_33		0x0303

#define BRCM_MSI_TARGET_ADDR_LT_4GB	0x0fffffffcULL
#define BRCM_MSI_TARGET_ADDR_GT_4GB	0xffffffffcULL

#define BURST_SIZE_128			0
#define BURST_SIZE_256			1
#define BURST_SIZE_512			2

// some offsets from PCIE_INTR2_CPU_BASE
#define STATUS          0x0
#define SET             0x4
#define CLR             0x8
#define MASK_STATUS     0xc
#define MASK_SET        0x10
#define MASK_CLR        0x14

#define PCIE_BUSNUM_SHIFT   20
#define PCIE_SLOT_SHIFT     15
#define PCIE_FUNC_SHIFT     12

#define	DATA_ENDIAN     0
#define MMIO_ENDIAN     0

#define MDIO_PORT0			0x0
#define MDIO_DATA_MASK			0x7fffffff
#define MDIO_DATA_SHIFT			0x0
#define MDIO_PORT_MASK			0xf0000
#define MDIO_PORT_SHIFT			0x16
#define MDIO_REGAD_MASK			0xffff
#define MDIO_REGAD_SHIFT		0x0
#define MDIO_CMD_MASK			0xfff00000
#define MDIO_CMD_SHIFT			0x14
#define MDIO_CMD_READ			0x1
#define MDIO_CMD_WRITE			0x0
#define MDIO_DATA_DONE_MASK		0x80000000
#define MDIO_RD_DONE(x)			(((x) & MDIO_DATA_DONE_MASK) ? 1 : 0)
#define MDIO_WT_DONE(x)			(((x) & MDIO_DATA_DONE_MASK) ? 0 : 1)
#define SSC_REGS_ADDR			0x1100
#define SET_ADDR_OFFSET			0x1f
#define SSC_CNTL_OFFSET			0x2
#define SSC_CNTL_OVRD_EN_MASK		0x8000
#define SSC_CNTL_OVRD_EN_SHIFT		0xf
#define SSC_CNTL_OVRD_VAL_MASK		0x4000
#define SSC_CNTL_OVRD_VAL_SHIFT		0xe
#define SSC_STATUS_OFFSET		0x1
#define SSC_STATUS_SSC_MASK		0x400
#define SSC_STATUS_SSC_SHIFT		0xa
#define SSC_STATUS_PLL_LOCK_MASK	0x800
#define SSC_STATUS_PLL_LOCK_SHIFT	0xb

#define IDX_ADDR                pcie_offsets[EXT_CFG_INDEX]
#define DATA_ADDR               pcie_offsets[EXT_CFG_DATA]
#define PCIE_RGR1_SW_INIT_1     pcie_offsets[RGR1_SW_INIT_1]

#define MEGABYTE    0x0100000
#define GIGABYTE    0x40000000UL

// high memory region (memory >= 3 GB is not safe to be DMA-able and is not used) ???
//#define MEM_HIGHMEM_START   GIGABYTE
//#define MEM_HIGHMEM_END     (3 * GIGABYTE - 1)

//#define MEM_PCIE_RANGE_START        0x600000000UL       // PCIe memory range (outbound)
//#define MEM_PCIE_RANGE_SIZE           0x4000000UL       // 67MB
//#define MEM_PCIE_RANGE_PCIE_START    0xF8000000UL       // mapping on PCIe side (inbound)
//#define MEM_PCIE_RANGE_START_VIRTUAL	MEM_PCIE_RANGE_START
//#define MEM_PCIE_RANGE_END_VIRTUAL	(MEM_PCIE_RANGE_START_VIRTUAL + MEM_PCIE_RANGE_SIZE - 1UL)


// PCI stuff ---------------------------
#define PCI_EXP_LNKSTA              18      /* Link Status */
#define PCI_EXP_LNKSTA_CLS          0x000f  /* Current Link Speed */
#define PCI_EXP_LNKSTA_NLW          0x03f0  /* Negotiated Link Width */
#define PCI_EXP_LNKSTA_NLW_SHIFT    4       /* start of NLW mask in link status */

#define PCI_CACHE_LINE_SIZE	0x0c	/* 8 bits */
#define PCI_CLASS_REVISION	0x08	/* High 24 bits are class, low 8 revision */
#define PCI_HEADER_TYPE		0x0e	/* 8 bits */
#define  PCI_HEADER_TYPE_NORMAL		0
#define  PCI_HEADER_TYPE_BRIDGE		1

#define PCI_PRIMARY_BUS		0x18	/* Primary bus number */
#define PCI_SECONDARY_BUS	0x19	/* Secondary bus number */
#define PCI_SUBORDINATE_BUS	0x1a	/* Highest bus number behind the bridge */

#define PCI_MEMORY_BASE		0x20	/* Memory range behind */
#define PCI_MEMORY_LIMIT	0x22

#define PCI_BRIDGE_CONTROL	0x3e
#define  PCI_BRIDGE_CTL_PARITY	0x01	/* Enable parity detection on secondary interface */

#define PCI_COMMAND		0x04	/* 16 bits */
#define  PCI_COMMAND_MEMORY	0x2	/* Enable response in Memory space */
#define  PCI_COMMAND_MASTER	0x4	/* Enable bus mastering */
#define  PCI_COMMAND_PARITY	0x40	/* Enable parity checking */
#define  PCI_COMMAND_SERR	0x100	/* Enable SERR */

#define PCI_EXP_RTCTL		28	/* Root Control */
#define  PCI_EXP_RTCTL_CRSSVE	0x0010	/* CRS Software Visibility Enable */

#define PCI_EXP_LNKCAP      12          /* Link Capabilities */
#define PCI_EXP_LNKCAP_SLS  0x0000000f  /* Supported Link Speeds */
#define PCI_EXP_LNKCTL2     48          /* Link Control 2 */

#define PCI_STATUS              0x06	/* 16 bits */
#define  PCI_STATUS_INTERRUPT   0x08	/* Interrupt status */
#define  PCI_STATUS_CAP_LIST    0x10	/* Support Capability List */

#define PCI_CAPABILITY_LIST     0x34	/* Offset of first capability list entry */

/* Capability lists */
#define PCI_CAP_LIST_ID     0	/* Capability ID */
#define  PCI_CAP_ID_PM      0x01	/* Power Management */
#define  PCI_CAP_ID_AGP     0x02	/* Accelerated Graphics Port */
#define  PCI_CAP_ID_VPD     0x03	/* Vital Product Data */
#define  PCI_CAP_ID_SLOTID  0x04	/* Slot Identification */
#define  PCI_CAP_ID_MSI     0x05	/* Message Signalled Interrupts */
#define  PCI_CAP_ID_CHSWP   0x06	/* CompactPCI HotSwap */
#define  PCI_CAP_ID_PCIX    0x07	/* PCI-X */
#define  PCI_CAP_ID_HT      0x08	/* HyperTransport */
#define  PCI_CAP_ID_VNDR    0x09	/* Vendor-Specific */
#define  PCI_CAP_ID_DBG     0x0A	/* Debug port */
#define  PCI_CAP_ID_CCRC    0x0B	/* CompactPCI Central Resource Control */
#define  PCI_CAP_ID_SHPC    0x0C	/* PCI Standard Hot-Plug Controller */
#define  PCI_CAP_ID_SSVID   0x0D	/* Bridge subsystem vendor/device ID */
#define  PCI_CAP_ID_AGP3    0x0E	/* AGP Target PCI-PCI bridge */
#define  PCI_CAP_ID_SECDEV  0x0F	/* Secure Device */
#define  PCI_CAP_ID_EXP     0x10	/* PCI Express */
#define  PCI_CAP_ID_MSIX    0x11	/* MSI-X */
#define  PCI_CAP_ID_SATA    0x12	/* SATA Data/Index Conf. */
#define  PCI_CAP_ID_AF      0x13	/* PCI Advanced Features */
#define  PCI_CAP_ID_EA      0x14	/* PCI Enhanced Allocation */
#define  PCI_CAP_ID_MAX     PCI_CAP_ID_EA
#define PCI_CAP_LIST_NEXT   1	/* Next capability in the list */
#define PCI_CAP_FLAGS       2	/* Capability defined flags (16 bits) */
#define PCI_CAP_SIZEOF      4

#define PCI_BASE_ADDRESS_0	0x10	/* 32 bits */
#define PCI_BASE_ADDRESS_1	0x14	/* 32 bits [htype 0,1 only] */
#define  PCI_BASE_ADDRESS_MEM_TYPE_32	0x00	/* 32 bit address */
#define  PCI_BASE_ADDRESS_MEM_TYPE_64	0x04	/* 64 bit address */

#define PCI_MSI_FLAGS		2	/* Message Control */
#define  PCI_MSI_FLAGS_ENABLE	0x0001	/* MSI feature enabled */
#define  PCI_MSI_FLAGS_QMASK	0x000e	/* Maximum queue size available */
#define  PCI_MSI_FLAGS_64BIT	0x0080	/* 64-bit addresses allowed */
#define PCI_MSI_DATA_32		8	/* 16 bits of data for 32-bit devices */
#define PCI_MSI_DATA_64		12	/* 16 bits of data for 64-bit devices */
#define PCI_MSI_ADDRESS_LO	4	/* Lower 32 bits */
#define PCI_MSI_ADDRESS_HI	8	/* Upper 32 bits (if PCI_MSI_FLAGS_64BIT set) */

#define  PCI_COMMAND_INTX_DISABLE 0x400 /* INTx Emulation Disable */

enum {
	RGR1_SW_INIT_1,
	EXT_CFG_INDEX,
	EXT_CFG_DATA,
};

enum {
	RGR1_SW_INIT_1_INIT_MASK,
	RGR1_SW_INIT_1_INIT_SHIFT,
	RGR1_SW_INIT_1_PERST_MASK,
	RGR1_SW_INIT_1_PERST_SHIFT,
};

static const int pcie_reg_field_info[] = {
	0x2,		// [RGR1_SW_INIT_1_INIT_MASK]
	0x1,		// [RGR1_SW_INIT_1_INIT_SHIFT]
};

static const int pcie_offsets[] = {
	0x9210,		// [RGR1_SW_INIT_1]
	0x9000,		// [EXT_CFG_INDEX]
	0x8000,		// [EXT_CFG_DATA]
};

/* These macros extract/insert fields to host controller's register set. */
#define EXTRACT_FIELD(val, reg, field) \
	((val & reg##_##field##_MASK) >> reg##_##field##_SHIFT)
#define INSERT_FIELD(val, reg, field, field_val) \
	((val & ~reg##_##field##_MASK) | \
	 (reg##_##field##_MASK & (field_val << reg##_##field##_SHIFT)))

// split and unsplit 
#define PCI_DEVFN(slot, func)	((((slot) & 0x1f) << 3) | ((func) & 0x07))
#define PCI_SLOT(devfn)		(((devfn) >> 3) & 0x1f)
#define PCI_FUNC(devfn)		((devfn) & 0x07)

#define lower_32_bits(v)	((v) & 0xFFFFFFFFU)
#define upper_32_bits(v)	((v) >> 32)



PCIe::Device PCIe::assignment[PCIe::CONFIG::SLOTS];

// returns base address of the inbound memory window
uint64_t PCIe::getDMAAddress (void) {
    return PCIe::ADDR::START;
}

void PCIe::set_gen (uint32_t gen) {
    uint32_t lnkcap = Peripherals::read32(PCIe::ADDR::BASE + BRCM_PCIE_CAP_REGS + PCI_EXP_LNKCAP);
    uint16_t lnkctl2 = Peripherals::read16(PCIe::ADDR::BASE + BRCM_PCIE_CAP_REGS + PCI_EXP_LNKCTL2);

    lnkcap = (lnkcap & ~PCI_EXP_LNKCAP_SLS) | gen;
    Peripherals::write32(PCIe::ADDR::BASE + BRCM_PCIE_CAP_REGS + PCI_EXP_LNKCAP, lnkcap);

    lnkctl2 = (lnkctl2 & ~0xFu) | static_cast<uint16_t>(gen);
    Peripherals::write16(PCIe::ADDR::BASE + BRCM_PCIE_CAP_REGS + PCI_EXP_LNKCTL2, lnkctl2);
}

/*
 * This is to convert the size of the inbound "BAR" region to the
 * non-linear values of PCIE_X_MISC_RC_BAR[123]_CONFIG_LO.SIZE
 */
int PCIe::encode_ibar_size (uint64_t size) {
    int log2_in = ilog2(size);

    if (log2_in >= 12 && log2_in <= 15)
        // Covers 4KB to 32KB (inclusive)
        return (log2_in - 12) + 0x1c;
    else if (log2_in >= 16 && log2_in <= 37)
        // Covers 64KB to 32GB, (inclusive)
        return log2_in - 15;
    // Something is awry so disable
    return 0;
} 

int PCIe::ilog2 (uint64_t v) {
    int l = 0;
    while (((uint64_t) 1 << l) < v)
        l++;
    return l;
}

bool PCIe::link_up (void) {
    uint32_t val = Peripherals::read32(PCIe::ADDR::BASE + PCIE_MISC_PCIE_STATUS);
    uint32_t dla = EXTRACT_FIELD(val, PCIE_MISC_PCIE_STATUS, PCIE_DL_ACTIVE);
    uint32_t plu = EXTRACT_FIELD(val, PCIE_MISC_PCIE_STATUS, PCIE_PHYLINKUP);

    return  (dla && plu) ? true : false;
}

void PCIe::init_set (unsigned val) {
    unsigned shift = pcie_reg_field_info[RGR1_SW_INIT_1_INIT_SHIFT];
    uint32_t mask =  pcie_reg_field_info[RGR1_SW_INIT_1_INIT_MASK];

    Peripherals::writeTo(
        PCIe::ADDR::BASE + PCIE_RGR1_SW_INIT_1,
        mask,
        shift,
        val
    );
    (void)Peripherals::read32(PCIe::ADDR::BASE + PCIE_RGR1_SW_INIT_1);
}

void PCIe::set_outbound_win (uint64_t cpu_addr, uint64_t pcie_addr, uint64_t size) {
    uint64_t cpu_addr_mb, limit_addr_mb;
    uint32_t tmp;

    // Set the m_base of the pcie_addr window
    Peripherals::write32(
        PCIe::ADDR::BASE + PCIE_MISC_CPU_2_PCIE_MEM_WIN0_LO,
        lower_32_bits(pcie_addr) + MMIO_ENDIAN
    );
    Peripherals::write32(
        PCIe::ADDR::BASE + PCIE_MISC_CPU_2_PCIE_MEM_WIN0_HI,
        upper_32_bits(pcie_addr)
    );

    cpu_addr_mb = cpu_addr >> 20;
    limit_addr_mb = (cpu_addr + size - 1) >> 20;

    // Write the addr m_base low register
    Peripherals::writeTo(
        PCIe::ADDR::BASE + PCIE_MISC_CPU_2_PCIE_MEM_WIN0_BASE_LIMIT + 0,
        PCIE_MISC_CPU_2_PCIE_MEM_WIN0_BASE_LIMIT_BASE_MASK,
        PCIE_MISC_CPU_2_PCIE_MEM_WIN0_BASE_LIMIT_BASE_SHIFT,
        cpu_addr_mb
    );
    
    // Write the addr limit low register
    Peripherals::writeTo(
        PCIe::ADDR::BASE + PCIE_MISC_CPU_2_PCIE_MEM_WIN0_BASE_LIMIT + 0,
        PCIE_MISC_CPU_2_PCIE_MEM_WIN0_BASE_LIMIT_LIMIT_MASK,
        PCIE_MISC_CPU_2_PCIE_MEM_WIN0_BASE_LIMIT_LIMIT_SHIFT,
        limit_addr_mb
    );

    // Write the cpu addr high register
    tmp = (uint32_t)(cpu_addr_mb >> PCIE_MISC_CPU_2_PCIE_MEM_WIN0_BASE_LIMIT_NUM_MASK_BITS);

    Peripherals::writeTo(
        PCIe::ADDR::BASE + PCIE_MISC_CPU_2_PCIE_MEM_WIN0_BASE_HI + 0,
        PCIE_MISC_CPU_2_PCIE_MEM_WIN0_BASE_HI_BASE_MASK,
        PCIE_MISC_CPU_2_PCIE_MEM_WIN0_BASE_HI_BASE_SHIFT,
        tmp
    );
    
    // Write the cpu limit high register
    tmp = (uint32_t)(limit_addr_mb >> PCIE_MISC_CPU_2_PCIE_MEM_WIN0_BASE_LIMIT_NUM_MASK_BITS);

    Peripherals::writeTo(
        PCIe::ADDR::BASE + PCIE_MISC_CPU_2_PCIE_MEM_WIN0_LIMIT_HI + 0,
        PCIE_MISC_CPU_2_PCIE_MEM_WIN0_LIMIT_HI_LIMIT_MASK,
        PCIE_MISC_CPU_2_PCIE_MEM_WIN0_LIMIT_HI_LIMIT_SHIFT,
        tmp
    );
}

bool PCIe::setup () {
    uint32_t tmp = Peripherals::read32(PCIe::ADDR::BASE + PCIE_MISC_REVISION);
    tmp = (tmp & 0xffff);
    VESA::me().print("PCIe revision: ").print((long)tmp, 16).print("\n");
    
    // Reset the bridge
    init_set(1);

    // Ensure that the fundamental reset is asserted
    Peripherals::writeTo(
        PCIe::ADDR::BASE + PCIE_RGR1_SW_INIT_1,
        PCIE_RGR1_SW_INIT_1_PERST_MASK,
        PCIE_RGR1_SW_INIT_1_PERST_SHIFT,
        1
    );
    (void)Peripherals::read32(PCIe::ADDR::BASE + PCIE_RGR1_SW_INIT_1);

    Scheduler::snooze();
    
    // Take the bridge out of reset
    init_set(0);

    // WR_FLD_RB(...)
    Peripherals::writeTo(
        PCIe::ADDR::BASE + PCIE_MISC_HARD_PCIE_HARD_DEBUG,
        PCIE_MISC_HARD_PCIE_HARD_DEBUG_SERDES_IDDQ_MASK,
        PCIE_MISC_HARD_PCIE_HARD_DEBUG_SERDES_IDDQ_SHIFT,
        0
    );
    (void)Peripherals::read32(PCIe::ADDR::BASE + PCIE_MISC_HARD_PCIE_HARD_DEBUG);
    
    // Wait for SerDes to be stable (Serializer/Deserializer)
    Scheduler::snooze();

    // Set SCB_MAX_BURST_SIZE, CFG_READ_UR_MODE, SCB_ACCESS_EN
    tmp = INSERT_FIELD(0, PCIE_MISC_MISC_CTRL, SCB_ACCESS_EN, 1);
    tmp = INSERT_FIELD(tmp, PCIE_MISC_MISC_CTRL, CFG_READ_UR_MODE, 1);
    tmp = INSERT_FIELD(tmp, PCIE_MISC_MISC_CTRL, MAX_BURST_SIZE, BURST_SIZE_128);
    Peripherals::write32(PCIe::ADDR::BASE + PCIE_MISC_MISC_CTRL, tmp);




    /*
     * Set up inbound memory view for the EP (called RC_BAR2,
     * not to be confused with the BARs that are advertised by
     * the EP).
     */
    tmp = Revision::get(); // new style Pi revision: Pi4B, 4GB => 0x00C0_3112
    // C0 = 1100 0000

    // set_pci_ranges inbound window -----------------------------------
    /// @todo Changes in this area where never done. maybe some mods here makes MMIO possible?
    
    uint64_t ramSize = 256 << ((tmp >> 20) & 7);
    // FF << b11 = 1000 0000 0 000 = 2048 =>   BUT THAT IS NOT 4096 MB !!!!?
    ramSize *= MEGABYTE;

    tmp = lower_32_bits(0UL);
    tmp = INSERT_FIELD(
        tmp,
        PCIE_MISC_RC_BAR2_CONFIG_LO,
        SIZE,
        encode_ibar_size(ramSize)
    );
    Peripherals::write32(
        PCIe::ADDR::BASE + PCIE_MISC_RC_BAR2_CONFIG_LO,
        tmp
    );
    Peripherals::write32(
        PCIe::ADDR::BASE + PCIE_MISC_RC_BAR2_CONFIG_HI,
        upper_32_bits(0UL)
    );

    uint32_t scb_size_val = ilog2(ramSize) - 15;
    Peripherals::writeTo(
        PCIe::ADDR::BASE + PCIE_MISC_MISC_CTRL,
        PCIE_MISC_MISC_CTRL_SCB0_SIZE_MASK,
        PCIE_MISC_MISC_CTRL_SCB0_SIZE_SHIFT,
        scb_size_val
    );
    // set_pci_ranges inbound window --------------------------------END


    // disable the PCIe->GISB memory window (RC_BAR1)
    Peripherals::writeTo(
        PCIe::ADDR::BASE + PCIE_MISC_RC_BAR1_CONFIG_LO,
        PCIE_MISC_RC_BAR1_CONFIG_LO_SIZE_MASK,
        PCIE_MISC_RC_BAR1_CONFIG_LO_SIZE_SHIFT,
        0
    );

    // disable the PCIe->SCB memory window (RC_BAR3)
    Peripherals::writeTo(
        PCIe::ADDR::BASE + PCIE_MISC_RC_BAR3_CONFIG_LO,
        PCIE_MISC_RC_BAR3_CONFIG_LO_SIZE_MASK,
        PCIE_MISC_RC_BAR3_CONFIG_LO_SIZE_SHIFT,
        0
    );

    // clear any interrupts we find on boot
    Peripherals::write32(PCIe::ADDR::BASE + PCIE_INTR2_CPU_BASE + CLR, 0xffffffff);
    (void)Peripherals::read32(PCIe::ADDR::BASE + PCIE_INTR2_CPU_BASE + CLR);

    // Mask all interrupts since we are not handling any yet
    Peripherals::write32(PCIe::ADDR::BASE + PCIE_INTR2_CPU_BASE + MASK_SET, 0xffffffff);
    (void)Peripherals::read32(PCIe::ADDR::BASE + PCIE_INTR2_CPU_BASE + MASK_SET);

    // set PCIe generation
    set_gen(PCIE_GEN);

    // Unassert the fundamental reset
    Peripherals::writeTo(
        PCIe::ADDR::BASE + PCIE_RGR1_SW_INIT_1,
        PCIE_RGR1_SW_INIT_1_PERST_MASK,
        PCIE_RGR1_SW_INIT_1_PERST_SHIFT,
        0
    );
    (void)Peripherals::read32(PCIe::ADDR::BASE + PCIE_RGR1_SW_INIT_1);
    
    // Give the RC/EP time to wake up, before trying to configure RC.
    Scheduler::snooze();
    
    // Intermittently check status for link-up
    for (int i = 0; i < 10; ++i) {
        if (link_up()) {
            break;
        } else {
            PIT::me()->delay(10);
        }
    }

    // check if link is up
    if(!link_up()) {
        VESA::me().print("PCIe link is down!\n");
        return false;
    }
    
    // check rc mode
    tmp = Peripherals::read32(PCIe::ADDR::BASE + PCIE_MISC_PCIE_STATUS);
    bool rc_mode = !!EXTRACT_FIELD(tmp, PCIE_MISC_PCIE_STATUS, PCIE_PORT);
    
    if (rc_mode == false) {
        VESA::me().print("PCIe misconfigured; is still in EP mode\n");
        return false;
    }

    /// @todo map F8000000 to F8000000 not possible, too!
    set_outbound_win(
        PCIe::ADDR::START, // inbount to ..
        PCIe::ADDR::START_VIRTUAL, // .. outbount
        PCIe::ADDR::SIZE
    );

    //For config space accesses on the RC, show the right class for
    //a PCIe-PCIe bridge (the default setting is to be EP mode).

    Peripherals::writeTo(
        PCIe::ADDR::BASE + PCIE_RC_CFG_PRIV1_ID_VAL3,
        PCIE_RC_CFG_PRIV1_ID_VAL3_CLASS_CODE_MASK,
        PCIE_RC_CFG_PRIV1_ID_VAL3_CLASS_CODE_SHIFT,
        0x060400
    );
    (void)Peripherals::read32(PCIe::ADDR::BASE + PCIE_RC_CFG_PRIV1_ID_VAL3);


    uint16_t linkstate = Peripherals::read16(PCIe::ADDR::BASE + BRCM_PCIE_CAP_REGS + PCI_EXP_LNKSTA);
    uint16_t cls = linkstate & PCI_EXP_LNKSTA_CLS;
    uint16_t nlw = (linkstate & PCI_EXP_LNKSTA_NLW) >> PCI_EXP_LNKSTA_NLW_SHIFT;

    VESA::me().print("Link up.");
    if (cls == 1) {
        VESA::me().print(" 2.5 Gbps");
    } else if (cls == 2) {
        VESA::me().print(" 5.0 Gbps");
    } else if (cls == 3) {
        VESA::me().print(" 8.0 Gbps");
    } else {
        VESA::me().print(" ??? Gbps");
    }
    VESA::me().print(" x").print((long)nlw).print("\n");

    // PCIe->SCB endian mode for BAR
    // field ENDIAN_MODE_BAR2 = DATA_ENDIAN
    Peripherals::writeTo(
        PCIe::ADDR::BASE + PCIE_RC_CFG_VENDOR_VENDOR_SPECIFIC_REG1,
        PCIE_RC_CFG_VENDOR_VENDOR_SPECIFIC_REG1_ENDIAN_MODE_BAR2_MASK,
        PCIE_RC_CFG_VENDOR_VENDOR_SPECIFIC_REG1_ENDIAN_MODE_BAR2_SHIFT,
        DATA_ENDIAN
    );
    (void)Peripherals::read32(PCIe::ADDR::BASE + PCIE_RC_CFG_VENDOR_VENDOR_SPECIFIC_REG1);

    // Refclk from RC should be gated with CLKREQ# input when ASPM L0s,L1
    // is enabled =>  setting the CLKREQ_DEBUG_ENABLE field to 1.
    Peripherals::writeTo(
        PCIe::ADDR::BASE + PCIE_MISC_HARD_PCIE_HARD_DEBUG,
        PCIE_MISC_HARD_PCIE_HARD_DEBUG_CLKREQ_DEBUG_ENABLE_MASK,
        PCIE_MISC_HARD_PCIE_HARD_DEBUG_CLKREQ_DEBUG_ENABLE_SHIFT,
        1
    );
    (void)Peripherals::read32(PCIe::ADDR::BASE + PCIE_MISC_HARD_PCIE_HARD_DEBUG);

    if (enable_bridge() == false) {
        VESA::me().print("Error while enabling PCIe-Bridge!\n");
        return false;
    }
    VESA::me().print("PCIe bridge enabled.\n");
    return true;
}

// Configuration space read/write support
int PCIe::cfg_index (unsigned busnr, unsigned devfn, int reg) {
    return ((PCI_SLOT(devfn) & 0x1f) << PCIE_SLOT_SHIFT) |
        ((PCI_FUNC(devfn) & 0x07) << PCIE_FUNC_SHIFT) |
        (busnr << PCIE_BUSNUM_SHIFT) |
        (reg & ~3);
}

uint64_t PCIe::map_conf (unsigned busnr, unsigned devfn, int where) {
    // Accesses to the RC go right to the RC registers if slot==0
    if (busnr == 0) {
        return PCI_SLOT(devfn) ? 0 : PCIe::ADDR::BASE + where;
    }
    // For devices, write to the config space index register
    int idx = cfg_index(busnr, devfn, 0);
    Peripherals::write32(PCIe::ADDR::BASE + IDX_ADDR, idx);
    return PCIe::ADDR::BASE + DATA_ADDR + where;
}

bool PCIe::enable_bridge (void) {
    uint64_t conf = map_conf(0, PCI_DEVFN(0, 0), 0);
    if (conf == 0) return false;

    if (
        (Peripherals::read32(conf + PCI_CLASS_REVISION) >> 8) != 0x060400 ||
        (Peripherals::read8(conf + PCI_HEADER_TYPE) != PCI_HEADER_TYPE_BRIDGE)
    ) {
        return false;
    }
    
    Peripherals::write8(conf + PCI_CACHE_LINE_SIZE, 64/4);
    Peripherals::write8(conf + PCI_SECONDARY_BUS, 1);
    Peripherals::write8(conf + PCI_SUBORDINATE_BUS, 1);

    Peripherals::write16(conf + PCI_MEMORY_BASE, PCIe::ADDR::START >> 16);
    Peripherals::write16(conf + PCI_MEMORY_LIMIT, PCIe::ADDR::START >> 16);

    Peripherals::write8(conf + PCI_BRIDGE_CONTROL, PCI_BRIDGE_CTL_PARITY);
    Peripherals::write8(conf + BRCM_PCIE_CAP_REGS + PCI_EXP_RTCTL, PCI_EXP_RTCTL_CRSSVE);

    Peripherals::write16(
        conf + PCI_COMMAND, 
        PCI_COMMAND_MEMORY | 
        PCI_COMMAND_MASTER | 
        PCI_COMMAND_PARITY |
        PCI_COMMAND_SERR
    );

    return true;
}

// capability search
uint64_t PCIe::find (uint64_t nPCIConfig, uint8_t capID) {
    if (!(Peripherals::read16(nPCIConfig + PCI_STATUS) & PCI_STATUS_CAP_LIST)) {
        VESA::me().print("no PCI capability listed.\n");
        return 0;
    }

    uint8_t capPtr = Peripherals::read8(nPCIConfig + PCI_CAPABILITY_LIST);
    while (capPtr != 0) {
        if (Peripherals::read8(nPCIConfig + capPtr + PCI_CAP_LIST_ID) == capID) {
            VESA::me().print(
                "capability ").
                print((long) nPCIConfig, 16).
                print(" with ID ").
                print((long) capID, 16).
                print(" found.\n"
            );
            return nPCIConfig + capPtr;
        }

        capPtr = Peripherals::read8(nPCIConfig + capPtr + PCI_CAP_LIST_NEXT);
    }

    return 0;
}

void PCIe::plugin () {
    Dispatcher::assign(PCIe::CONFIG::SPI, me());

    /*
     * ffe0 -- least sig 5 bits are 0 indicating 32 msgs
     * 6540 -- this is our arbitrary unique data value
     */
    uint32_t data_val = 0xffe06540;

    /*
     * Make sure we are not masking MSIs. Note that MSIs can be masked,
     * but that occurs on the PCIe EP device
     */
    Peripherals::write32(PCIe::ADDR::BASE + PCIE_MSI_INTR2_BASE + MASK_CLR, 0xffffffff);

    uint32_t msi_lo = lower_32_bits(BRCM_MSI_TARGET_ADDR_LT_4GB);
    uint32_t msi_hi = upper_32_bits(BRCM_MSI_TARGET_ADDR_LT_4GB);
    
    // The 0 bit of PCIE_MISC_MSI_BAR_CONFIG_LO is repurposed to MSI enable, which we set to 1.
    Peripherals::write32(PCIe::ADDR::BASE + PCIE_MISC_MSI_BAR_CONFIG_LO, msi_lo | 1);
    Peripherals::write32(PCIe::ADDR::BASE + PCIE_MISC_MSI_BAR_CONFIG_HI, msi_hi);
    Peripherals::write32(PCIe::ADDR::BASE + PCIE_MISC_MSI_DATA_CONFIG, data_val);
}

void PCIe::plugout (void) {
    Peripherals::write32(PCIe::ADDR::BASE + PCIE_MSI_INTR2_BASE + MASK_SET, 0xffffffff);
    Peripherals::write32(PCIe::ADDR::BASE + PCIE_MISC_MSI_BAR_CONFIG_LO, 0);
    Dispatcher::unassign(PCIe::CONFIG::SPI);
}

void PCIe::trigger () {
    uint32_t status;
    // while-loop until all MSI stati are handled
    while (
        (status = Peripherals::read32(PCIe::ADDR::BASE + PCIE_MSI_INTR2_BASE + STATUS)) != 0
    ) {
        // loop through all (32) possible MSI bits
        for (unsigned slot = 0; status && (slot < PCIe::CONFIG::SLOTS); ++slot) {
            uint32_t mask = 1 << slot;
            if (!(status & mask)) continue;

            // clear the MSI
            Peripherals::write32(PCIe::ADDR::BASE + PCIE_MSI_INTR2_BASE + CLR, mask);

            /// use handler to the corresponding slot
            if (PCIe::assignment[slot].isSet) {
                PCIe::assignment[slot].sr->trigger();
            } else {
                /// @todo this is for debugging - and never seen!
                VESA::me().
                    print("Error. MSI (slot ").
                    print(slot).
                    print(") without handler.\n");
            }

            status &= ~mask;
        }
    }
    // End-Of-Interrupt: clear interrupt
    Peripherals::write32(PCIe::ADDR::EOI_CTRL, 1);
}

bool PCIe::addDevice (uint32_t classCode, unsigned func, unsigned slot, ServiceRoutine * handler) {
    uint64_t conf = map_conf(1, PCI_DEVFN (slot, func), 0);
    if (conf == 0) return false;

    if (
        Peripherals::read32(conf + PCI_CLASS_REVISION) >> 8 != classCode ||
        Peripherals::read8(conf + PCI_HEADER_TYPE) != PCI_HEADER_TYPE_NORMAL
    ) {
        return false;
    }
    
    VESA::me().
        print("Vendor:Device = ").
        print((long)Peripherals::read16(conf), 16).
        print(":").
        print((long)Peripherals::read16(conf + 2), 16).
        print("\n");
    
    Peripherals::write8(conf + PCI_CACHE_LINE_SIZE, 64/4);

    Peripherals::write32(
        conf + PCI_BASE_ADDRESS_0,
        lower_32_bits(PCIe::ADDR::START) | PCI_BASE_ADDRESS_MEM_TYPE_64
    );
    Peripherals::write32(
        conf + PCI_BASE_ADDRESS_1,
        upper_32_bits(PCIe::ADDR::START)
    );

    
    uint64_t msiConf = find(conf, PCI_CAP_ID_MSI);
    if (msiConf == 0) return false;

    Peripherals::write32(msiConf + PCI_MSI_ADDRESS_LO, lower_32_bits(PCIe::ADDR::START));
    Peripherals::write32(msiConf + PCI_MSI_ADDRESS_HI, upper_32_bits(PCIe::ADDR::START));
    Peripherals::write16(msiConf + PCI_MSI_DATA_64, 0x6540);

    uint8_t queueSize = (Peripherals::read8(msiConf + PCI_MSI_FLAGS) & PCI_MSI_FLAGS_QMASK) >> 1;
    Peripherals::write8(
        msiConf + PCI_MSI_FLAGS,
        PCI_MSI_FLAGS_ENABLE |
        PCI_MSI_FLAGS_64BIT |
        (queueSize << 4)
    );
    

    Peripherals::write16(
        conf + PCI_COMMAND, 
        PCI_COMMAND_MEMORY |
        PCI_COMMAND_MASTER |
        PCI_COMMAND_PARITY |
        PCI_COMMAND_SERR |
        PCI_COMMAND_INTX_DISABLE
    );
    
    PCIe::assignment[slot].isSet = true;
    PCIe::assignment[slot].classCode = classCode;
    PCIe::assignment[slot].func = func;
    PCIe::assignment[slot].queueSize = queueSize;
    PCIe::assignment[slot].msiConf = msiConf;
    PCIe::assignment[slot].sr = handler;
    
    //breakpoint(32);
    return true;
}

PCIe::PCIe () {
    for (int slot=0; slot < PCIe::CONFIG::SLOTS; ++slot) {
        PCIe::assignment[slot].isSet = false;
    }
}
