#include <cstdint> // the uintX_t stuff is cool
#include "devices/DMA.hpp"
#include "devices/Peripherals.hpp"
#include "exceptions/Calls.hpp" // breakpoint

#include "threads/Scheduler.hpp" // snooze

DMA::DMA (uint8_t chan) {
    if (chan > 14) breakpoint(111);
    _chan = chan;
    _base = ADDR::CHAN0 + (_chan * 0x0100);
}

uint8_t DMA::getChan () {
    return _chan;
}

void DMA::set (ControlBlock * cb) {
    _cb = cb;
    Scheduler::snooze();
}

void DMA::start () {
    Peripherals::write32(ADDR::ENABLE_REG, (1 << _chan));
    Scheduler::snooze();
    Peripherals::write32(_base + OFFSET::CBLK_ADDR, (uint32_t) _cb);
    Scheduler::snooze();
    Peripherals::write32(_base + OFFSET::CS, MASK::CS_ACTIVE);
}

void DMA::pause () {
    uint32_t tmp = Peripherals::read32(_base + OFFSET::CS);
    tmp = tmp & ~(MASK::CS_ACTIVE);
    Peripherals::write32(_base + OFFSET::CS, tmp);
    Scheduler::snooze();
}

void DMA::stop () {
    pause();
    uint32_t tmp = Peripherals::read32(_base + OFFSET::CS);
    tmp = tmp | MASK::CS_RESET;
    Peripherals::write32(_base + OFFSET::CS, tmp);
    Scheduler::snooze();
}

float DMA::progress () {
    uint32_t tmp = Peripherals::read32(_base + OFFSET::TRANSFERED);
    return (_cb->length - tmp)/(float)(_cb->length);
}

bool DMA::isActive () {
    uint32_t tmp = Peripherals::read32(_base + OFFSET::CS);
    return ((tmp & MASK::CS_ACTIVE)>0);
}
