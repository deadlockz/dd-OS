#include "OS.hpp"
#include "threads/Scheduler.hpp"
#include "exceptions/Dispatcher.hpp"
#include "devices/PIT.hpp"
#include "devices/Peripherals.hpp"

int PIT::interval () {
    return PIT::_timerInterval;
}
    
void PIT::intervalSet (unsigned int us, unsigned int usDelay) {
    /// the ARMv8 System Timer still counts in 1MHz steps (1us) ! @todo or is it a Broadcom default?
    _timerInterval = us;
    comparatorSet(counter() + us + usDelay);
}

bool PIT::delay (unsigned int ms) {
    unsigned int ticks = (ms*1000) / interval();
    if (ticks == 0) return false;
    unsigned long soll = OS::systime + ticks;
    while (OS::systime < soll);
    return true;
}

void PIT::stampToTime (unsigned long long ti, char * str) {
    int h = ti / (unsigned long long) 360000;
    if (h < 10) {
        str[0] = '0';
    } else {
        str[0] = '0' + (char) (h/10);
    }
    str[1] = '0' + (char) ( h - (h/10)*10 );
    str[2] = ':';

    int m = (ti / 6000) - (h*60);
    if (m < 10) {
        str[3] = '0';
    } else {
        str[3] = '0' + (char) (m/10);
    }
    str[4] = '0' + (char) ( m - (m/10)*10 );
    str[5] = ':';

    int s = ti/100 - h*3600 - m*60;
    if (s < 10) {
        str[6] = '0';
    } else {
        str[6] = '0' + (char) (s/10);
    }
    str[7] = '0' + (char) ( s - (s/10)*10 );
    str[8] = '\0';
}

unsigned int PIT::counter () {
    unsigned int hi = Peripherals::read32(PIT::ADDR::HIGH);
    unsigned int lo = Peripherals::read32(PIT::ADDR::LOW);
    // repeat, if high word changed during read
    if (hi != Peripherals::read32(PIT::ADDR::HIGH)) {
        hi = Peripherals::read32(PIT::ADDR::HIGH);
        lo = Peripherals::read32(PIT::ADDR::LOW);
    }
    //return ((unsigned long) hi << 32) | lo;
    return lo;
}

unsigned int PIT::comparator () {
    return Peripherals::read32(PIT::CONFIG::COMPAREADDR);
} 

void PIT::comparatorSet (unsigned int val) {
    Peripherals::write32(PIT::CONFIG::COMPAREADDR, val);
}

void PIT::statusClear () {
    Peripherals::write32(PIT::ADDR::STATUS, 1 << PIT::CONFIG::MASKBIT);
}

void PIT::plugin () {
    intervalSet(CONFIG::INTERVALL);
    _id = Dispatcher::assign(PIT::CONFIG::SPI, me());
    statusClear();
}

void PIT::trigger () {
    // all 10ms, systemtime increment
    OS::systime++;
    Scheduler::switchover();

    unsigned int nextval = Peripherals::read32(PIT::ADDR::LOW) + _timerInterval;
    comparatorSet(nextval);
    statusClear();
}