#ifndef __VESA_HPP__
#define __VESA_HPP__ 1

#include <cstdint> // the uintX_t stuff is cool

#include "libs/Font.hpp"
#include "libs/fonts/Font_acorn_8x8.hpp"
#include "libs/Graphics.hpp"
#include "libs/Byte.hpp"

/**
 * VESA Driver.
 * This Singelton class is the VESA driver for the Linear Frame Buffer Device.
 * Maybe "Vesa" is a kind of wrong, because other call similar fb or Pi VideoCore/VPU 
 * The CGA stringStream stuff is not implemented. You can use the Fluent
 * Interface for print:
 * 
 * ~~~~~~~~~~~~~{.cpp}
 * VESA::me()
 *    .print("hallo")
 *    .print(val, 16)
 *    .print("\n");
 * ~~~~~~~~~~~~~
 * 
 */
class VESA : public Graphics {

public:
    /// makes it easier to map 0-15 to the RGB enumeration
    static const int _col[16];
    
    /// Singelton: get the VESA instance.
    static VESA & me() {
        static VESA me;
        return me;
    }

    /// a special cursor setter
    void left ();
    /// a special cursor setter
    void right ();
    /// a special cursor setter
    void up ();
    /// a special cursor setter
    void down ();
    /// a special cursor setter and print
    void del ();

    /**
     * set text Color via VESA::RGB.
     * @param col foreground color
     * @param bg optionaly background color
     */
    void setColor (int col, int bg = -1);
    void getColor (int & col, int & bg);
    
    /**
     * set text Color.
     * @param r red (0-255)
     * @param g green (0-255)
     * @param b blue (0-255)
     * @param isBackground optionaly. set it to true, if the RGB should be a new defalut Background color.
     */
    void setColor (int r, int g, int b, bool isBackground = false);
    /// set the new default Font.
    void setFont (Font & fnt);
    Font & getFont (void);

    /// print a 64bit value
    VESA & print (unsigned long long z, int base = 16, bool without0x = false);
    /// print a 32bit value
    VESA & print (unsigned long z, int base = 16, bool without0x = false);
    /// print a number
    VESA & print (unsigned int z, int base = 10, bool without0x = false);
    /// print a number. if base = 0, then it is printed as double (with 2 digits: 123.00)
    VESA & print (long z, int base = 10, bool without0x = false);
    /// print a double
    VESA & dblOut (double z, uint8_t digits = 2);
    /// print 'yes' or 'no'
    VESA & print (bool bo);
    /// print a single char
    VESA & print (char str);
    /// print a single char
    VESA & print (unsigned char str);
    /**
     * print a char array ('\0' is the end).
     */
    VESA & print (const char * str);
    /**
     * print a char array ('\0' is the end).
     * @param x set a new column and get the new column after printing the chars
     * @param y set a new row and get the new row after printing the chars
     * @param str the char array ('\0' is the end)
     */
    VESA & print (int & x, int & y, const char * str);

    /**
     * print a number. the cursor is not touched.
     * @param x set the column
     * @param y set the row
     * @param number the integer value
     * @param base can be 8, 10(default) or 16
     */
    VESA & print (int x, int y, int number, int base = 10);
    /**
     * print a character. the cursor is not touched.
     * @param x set the column
     * @param y set the row
     * @param c the character
     */
    VESA & print (int x, int y, char c);

    /**
     * set cursor position.
     * use -1 if you do not touch the value.
     * @param x is the new column (start with 0)
     * @param y is the new row (start with 0)
     * @param xAlign with this value you can align Text to a column. Every new line will set the cursor to xAlign
     */
    void setpos (int x, int y, int xAlign = 0);

    /**
     * get the cursor position.
     * @param x refrence to the column of the cursor
     * @param y refrence to the row of the cursor
     */
    void getpos (int & x, int & y);

    /// get max number of text lines. similar to cga: it uses _fnt size to calc the rows
    int rows ();
    /// get max number of chars per line. similar to cga: it uses _fnt size to calculate it
    int columns ();

    /// set the a attribute for print()
    void setAttribute (byte attr);

    /**
     * set a framebuffer size and color resolution
     */
    void init (int width=640, int height=480, int colres=32);
    
    /**
     * Modify y offset of the virtual screen.
     * The virtal screen is 2x bigger then _yres.
     * @todo implement a text scroll with it!
     * @param y add +/- position of virtual screen on the hardware screen
     * @param add if false (= not default) the y parameter is set as offset (not added) 
     */
    void offset (int y, bool add = true);
    
private:
    Font * _fnt;

    int  _textCol;
    int  _textColBg;
    /// cursor position (column = ?)
    int _posX;
    /// cursor position (row = ?)
    int _posY;

    VESA (const VESA &) = default;
    
    VESA () : _posX(0), _posY(0) {
        static Font font_8x8;
        font_8x8.set(fontdata_acorn_8x8, 8, 8);
        setFont(font_8x8);
        setColor(RGB::lightgray, RGB::black);
        init();
    }
    
    void operator= (VESA const &) {}

};

#endif
