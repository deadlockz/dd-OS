#ifndef __Serial_HPP__
#define __Serial_HPP__ 1

class Serial {

public:
    virtual void write (const char *buffer) = 0;
    virtual void write (unsigned char ch) = 0;
    virtual unsigned char read () = 0;
    virtual bool readReady () = 0;
    virtual bool writeReady () = 0;
    
    /// dummy. only important for miniuart1 with software buffer.
    virtual void loadOutputFifo () {}

    virtual ~Serial () {}

protected:
    Serial () {}

private:
    Serial (const Serial &) = delete;
    void operator= (Serial const &) = delete;
};

#endif
