#ifndef __Temperature_HPP__
#define __Temperature_HPP__ 1

namespace Temperature {
    
    /**
     * get the system temperature in celsius
     */
    double get ();
}

#endif
