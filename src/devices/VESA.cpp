#include <cstdint> // the uintX_t stuff is cool

#include "devices/VESA.hpp"
#include "devices/GpuMbox.hpp"
#include "devices/Peripherals.hpp"

#include "libs/Graphics.hpp" // for the RGB:: stuff!

#include "libs/Font.hpp"
#include "libs/CharTool.hpp"
#include "libs/Byte.hpp"

const int VESA::_col[16] = {
    RGB::black,     RGB::blue,      RGB::green,         RGB::cyan, 
    RGB::red,       RGB::violett,   RGB::brown,         RGB::lightgray, 
    /// .. and this is vor CGA-a-like background colors:
    RGB::gray,      RGB::lightblue, RGB::lightgreen,    RGB::lightcyan,
    RGB::lightred,  RGB::pink,      RGB::yellow,        RGB::white
};

void VESA::init (int width, int height, int colres) {
    _yoffset = 0;
    GpuMbox::_callBuffer[0] = 35*4; // Length of message in bytes
    GpuMbox::_callBuffer[1] = GpuMbox::FLAG::REQUEST;

    GpuMbox::_callBuffer[2] = GpuMbox::TAG::SETPHYWH; // Tag identifier
    GpuMbox::_callBuffer[3] = 8; // Value size in bytes for the request
    GpuMbox::_callBuffer[4] = 8; // Value size in bytes for the response
    GpuMbox::_callBuffer[5] = width;
    GpuMbox::_callBuffer[6] = height; 

    /// @todo how to use it like in pi3 docu to scroll screen?
    GpuMbox::_callBuffer[7]  = GpuMbox::TAG::SETVIRTWH;
    GpuMbox::_callBuffer[8]  = 8;
    GpuMbox::_callBuffer[9]  = 8;
    GpuMbox::_callBuffer[10] = width;
    GpuMbox::_callBuffer[11] = height*2;// height x 2 -> try to implement a scroll here

    GpuMbox::_callBuffer[12] = GpuMbox::TAG::SETVIRTOFF;
    GpuMbox::_callBuffer[13] = 8;
    GpuMbox::_callBuffer[14] = 8;
    GpuMbox::_callBuffer[15] = 0; // Value(x)
    GpuMbox::_callBuffer[16] = 0; // Value(y)

    GpuMbox::_callBuffer[17] = GpuMbox::TAG::SETDEPTH;
    GpuMbox::_callBuffer[18] = 4;
    GpuMbox::_callBuffer[19] = 4;
    GpuMbox::_callBuffer[20] = colres; // Bits per pixel

    GpuMbox::_callBuffer[21] = GpuMbox::TAG::SETPXLORDR;
    GpuMbox::_callBuffer[22] = 4;
    GpuMbox::_callBuffer[23] = 4;
    GpuMbox::_callBuffer[24] = 1; // RGB
    
    GpuMbox::_callBuffer[25] = GpuMbox::TAG::GETFB;
    GpuMbox::_callBuffer[26] = 8;
    GpuMbox::_callBuffer[27] = 4;
    GpuMbox::_callBuffer[28] = 4096; /// alignment parameter
    GpuMbox::_callBuffer[29] = 0;    /// result FrameBuffer base address

    // seams to not working that way. we did it in code, now!
    GpuMbox::_callBuffer[30] = GpuMbox::TAG::SETALPHA;
    GpuMbox::_callBuffer[31] = 4;
    GpuMbox::_callBuffer[32] = 4;
    GpuMbox::_callBuffer[33] = 0; // A-byte FF = transparent

    GpuMbox::_callBuffer[34] = GpuMbox::TAG::LAST;
    
    // 28-bit address (MSB) and 4-bit value (LSB)
    unsigned int reqBuffer = ((unsigned int)((long) &(GpuMbox::_callBuffer)) &~ 0xF) | (GpuMbox::CHANNEL::PROP & 0xF);

    // Wait until we can write
    while (Peripherals::read32(GpuMbox::ADDR::STATUS) & GpuMbox::FLAG::FULL);
    
    // Write the address of our buffer to the mailbox with the channel appended
    Peripherals::write32(GpuMbox::ADDR::WRITE, reqBuffer);

    while (1) {
        // Is there a reply?
        while (Peripherals::read32(GpuMbox::ADDR::STATUS) & GpuMbox::FLAG::EMPTY);

        // Is it a reply to our message?
        if (reqBuffer == Peripherals::read32(GpuMbox::ADDR::READ)) {
            // Is it successful?
            if (GpuMbox::_callBuffer[1] == GpuMbox::FLAG::RESPONSE) {
                GpuMbox::_callBuffer[28] &= 0x3FFFFFFF;
                _xres = GpuMbox::_callBuffer[5];
                _yres = GpuMbox::_callBuffer[6];
                _bpp = GpuMbox::_callBuffer[20];
                _lfb = (unsigned char *) ( (long) GpuMbox::_callBuffer[28] );
            }
            break;
        }
    }
}

// just a workaround to test
void VESA::offset (int y, bool add) {
    if (add == true) {
        _yoffset += y;
    } else {
        _yoffset = y;
    }
    if (_yoffset < 0) _yoffset = 0;
    if (_yoffset > _yres) {
        // that is not nice: if your scroll to the end, you got a fresh black screen
        _yoffset = 0;
        clear();
    }
    GpuMbox::_callBuffer[0] = 8*4; // Length of message in bytes
    GpuMbox::_callBuffer[1] = GpuMbox::FLAG::REQUEST;
    GpuMbox::_callBuffer[2] = GpuMbox::TAG::SETVIRTOFF;
    GpuMbox::_callBuffer[3] = 8;
    GpuMbox::_callBuffer[4] = 8;
    GpuMbox::_callBuffer[5] = 0;        // Value(x)
    GpuMbox::_callBuffer[6] = _yoffset; // Value(y)
    GpuMbox::_callBuffer[7] = GpuMbox::TAG::LAST;
    GpuMbox::call(GpuMbox::CHANNEL::PROP);
}

void VESA::setColor (int r, int g, int b, bool isBackground) {
    if (isBackground) {
        _textColBg = RGB_24(r,g,b);
    } else {
        _textCol = RGB_24(r,g,b);
    }
}

void VESA::setColor (int col, int bg) {
    _textCol = col;

    if (bg >= 0) {
        _textColBg = bg;
    }
}

void VESA::getColor (int & col, int & bg) {
    col = _textCol;
    bg = _textColBg;
}

void VESA::setFont (Font & fntReference) {
    _fnt = &fntReference; // convert to pointer
}

Font & VESA::getFont (void) {
    return *_fnt;
}

VESA & VESA::print (unsigned long z, int base, bool without0x) {
    print((unsigned long long) z, base, without0x);
    return *this;
}

VESA & VESA::print (unsigned long long z, int base, bool without0x) {
    char buffer[26];
    int pos = CharTool::bit64ToStr(z, buffer, base, without0x);
    buffer[pos] = '\0';
    print(buffer);
    return *this;
}

VESA & VESA::print (unsigned int z, int base, bool without0x) {
    char buffer[26];
    int pos = CharTool::bit64ToStr(z, buffer, base, without0x);
    buffer[pos] = '\0';
    print(buffer);
    return *this;
}

VESA & VESA::print (long z, int base, bool without0x) {
    char buffer[50];
    int pos;
    if (base == 0) {
        pos = CharTool::doubleToStr((double)z, buffer, 2);
    } else {
        if (base==16) {
            pos = CharTool::bit64ToStr(z, buffer, base, without0x);
        } else {
            pos = CharTool::intToStr(z, buffer, base);
        }
    }
    buffer[pos] = '\0';
    print(buffer);
    return *this;
}

VESA & VESA::dblOut (double z, uint8_t digits) {
    char buffer[50];
    int pos = CharTool::doubleToStr(z, buffer, digits);
    buffer[pos] = '\0';
    print(buffer);
    return *this;
}

VESA & VESA::print (bool bo) {
    if (bo) {
        char buf[] = {'y','e','s','\0'};
        return print(buf);
    }
    char buf[] = {'n','o','\0'};
    return print(buf);
}

VESA & VESA::print (char str) {
    char buf[] = {str, '\0'};
    return print(buf);
}

VESA & VESA::print (unsigned char str) {
    char buf[] = {str, '\0'};
    return print(buf);
}

VESA & VESA::print (const char * str) {
    print(_posX, _posY, str);
    return *this;
}

int VESA::rows () {
    return (_yres/_fnt->height());
}

int VESA::columns () {
    return (_xres/_fnt->width());
}

VESA & VESA::print (int x, int y, int number, int base) {
    char buffer[26];
    int pos = CharTool::intToStr(number, buffer, base);
    buffer[pos] = '\0';
    print(x, y, buffer);
    return *this;
}

VESA & VESA::print (int & x, int & y, const char * str) {
    int len = CharTool::len(str);
    x = _fnt->width() * x;
    y = _fnt->height() * y;
    drawString(*_fnt, x,  y,  _textCol, str, len, _textColBg);
    x =  x / _fnt->width();
    y =  y / _fnt->height();
    return *this;
}

VESA & VESA::print (int x, int y, char c) {
    char buf[] = {c, '\0'};
    return print(x, y, buf);
}

void VESA::getpos (int & x, int & y) {
    x = _posX;
    y = _posY;
}

void VESA::setpos (int x, int y, int xAlign) {
    if (xAlign >= 0) _xAlign = _fnt->width() * xAlign;
    if (x >= 0) _posX = x;
    if (y >= 0) _posY = y;
}

void VESA::setAttribute (byte attr) {
    byte dummy = attr << 4;
    dummy = dummy >> 4;
    _textCol   = _col[dummy];

    dummy = attr >> 4;
    _textColBg = _col[dummy];
}

void VESA::left () {
    int x,y;
    getpos(x,y);
    x = _fnt->width() * x;
    y = _fnt->height() * y;

    x -= _fnt->width();
    if (x < _xAlign) {
        x = _xres - _fnt->width();
        y -= _fnt->height();
    }
    x =  x / _fnt->width();
    y =  y / _fnt->height();
    if(y >= 0) setpos(x,y,_xAlign);
}

void VESA::right () {
    int x,y;
    getpos(x,y);
    x = _fnt->width() * x;
    y = _fnt->height() * y;

    x += _fnt->width();
    if (x > _xres) {
        x = _xAlign;
        y += _fnt->height();
    }
    if (y > _yres) {
        y -= _fnt->height();
        //scrollup();
    }
    x =  x / _fnt->width();
    y =  y / _fnt->height();
    setpos(x,y,_xAlign);
}

void VESA::up () {
    int x,y;
    getpos(x,y);
    x = _fnt->width() * x;
    y = _fnt->height() * y;

    y -= _fnt->height();

    x =  x / _fnt->width();
    y =  y / _fnt->height();
    if(y >= 0) setpos(x,y,_xAlign);
}

void VESA::down () {
    int x,y;
    getpos(x,y);
    x = _fnt->width() * x;
    y = _fnt->height() * y;

    y += _fnt->height();
    if (y > _yres) {
        y -= _fnt->height();
        //scrollup();
    }
    x =  x / _fnt->width();
    y =  y / _fnt->height();
    setpos(x,y,_xAlign);
}

void VESA::del () {
    int x,y;
    left();
    getpos(x,y);
    char buf[] = {' ', '\0'};
    print(x, y, buf);
}
