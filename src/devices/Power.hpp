#ifndef __POWER_HPP__
#define __POWER_HPP__ 1

#include "devices/Peripherals.hpp"

/** doing reset or power of.
 * the relevant Pi4 or Pi3 registers are not documented yet.
 * Some code parts coming from github raspi3-tutorial without
 * deep explainations. some other hints coming from the cicle lib.
 */
namespace Power {
    
    enum ADDR {
        BASE = Peripherals::ADDR::POWER,
        RESET_CTR   = BASE + 0x1c,
        RESET_SETUP = BASE + 0x20,
        WATCHDOG    = BASE + 0x24
    };
    
    enum CONFIG {
        GPU_Devices = 16,
        WDOG_PASSWD = 0x5a000000,
        FULL_RESET  = 0x00000020
    };
    
    void poweroff ();
    void reset ();
}

#endif
