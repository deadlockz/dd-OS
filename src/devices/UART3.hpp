#ifndef __UART3_HPP__
#define __UART3_HPP__ 1

#include "devices/Serial.hpp"
#include "devices/Peripherals.hpp"

/// hardware optimal UART on GPIO Pins 4 and 5

class UART3 : public Serial {

public:

    enum REGISTER {
        BASE = Peripherals::ADDR::UART3,
        DATA                        = BASE,
        FLAG                        = BASE + 0x18,
        INTEGER_BAUD_RATE_DIVISOR   = BASE + 0x24,
        FRACTAL_BAUD_RATE_DIVISOR   = BASE + 0x28,
        LINE_CONTROL                = BASE + 0x2c,
        CONTROL                     = BASE + 0x30,
        IRQ_CLEAR                   = BASE + 0x44
    };
    
    enum CONFIG {
        CLOCK = 3000000,
        PINA = 4,
        PINB = 5
    };

    void write (const char *buffer);
    void write (unsigned char ch);
    unsigned char read ();
    bool readReady ();
    bool writeReady ();

    /// This Serial connection is a singleton
    static UART3 & me () {
        static UART3 inst;
        return inst;
    }

private:

    /// singleton = constructor is private
    UART3 ();
    UART3 (const UART3 &) = delete;
    void operator= (UART3 const &) = delete;
};

#endif
