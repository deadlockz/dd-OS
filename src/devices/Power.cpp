#include "devices/Power.hpp"
#include "devices/Peripherals.hpp"
#include "devices/GpuMbox.hpp"
#include "devices/CPU.hpp"
#include "devices/Gpio.hpp"

void Power::poweroff () {
    // all gpio pins off
    for(unsigned int pin = 0; pin <= Gpio::CONFIG::MAX_PIN; ++pin) {
        Gpio::useOutput(pin, Gpio::PULL::NONE);
        Gpio::setPin(pin, false);
    }

    // power off peripherals
    for(unsigned int devid = 0; devid < Power::CONFIG::GPU_Devices; ++devid) {
        GpuMbox::_callBuffer[0] = 8*4;
        GpuMbox::_callBuffer[1] = GpuMbox::FLAG::REQUEST;
        GpuMbox::_callBuffer[2] = GpuMbox::TAG::SETPOWER; // Tag identifier
        GpuMbox::_callBuffer[3] = 8;
        GpuMbox::_callBuffer[4] = 8;
        GpuMbox::_callBuffer[5] = devid;    // device id
        GpuMbox::_callBuffer[6] = 0;        // bit 0: off, bit 1: no wait
        GpuMbox::_callBuffer[7] = GpuMbox::TAG::LAST;
        GpuMbox::call(GpuMbox::CHANNEL::PROP);
    }
    
    unsigned int r = Peripherals::read32(Power::ADDR::RESET_SETUP);
    
    // power off the SoC (GPU + CPU)
    r &= ~0xfffffaaa;
    // partition 63 used to indicate halt (?!?)
    r |= 0x555;
    Peripherals::write32(
        Power::ADDR::RESET_SETUP,
        Power::CONFIG::WDOG_PASSWD | r
    );
    Peripherals::write32(
        Power::ADDR::WATCHDOG,
        Power::CONFIG::WDOG_PASSWD | 10
    );
    Peripherals::write32(
        Power::ADDR::RESET_CTR, 
        Power::CONFIG::WDOG_PASSWD | Power::CONFIG::FULL_RESET
    );
    
    CPU::me().halt();
}

void Power::reset () {
    unsigned int r = Peripherals::read32(Power::ADDR::RESET_SETUP);
    
    // trigger a restart by instructing the GPU to boot from partition 0
    r &= ~0xfffffaaa;
    Peripherals::write32(
        Power::ADDR::RESET_SETUP,
        Power::CONFIG::WDOG_PASSWD | r
    );
    Peripherals::write32(
        Power::ADDR::WATCHDOG,
        Power::CONFIG::WDOG_PASSWD | 10
    );
    Peripherals::write32(
        Power::ADDR::RESET_CTR, 
        Power::CONFIG::WDOG_PASSWD | Power::CONFIG::FULL_RESET
    );
}
