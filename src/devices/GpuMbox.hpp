#ifndef __GPUMBOX_HPP__
#define __GPUMBOX_HPP__ 1

#include "devices/Peripherals.hpp"

namespace GpuMbox {

    enum ADDR {
        BASE = Peripherals::ADDR::GPU_MBOX,
        READ      = (BASE + 0x0),
        POLL      = (BASE + 0x10),
        SENDER    = (BASE + 0x14),
        STATUS    = (BASE + 0x18),
        CONFIG    = (BASE + 0x1C),
        WRITE     = (BASE + 0x20)
    };
    
    enum CHANNEL {
        POWER  = 0, ///< unused!
        FB     = 1,
        VUART  = 2,
        VCHIQ  = 3,
        LEDS   = 4,
        BTNS   = 5,
        TOUCH  = 6,
        COUNT  = 7,
        PROP   = 8, ///< Request from ARM for response by VideoCore
        PROP2  = 9  ///< Request from VideoCore for response by ARM (undefined)
    };
    
    enum FLAG {
        RESPONSE  = 0x80000000,
        FULL      = 0x80000000,
        EMPTY     = 0x40000000,
        REQUEST   = 0
    };

    enum TAG {
        REVISION   = 0x010002,
        GETARM_MEM = 0x010005,
        
        SETPOWER   = 0x028001,
        GET_TEMP   = 0x030006,
        SETCLK     = 0x038002,
        GETCLK     = 0x030002,
        MESCLK     = 0x030047,
        XHCI_RESET = 0x030058,
        
        SETPHYWH   = 0x048003,
        SETVIRTWH  = 0x048004,
        SETVIRTOFF = 0x048009,
        SETDEPTH   = 0x048005,
        SETPXLORDR = 0x048006,
        SETALPHA   = 0x048007, ///< Alpha Mode 0,1,2: 00 is opaque, transparent, ignored

        GETFB      = 0x040001,
        GETPITCH   = 0x040008,
        LAST = 0
    };

    /**
     * The buffer must be 16-byte aligned as only the upper 28
     * bits of the address can be passed via the mailbox to the GPU
     */
    extern volatile unsigned int _callBuffer[40];

    bool call (unsigned char channel);

}

#endif
