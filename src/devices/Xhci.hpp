#ifndef __XHCI_HPP__
#define __XHCI_HPP__ 1

#include "exceptions/ServiceRoutine.hpp"
#include "devices/PCIe.hpp"
#include <cstdint> // the uintX_t stuff is cool

/**
 * XHCI (USB) needs PCIe.
 * MMIO mapping via in/outbound mem in PCIe does not work correctly.
 * Maybe virtual memory is a must-have in pcie config/setup but
 * we do not have -> thus 0x6_0000_0000 address access is not
 * possible (?)
 * 
 * @todo implement & debug pcie and xhci
 */

class Xhci : public ServiceRoutine {

private:

    /// it is private to avoid copying.
    Xhci (const Xhci &copy) = delete;
    Xhci &operator= (const Xhci &copy) = delete;
    Xhci ();
    
    /// send GPU xhci reset message to load VL805 firmware after PCIe reset
    /// https://github.com/raspberrypi/linux/commit/1c082fa78a09436ff22f30bfc82b9b7a127686d5
    void reset (void);
    
public:

// Port Configuration (from Extended Capabilities "Protocol")
// ??????
// USB2: Port 1, HSO=1, IHI=1, HLC=0, PSIC=1 (PSIV=3, PSIE=2, PLT=0, PFD=0, PSIM=480)
// USB3: Port 2-5, PSIC=1 (PSIV=4, PSIE=3, PLT=0, PFD=1, PSIM=5)

    enum ADDR {
        //BASE            = PCIe::ADDR::START_VIRTUAL,
        BASE            = PCIe::ADDR::START,
        CAP_CAPLENGTH   = BASE + 0x00,
        CAP_HCIVERSION  = BASE + 0x02,
        END             = (BASE + 0x0FFF)
    };
    
    enum CONFIG {
        MSI_SLOT    = 0,
        PCIE_FUNC   = 0,
        PCIE_BUS    = 1,
        USB_PORTS   = 5,
        USB_SLOTS   = 32,
        /**
         * 0c = Class ID
         * 03 = Subclass ID
         * 30 = Interface Value
         */
        CLASS_CODE  = 0x0C0330
    };
    
    /// Singelton: get the instance.
    static Xhci * me () {
        static Xhci me;
        return &me;
    }
    
    void scanPorts ();
    
    /**
     * plugin.
     * this adds the ServiceRoutine to a Dispatching System
     */
    void plugin ();
    
    /** trigger.
     * A Dispatcher uses this trigger to handle assigned events.
     */
    void trigger ();
    
};

#endif
