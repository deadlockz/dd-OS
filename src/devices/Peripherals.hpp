#ifndef __PERIPHERALS_HPP__
#define __PERIPHERALS_HPP__ 1
#include <cstdint> // the uintX_t stuff is cool

/**
 * many peripheral information comes from BCM2835 ARM Peripherals Doc (Feb. 2012).
 * 
 * The Pi4 35bit high peripheral mode (mid 2020) is still experimental and needs
 * more fine tuning in armstubv8 and devicetree!
 */
namespace Peripherals {
    
    enum ADDR {
        PCIe        =  0xFD500000,      ///< "unused": PCIe low base address
        //PCIe      = 0x47D500000,      ///< PCIe base address (35bit)
        ETH         = PCIe + 0x80000,   ///< unused: BCM54213PE Gigabit Ethernet Transceiver

        //BASE      =  0x20000000,       ///< Pi1
        //BASE      =  0x3F000000,       ///< Pi2 + 3
        BASE        =  0xFE000000,       ///< Pi4 low peripheral
        //BASE      = 0x47E000000,       ///< Pi4 low peripheral (35bit)
        LOCAL       =  0xFF800000,       ///< ARM local low peripheral
        //LOCAL     = 0x4C0000000,       ///< ARM local peripheral (35bit)
        
        /// System Timer (Not ARMC Timer-Interrupts), https://github.com/Chadderz121/csud/blob/master/source/platform/arm/broadcom2835.c
        PIT         = BASE +   0x3000,
        
        /** unused: old Pi1..3 Legacy Interrupt Controller (not new default GIC-400 on Pi4).
         *  Catches also 64 (SPI ID 96..159) VideoCore Interupts (0..3 = are 4 System Timers).
         */
        LIC         = BASE +   0xB200,
        
        /// unused: ARM Core SP804 alike timers (dependend on power modes and cpu clock speed!) 
        ARMTIMERS   = BASE +   0xB400,
        
        GPU_MBOX    = BASE +   0xB880, ///< VideoCore mailbox
        DMA15       = BASE +  0x05000, ///< reserved by GPU
        DMA         = BASE +  0x07000, 
        POWER       = BASE + 0x100000, ///< Power management
        CM          = BASE + 0x101000, ///< Clock Manager; we need it for PWM (audio) on GPIO pins 40/41
        RND         = BASE + 0x104000, ///< unused: Random Number Generator
        GPIO        = BASE + 0x200000, ///< GPIO controller
        UART0       = BASE + 0x201000, ///< UART0 (GPIO 14,15 serial port, PL011)
        UART3       = BASE + 0x201600, ///< UART3 (GPIO 4,5 serial port, PL011)
        UART1       = BASE + 0x215000, ///< UART1 (GPIO 14,15 serial port, AUX mini UART)
        EXTSTORE    = BASE + 0x300000, ///< unused: External Mass Media Controller (SD card reader)
        USB         = BASE + 0x980000, ///< unused: internal Universal Serial Bus controller (OTG via Power plug)
        
        GIC         = LOCAL + 0x40000  ///< This is a ARM local adress to GIC-400
    };

    void write32 (uint64_t addr, uint32_t val);
    uint32_t read32 (uint64_t reg);
    void writeTo (uint64_t addr, uint32_t mask, uint32_t shift, uint32_t val);

    uint16_t read16 (uint64_t addr);
    void write16 (uint64_t addr, uint16_t val);
    
    uint8_t read8 (uint64_t addr);
    void write8 (uint64_t addr, uint8_t val);

}

#endif
