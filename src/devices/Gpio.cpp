#include <cstdint> // the uintX_t stuff is cool
#include "devices/Gpio.hpp"
#include "devices/Peripherals.hpp"

unsigned int Gpio::call (
    unsigned int pin, 
    unsigned int value, 
    uint64_t base,
    unsigned int field_size,
    unsigned int field_max
) {
    unsigned int field_mask = (1 << field_size) - 1;
  
    if (pin > field_max) return 0;
    if (value > field_mask) return 0; 

    unsigned int num_fields = 32 / field_size;
    unsigned int reg = base + ((pin / num_fields) * 4);
    unsigned int shift = (pin % num_fields) * field_size;

    unsigned int curval = Peripherals::read32(reg);
    curval &= ~(field_mask << shift);
    curval |= value << shift;
    Peripherals::write32(reg, curval);

    return 1;
}

unsigned int Gpio::set (unsigned int pin, unsigned int value) {
    return Gpio::call(pin, value, Gpio::ADDR::GPSET0, 1, Gpio::CONFIG::MAX_PIN);
}

unsigned int Gpio::clear (unsigned int pin, unsigned int value) {
    return Gpio::call(pin, value, Gpio::ADDR::GPCLR0, 1, Gpio::CONFIG::MAX_PIN);
}

unsigned int Gpio::pull (unsigned int pin, unsigned int value) {
    return Gpio::call(pin, value, Gpio::ADDR::GPPUPPDN0, 2, Gpio::CONFIG::MAX_PIN);
}

unsigned int Gpio::function (unsigned int pin, unsigned int value) {
    return Gpio::call(pin, value, Gpio::ADDR::GPFSEL0, 3, Gpio::CONFIG::MAX_PIN);
}

void Gpio::useAlt0 (unsigned int pin) {
    Gpio::pull(pin, Gpio::PULL::NONE);
    Gpio::function(pin, Gpio::CONFIG::FUNCTION_ALT0);
}

void Gpio::useAlt1 (unsigned int pin) {
    Gpio::pull(pin, Gpio::PULL::NONE);
    Gpio::function(pin, Gpio::CONFIG::FUNCTION_ALT1);
}

void Gpio::useAlt2 (unsigned int pin) {
    Gpio::pull(pin, Gpio::PULL::NONE);
    Gpio::function(pin, Gpio::CONFIG::FUNCTION_ALT2);
}

void Gpio::useAlt3 (unsigned int pin) {
    Gpio::pull(pin, Gpio::PULL::NONE);
    Gpio::function(pin, Gpio::CONFIG::FUNCTION_ALT3);
}

void Gpio::useAlt4 (unsigned int pin) {
    Gpio::pull(pin, Gpio::PULL::NONE);
    Gpio::function(pin, Gpio::CONFIG::FUNCTION_ALT4);
}

void Gpio::useAlt5 (unsigned int pin) {
    Gpio::pull(pin, Gpio::PULL::NONE);
    Gpio::function(pin, Gpio::CONFIG::FUNCTION_ALT5);
}

void Gpio::useOutput (unsigned int pin, unsigned int pull) {
    Gpio::pull(pin, pull);
    Gpio::function(pin, Gpio::CONFIG::FUNCTION_OUT);
}

void Gpio::useInput (unsigned int pin, unsigned int pull) {
    Gpio::pull(pin, pull);
    Gpio::function(pin, Gpio::CONFIG::FUNCTION_IN);
}

void Gpio::setPin (unsigned int pin, bool state) {
    if (state) {
        Gpio::set(pin, 1);
    } else {
        Gpio::clear(pin, 1);
    }
}

bool Gpio::get (unsigned int pin) {
    return getFromRegister(pin, Gpio::ADDR::GPLEV0);
}

void Gpio::setToRegister (unsigned int pin, uint64_t base, bool value) {
    unsigned int curval;
    uint64_t reg = base;
    if (pin >= 32) {
        reg = base + 0x04;
        pin -= 32;
    }
    curval = Peripherals::read32(reg);
    if (value == true) {
        curval |= (1 << pin);
    } else {
        curval &= ~(1 << pin);
    }
    Peripherals::write32(reg, curval);
}
    
bool Gpio::getFromRegister (unsigned int pin, uint64_t base) {
    unsigned int curval;
    if (pin < 32) {
        curval = Peripherals::read32(base);
    } else {
        curval = Peripherals::read32(base + 0x04);
        pin -= 32;
    }
    return (curval & (1<<pin)) > 0;
}


bool Gpio::getStatus (unsigned int pin) {
    return getFromRegister(pin, Gpio::ADDR::EVENT_STATUS0);
}

void Gpio::clearStatus (unsigned int pin) {
    return setToRegister(pin, Gpio::ADDR::EVENT_STATUS0, true);
}

void Gpio::setFalling (unsigned int pin, bool value) {
    return setToRegister(pin, Gpio::ADDR::EVENT_FALL0, value);
}

void Gpio::setRising (unsigned int pin, bool value) {
    return setToRegister(pin, Gpio::ADDR::EVENT_RISE0, value);
}


