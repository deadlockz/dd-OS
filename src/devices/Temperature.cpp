#include "devices/Temperature.hpp"
#include "devices/GpuMbox.hpp"

double Temperature::get () {
    GpuMbox::_callBuffer[0] = 8*4; // Length of message in bytes
    GpuMbox::_callBuffer[1] = GpuMbox::FLAG::REQUEST;
    GpuMbox::_callBuffer[2] = GpuMbox::TAG::GET_TEMP; // Tag identifier
    GpuMbox::_callBuffer[3] = 8; // result buffer size
    GpuMbox::_callBuffer[4] = 8; // ... too (?)
    GpuMbox::_callBuffer[5] = 0; ///< temperature id
    GpuMbox::_callBuffer[6] = 0; ///< return 2 value: size
    GpuMbox::_callBuffer[7] = GpuMbox::TAG::LAST;

    GpuMbox::call(GpuMbox::CHANNEL::PROP);

    return GpuMbox::_callBuffer[6]/1000.0;
}

