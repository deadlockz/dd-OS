#ifndef __PCIE_HPP__
#define __PCIE_HPP__ 1

#include "exceptions/ServiceRoutine.hpp"
#include "devices/Peripherals.hpp"
#include <cstdint> // the uintX_t stuff is cool

/**
 * XHCI (USB) needs PCIe.
 * MMIO mapping via in/outbound mem in PCIe does not work correctly.
 * Maybe virtual memory is a must-have in pcie config/setup but
 * we do not have -> thus 0x6_0000_0000 address access is not
 * possible (?) and a map to an existing memory region was without
 * success (0xF800_0000, 0x8880_0000, 0x80_0000) - in 32 or 64 MEM
 * config!
 * 
 * @todo implement & debug pcie and xhci
 * @todo pcie irq (spi 180) never seen!
 */
class PCIe : public ServiceRoutine {

private:

    /// it is private to avoid copying.
    PCIe (const PCIe &copy) = delete;
    PCIe &operator= (const PCIe &copy) = delete;
    PCIe ();

    bool link_up (void);
    bool enable_bridge (void);
    uint64_t find (uint64_t nPCIConfig, uint8_t uchCapID);
    
    void set_outbound_win (uint64_t cpu_addr, uint64_t pcie_addr, uint64_t size);
    void init_set (unsigned val);
    void set_gen (uint32_t gen);

    int encode_ibar_size (uint64_t size);
    int ilog2 (uint64_t v);

    uint64_t map_conf (unsigned busnr, unsigned devfn, int where);
    int cfg_index (unsigned busnr, unsigned devfn, int reg);
    
public:

    enum ADDR {
        BASE            = Peripherals::ADDR::PCIe,
        EOI_CTRL        = BASE + 0x04060,
        END             = BASE + 0x0930f,
        START           = 0x0F8000000UL,
        SIZE            = 0x004000000UL,    // 67MB
        START_VIRTUAL   = 0x600000000UL
    };
    
    enum CONFIG {
        SLOTS = 32,
        SPI = 180
    };

    struct Device {
        bool isSet;
        uint32_t classCode;
        unsigned func;
        uint8_t queueSize;
        uint64_t msiConf;
        ServiceRoutine * sr;
    };
    
    static Device assignment[PCIe::CONFIG::SLOTS];
    
    /// Singelton: get the instance.
    static PCIe * me () {
        static PCIe me;
        return &me;
    }
    
    uint64_t getDMAAddress (void);

    /**
     * plugin.
     * this adds the ServiceRoutine to the Dispatcher + GIC
     */
    void plugin ();
    
    /**
     * plug-out.
     * this removes the ServiceRoutine from the Dispatcher + GIC
     */
    void plugout ();
    
    /** trigger.
     * The Dispatcher uses this trigger to handle the interrupt
     * of the PCIe-Bridge (SPI 180). Trigger is a other/sub dispatcher
     * for all MSI interrupts comming from PCI devices!
     */
    void trigger ();
    
    /// reset bus, configure, linkup and enable bridge.
    bool setup ();
    
    /// combine trigger and an array of slots with handler
    bool addDevice (uint32_t classCode, unsigned func, unsigned slot, ServiceRoutine * handler);

};

#endif
