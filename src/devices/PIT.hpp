#ifndef __PIT_HPP__
#define __PIT_HPP__ 1

#include "exceptions/ServiceRoutine.hpp"
#include "devices/Peripherals.hpp"

class PIT : public ServiceRoutine {
    
private:
    
    /// it is private to avoid copying.
    PIT (const PIT &copy) = default;
    PIT () = default;
    
    unsigned int _timerInterval;

public:

    enum ADDR {
        BASE = Peripherals::ADDR::PIT,
        STATUS      = BASE,
        LOW         = BASE + 0x04,
        HIGH        = BASE + 0x08,
        Compare0    = BASE + 0x0c, ///< reserved: SPI 96 by GPU?!
        Compare1    = BASE + 0x10, ///< unused
        Compare2    = BASE + 0x14, ///< reserved: IRQ 2 by GPU?!
        Compare3    = BASE + 0x18  ///< GIC-400 SPI ID 99 (= GPU IRQ 3)
    };

    enum CONFIG {
        SPI         = 99,               // 97
        MASKBIT     = 3,                // 1
        INTERVALL   = 10000,
        COMPAREADDR = ADDR::Compare3    // ADDR::Compare1
    };

    /// Singelton: get the PIT instance.
    static PIT * me () {
        static PIT me;
        return &me;
    }

    /**
     * delay.
     * it runns into a loop, until the systime is reached.
     * 
     * @param ms delay in milliseconds
     * @return false, if the delay is to short for the PIT interrupt.
     */
    bool delay (unsigned int ms);
    
    /// getter for the pit intervall
    int interval ();
    
    /**
     * setter for the pit intervall.
     * Take **Attention:** the Thread run time (ps command of the Shell) and printTime are not converted!
     * 
     * @param us new timespan in microseconds between PIT interrupts (eg. for preemptive multitasking)
     * @param usDelay default 1sec. set an different optional first delay to the comparator
     */
    void intervalSet (unsigned int us, unsigned int usDelay=1000000);
    
    /**
     * stamp to time string.
     * it converts a timestamp (+1 every 10ms) to a string (HH:MM:ss).
     * 
     * @param ti ticks of the pit (like OS::systime)
     * @param str an array with at least 9 free char fields
     */
    void stampToTime (unsigned long long ti, char * str);

    /**
     * Get System Counter.
     * 
     * Because only the lower 32Bits are important for the Comparator, this
     * methode just gives you the lower 32Bits.
     */
    unsigned int counter ();

    /**
     * get the compare value, when an IRQ is fired.
     */
    unsigned int comparator ();

    /**
     * set the compare value, when an IRQ is fired.
     * 
     * @param val normaly in 1 us tick = 10000 is 10ms
     */
    void comparatorSet (unsigned int val);
    
    /**
     * clear the Pit event bit.
     */
    void statusClear ();

    /**
     * plugin.
     * this adds the ServiceRoutine to the Dispatcher
     */
    void plugin ();

    /**
     * trigger.
     * The Dispatcher uses this trigger to handle the interrupt
     * of this device. It counts the systime and triggers the Scheduler
     * switch through the tasks / threads.
     * @see Dispatcher.cpp
     */
    void trigger ();
};

#endif
