#ifndef __USB_HPP__
#define __USB_HPP__ 1

#include "devices/Peripherals.hpp"

/**
 * # USB2 internal hints (not PCIe)
 * 
 * Pi4 uses xHCI (via PCIe) and not (?) this dwhci (?) stuff
 * 
- https://rsta2.readthedocs.io/projects/circle/en/latest/subsystems/usb.html
- https://github.com/rsta2/circle/blob/master/include/circle/bcm2711.h
- https://github.com/rsta2/circle/blob/master/include/circle/usb/dwhci.h

- implement stuff? orient: https://github.com/rsta2/circle/blob/master/include/circle/usb/dwhcidevice.h
- implement stuff? orient: https://github.com/Chadderz121/csud
- csud on RTOS and Pi3 64bit: https://github.com/LdB-ECM/Raspberry-Pi

More Details Pi3 64bit (project uses mmu!!)

=> https://github.com/LdB-ECM/Raspberry-Pi/blob/bc38ce183f731891d52a31df87df24904e466d0c/Arm32_64_USB/rpi-usb.c#L648

*/

namespace USB {
    
    enum ADDR {
        BASE = Peripherals::ADDR::USB, // that is not the pcie stuff?
        CORE         = BASE,
        HOST         = BASE + 0x0400,
        POWER        = BASE + 0x0E00,
        
        XHCI_BASE    = BASE + 0x040000,
        XHCI_END     = XHCI_BASE + 0x0FFF
    };

}

#endif
