#include "devices/GpuMbox.hpp"

// The buffer must be 16-byte aligned as only the upper 28
// bits of the address can be passed via the mailbox to the GPU
volatile unsigned int __attribute__((aligned(16))) GpuMbox::_callBuffer[40];

bool GpuMbox::call (unsigned char channel) {
    // 28-bit address (MSB) and 4-bit value (LSB)
    unsigned int reqBuffer = ((unsigned int)((long) &(GpuMbox::_callBuffer)) &~ 0xF) | (channel & 0xF);

    // Wait until we can write
    while (Peripherals::read32(GpuMbox::ADDR::STATUS) & GpuMbox::FLAG::FULL);
    
    // Write the address of our buffer to the mailbox with the channel appended
    Peripherals::write32(GpuMbox::ADDR::WRITE, reqBuffer);

    while (1) {
        // Is there a reply?
        while (Peripherals::read32(GpuMbox::ADDR::STATUS) & GpuMbox::FLAG::EMPTY);

        // Is it a reply to our message?
        if (reqBuffer == Peripherals::read32(GpuMbox::ADDR::READ)) {
            // Is it successful?
            return GpuMbox::_callBuffer[1] == GpuMbox::FLAG::RESPONSE;
        }
    }
    return false;
}