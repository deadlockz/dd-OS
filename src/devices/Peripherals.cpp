#include "devices/Peripherals.hpp"
#include <cstdint> // the uintX_t stuff is cool

void Peripherals::write32 (uint64_t addr, uint32_t val) { 
    *(volatile uint32_t *)addr = val;
}

uint32_t Peripherals::read32 (uint64_t reg) {
    return *(volatile uint32_t *)reg;
}

void Peripherals::writeTo (uint64_t addr, uint32_t mask, uint32_t shift, uint32_t val) {
    uint32_t tmp = Peripherals::read32(addr);
    tmp &= ~mask;
    tmp |= (val << shift) & mask;
    *(volatile uint32_t *)addr = val;
}

void Peripherals::write16 (uint64_t addr, uint16_t val) { 
    *(volatile uint16_t *)addr = val;
}

uint16_t Peripherals::read16 (uint64_t addr) {
    return *(volatile uint16_t *)addr;
}

void Peripherals::write8 (uint64_t addr, uint8_t val) { 
    *(volatile uint8_t *)addr = val;
}

uint8_t Peripherals::read8 (uint64_t addr) {
    return *(volatile uint8_t *)addr;
}