#include <cstdint> // the uintX_t stuff is cool
#include "devices/Clocks.hpp"
#include "devices/GpuMbox.hpp"

uint32_t Clocks::get (uint32_t device) {
    GpuMbox::_callBuffer[0] = 8*4; // Length of message in bytes
    GpuMbox::_callBuffer[1] = GpuMbox::FLAG::REQUEST;
    GpuMbox::_callBuffer[2] = GpuMbox::TAG::GETCLK; // Tag identifier
    GpuMbox::_callBuffer[3] = 8; // request parameter size
    GpuMbox::_callBuffer[4] = 8; // result buffer size
    GpuMbox::_callBuffer[5] = device;   // request parameter
    GpuMbox::_callBuffer[6] = 0;        // return 2 value (Hz)
    GpuMbox::_callBuffer[7] = GpuMbox::TAG::LAST;
    GpuMbox::call(GpuMbox::CHANNEL::PROP);
    uint32_t clk = GpuMbox::_callBuffer[6];
    return clk;
}

uint32_t Clocks::getMessured (uint32_t device) {
    GpuMbox::_callBuffer[0] = 8*4; // Length of message in bytes
    GpuMbox::_callBuffer[1] = GpuMbox::FLAG::REQUEST;
    GpuMbox::_callBuffer[2] = GpuMbox::TAG::MESCLK; // Tag identifier
    GpuMbox::_callBuffer[3] = 8; // request parameter size
    GpuMbox::_callBuffer[4] = 8; // result buffer size
    GpuMbox::_callBuffer[5] = device;   // request parameter
    GpuMbox::_callBuffer[6] = 0;        // return 2 value (Hz)
    GpuMbox::_callBuffer[7] = GpuMbox::TAG::LAST;
    GpuMbox::call(GpuMbox::CHANNEL::PROP);
    uint32_t clk = GpuMbox::_callBuffer[6];
    return clk;
}

void Clocks::set (uint32_t device, uint32_t rate) {
    GpuMbox::_callBuffer[0] = 9*4; // Length of message in bytes
    GpuMbox::_callBuffer[1] = GpuMbox::FLAG::REQUEST;
    GpuMbox::_callBuffer[2] = GpuMbox::TAG::SETCLK; // Tag identifier
    GpuMbox::_callBuffer[3] = 12; // Value size in bytes for the request
    GpuMbox::_callBuffer[4] = 8; // Value size in bytes for the response
    GpuMbox::_callBuffer[5] = device;
    GpuMbox::_callBuffer[6] = rate;
    GpuMbox::_callBuffer[7] = 0; ///< skip, if setting turbo = 0 = false
    GpuMbox::_callBuffer[8] = GpuMbox::TAG::LAST;
    GpuMbox::call(GpuMbox::CHANNEL::PROP);
}

uint32_t Clocks::auxMiniUartReg (uint32_t hz, uint32_t baud) {
    return (hz/(baud*8))-1;
}


