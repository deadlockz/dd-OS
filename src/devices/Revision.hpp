#ifndef __Revision_HPP__
#define __Revision_HPP__ 1

#include <cstdint> // the uintX_t stuff is cool

namespace Revision {
    
    /**
     * get the hardware revision
     */
    uint32_t get ();
}

#endif
