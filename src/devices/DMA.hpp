#ifndef __DMA_HPP__
#define __DMA_HPP__ 1

#include <cstdint> // the uintX_t stuff is cool
#include "devices/Peripherals.hpp"

class DMA {
public:
    
    struct alignas(32) ControlBlock {
       uint32_t transferInfo;   ///< short: TI
       uint32_t sourceAddr;
       uint32_t destAddr;
       uint32_t length;         ///< default: number of 32bit integer values
       uint32_t stride;         ///< @todo stride?
       uint32_t nextBlockAddr;
       uint32_t null1;
       uint32_t null2;
    };

    DMA (uint8_t chan);
    
    uint8_t getChan ();
    void set (ControlBlock * cb);

    void start ();
    void pause ();
    void stop ();
    float progress ();
    bool isActive ();

    enum ADDR {
        /// legacy master addresses. Maybe a BCM2711 bug, because DMA destination needs it
        LEGACY_BASE = 0x7E000000,
        
        BASE = Peripherals::ADDR::DMA,
        CHAN0 = BASE,
        CHAN1 = BASE + 0x0100,
        // ... done via code, now
        CHAN14 = BASE + 0x0E00,
        
        IRQ_STATES = BASE + 0xFE0,
        ENABLE_REG = BASE + 0xFF0
    };
    
    enum OFFSET {
        CS          = 0x00,
        CBLK_ADDR   = 0x04,
        TRANSFERED  = 0x14
    };
    
    enum MASK {
        CS_ACTIVE   = 0x00000001,
        CS_RESET    = (1<<31),
        ENABLE0     = 0x00000001,
        ENABLE1     = 0x00000002,
        // ... done via code, now
        ENABLE14    = 0x00004000,
        TI_WIDTH        = (1<<9),       ///< unused; default is 0 = not set = 32bit datas
        TI_INCREMENT    = (1<<8),       ///< if TI_WIDTH is set, address is increment by 32 and not 4(bytes)
        TI_PERMAP_BITS  = 16,           ///< the 5bits 20:16 give peripheral number 1-32 to let DMA react
        TI_DMA_REQ      = (1<<6)        ///< short: DREQ. If it is set, the DREQ signal of the PERMAP device is use for transfer control.
    };

private:
    uint8_t _chan;
    uint64_t _base;
    
    ControlBlock * _cb;
};

#endif
