#include "devices/UART0.hpp"
#include "devices/Peripherals.hpp"
#include "devices/Gpio.hpp"
#include "devices/Clocks.hpp"

UART0::UART0 (void) {
    /// receive and transmit bits disabled
    Peripherals::write32(UART0::REGISTER::CONTROL, 0);
    
    /// set GPIO Pins 14 and 15 to UART0 function (it removes pullup and down, too)
    Gpio::useAlt0(UART0::CONFIG::PINA);
    Gpio::useAlt0(UART0::CONFIG::PINB);
    
    /// clear pending UART interrupts
    Peripherals::write32(UART0::REGISTER::IRQ_CLEAR, 0x7FF);

    /**
     * send a Mailbox message with set clock rate of PL011 (non mini UART)
     * to 3MHz
     */
    Clocks::set(Clocks::Device::UART_PL011, UART0::CONFIG::CLOCK);
    
    /**
     * @todo do this in code:
     * (PL011_CLOCK / (16 * 115200 baud) = 1.627
     * (0.627*64)+0.5 = 40
     * = int 1, frac 40
     */
    Peripherals::write32(UART0::REGISTER::INTEGER_BAUD_RATE_DIVISOR, 1);
    Peripherals::write32(UART0::REGISTER::FRACTAL_BAUD_RATE_DIVISOR, 40);

    /// Enable FIFO and 8 bit data transmission (1 stop bit, no parity)
    Peripherals::write32(
        UART0::REGISTER::LINE_CONTROL,
        (1 << 6) | (1 << 5) | (1 << 4)
    );
    
    /// receive, transmit and UART0 enabled
    Peripherals::write32(
        UART0::REGISTER::CONTROL,
        (1 << 9) | (1 << 8) | (1 << 0)
    );
}

bool UART0::readReady () { 
    /// if flag TX FF is set, UART FiFo is sending data and can not get
    return (Peripherals::read32(UART0::REGISTER::FLAG) & 0x10) == 0; 
}

bool UART0::writeReady () {
    /// if flag RX FF is set, UART FiFo is getting data and can not send
    return (Peripherals::read32(UART0::REGISTER::FLAG) & 0x20) == 0;
}

unsigned char UART0::read () {
    while (UART0::readReady() == false);
    return (unsigned char) Peripherals::read32(UART0::REGISTER::DATA);
}

void UART0::write (unsigned char ch) {
    while (UART0::writeReady() == false);
    Peripherals::write32(UART0::REGISTER::DATA, (unsigned int)ch);
}

void UART0::write (const char *buffer) {
    while (*buffer) {
       if (*buffer == '\n') UART0::write('\r');
       UART0::write(*buffer++);
    }
}

