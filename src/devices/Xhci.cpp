#include "devices/Xhci.hpp"

#include "threads/Scheduler.hpp"
#include "devices/Peripherals.hpp"
#include "devices/GpuMbox.hpp"
#include "devices/PCIe.hpp"
#include <cstdint> // the uintX_t stuff is cool

#include "devices/VESA.hpp"
#include "exceptions/Calls.hpp" // breakpoint

/// @todo hint to circle license - but this porting is just a minimum of many files!

#define XHCI_REG_OP_PORTS_BASE          0x400
#define XHCI_REG_OP_PORTS_PORTSC        0x00
#define XHCI_REG_OP_PORTS_PORTSC_CCS    (1 << 0)
#define XHCI_REG_OP_PORTS_PORT__SIZE    (4 * 4)
#define XHCI_REG_CAP_HCSPARAMS1         0x04
#define XHCI_REG_CAP_HCSPARAMS1_MAX_PORTS_SHIFT 24
#define XHCI_REG_CAP_HCSPARAMS1_MAX_PORTS_MASK  (0xFF << 24)

Xhci::Xhci () {
    // nothing to do. maybe do pcie init or similar here?
}

void Xhci::reset (void) {
    uint32_t connection = (Xhci::CONFIG::PCIE_BUS << 20) |
        (Xhci::CONFIG::MSI_SLOT << 15) |
        (Xhci::CONFIG::PCIE_FUNC << 12);
    GpuMbox::_callBuffer[0] = 8*4; // Length of message in bytes
    GpuMbox::_callBuffer[1] = GpuMbox::FLAG::REQUEST;
    GpuMbox::_callBuffer[2] = GpuMbox::TAG::XHCI_RESET; // Tag identifier
    GpuMbox::_callBuffer[3] = 8; // result buffer size
    GpuMbox::_callBuffer[4] = 8; // ... too (?)
    GpuMbox::_callBuffer[5] = connection; // parameter
    GpuMbox::_callBuffer[6] = 0; ///< return ?
    GpuMbox::_callBuffer[7] = GpuMbox::TAG::LAST;

    GpuMbox::call(GpuMbox::CHANNEL::PROP);
    Scheduler::snooze();
}

void Xhci::plugin () {
    Scheduler::snooze();

    reset();
    PCIe::me()->addDevice(
        Xhci::CONFIG::CLASS_CODE,
        Xhci::CONFIG::PCIE_FUNC,
        Xhci::CONFIG::MSI_SLOT,
        me()
    );
    
    uint8_t maxPorts = Peripherals::read8(Xhci::ADDR::BASE + 0x07);

    // 1st: 133, next boot: 0
    // => thus it does not work correct and must be 0x05
    VESA::me().print("xHCI max Ports: ").print((long)maxPorts).print("\n");
}

void Xhci::trigger () {
    /// @todo this is for debugging - and never seen!
    VESA::me().print("XHCI MSI handle.\n");
    //breakpoint(666);
}

void Xhci::scanPorts () {
    uint64_t OpBase = Xhci::ADDR::BASE
        + Peripherals::read8(Xhci::ADDR::CAP_CAPLENGTH);
    
    for (int i=0; i < Xhci::CONFIG::USB_PORTS; ++i) {
        uint32_t nPortSC = Peripherals::read32(
            OpBase
            + XHCI_REG_OP_PORTS_BASE
            + i * XHCI_REG_OP_PORTS_PORT__SIZE 
            + XHCI_REG_OP_PORTS_PORTSC
        );

        if (!(nPortSC & XHCI_REG_OP_PORTS_PORTSC_CCS)) {
            VESA::me().print("USB port ").
                print((long)i).
                print(" NOT connected.\n");
        } else {
            VESA::me().print("USB port ").
                print((long)i).
                print(" connected.\n");
        }
    }
}