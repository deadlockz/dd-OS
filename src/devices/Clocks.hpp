#ifndef __Clocks_HPP__
#define __Clocks_HPP__ 1

#include <cstdint> // the uintX_t stuff is cool
#include "devices/Peripherals.hpp"

/**
 * get and set general purpose or other (no)peripherial clocks.
 */
namespace Clocks {

    enum GPIOClock {    // enum coming from circle. These are Clocklines?
        CLK0    = 0,    //        on GPIO4 Alt0 or GPIO20 Alt5
        CLK1    = 1,    // RPi 4: on GPIO5 Alt0 or GPIO21 Alt5
        CLK2    = 2,    //        on GPIO6 Alt0
        CLK_PCM = 5,
        CLK_PWM = 6
    };
    
    enum ADDR {
        BASE = Peripherals::ADDR::CM,
        CLK0_CTL = BASE + 0x070,
        CLK0_DIV = BASE + 0x074,
        // ...
        CLK_PWM_CTL = CLK0_CTL + (Clocks::GPIOClock::CLK_PWM * 8), //BASE + 0x0A0,
        CLK_PWM_DIV = CLK0_DIV + (Clocks::GPIOClock::CLK_PWM * 8) //BASE + 0x0A4
    };
    
    enum CM_CTL {
        PASSWD     = 0x5A000000,
        OSC_SRC    = 0b0001,        ///< 1 = (normal) oscilator
        MASH_BIT   = 9,   ///< 10:9 (2Bits) take a look to Manual (BCM2711 Peripherals!)
        BUSY_BIT   = 7,
        KILL_BIT   = 5,
        ENABLE_BIT = 4,   ///< do not set SRC at same time with enable or busy=1!
        SRC_BIT    = 0    ///< 3:0 (4Bits)
    };
    
    enum CM_DIV {
        INTEGER_BIT = 12,  ///< 23:12 (12Bits)
        FRACT_BIT   = 0    ///< 11:0  (12Bits)
    };
    
    enum Device {
        EMMC        = 0x000000001,
        UART_PL011  = 0x000000002,
        ARM         = 0x000000003,
        CORE        = 0x000000004,
        PWM         = 0x00000000A  ///< an other way (not via CLOCK_MANAGER) to change PWM clocks?
    };
    
    /**
     * Get actual enabled clock rate in Hz
     */
    uint32_t get (uint32_t device) ;
    
    /**
     * Get measured/exact clock rate in Hz
     */
    uint32_t getMessured (uint32_t device) ;
    
    /**
     * Set clock rate in Hz
     */
    void set (uint32_t device, uint32_t rate) ;
    
    uint32_t auxMiniUartReg (uint32_t hz, uint32_t baud) ;
}

#endif
