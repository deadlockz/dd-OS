/*
  * based on: https://github.com/PaulStoffregen/PS2Keyboard/
  * https://github.com/fbergama/pigfx/blob/master/src/ps2.h
  * https://github.com/fbergama/pigfx/blob/master/src/ps2.c


  PS2Keyboard.cpp - PS2Keyboard library
  Copyright (c) 2007 Free Software Foundation.  All right reserved.
  Written by Christian Weichel <info@32leaves.net>

  ** Mostly rewritten Paul Stoffregen <paul@pjrc.com> 2010, 2011
  ** Modified for use beginning with Arduino 13 by L. Abraham Smith, <n3bah@microcompdesign.com> * 
  ** Modified for easy interrup pin assignement on method begin(datapin,irq_pin). Cuningan <cuninganreset@gmail.com> **

  for more information you can read the original wiki in arduino.cc
  at http://www.arduino.cc/playground/Main/PS2Keyboard
  or http://www.pjrc.com/teensy/td_libs_PS2Keyboard.html

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include <cstdint> // the uintX_t stuff is cool
#include "exceptions/Dispatcher.hpp"
#include "devices/PS2Keyboard.hpp"
#include "devices/Gpio.hpp"
#include "devices/PIT.hpp"

/* http://www.quadibloc.com/comp/scan.htm
   http://www.computer-engineering.org/ps2keyboard/scancodes2.html
   These arrays provide a simple key map, to turn scan codes into ISO8859
   output.  If a non-US keyboard is used, these may need to be modified
   for the desired output.
 */
const PS2Keyboard::PS2Keymap PS2Keyboard::Keymap_US = {
  // without shift
    {0, PS2_F9, 0, PS2_F5, PS2_F3, PS2_F1, PS2_F2, PS2_F12,
    0, PS2_F10, PS2_F8, PS2_F6, PS2_F4, PS2_TAB, '`', 0,
    0, 0 /*Lalt*/, 0 /*Lshift*/, 0, 0 /*Lctrl*/, 'q', '1', 0,
    0, 0, 'z', 's', 'a', 'w', '2', 0,
    0, 'c', 'x', 'd', 'e', '4', '3', 0,
    0, ' ', 'v', 'f', 't', 'r', '5', 0,
    0, 'n', 'b', 'h', 'g', 'y', '6', 0,
    0, 0, 'm', 'j', 'u', '7', '8', 0,
    0, ',', 'k', 'i', 'o', '0', '9', 0,
    0, '.', '/', 'l', ';', 'p', '-', 0,
    0, 0, '\'', 0, '[', '=', 0, 0,
    0 /*CapsLock*/, 0 /*Rshift*/, PS2_ENTER /*Enter*/, ']', 0, '\\', 0, 0,
    0, 0, 0, 0, 0, 0, PS2_BACKSPACE, 0,
    0, '1', 0, '4', '7', 0, 0, 0,
    '0', '.', '2', '5', '6', '8', PS2_ESC, 0 /*NumLock*/,
    PS2_F11, '+', '3', '-', '*', '9', PS2_SCROLL, 0,
    0, 0, 0, PS2_F7 },
  // with shift
    {0, PS2_F9, 0, PS2_F5, PS2_F3, PS2_F1, PS2_F2, PS2_F12,
    0, PS2_F10, PS2_F8, PS2_F6, PS2_F4, PS2_TAB, '~', 0,
    0, 0 /*Lalt*/, 0 /*Lshift*/, 0, 0 /*Lctrl*/, 'Q', '!', 0,
    0, 0, 'Z', 'S', 'A', 'W', '@', 0,
    0, 'C', 'X', 'D', 'E', '$', '#', 0,
    0, ' ', 'V', 'F', 'T', 'R', '%', 0,
    0, 'N', 'B', 'H', 'G', 'Y', '^', 0,
    0, 0, 'M', 'J', 'U', '&', '*', 0,
    0, '<', 'K', 'I', 'O', ')', '(', 0,
    0, '>', '?', 'L', ':', 'P', '_', 0,
    0, 0, '"', 0, '{', '+', 0, 0,
    0 /*CapsLock*/, 0 /*Rshift*/, PS2_ENTER /*Enter*/, '}', 0, '|', 0, 0,
    0, 0, 0, 0, 0, 0, PS2_BACKSPACE, 0,
    0, '1', 0, '4', '7', 0, 0, 0,
    '0', '.', '2', '5', '6', '8', PS2_ESC, 0 /*NumLock*/,
    PS2_F11, '+', '3', '-', '*', '9', PS2_SCROLL, 0,
    0, 0, 0, PS2_F7 },
    0,
    {}
};

const PS2Keyboard::PS2Keymap PS2Keyboard::Keymap_German = {
  // without shift
    {0, PS2_F9, 0, PS2_F5, PS2_F3, PS2_F1, PS2_F2, PS2_F12,
    0, PS2_F10, PS2_F8, PS2_F6, PS2_F4, PS2_TAB, '^', 0,
    0, 0 /*Lalt*/, 0 /*Lshift*/, 0, 0 /*Lctrl*/, 'q', '1', 0,
    0, 0, 'y', 's', 'a', 'w', '2', 0,
    0, 'c', 'x', 'd', 'e', '4', '3', 0,
    0, ' ', 'v', 'f', 't', 'r', '5', 0,
    0, 'n', 'b', 'h', 'g', 'z', '6', 0,
    0, 0, 'm', 'j', 'u', '7', '8', 0,
    0, ',', 'k', 'i', 'o', '0', '9', 0,
    0, '.', '-', 'l', PS2_o_DIAERESIS, 'p', PS2_SHARP_S, 0,
    0, 0, PS2_a_DIAERESIS, 0, PS2_u_DIAERESIS, '\'', 0, 0,
    0 /*CapsLock*/, 0 /*Rshift*/, PS2_ENTER /*Enter*/, '+', 0, '#', 0, 0,
    0, '<', 0, 0, 0, 0, PS2_BACKSPACE, 0,
    0, '1', 0, '4', '7', 0, 0, 0,
    '0', '.', '2', '5', '6', '8', PS2_ESC, 0 /*NumLock*/,
    PS2_F11, '+', '3', '-', '*', '9', PS2_SCROLL, 0,
    0, 0, 0, PS2_F7 },
  // with shift
    {0, PS2_F9, 0, PS2_F5, PS2_F3, PS2_F1, PS2_F2, PS2_F12,
    0, PS2_F10, PS2_F8, PS2_F6, PS2_F4, PS2_TAB, PS2_DEGREE_SIGN, 0,
    0, 0 /*Lalt*/, 0 /*Lshift*/, 0, 0 /*Lctrl*/, 'Q', '!', 0,
    0, 0, 'Y', 'S', 'A', 'W', '"', 0,
    0, 'C', 'X', 'D', 'E', '$', PS2_SECTION_SIGN, 0,
    0, ' ', 'V', 'F', 'T', 'R', '%', 0,
    0, 'N', 'B', 'H', 'G', 'Z', '&', 0,
    0, 0, 'M', 'J', 'U', '/', '(', 0,
    0, ';', 'K', 'I', 'O', '=', ')', 0,
    0, ':', '_', 'L', PS2_O_DIAERESIS, 'P', '?', 0,
    0, 0, PS2_A_DIAERESIS, 0, PS2_U_DIAERESIS, '`', 0, 0,
    0 /*CapsLock*/, 0 /*Rshift*/, PS2_ENTER /*Enter*/, '*', 0, '\'', 0, 0,
    0, '>', 0, 0, 0, 0, PS2_BACKSPACE, 0,
    0, '1', 0, '4', '7', 0, 0, 0,
    '0', '.', '2', '5', '6', '8', PS2_ESC, 0 /*NumLock*/,
    PS2_F11, '+', '3', '-', '*', '9', PS2_SCROLL, 0,
    0, 0, 0, PS2_F7 },
    1,
  // with altgr
    {0, PS2_F9, 0, PS2_F5, PS2_F3, PS2_F1, PS2_F2, PS2_F12,
    0, PS2_F10, PS2_F8, PS2_F6, PS2_F4, PS2_TAB, 0, 0,
    0, 0 /*Lalt*/, 0 /*Lshift*/, 0, 0 /*Lctrl*/, '@', 0, 0,
    0, 0, 0, 0, 0, 0, PS2_SUPERSCRIPT_TWO, 0,
    0, 0, 0, 0, PS2_CURRENCY_SIGN, 0, PS2_SUPERSCRIPT_THREE, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, PS2_MICRO_SIGN, 0, 0, '{', '[', 0,
    0, 0, 0, 0, 0, '}', ']', 0,
    0, 0, 0, 0, 0, 0, '\\', 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0 /*CapsLock*/, 0 /*Rshift*/, PS2_ENTER /*Enter*/, '~', 0, '#', 0, 0,
    0, '|', 0, 0, 0, 0, PS2_BACKSPACE, 0,
    0, '1', 0, '4', '7', 0, 0, 0,
    '0', '.', '2', '5', '6', '8', PS2_ESC, 0 /*NumLock*/,
    PS2_F11, '+', '3', '-', '*', '9', PS2_SCROLL, 0,
    0, 0, 0, PS2_F7 }
};

/**
 * trigger.
 * The Dispatcher tries to trigger this ServiceRoutine.
 */
void PS2Keyboard::trigger () {
    if (Gpio::getStatus(PS2Keyboard::CONFIG::IRQPIN) == false) return;

    static uint8_t bitcount=0;
    static uint8_t incoming=0;
    static unsigned long prev_us=0;
    unsigned long now_us;
    uint8_t n, val;

    val = Gpio::get(PS2Keyboard::CONFIG::DATAPIN);
    now_us = PIT::me()->counter();
    
    if (now_us - prev_us > 25000) {
        bitcount = 0;
        incoming = 0;
    }
    prev_us = now_us;
    n = bitcount - 1;
    if (n <= 7) {
        incoming |= (val << n);
    }
    bitcount++;
    if (bitcount == 11) {
        uint8_t i = head + 1;
        if (i >= PS2Keyboard::BUFFER_SIZE) i = 0;
        if (i != tail) {
            buffer[i] = incoming;
            head = i;
        }
        bitcount = 0;
        incoming = 0;
    }
    Gpio::clearStatus(PS2Keyboard::CONFIG::IRQPIN);
}

char PS2Keyboard::getIso8859Code (void) {
    static uint8_t state=0;
    uint8_t s;
    char c;

    while (1) {
        s = readScanCode();
        if (!s) return 0;
        if (s == 0xF0) {
            state |= PS2Keyboard::MOD::BREAK;
        } else if (s == 0xE0) {
            state |= PS2Keyboard::MOD::MODIFIER;
        } else {
            if (state & PS2Keyboard::MOD::BREAK) {
                if (s == 0x12) {
                    state &= ~PS2Keyboard::MOD::SHIFT_L;
                } else if (s == 0x59) {
                    state &= ~PS2Keyboard::MOD::SHIFT_R;
                } else if (s == 0x11 && (state & PS2Keyboard::MOD::MODIFIER)) {
                    state &= ~PS2Keyboard::MOD::ALTGR;
                }
                // CTRL, ALT & WIN keys could be added
                // but is that really worth the overhead?
                state &= ~(PS2Keyboard::MOD::BREAK | PS2Keyboard::MOD::MODIFIER);
                continue;
            }
            if (s == 0x12) {
                state |= PS2Keyboard::MOD::SHIFT_L;
                continue;
            } else if (s == 0x59) {
                state |= PS2Keyboard::MOD::SHIFT_R;
                continue;
            } else if (s == 0x11 && (state & PS2Keyboard::MOD::MODIFIER)) {
                state |= PS2Keyboard::MOD::ALTGR;
            }
            c = 0;
            if (state & PS2Keyboard::MOD::MODIFIER) {
                switch (s) {
                  case 0x70: c = PS2_INSERT;      break;
                  case 0x6C: c = PS2_HOME;        break;
                  case 0x7D: c = PS2_PAGEUP;      break;
                  case 0x71: c = PS2_DELETE;      break;
                  case 0x69: c = PS2_END;         break;
                  case 0x7A: c = PS2_PAGEDOWN;    break;
                  case 0x75: c = PS2_UPARROW;     break;
                  case 0x6B: c = PS2_LEFTARROW;   break;
                  case 0x72: c = PS2_DOWNARROW;   break;
                  case 0x74: c = PS2_RIGHTARROW;  break;
                  case 0x4A: c = '/';             break;
                  case 0x5A: c = PS2_ENTER;       break;
                  default: break;
                }
            } else if ((state & PS2Keyboard::MOD::ALTGR) && keymap->uses_altgr) {
                if (s < PS2Keyboard::KEYMAP_SIZE)
                    c = keymap->altgr[s];
                    
            } else if (state & (PS2Keyboard::MOD::SHIFT_L | PS2Keyboard::MOD::SHIFT_R)) {
                if (s < PS2Keyboard::KEYMAP_SIZE)
                    c = keymap->shift[s];
                    
            } else {
                if (s < PS2Keyboard::KEYMAP_SIZE)
                    c = keymap->noshift[s];
            }
            state &= ~(PS2Keyboard::MOD::BREAK | PS2Keyboard::MOD::MODIFIER);
            if (c) return c;
        }
    }
}

bool PS2Keyboard::available () {
    if (CharBuffer || UTF8next) return true;
    CharBuffer = getIso8859Code();
    if (CharBuffer) return true;
    return false;
}

void PS2Keyboard::clear () {
    CharBuffer = 0;
    UTF8next = 0;
}

uint8_t PS2Keyboard::readScanCode (void) {
    uint8_t c, i;

    i = tail;
    if (i == head) return 0;
    i++;
    if (i >= PS2Keyboard::BUFFER_SIZE) i = 0;
    c = buffer[i];
    tail = i;
    return c;
}

char PS2Keyboard::read () {
    uint8_t result;

    result = UTF8next;
    if (result) {
        UTF8next = 0;
    } else {
        result = CharBuffer;
        if (result) {
            CharBuffer = 0;
        } else {
            result = getIso8859Code();
        }
        if (result >= 128) {
            UTF8next = (result & 0x3F) | 0x80;
            result = ((result >> 6) & 0x1F) | 0xC0;
        }
    }
    if (!result) return -1;
    return result;
}

int PS2Keyboard::readUnicode () {
    int result;

    result = CharBuffer;
    if (!result) result = getIso8859Code();
    if (!result) return -1;
    UTF8next = 0;
    CharBuffer = 0;
    return result;
}

PS2Keyboard::PS2Keyboard () {
    Gpio::useInput(CONFIG::DATAPIN, Gpio::PULL::UP);
    Gpio::useInput(CONFIG::IRQPIN, Gpio::PULL::UP);
    head = 0;
    tail = 0;
    setKeymap(PS2Keyboard::Keymap_US);
}

void PS2Keyboard::setKeymap (const PS2Keyboard::PS2Keymap &map) {
    keymap = &map;
}

void PS2Keyboard::plugin () {
    _id = Dispatcher::assign(PS2Keyboard::CONFIG::SPI, me());
    Gpio::setFalling(PS2Keyboard::CONFIG::IRQPIN, true);
}

