#include "devices/UART1.hpp"
#include "devices/Gpio.hpp"
#include "devices/Clocks.hpp"
#include "devices/Peripherals.hpp"

UART1::UART1 () {
    UART1::outputQueueWrite = 0;
    UART1::outputQueueRead = 0;

    Peripherals::write32(UART1::ADDR::AUX_ENABLES, 1); //enable UART1
    Peripherals::write32(UART1::ADDR::AUX_MU_IER_REG, 0);
    Peripherals::write32(UART1::ADDR::AUX_MU_CNTL_REG, 0);
    Peripherals::write32(UART1::ADDR::AUX_MU_LCR_REG, 3); //8 bits
    Peripherals::write32(UART1::ADDR::AUX_MU_MCR_REG, 0);
    Peripherals::write32(UART1::ADDR::AUX_MU_IER_REG, 0);
    Peripherals::write32(UART1::ADDR::AUX_MU_IIR_REG, 0xC6); //disable interrupts
    Peripherals::write32(
        UART1::ADDR::AUX_MU_BAUD_REG,
        Clocks::auxMiniUartReg(UART1::CONFIG::CLOCK, UART1::CONFIG::BAUD)
    );
    Gpio::useAlt5(UART1::CONFIG::PINA); // Alt0 = UART0 (not mini UART!)
    Gpio::useAlt5(UART1::CONFIG::PINB);
    Peripherals::write32(UART1::ADDR::AUX_MU_CNTL_REG, 3); //enable RX/TX
}

bool UART1::outputEmpty () {
    return UART1::outputQueueRead == UART1::outputQueueWrite;
}

bool UART1::readReady () { 
    return Peripherals::read32(UART1::ADDR::AUX_MU_LSR_REG) & 0x01; 
}

bool UART1::writeReady () {
    return Peripherals::read32(UART1::ADDR::AUX_MU_LSR_REG) & 0x20;
}

unsigned char UART1::read () {
    while (UART1::readReady() == false);
    return (unsigned char) Peripherals::read32(UART1::ADDR::AUX_MU_IO_REG);
}

void UART1::writeByteBlockingActual (unsigned char ch) {
    while (UART1::writeReady() == false); 
    Peripherals::write32(UART1::ADDR::AUX_MU_IO_REG, (unsigned int)ch);
}

void UART1::loadOutputFifo () {
    while (
        UART1::outputEmpty() == false &&
        UART1::writeReady() == true
    ) {
        UART1::writeByteBlockingActual(UART1::outputQueue[UART1::outputQueueRead]);
        // Don't overrun
        UART1::outputQueueRead = (UART1::outputQueueRead + 1) & (UART1::CONFIG::MAX_QUEUE - 1);
    }
}

void UART1::write (unsigned char ch) {
    // Don't overrun
    unsigned int next = (UART1::outputQueueWrite + 1) & (UART1::CONFIG::MAX_QUEUE - 1);

    while (next == UART1::outputQueueRead) UART1::loadOutputFifo();

    UART1::outputQueue[UART1::outputQueueWrite] = ch;
    UART1::outputQueueWrite = next;
}

void UART1::write (const char *buffer) {
    while (*buffer) {
       if (*buffer == '\n') UART1::write('\r');
       UART1::write(*buffer++);
    }
}

void UART1::flush () {
    while (UART1::outputEmpty() == false) UART1::loadOutputFifo();
}
