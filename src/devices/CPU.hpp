#ifndef __CPU_include__
#define __CPU_include__ 1

extern "C" void _CPU_halt (void);
extern "C" void _CPU_wait (void);
extern "C" void _CPU_idle (void);
extern "C" void _CPU_enable_int (void);
extern "C" void _CPU_disable_int (void);

/**
 * @todo implement it not as singelton, but as "primary and secondary" cores.
 * -> checking, which core is executing now.
 */
class CPU {

private:
    CPU() {}
    CPU (const CPU &);
    void operator= (CPU const &) {}

public:
    static CPU & me () {
        static CPU d;
        return d;
    }
    
    /// allow cpu to react on interrupts
    void enable_int () {
        _CPU_enable_int();
    }
    
    /// deny/mask cpu to react on interrupts
    void disable_int () {
        _CPU_disable_int();
    }
    
    /// let core wait for an exception event
    void wait () {
        _CPU_wait();
    }
    
    /// let core wait until IRQ occours
    void idle () {
        _CPU_idle();
    }
    
    /// let core wait for interrupt, but they are mask
    void halt () {
        _CPU_halt();
    }
};

#endif
