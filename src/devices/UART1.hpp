#ifndef __UART1_HPP__
#define __UART1_HPP__ 1

#include "devices/Serial.hpp"
#include "devices/Peripherals.hpp"

/// mini UART on GPIO Pins 14 and 15 (software queue!)

class UART1 : public Serial {

public:

    enum ADDR {
        BASE = Peripherals::ADDR::UART1,
        AUX_IRQ         = BASE,
        AUX_ENABLES     = BASE + 4,
        AUX_MU_IO_REG   = BASE + 64,
        AUX_MU_IER_REG  = BASE + 68,
        AUX_MU_IIR_REG  = BASE + 72,
        AUX_MU_LCR_REG  = BASE + 76,
        AUX_MU_MCR_REG  = BASE + 80,
        AUX_MU_LSR_REG  = BASE + 84,
        AUX_MU_MSR_REG  = BASE + 88,
        AUX_MU_SCRATCH  = BASE + 92,
        AUX_MU_CNTL_REG = BASE + 96,
        AUX_MU_STAT_REG = BASE + 100,
        AUX_MU_BAUD_REG = BASE + 104
    };
    
    enum CONFIG {
        CLOCK     = 500000000,
        MAX_QUEUE = 16 * 1024,
        BAUD      = 115200,
        PINA      = 14,
        PINB      = 15
    };

    unsigned char outputQueue[UART1::CONFIG::MAX_QUEUE];
    unsigned int outputQueueWrite;
    unsigned int outputQueueRead;
    
    void write (const char *buffer);
    void write (unsigned char ch);
    unsigned char read ();
    bool writeReady ();
    bool readReady ();
    
    void flush ();
    void loadOutputFifo ();
    bool outputEmpty ();
    void writeByteBlockingActual (unsigned char ch);
    
    /// This Serial connection is a singleton
    static UART1 & me () {
        static UART1 inst;
        return inst;
    }

private:

    /// singleton = constructor is private
    UART1 ();
    UART1 (const UART1 &) = delete;
    void operator= (UART1 const &) = delete;
};

#endif
