#ifndef __AUDIO_HPP__
#define __AUDIO_HPP__ 1

#include <cstdint> // the uintX_t stuff is cool
#include "devices/Peripherals.hpp"
#include "devices/Gpio.hpp"
#include "devices/DMA.hpp"

/**
 * Stereo 8-bit at 44.1Khz.
 * 
 * hardware detail:
 * - each PWM has 2 output channels!
 * - audio PWM1 DMA is mapped to DMA channel 1
 * - audio PWM1 is mapped to D(ata) REQ(est) 1
 * - and PWM0 is mapped to DMA5
 * 
 * based on https://github.com/isometimes/rpi4-osdev/tree/master/part9-sound
 */

class Audio {

public:
    static uint8_t WAVE[1024];
    uint32_t * _buffer;

    enum OUTDEV {
        USE_DMA     = 1,
        PWM         = Gpio::ADDR::BASE + 0x0c800,  ///< is PWM1
        PIN_FUNC    = Gpio::CONFIG::FUNCTION_ALT0,
        RIGHT_PIN   = 40,           ///< Pi4 use 40,alt0 (PWM1 AV-connector)
        LEFT_PIN    = 41            ///< Pi4 use 41,alt0 (PWM1 AV-connector)
    };

    /* this should work
    enum OUTDEV {
        USE_DMA     = 5,
        PWM         = Gpio::ADDR::BASE + 0x0c000,  ///< is PWM0
        PIN_FUNC    = Gpio::CONFIG::FUNCTION_ALT5,
        RIGHT_PIN   = 18,
        LEFT_PIN    = 19
    };
    */
    
    /* this should work
    enum OUTDEV {
        USE_DMA     = 5,
        PWM         = Gpio::ADDR::BASE + 0x0c000,  ///< is PWM0
        PIN_FUNC    = Gpio::CONFIG::FUNCTION_ALT0,
        RIGHT_PIN   = 12,
        LEFT_PIN    = 13
    };
    */
    
    enum OFFSET {
        CONTROL = 0x00,
        STATUS  = 0x04,
        DMA_CTL = 0x08, ///< short: DMAC
        RANGE0  = 0x10, ///< 1st output channel (right)
        DATA0   = 0x14,
        FIFO    = 0x18, ///< 64x 32bit buffer for both channels ?
        RANGE1  = 0x20, ///< 2nd output channel (left)
        DATA1   = 0x24
    };

    enum CONFIG {
        DIVIDER     = 2,
        BUFFERSIZE  = 0x1000000,    ///< 16*1024*1024 *4byte = 64MB
        RANGE       = 612,
        INITIAL_HZ  = 658,
        SAMPLING    = 44117
    };
   
    enum MASK {
        USEFIFO1    = 0x2000,
        ENABLE1     = 0x0100,
        USEFIFO0    = 0x0020,
        ENABLE0     = 0x0001,
        CLEAR_FIFOs = 0x0040,
        
        GAPO1 = 0x20,
        GAPO0 = 0x10,
        RERR0 = 0x08,
        WERR0 = 0x04,
        FULL0 = 0x01,
        ERROR = (GAPO1 | GAPO0 | RERR0 | WERR0),
        
        DMAC_ENAB           = (1<<31),
        DMAC_THESHOLD_DREQ  = (7<<0),
        DMAC_THESHOLD_PANIC = (7<<8)
    };

    /// This is a singleton
    static Audio * me () {
        static Audio inst;
        return &inst;
    }
    
    /// it is a blocking loop and you got breaks via scheduling
    void wav (uint64_t ramdiskptr);
    
    void tone (int hz);
    void load (uint64_t ramdiskptr);
    void play (bool loop=false);
    void pause ();
    void stop ();
    
    float progress ();
    bool isPlaying ();
    uint32_t getDuration ();

private:
    DMA * _dma;
    DMA::ControlBlock _cb;

    /// singleton = constructor is private (and init!)
    Audio ();
    Audio (const Audio &) = delete;
    void operator= (Audio const &) = delete;
};

#endif
