#include <cstdint> // the uintX_t stuff is cool
#include "main.hpp"
#include "OS.hpp"

#include "heap/MMGready.hpp"
#include "heap/MMLinkedList.hpp"
#include "heap/MMBitmask.hpp"

#include "exceptions/Dispatcher.hpp"
#include "exceptions/GIC.hpp"
#include "exceptions/Calls.hpp"            // calls and breakpoint
#include "exceptions/SysinfoCall.hpp"
#include "exceptions/PsDumpCall.hpp"

#include "threads/Scheduler.hpp"
#include "devices/CPU.hpp"
#include "devices/PIT.hpp"
#include "devices/UART0.hpp"
#include "devices/UART3.hpp"
#include "devices/UART1.hpp"
#include "devices/VESA.hpp"
#include "devices/DMA.hpp"
#include "devices/Audio.hpp"
#include "devices/PS2Keyboard.hpp"

#include "apps/Shell.hpp"
#include "apps/ClockApp.hpp"
#include "apps/Interact.hpp"
#include "apps/LOGO.hpp"

int main (uint64_t startAddr, uint64_t endAddr) {
    
    // set a serial interface (we use it as keyboard replacement)
    
    // alternative UART on gpio 4,5 => pin 7,29
    //OS::serial = &(UART3::me());
    
    // alternative UART on gpio 14,15 => pin 8,10 
    OS::serial = &(UART0::me());
    // alternative UART on gpio 14,15 => pin 8,10
    // -> but mini uart (fast, but without hardware buffer)
    // -> may not work with InteractApp out of the box.
    //OS::serial = &(UART1::me());
    
    OS::serial->write("hhuOS Pi4\n---------\n");
    
    CPU::me().disable_int();
        // create a free map of (interrupt) service routines
        Dispatcher::init();
        // create a free map of (call) service routines
        Calls::init();
        // init says the General Interrupt Controllor to forward Interrupts to Core0
        GIC::init();
        // set a flag in the Thread Scheduler to still NOT react on timer interrupts
        Scheduler::init();
        
        // set a very gready Memory Management (only alloc is implemented in MMGready)
        //OS::mm = &(MMGready::me());
        
        OS::mm = &(MMLinkedList::me());
        
        // a MM version with bitmask
        //OS::mm = &(MMBitmask::me());
        
        // assign SystemTimer interrupts to the ServiceRoutine
        PIT::me()->plugin();
        // assing some example system call service routines
        SysinfoCall::me()->plugin();
        PsDumpCall::me()->plugin();
        
        // assign PS/2 keyboard interrupts to the ServiceRoutine
        PS2Keyboard::me()->plugin();
        PS2Keyboard::me()->setKeymap(PS2Keyboard::Keymap_German);
        
    CPU::me().enable_int();
    
    // create some Threads for the Scheduler
    OS::shell = new Shell();
    Interact * key = new Interact();
    ClockApp * clk = new ClockApp();
    
    OS::dma1 = new DMA(1);
    OS::dma5 = new DMA(5);
    // very hungry! Audio reseves 64MB from lower memory area als DMA data buffer.
    Audio::me();
    
    // add the shell to execute some commands
    Scheduler::add(OS::shell);
    // add the interactive Thread, which feeds the shell with "key input" (actual serial)
    Scheduler::add(key);
    // add a small digital clock app to the still not running Scheduler
    Scheduler::add(clk);

    // welcome ------------------------------------------------
    //VESA::me().init(320, 240, 32);
    VESA::me().setColor(VESA::RGB::lightgray, VESA::RGB::black);
    VESA::me().print("hhuOS Pi4\n")
        .print((endAddr-startAddr)/1024, 10)
        .print("K KERNEL SIZE\n")
        .print((OS::mm->unused())/(1024*1014), 10)
        .print("M BYTES FREE\n")
        .print("\nREADY.\n");
    VESA::me().drawSprite(VESA::me()._xres - 151, 3, LOGO);
    
    /*
     * let the scheduler run on a non-empty chain of Threads!
     * it set a flag in the Thread Scheduler to DO react on timer interrupts.
     * endless loop integrated!
     */
    Scheduler::run();
    
    return 0;
}

/**
 * part of the boot system.
 * @page startup_S startup.S
 * @verbinclude startup.S
 */
