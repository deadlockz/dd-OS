#include <cstddef> // we need it for size_t and sizeof()
#include <cstdint> // the uintX_t stuff is cool

#include "OS.hpp"
#include "heap/MM.hpp"
#include "devices/CPU.hpp"
#include "exceptions/Calls.hpp" // breakpoint
#include "exceptions/Dispatcher.hpp"
#include "apps/Shell.hpp"
#include "devices/Serial.hpp"
#include "devices/DMA.hpp"

// a 16byte address alignment is good to store 128bit float values every 2nd index
// = last 4 bits of start address are 0.
uint64_t os_regs[104] alignas(16);

unsigned long OS::systime = 0;
Shell * OS::shell;
MM * OS::mm;
uint64_t OS::ramdiskAddr = 0x1C0000; // a initial default value
Serial * OS::serial;
DMA * OS::dma1;
DMA * OS::dma5;
//DMA * OS::dma14;

#ifdef __cplusplus
extern "C" {
#endif

    /**
     * it defines a static object destructor.
     */
    void __cxa_atexit (void *, void (*)(void *), void *) {
        // nothing
    }

    /**
     * it handles, if a pure virtual methode is called. this
     * has to stop the system, because these methods not exists.
     */
    void __cxa_pure_virtual (void) {
        /// @todo throw/implement a software exception!
        breakpoint(1);
    }

    /**
     * a registered pFunc is called as additional subfunction
     * after the registering function exits.
     */
    int atexit (void (*)(void)) {
        /// @todo create a valid atexit
        //if (pFunc == NULL) return 1;
        //pFunc();
        return 0;
    }

#ifdef __cplusplus
}
#endif

namespace std {
    /// this hack makes #include <functional> possible without stdinc !
    void __throw_bad_function_call() {
        breakpoint(2);
    }
}

void * operator new (size_t s) {
    if (Dispatcher::isDispatching == false) CPU::me().disable_int();
    void * result = OS::mm->alloc(s);
    if (Dispatcher::isDispatching == false) CPU::me().enable_int();
    return result;
}

void * operator new[] (size_t c) {
    if (Dispatcher::isDispatching == false) CPU::me().disable_int();
    void * result = OS::mm->alloc(c);
    if (Dispatcher::isDispatching == false) CPU::me().enable_int();
    return result;
}

void operator delete (void * ptr)  {
    if (Dispatcher::isDispatching == false) CPU::me().disable_int();
    OS::mm->free(ptr);
    if (Dispatcher::isDispatching == false) CPU::me().enable_int();
}

void operator delete[] (void * ptr) {
    if (Dispatcher::isDispatching == false) CPU::me().disable_int();
    OS::mm->free(ptr);
    if (Dispatcher::isDispatching == false) CPU::me().enable_int();
}

/// 2nd parameter is a size. where does c++ store this?
void operator delete[] (void * ptr, long unsigned int) {
    if (Dispatcher::isDispatching == false) CPU::me().disable_int();
    OS::mm->free(ptr);
    if (Dispatcher::isDispatching == false) CPU::me().enable_int();
}

/// 2nd parameter is a size. where does c++ store this?
void operator delete (void * ptr, long unsigned int)  {
    if (Dispatcher::isDispatching == false) CPU::me().disable_int();
    OS::mm->free(ptr);
    if (Dispatcher::isDispatching == false) CPU::me().enable_int();
}
