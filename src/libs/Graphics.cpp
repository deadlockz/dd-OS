/*****************************************************************************
 *                                                                           *
 *                             G R A P H I C S                               *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * Beschreibung:    Zeichenfunktionen fuer Grafikmodi, die auf einem         *
 *                  Buffer basieren. Verwendet in VESA.                      *
 *                                                                           *
 * Autoren:           Michael Schoettner, Jochen Peters, HHU, 2016, 2021     *
 *****************************************************************************/

#include "libs/Graphics.hpp"
#include "libs/Byte.hpp"
#include "libs/Font.hpp"
#include "libs/Sprite.hpp"

void Graphics::drawMonoBitmap (
    int x, 
    int y,
    unsigned int width,
    unsigned int height,
    unsigned char * bitmap, 
    int color, 
    int bg
) {
    // Breite in Bytes
    unsigned short width_byte = width/8 + ((width%8 != 0) ? 1 : 0);

    for (unsigned int yoff=0; yoff<height; ++yoff) {
        int xpos=x;
        int ypos=y+yoff;
        for (unsigned int xb=0; xb < width_byte; ++xb) {
            for (int src=7; src>=0; --src) {
                if ((1 << src) & *bitmap) {
                    drawPixel(xpos, ypos, color);
                } else {
                    if (bg >= 0) {
                        drawPixel(xpos, ypos, bg);
                    }
                }
                xpos++;
            }
            bitmap++;
        }
    }
}

void Graphics::drawString (
    Font & fnt,
    const int & x,  
    const int & y,
    int col,
    const char * str,
    int len,
    int bgColor
 ) {
    int dummx = x;
    int dummy = y;
    drawString(fnt,dummx,dummy,col,str,len,bgColor);
}

void Graphics::drawString (
    Font & fnt,
    int & x,  
    int & y,
    int col,
    const char * str,
    int len,
    int bgColor
) {
    for(int i = 0; i < len; ++i) {
        //if (y >= _yres) y = _yres - fnt.height() -2;
        if (y >= _yres) y = 0;
        if (*(str+i) == '\n') {
            x = _xAlign; 
            y += fnt.height();
        } else if (*(str+i) == '\t') {
            x += (fnt.width() * CONFIG::TABLEN) - x%(fnt.width() * CONFIG::TABLEN);
        } else {
            drawMonoBitmap(
                x, y,
                fnt.width(),
                fnt.height(),
                fnt.getChar( *(str+i) ),
                col,
                bgColor
            );
            y += ((x + fnt.width() ) / _xres) * fnt.height();
            x =  ( x + fnt.width() ) % _xres;
        }
    }
}


void Graphics::readPixel (int x, int y, byte &r, byte &g, byte &b) {
    y += _yoffset;
    
    // 16bit unused
    byte * ptr = (byte *) (_lfb + 2*x + 2*y*_xres);

    // Pixel ausserhalb des sichtbaren Bereichs?
    if (x<0 || x>=_xres || y<0 || y>_yres*2) return;
    
    if (_bpp == 24) {
        ptr = (byte *) (_lfb + 3*x + 3*y * _xres);
    } else if (_bpp == 32) {
        ptr = (byte *) (_lfb + 4*x + 4*y *_xres);
        ptr++; // alpha
    }
    r = *ptr; ptr++;
    g = *ptr; ptr++;
    b = *ptr;
    return;
}

void Graphics::drawPixel (int x, int y, int col) {
    int offs = 0;
    y += _yoffset;
    // Pixel ausserhalb des sichtbaren Bereichs?
    if (x<0 || x>=_xres || y<0 || y>=_yres*2)
        return;

    // Adresse des Pixels berechnen und Inhalt schreiben
    switch (_bpp) {
        case 8:
            offs = y * _xres + x;
            *((unsigned int*)(_lfb + offs)) = col;
            return;
        case 15:
        case 16:
            offs = (y * 2 * _xres) + (x * 2);
            *((unsigned int*)(_lfb + offs)) = col;
            return;
        case 24:
            offs = (y * 3 * _xres) + (x * 3);
            *((unsigned int*)(_lfb + offs)) = col;
            return;
        case 32:
            offs = (y * 4 * _xres) + (x * 4);
            // a small hack to handle alpha value
            Bit32 * color1 = (Bit32 *) &col;
            Bit32 * color2 = (Bit32 *) (_lfb + offs);
            blend(color1, color2);
            return;
    }
}

void Graphics::blend (Bit32 * from, Bit32 * to) {
    if (from->a == 255) {
        // incoming color total transparent (nothing to do)
        return;
    }

    if (from->a == 0) {
        // result will be total opaque
        to->r = from->r;
        to->g = from->g;
        to->b = from->b;
        to->a = from->a;
        return;
    }

    float alpha = (255 - from->a) / 255.0;
    
    float col = to->r - from->r;
    if (col<0) col *= -1;
    col = alpha * col + to->r; // add the difference between red values (but modified via alpha)
    if (col > 255) col = 255;
    to->r = col;
    
    col = to->g - from->g;
    if (col<0) col *= -1;
    col = alpha * col + to->g;
    if (col > 255) col = 255;
    to->g = col;
    
    col = to->b - from->b;
    if (col<0) col *= -1;
    col = alpha * col + to->b;
    if (col > 255) col = 255;
    to->b = col;
    
    // to alpha is not relevant in this blend
}


void Graphics::clear (int fillCol) {
    int offs = _xres * _yoffset * 4;
    byte stepsize = 4;
    int lfb_end = _xres * _yres * 4 + offs;
    
    if (fillCol == -1) fillCol = Graphics::RGB::black;
    
    switch (_bpp) {
        case 8:
            offs = _xres * _yoffset;
            lfb_end = _xres * _yres + offs;
            stepsize = 1;
            break;
        case 15:
        case 16:
            offs = _xres * _yoffset * 2;
            lfb_end = _xres * _yres * 2 + offs;
            stepsize = 2;
            break;
        case 24:
            offs = _xres * _yoffset * 3;
            lfb_end = _xres * _yres * 3 + offs;
            stepsize = 3;
            break;
        default:
            ;
    }
    while (offs <= lfb_end) {
        *((unsigned int*)(_lfb + offs)) = fillCol;
        offs += stepsize;
    }
}

void Graphics::line (int x,  int y, int tox,  int toy,  int col) {
    int sx = x<tox ? 1 : -1;
    int sy = y<toy ? 1 : -1;

    int dx =  sx * (tox-x);
    int dy =  sy * (y-toy);
    int err = dx+dy, e2;

    while(true) {
        drawPixel(x, y, col);
        
        if (x==tox && y==toy) break;
        e2 = 2*err;
        if (e2 > dy) { err += dy; x += sx; }
        if (e2 < dx) { err += dx; y += sy; }
    }
}

void Graphics::rect (int x,  int y, int tox,  int toy,  int col, int fillCol) {
    int i,j;
    if (fillCol != -1) {
        if (tox < x) { i = x; x = tox; tox = i; }
        if (toy < y) { i = y; y = toy; toy = i; }

        for (i=x; i<=tox; ++i) {
            for (j=y; j<=toy; ++j) {
                drawPixel(i, j, fillCol);
            }
        }
    }
    line(x,y,     tox,y,   col);
    line(x,y,       x,toy, col);
    line(x,toy,   tox,toy, col);
    line(tox,toy, tox,y,   col);
}

void Graphics::circle (int x0, int y0, int radius, int col, int fillCol) {
    int x = radius;
    int y = 0;
    int err = 0;
 
    while (x >= y) {
        if (fillCol != -1) {
           line(x0 - y, y0 + x, x0 + y, y0 + x, fillCol);
           line(x0 - x, y0 + y, x0 + x, y0 + y, fillCol);
           line(x0 - x, y0 - y, x0 + x, y0 - y, fillCol);
           line(x0 - y, y0 - x, x0 + y, y0 - x, fillCol);
           if (col != fillCol) {
            drawPixel(x0 - y, y0 + x, col);
            drawPixel(x0 + y, y0 + x, col);
            drawPixel(x0 - x, y0 + y, col);
            drawPixel(x0 + x, y0 + y, col);
            drawPixel(x0 - x, y0 - y, col);
            drawPixel(x0 + x, y0 - y, col);
            drawPixel(x0 - y, y0 - x, col);
            drawPixel(x0 + y, y0 - x, col);
           }
        } else {
            drawPixel(x0 - y, y0 + x, col);
            drawPixel(x0 + y, y0 + x, col);
            drawPixel(x0 - x, y0 + y, col);
            drawPixel(x0 + x, y0 + y, col);
            drawPixel(x0 - x, y0 - y, col);
            drawPixel(x0 + x, y0 - y, col);
            drawPixel(x0 - y, y0 - x, col);
            drawPixel(x0 + y, y0 - x, col);
        }
        if (err <= 0) {
            y += 1;
            err += 2*y + 1;
        }

        if (err > 0) {
            x -= 1;
            err -= 2*x + 1;
        }
    }
}

void Graphics::drawSprite (int x, int y, const Sprite & bmp) {
    unsigned int widthE =  bmp.width + x;
    unsigned int heightE = bmp.height + y;

    if (bmp.bytes_per_pixel == 2) {
        unsigned short * pxl = (unsigned short *) bmp.pixel_data;
        
        for (unsigned int yy = y; yy < heightE; ++yy) {
            for (unsigned int xx = x; xx < widthE; ++xx) {
                drawPixel(xx, yy, lowColorToRGB(*pxl));
                ++pxl;
            }
        }
    } else if (bmp.bytes_per_pixel == 4) {
        Bit32 * pxl = (Bit32 *) bmp.pixel_data;
        
        for (unsigned int yy = y; yy < heightE; ++yy) {
            for (unsigned int xx = x; xx < widthE; ++xx) {
                // these files are in a bit different order.
                pxl->a = 255 - pxl->a;
                drawPixel(xx, yy, RGB_32(pxl->a, pxl->b, pxl->g, pxl->r));
                ++pxl;
            }
        }
    } else {
        Bit24 * pxl = (Bit24 *) bmp.pixel_data;
        
        for (unsigned int yy = y; yy < heightE; ++yy) {
            for (unsigned int xx = x; xx < widthE; ++xx) {
                drawPixel(xx, yy, RGB_24(pxl->r, pxl->g, pxl->b));
                ++pxl;
            }
        }
    }
}
