#ifndef _BOUNDED_BUFFER_HPP
#define _BOUNDED_BUFFER_HPP 1
#include "devices/CPU.hpp"
#include "libs/Semaphore.hpp"

template <typename T>
class BoundedBuffer {
private:
    Semaphore *_mtx;
    Semaphore *_free;
    Semaphore *_fill;
    int _beg, _end, _size;
    T *_values;
    unsigned long long int _count;

public:
    BoundedBuffer(int size) {
        _size = size;
        _beg = 0;
        _end = 0;
        _count = 0;
        _values = new T[size];
        _mtx = new Semaphore(1); ///< @todo system stops, if you use it. why?
        _free = new Semaphore(size);
        _fill = new Semaphore(0);
    }
    ~BoundedBuffer() {
        delete _values; // or delete[] ?
        delete _free;
        delete _fill;
        delete _mtx;
    }
    void push(T val) {
        _free->P();
        //_mtx->P();
        _values[_end] = val;
        _end = (_end + 1) % _size;
        ++_count;
        //_mtx->V();
        _fill->V();
    }
    T pop() {
        T res;
        _fill->P();
        //_mtx->P();
        res = _values[_beg];
        _beg = (_beg + 1) % _size;
        //_mtx->V();
        _free->V();
        return res;
    }
    unsigned long long int getCount() {
        return _count;
    }
};
#endif
