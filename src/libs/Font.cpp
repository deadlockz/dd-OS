#include "libs/Font.hpp"

Font::Font() {
    _width = 8;
    _height = 8;
    _memSize = ((_width + (8 >> 1)) / 8) * _height;
}

void Font::set (unsigned char * data, unsigned int xwidth, unsigned int yheight) {
    if (xwidth > 0)  _width = xwidth;
    if (yheight > 0) _height = yheight;
    _data = data;
    _memSize = ((_width + (8 >> 1)) / 8) * _height;
}

unsigned char* Font::getChar (int c) {
    return &(_data[_memSize * c]);
}

unsigned int Font::width () {
    return _width;
}

unsigned int Font::height () {
    return _height;
}