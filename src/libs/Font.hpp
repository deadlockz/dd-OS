#ifndef __FONT_HPP__
#define __FONT_HPP__ 1

class Font {

private:
    unsigned int _width;
    unsigned int _height;
    unsigned int _memSize;
    unsigned char * _data;

public:
    Font();
    void set (unsigned char * data, unsigned int xwidth = 0, unsigned int yheight = 0);

    unsigned char* getChar (int c);
    unsigned int width ();
    unsigned int height ();
};

#endif
