// Acorn-like font definition, with PC graphics characters

#ifndef FONTDATAMAX_ACORNDATA
#define FONTDATAMAX_ACORNDATA 2048

extern unsigned char fontdata_acorn_8x8[FONTDATAMAX_ACORNDATA];

#endif
