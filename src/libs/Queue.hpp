#ifndef __Queue_include__
#define __Queue_include__ 1

/**
 * Queue.
 * This is a linked list optimized for FIFO.
 */
template<class T>
class Queue {
public:

private:
    /**
     * Queue Node.
     * store your value reference inside a queue node.
     */
    struct Node {
        Node * _next;
        T * _value;
        Node () {
            // dummy
        }
        Node (T * val) {
            _value = val;
        }
        void set (T * val) {
            _value = val;
        }
    };
    
    Node * _first;
    Node * _last;
    
    unsigned long _size;

public:
    Queue () {
        _first = nullptr;
        _last = nullptr;
        _size = 0;
    }

    /**
     * push into.
     * add a node to the end.
     */
    Queue & push (T * thing) {
        Node * n = new Node(thing);
        
        if (_size == 0) {
            _last = n;
            _first = n;
            n->_next = n;
        } else {
            n->_next = _last->_next;
            _last->_next = n;
            _last = n;
        }
        ++_size;
        return *this;
    }

    /** pop out.
     * Get value from first Node and delete it.
     * @return pointer to the value
     */
    T * pop () {
        T * val = _first->_value;
        _last->_next = _first->_next;
        delete _first;
        _first = _last->_next;
        --_size;
        return val;
    }

    /**
     * pop last.
     * It warps though the Queue and sets the last Node to the first.
     * Finaly it pops (removes) this node.
     * @return pointer to the last value
     */
    T * popLast () {
        for (unsigned long i=0; i<_size-1; ++i) warp();
        return pop();
    }

    /**
     * Walk through.
     * Get the first Node value and set it to the end of the Queue.
     * @return the first node value (which is now at the last node)
     */
    T * warp () {
        Node * n = _first;
        
        T * val = _first->_value;
        _last->_next = _first->_next;
        _first = _last->_next;
        
        n->_value = val;
        if (_size == 1) {
            _last = n;
            _first = n;
            n->_next = n;
        } else {
            n->_next = _last->_next;
            _last->_next = n;
            _last = n;
        }
        
        return val;
    }

    /**
     * get the first Node value without delete
     */
    T * first () {
        return _first->_value;
    }

    /**
     * get the last Node value without delete
     */
    T * last () {
        return _last->_value;
    }

    unsigned long & size () {
        return _size;
    }

    bool isEmpty () {
        return (_size == 0);
    }
};

#endif
