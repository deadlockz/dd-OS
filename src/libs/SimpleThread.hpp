#ifndef __SimpleThread_include__
#define __SimpleThread_include__ 1

#include "threads/Thread.hpp"
#include <functional>

/**
 * A Thread implementation with the main() as parameter in constructor.
 * 
 * @see the Shell.cpp for examples.
 */
template<class T>
class SimpleThread : public Thread {

private:
    /// we do not want copying instances
    SimpleThread (const SimpleThread &copy) = delete;
    void operator= (SimpleThread const &) = delete;
    
    T _para;
    std::function<void(T &)> _foo;
    
public:
    SimpleThread (
        T para,
        std::function<void(T &)> foo,
        bool cooperative = false
    ) : Thread(cooperative) {
        _para = para;
        _foo = foo;
    }
    
    char * name () {return "a Simple Thread";}
    
    void main () {
        _foo(_para);
    }
};

#endif
