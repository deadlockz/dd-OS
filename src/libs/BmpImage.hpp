#ifndef __BMPIMAGE_HPP__
#define __BMPIMAGE_HPP__ 1

#include <cstdint> // the uintX_t stuff is cool
#include "libs/Byte.hpp"
#include "libs/Graphics.hpp"
#include "devices/Peripherals.hpp"

namespace BmpImage {

    byte row[2000];
    
    int32_t width;
    int32_t height;
    
    /**
     * read a bmp file (32bit)
     */
    void draw (uint64_t ramdiskptr, int x, int y) {
        uint16_t a = Peripherals::read16(ramdiskptr +18);
        uint16_t b = Peripherals::read16(ramdiskptr +20);
        width = ((b&0x0000FFFF)<<16) | (a&0x0000FFFF);

        a = Peripherals::read16(ramdiskptr +22);
        b = Peripherals::read16(ramdiskptr +24);
        height = ((b&0x0000FFFF)<<16) | (a&0x0000FFFF);
        
        uint32_t psize;
        a = Peripherals::read16(ramdiskptr +34);
        b = Peripherals::read16(ramdiskptr +36);
        psize = ((b&0x0000FFFF)<<16) | (a&0x0000FFFF);

        int row_padded;
        row_padded = (psize/height);
        ramdiskptr += 134; // +=54 on 24bit
        
        for (int yy = height-1; yy >= 0; --yy) {
            for (int i = 0; i < row_padded; ++i) {
                row[i] = Peripherals::read8(ramdiskptr++);
            }
            for (int xx = 0; xx < width; ++xx) {
                VESA::me().drawPixel(xx+x, yy+y, RGB_32(255 - row[(4*xx)+3],row[(4*xx)+2],row[(4*xx)+1],row[(4*xx)]));
                // 24bit
                //VESA::me().drawPixel(xx+x, yy+y, RGB_32(0,row[(3*xx)+2],row[(3*xx)+1],row[(3*xx)]));
            }
        }
    }
}

#endif
