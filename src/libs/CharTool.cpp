#include <cstdint> // the uintX_t stuff is cool

#include "libs/CharTool.hpp"

int CharTool::bit64ToStr (unsigned long long number, char * str, int base, bool without0x) {
    int pos = 0;
    
    unsigned long long div;
    char digit;

    if (base == 8)
        str[pos++] = '0';         // oktale Zahlen erhalten eine fuehrende Null
    else if (base == 16 && without0x == false) {
        str[pos++] = '0';         // hexadezimale Zahlen ein "0x"
        str[pos++] = 'x';
    }

    // Bestimmung der groessten Potenz der gewaehlten Zahlenbasis, die
    // noch kleiner als die darzustellende Zahl ist.
    for (div = 1; number/div >= (unsigned long long) base; div *= base);

    // ziffernweise Ausgabe der Zahl
    for (; div > 0; div /= (unsigned long long) base) {
        digit = number / div;
        if (digit < 10)
            str[pos++] = '0' + digit;
        else
            str[pos++] = 'a' + digit - 10;
        number %= div;
    }
    
    return pos;
}

/**
 doubleToStr() based on Print::printFloat()
 Copyright (c) 2008 David A. Mellis.  All right reserved.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 
 Modified 23 November 2006 by David A. Mellis
 Modified 03 August 2015 by Chuck Todd
 */
int CharTool::doubleToStr (double number, char * str, uint8_t digits) {
    int pos = 0;
    long div;
    char digit;
    
    // Handle negative numbers
    if (number < 0.0) {
        str[pos++] = '-';
        number = -number;
    }

    // Round correctly so that print(1.999, 2) prints as "2.00"
    double rounding = 0.5;
    for (uint8_t i=0; i<digits; ++i) {
        rounding /= 10.0;
    }
    number += rounding;

    // Extract the integer part of the number and print it
    unsigned long int_part = (unsigned long)number;
    double remainder = number - (double)int_part;
    
    // get the highest number to divide from for splitting each digit
    for (div = 1; number/div >= 10; div *= 10);
    
    // write each digit in a loop
    for (; div > 0; div /= 10) {
        digit = int_part / div;
        str[pos++] = '0' + digit;
        int_part %= div;
    }

    // Print the decimal point, but only if there are digits beyond
    if (digits > 0) {
        str[pos++] = '.';
    }

    // Extract digits from the remainder one at a time
    while (digits-- > 0) {
        remainder *= 10.0;
        unsigned int toPrint = (unsigned int)(remainder);
        str[pos++] = '0' + toPrint;
        remainder -= toPrint; 
    } 

    return pos;
}

int CharTool::intToStr (long number, char * str, int base) {
    int pos = 0;
    
    long div;
    char digit;

    if (number < 0) {
        str[pos++] = '-';
        number = -number;
    }

    if (base == 8)
        str[pos++] = '0';         // oktal numbers get a zero
    else if (base == 16) {
        str[pos++] = '0';         // hex numbers get "0x"
        str[pos++] = 'x';
    }

    // get the highest number to divide from for splitting each digit
    for (div = 1; number/div >= (long) base; div *= base);

    // write each digit in a loop
    for (; div > 0; div /= (long) base) {
        digit = number / div;
        if (digit < 10)
            str[pos++] = '0' + digit;
        else
            str[pos++] = 'a' + digit - 10;
        number %= div;
    }
    
    return pos;
}

void CharTool::strCopy (char * from, char * to) {
    int i=0;
    while (from[i] != '\0') {
        to[i] = from[i];
        ++i;
    }
    to[i] = '\0';
}

int CharTool::len (const char * str, int maximum) {
    int i=0;
    while (str[i] != '\0' && i != maximum) ++i;
    return i;
}

bool CharTool::equal (const char * a, const char * b) {
    int l1 = CharTool::len(a);
    int l2 = CharTool::len(b);
    if (l1 != l2) return false;
    
    for (int i=0; i<l1; ++i) {
        if(a[i] != b[i]) return false;
    }
    return true;
}

bool CharTool::startsWith (const char * a, const char * with) {
    int l1 = CharTool::len(a);
    int w  = CharTool::len(with);
    if (l1 < w) return false;
    
    for (int i=0; i<w; ++i) {
        if(a[i] != with[i]) return false;
    }
    return true;
}

int CharTool::strToInt (const char * str) {
    int neg    = 1;
    int result = 0;
    int base   = 10;
    int b      = 1;
    int start  = 0;
    int l = CharTool::len(str);
    if (str[0] == '-') {
        start = 1;
        neg = -1;
    }
    for(int i=(1+start); i<l; ++i) b *= base;
    for(int i=start; i<l; ++i) {
        result += b * (int)(str[i] - '0');
        b /= base;
    }
    return neg*result;
}

double CharTool::strToDouble (const char * str) {
    double res = 0.0;
    double neg = 1.0;
    int l;
    int i=0;
    int start = 0;
    int base  = 10;
    int b     = 1;
    while (str[i] != '.' && i != 60) ++i;
    if (i == 0) { // ".12300"
        res = CharTool::strToInt((const char *) &(str[1]));
        l = CharTool::len((const char *) &str[1], 60);
        for (i=0; i<l; ++i) res /= 10.0;
    } else if (i == 60) { // "-123000"
        return CharTool::strToInt(str);
    } else { // "-123.0012"
        // "xxxxxi0012"
        res = CharTool::strToInt((const char *) &(str[i+1]));
        l = CharTool::len((const char *) &str[i+1], 60);
        for (int j=0; j<l; ++j) res /= 10.0;
        // "-xxxxxxxx"
        if (str[0] == '-') {
            start = 1;
            neg = -1;
        }
        // "x123ixxxx"
        l = i; // position of the dot
        for(int i=(1+start); i<l; ++i) b *= base;
        for(int i=start; i<l; ++i) {
            res += b * (int)(str[i] - '0');
            b /= base;
        }
    }
    
    return neg*res;
}
