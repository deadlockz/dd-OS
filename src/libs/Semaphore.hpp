#ifndef __Semaphore_include__
#define __Semaphore_include__ 1

#include "OS.hpp"
#include "threads/Thread.hpp"
#include "exceptions/Calls.hpp" // breakpoint and "System" calls

/**
 * A Semaphore based on delays. It is extremly slow (>1000x)
 * Idea:
 *   test and set are at the end/beginning of an preemtive
 *   Scheduling cycle. this garantees some atomic instructions
 *   for a simple mutex, which saves a code area for a
 *   counting Semaphore.
 */

class Semaphore {

private:
    Semaphore (const Semaphore &copy);
    int _atomicCount;
    bool _lock;
    
public:
    Semaphore (int c) {
        _atomicCount = c;
        _lock = false;
    }

    void P () {
        unsigned long now = OS::systime;
        // active waiting on a new scheduling event..
        while (now == OS::systime);
        // .. and we have the whole thread timespan, now!
        while (_atomicCount == 0) {
            while (_lock == true) {
                now = OS::systime;
                while (now == OS::systime);
                // .. and we have the whole thread timespan, now!
            }
            _lock = true;
        }
        _atomicCount--;
    }

    void V () {
        Calls::calling(Calls::ID::PAUSE);
        _atomicCount++;
        _lock = false;
        Calls::calling(Calls::ID::OYIELD);
    }
};

#endif
