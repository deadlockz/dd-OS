#ifndef __CHARTOOL_HPP__
#define __CHARTOOL_HPP__ 1

#include <cstdint> // the uintX_t stuff is cool

/**
 * Character Tools.
 * This namespace contains many methodes to handle with char arrays.
 */
namespace CharTool {
	
	int bit64ToStr (unsigned long long number, char * str, int base=16, bool without0x=false);
	int intToStr (long number, char * str, int base=10);
	int doubleToStr (double number, char * str, uint8_t digits);
	void strCopy (char * from, char * to);
	int len (const char * str, int maximum=16000000);
	bool equal (const char * a, const char * b);
	bool startsWith (const char * a, const char * with);
	
	/**
	 * string to integer.
	 * This method converts a int value in a string array to
	 * an integer value.
	 */
	int strToInt (const char * str);

	double strToDouble (const char * str);
}

#endif
