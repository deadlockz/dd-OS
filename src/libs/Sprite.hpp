#ifndef __Sprite_include__
#define __Sprite_include__

/** Sprite (color).
 * A struct for a GIMP RGB C-Source image dump
 */
struct Sprite {
    int    width;
    int    height;
    /// 2:RGB16, 3:RGB, 4:RGBA
    int    bytes_per_pixel; 
    /// the data array
    char * pixel_data;
};

#endif
