#ifndef __Graphics_HPP__
#define __Graphics_HPP__ 1

#include <cstdint> // the uintX_t stuff is cool

#include "libs/Font.hpp"
#include "libs/Byte.hpp"
#include "libs/Sprite.hpp"

/// macro to convert 3 bytes to a 24bit color value
#define RGB_24(r,g,b) (unsigned int) (((r) << 16) + ((g) << 8) + b )
/// macro to make 4 bytes to argb order
#define RGB_32(a,r,g,b) (unsigned int) (((a) << 24) + ((r) << 16) + ((g) << 8) + b )

struct Bit32 {
    byte b;
    byte g;
    byte r;
    byte a;
};

struct Bit24 {
    byte r;
    byte g;
    byte b;
};

/**
 * linear frame buffer graphic driver.
 * The VESA (wrapper) class uses these very basic framebuffer methods to make great magic.
 * @author Michael Schoettner (HHU), Jochen Peters
 */
class Graphics {

private:
    Graphics (const Graphics & copy);

public:

    /// enum with some nice colors similar to the CGA attribut colors.
    enum RGB : uint32_t {
        black     = 0x000000,
        blue      = 0x0000BB,
        green     = 0x00CC00,
        cyan      = 0x00AAAA,
        red       = 0xCC0000,
        violett   = 0xAA00AA,
        brown     = 0xAA5500,
        lightgray = 0xCACACA,
        
        gray      = 0x888888,
        lightblue = 0xABABFF,
        lightgreen= 0xABFFAB,
        lightcyan = 0x55FFFF,
        lightred  = 0xFF5555,
        pink      = 0xFF55FF,
        yellow    = 0xFFFF55,
        white     = 0xFFFFFF,
        
        hhu       = 0x006AB3,
        sun       = 0xFFCC00
    };
    
    enum CONFIG {
        TABLEN = 8
    };
    
    /**
     * draw a binary image.
     * it is possible to set a background color.
     */
    void drawMonoBitmap (
        int x,
        int y,
        unsigned int width,
        unsigned int height,
        unsigned char* bitmap,
        int col,
        int bg = -1
    );

    int _xres;
    int _yres;
    int _yoffset;

    /// bits per pixel @todo protected?
    unsigned int _bpp;
    /// address of the linear framebuffer @todo protected?
    unsigned char * _lfb;

    Graphics () {
        _yoffset= 0;
        _xAlign = 0;
        _xres = 800;
        _yres = 600;
        _bpp = 32;
    }

    /**
     * 16bit to 24bit convert.
     * this method splits the 5bit red, 6bit green and 5bit blue from a 16bit
     * color into 3 bytes and creates a 24bit color. it does not spread the
     * 5 or 6 bit into the hole 0-255 range. black would be black, but white
     * gets a bit yellow or green taste.
     * @todo spread the color range
     */
    static unsigned int lowColorToRGB (unsigned short col565) {
        byte red   = (col565 & 0b1111100000000000) >> 8;
        byte green = (col565 & 0b0000011111100000) >> 3;
        byte blue  = (col565 & 0b0000000000011111) << 2;
        return RGB_24(red,green,blue);
    }

    /// read the red, green and blue color value from a position
    void readPixel (int x, int y, byte &r, byte &g, byte &b);

    // x and y are not columns and rows
    void drawString (Font & fnt, const int & x,  const int & y,  int col, const char * str, int len, int bgColor = -1);
    void drawString (Font & fnt, int & x,  int & y,  int col, const char * str, int len, int bgColor = -1);

    void line (int x,  int y, int tox,  int toy,  int col);
    void rect (int x,  int y, int tox,  int toy,  int col, int fillCol = -1);
    void circle (int x0, int y0, int radius, int col, int fillCol = -1);

    void drawPixel (int x,  int y,  int col);
    void clear (int fillCol = -1);
    
    /**
     * draw a Sprite.
     * @param x the x position of the upper left corner of the sprite.
     * @param y the y position of the upper left corner of the sprite.
     * @param bmp a 16bit, 24bit or 32bit color image
     */
    void drawSprite (int x, int y, const Sprite & bmp);
    
    /**
     * use alpha as a wighted addition to the difference of color channels.
     * This is not a typical practice to handle alpha!
     * 
     * @param from is the color with alpha value, we want to add
     * @param to is the color, where we want to add some additional color
     */
    void blend (Bit32 * from, Bit32 * to);
    
protected:
    int _xAlign;
    
};

#endif
