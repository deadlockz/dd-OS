#ifndef __PsDumpCall_HPP__
#define __PsDumpCall_HPP__ 1

#include "exceptions/Calls.hpp"
#include "exceptions/ServiceRoutine.hpp"

class PsDumpCall : public ServiceRoutine {
    
private:
    
    /// it is private to avoid copying.
    PsDumpCall (const PsDumpCall &copy) = default;
    PsDumpCall () = default;
    
public:

    enum CONFIG {
        ID = Calls::ID::PS_DUMP
    };

    /// Singelton: get the instance.
    static PsDumpCall * me() {
        static PsDumpCall me;
        return &me;
    }

    /**
     * plugin.
     * this adds the ServiceRoutine to the Calls
     */
    void plugin () {
        Calls::assign(PsDumpCall::CONFIG::ID, me());
    }
    
    /// remove this routine from Call map
    void plugout () {
        Calls::unassign(PsDumpCall::CONFIG::ID);
    }

    /**
     * trigger.
     * The Calls::report() tries to trigger this ServiceRoutine.
     */
    void trigger ();
};

#endif
