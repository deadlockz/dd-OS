#ifndef __GIC_include__
#define __GIC_include__ 1

#include "devices/Peripherals.hpp"

/**
 * GIC.
 * The General Interrupt Controller makes it possible to modify
 * the interrupt handling and detecting, which interrupt is occured.
 * It is the default GIC-400 (GICv2) Controller on Pi4.
 * For GIC-400 see https://github.com/rsta2/circle/blob/master/lib/interruptgic.cpp
 * and the GIC-400 docu: this is a good entry to implement GIC
 * instead of "LIC".
 * 
 * https://www.raspberrypi.org/forums/viewtopic.php?t=251022
 * https://github.com/rsta2/circle/blob/master/lib/interruptgic.cpp
 * 
 * @see Dispatcher.hpp
 */

// conversion from device tree source (unused in hhuOS - we still add +32)

#define GIC_PPI(n)      (16 + (n))  // private per core
#define GIC_SPI(n)      (32 + (n))  // Shared between cores Peripheral Interrupt

namespace GIC {

    enum ADDR {
        BASE = Peripherals::ADDR::GIC,
        
        /// Distributor registers
        GICD_BASE   = BASE + 0x1000,
        CTLR        = GICD_BASE,
        ENABLE      = GICD_BASE + 0x100,
        DISABLE     = GICD_BASE + 0x180,
        PEND_CLR    = GICD_BASE + 0x280,
        ACTIVE_CLR  = GICD_BASE + 0x380,
        PRIO        = GICD_BASE + 0x400,
        TARGET      = GICD_BASE + 0x800,
        CFG         = GICD_BASE + 0xc00,
        
        /// CPU interface Controller
        GICC_BASE   = BASE + 0x2000,
        GICC_CTLR   = GICC_BASE,
        GICC_PRIO   = GICC_BASE + 0x004,
        GICC_ACK    = GICC_BASE + 0x00c,
        GICC_EOI    = GICC_BASE + 0x010
    };

    enum CONFIG {
        IRQS = 192,   ///< 16 SGI + 16 PPI + 16 ARMC peripheral ... + 64 GPU ... + ethernet
        SPURIOUS = 1023
    };
    
    /**
     * this init code is primary coming from
     * https://github.com/rsta2/circle/blob/master/lib/interruptgic.cpp
     * and with GNU General Public License
     * Copyright (C) 2019-2020  R. Stange <rsta2@o2online.de>
     */
    void init();
    
    /**
     * allow.
     * 
     * set (= unmask) bit for an IRQ. the ISR assign to this interrupt will been triggered.
     * @param spi the IRQ (Shared Peripheral Interrupt)
     */
    void allow (unsigned int spi);

    /**
     * forbid.
     * 
     * clear (= mask) bit for an IRQ. 
     * @param spi the IRQ (Shared Peripheral Interrupt)
     */
    void forbid (unsigned int spi);
    
    /**
     * Get Acknowledged (pending?) IRQ from CPU.
     */
    unsigned int pending ();
    
    /**
     * clear pending IRQ.
     * 
     * if an interrupt/exception occurs and the ISR is done, you have to clear
     * the pending IRQ.
     * @param spi the IRQ (Shared Peripheral Interrupt)
     */
    void clear (unsigned int spi);
}

#endif
