#include <cstdint> // the uint32_t stuff is cool

#include "OS.hpp"
#include "devices/VESA.hpp"
#include "exceptions/Dispatcher.hpp"
#include "exceptions/Bluescreen.hpp"
#include "libs/CharTool.hpp"

#include "threads/Scheduler.hpp"
#include "devices/Power.hpp"
#include "devices/Serial.hpp"

// if you uncomment this, this mem area is shown instead of the SP area
//#define DEBUGADDRESS 0x800000

// normally with just hardware addr only 35bit are relevant on Pi4
//#define DISPLAYADDRMASK 0x0000000fffffffff  // 36bit
#define DISPLAYADDRMASK   0x00000000ffffffff  // 32bit

void bluescreen (unsigned long type, unsigned long spi) {
    uint64_t sp = os_regs[31];

#ifndef DEBUGADDRESS
    uint64_t mem = sp;
#else
    uint64_t mem = DEBUGADDRESS;
#endif

    int64_t addr;
    int addrOff = 0;
    int i;
    int base = 16;
    bool interacting = true;

    for (;;) {
        VESA::me().offset(VESA::me()._yres, false);
        VESA::me().setColor(VESA::RGB::white, VESA::RGB::blue);
        VESA::me().clear(VESA::RGB::blue);
        
        VESA::me().setpos(27, 6, 27);
        for (i=0; i<10; ++i) {
            if(i==8) VESA::me().setColor(VESA::RGB::black, VESA::RGB::lightblue);
            VESA::me().print("x").print((long)i).print(": ").print((long)(os_regs[i]), base).print("\n");
            VESA::me().setColor(VESA::RGB::white, VESA::RGB::blue);
        }
        VESA::me().setpos(51, 1, 51);
        for (i=10; i<29; ++i) {
            VESA::me().print("x").print((long)i).print(": ").print((long)(os_regs[i]), base);
            VESA::me().setpos(51, i-8, 51);
        }
        VESA::me().setColor(VESA::RGB::black, VESA::RGB::yellow);
        VESA::me().print(" FP: ").print(os_regs[29]).print("\n");
        VESA::me().setColor(VESA::RGB::lightgray, VESA::RGB::gray);
        VESA::me().print(" LR: ").print(os_regs[30]).print("\n");
        VESA::me().setColor(VESA::RGB::white, VESA::RGB::blue);
        
        VESA::me().setpos(0, 0, 0);
        VESA::me().print("OS EXCEPTION\n");

        unsigned long esr  = os_regs[34];
        unsigned long spsr = os_regs[33];
        
        switch (esr>>26) {
            case 0b000000: VESA::me().print("total unknown"); break;
            case 0b000001: VESA::me().print("Trapped WFI/WFE"); break;
            case 0b000111: VESA::me().print("Trapped access to SIMD/FP"); break;
            case 0b001000: VESA::me().print("Trapped VMRS access"); break;
            case 0b001110: VESA::me().print("Illegal execution state"); break;

            case 0b010001: VESA::me().print("SVC instuction execution in 32bit"); break;
            case 0b010101: 
                VESA::me().print("EL1 Supervisor CALL #")
                    .print((unsigned long long) (esr & 0x0000FFFF), 10)
                    .print(" (without service routine?)");
                break;
            case 0b010010: VESA::me().print("HVC instuction execution in 32bit"); break;
            case 0b010110:
                VESA::me().print("EL2 Hypervisor CALL #")
                    .print((unsigned long long) (esr & 0x0000FFFF), 10)
                    .print(" (without service routine?)");
                break;
            case 0b010011: VESA::me().print("SMC instuction execution in 32bit"); break;
            case 0b010111: VESA::me().print("SMC instuction execution in 64bit"); break;

            case 0b011000: VESA::me().print("Trapped msr, mrs or System instruction execution"); break;
            case 0b011111: VESA::me().print("Implementation defined exception to EL3"); break;
            
            case 0b100000: VESA::me().print("Instruction abort from lower EL"); break;
            case 0b100001: VESA::me().print("Instruction abort from same EL"); break;

            case 0b100010: VESA::me().print("PC alignment fault"); break;
            case 0b100110: VESA::me().print("SP alignment fault"); break;

            case 0b100100:
            case 0b100101:
                switch((esr>>2) & 0x3) {
                    case 0: VESA::me().print("Data abort. Address size fault"); break;
                    case 1: VESA::me().print("Data abort. Translation fault"); break;
                    case 2: VESA::me().print("Data abort. Access flag fault"); break;
                    case 3: VESA::me().print("Data abort. Permission fault"); break;
                }
                switch (esr & 0x3) {
                    case 0: VESA::me().print(" at level 0"); break;
                    case 1: VESA::me().print(" at level 1"); break;
                    case 2: VESA::me().print(" at level 2"); break;
                    case 3: VESA::me().print(" at level 3"); break;
                }
                break;
            
            case 0b101000: VESA::me().print("Trapped floating point exception 32bit"); break;
            case 0b101100: VESA::me().print("Trapped floating point exception 64bit"); break;
            case 0b101111: VESA::me().print("SError interrupt"); break;
            
            case 0b110000: VESA::me().print("Breakpoint exception from lower EL"); break;
            case 0b110001: VESA::me().print("Breakpoint exception from same EL"); break;
            case 0b110010: VESA::me().print("Software Step exception from lower EL"); break;
            case 0b110011: VESA::me().print("Software Step exception from same EL"); break;
            case 0b110100: VESA::me().print("Watchpoint exception from lower EL"); break;
            case 0b110101: VESA::me().print("Watchpoint exception from same EL"); break;
            case 0b111000: VESA::me().print("BKPT instruction exception 32bit"); break;
            case 0b111100: 
                VESA::me().print("BRK instruction exception 64bit #")
                    .print((unsigned long long) (esr & 0x0000FFFF), 10);
                break;

        }
        VESA::me().print("\n\n");
        
        if (type == Dispatcher::TYPES::FIQ) {
            VESA::me().print("type:           FIQ, SPI=").print(spi,10).print("\n");
        } else if (type == Dispatcher::TYPES::IRQ) {
            VESA::me().print("type:           IRQ, SPI=").print(spi,10).print("\n");
        } else if (type == Dispatcher::TYPES::SERROR) {
            VESA::me().print("type:           SError (asynchronous)\n");
        } else if (type == Dispatcher::TYPES::DEBUG) {
            VESA::me().print("type:           DEBUG (synchronous)\n");
        }
        VESA::me().setColor(VESA::RGB::lightred, VESA::RGB::black);
        VESA::me().print("fault addr.:    ").print(os_regs[35]).print("\n");
        VESA::me().setColor(VESA::RGB::lightgreen, VESA::RGB::black);
        VESA::me().print("instr. addr.:   ").print(os_regs[32]).print("\n");
        VESA::me().setColor(VESA::RGB::white, VESA::RGB::blue);
        VESA::me().print("syndrom:        ").print(esr).print("\n");
        VESA::me().print("SPSR:           ").print(spsr).print("\n");
        VESA::me().print("system control: ").print(os_regs[36]).print("\n");
        VESA::me().setColor(VESA::RGB::sun, VESA::RGB::brown);
        VESA::me().print("old SP:         ").print(sp).print("\n");
        VESA::me().setColor(VESA::RGB::white, VESA::RGB::blue);
        VESA::me().print("Bluescreen SP:  ").print(os_regs[37]).print("\n");
        
        VESA::me().print("\n\n\n\n\n\nSAVED PROGRAM STATUS DETAILS\n\n");
        
        VESA::me().print("Flags:               ");
        if ((spsr>>31) == 1) VESA::me().print("N ");
        if ((spsr>>30)%2 == 1) VESA::me().print("Z ");
        if ((spsr>>29)%2 == 1) VESA::me().print("C ");
        if ((spsr>>28)%2 == 1) VESA::me().print("oVerflow ");
        if ((spsr>>21)%2 == 1) VESA::me().print("Step ");
        if ((spsr>>20)%2 == 1) VESA::me().print("ILL");
        
        VESA::me().print("\nDAIF mask:           ");
        if ((spsr>>9)%2 == 1) VESA::me().print("1"); else VESA::me().print("o");
        if ((spsr>>8)%2 == 1) VESA::me().print("1"); else VESA::me().print("o");
        if ((spsr>>7)%2 == 1) VESA::me().print("1"); else VESA::me().print("o");
        if ((spsr>>6)%2 == 1) VESA::me().print("1"); else VESA::me().print("o");
        VESA::me().print("\nExecution state was: ");
        if ((spsr>>4)%2 == 1) VESA::me().print("32bit\n"); else VESA::me().print("64bit\n");
        VESA::me().print("Execution level was: ");
        switch ((spsr>>2) & 0x3) {
            case 0: VESA::me().print("EL0"); break;
            case 1: VESA::me().print("EL1"); break;
            case 2: VESA::me().print("EL2"); break;
            case 3: VESA::me().print("EL3"); break;
        }
        if (spsr%2 == 1) VESA::me().print("h (using SP from ELn)\n\n"); else VESA::me().print("t (using the USER level SP)\n\n");

        VESA::me().print("DISPLAY MEMORY:      ")
            .print(mem - 0x180 + addrOff)
            .print(" till ")
            .print(mem + 0x180 + addrOff);
        
        uint64_t spCutted = mem + addrOff;
        if (spCutted >= 0x1000000) {
            spCutted = spCutted >> 16;
            spCutted = spCutted << 16;
            spCutted = mem - spCutted;
        }
        if (base == 16) {
            VESA::me().print(" as hex");
        } else if (base == 0) {
            VESA::me().print(" as double");
        } else {
            VESA::me().print(" as integer");
        }
        
        VESA::me().setpos(0, 27, 0);
        for (addr = - 0x180; addr < - 0x80; addr += 0x8) {
            unsigned long long content = *((unsigned long long *)(mem - addr + addrOff));
            if ((mem - addr + addrOff) == os_regs[29]) {
                VESA::me().setColor(VESA::RGB::black, VESA::RGB::yellow);
            } else if ((mem - addr + addrOff) == sp) {
                VESA::me().setColor(VESA::RGB::sun, VESA::RGB::brown);
            }
            VESA::me().print(
                (spCutted - addr) & DISPLAYADDRMASK,
                16,
                true
            ).print(":");
            VESA::me().setColor(VESA::RGB::white, VESA::RGB::blue);
            
            if (content == os_regs[35]) {
                VESA::me().setColor(VESA::RGB::lightred, VESA::RGB::black);
            } else if (content == os_regs[32]) {
                VESA::me().setColor(VESA::RGB::lightgreen, VESA::RGB::black);
            } else if (content == os_regs[30]) {
                VESA::me().setColor(VESA::RGB::lightgray, VESA::RGB::gray);
            } else if (content == os_regs[8]) {
                VESA::me().setColor(VESA::RGB::black, VESA::RGB::lightblue);
            }
            VESA::me().print((long)content, base, true).print("\n");
            VESA::me().setColor(VESA::RGB::white, VESA::RGB::blue);
        }
        VESA::me().setpos(27, 27, 27);
        for (addr = - 0x80; addr < 0x80; addr += 0x8) {
            unsigned long long content = *((unsigned long long *)(mem - addr + addrOff));
            if ((mem - addr + addrOff) == os_regs[29]) {
                VESA::me().setColor(VESA::RGB::black, VESA::RGB::yellow);
            } else if ((mem - addr + addrOff) == sp) {
                VESA::me().setColor(VESA::RGB::sun, VESA::RGB::brown);
            }
            VESA::me().print(
                (spCutted - addr) & DISPLAYADDRMASK,
                16,
                true
            ).print(":");
            VESA::me().setColor(VESA::RGB::white, VESA::RGB::blue);

            if (content == os_regs[35]) {
                VESA::me().setColor(VESA::RGB::lightred, VESA::RGB::black);
            } else if (content == os_regs[32]) {
                VESA::me().setColor(VESA::RGB::lightgreen, VESA::RGB::black);
            } else if (content == os_regs[30]) {
                VESA::me().setColor(VESA::RGB::lightgray, VESA::RGB::gray);
            } else if (content == os_regs[8]) {
                VESA::me().setColor(VESA::RGB::black, VESA::RGB::lightblue);
            }
            VESA::me().print((long)content, base, true).print("\n");
            VESA::me().setColor(VESA::RGB::white, VESA::RGB::blue);
        }
        VESA::me().setpos(54, 27, 54);
        i=1;
        for (addr = 0x80; addr < 0x180; addr += 0x8) {
            unsigned long long content = *((unsigned long long *)(mem - addr + addrOff));
            if ((mem - addr + addrOff) == os_regs[29]) {
                VESA::me().setColor(VESA::RGB::black, VESA::RGB::yellow);
            } else if ((mem - addr + addrOff) == sp) {
                VESA::me().setColor(VESA::RGB::sun, VESA::RGB::brown);
            }
            VESA::me().print(
                (spCutted - addr) & DISPLAYADDRMASK,
                16,
                true
            ).print(":");
            VESA::me().setColor(VESA::RGB::white, VESA::RGB::blue);

            if (content == os_regs[35]) {
                VESA::me().setColor(VESA::RGB::lightred, VESA::RGB::black);
            } else if (content == os_regs[32]) {
                VESA::me().setColor(VESA::RGB::lightgreen, VESA::RGB::black);
            } else if (content == os_regs[30]) {
                VESA::me().setColor(VESA::RGB::lightgray, VESA::RGB::gray);
            } else if (content == os_regs[8]) {
                VESA::me().setColor(VESA::RGB::black, VESA::RGB::lightblue);
            }
            VESA::me().print((long)content, base, true);
            VESA::me().setpos(54, 27+(i++));
            VESA::me().setColor(VESA::RGB::white, VESA::RGB::blue);
        }

        /// Interaction --------------------------------
        OS::serial->write(
            "DEBUG:\n"
            "- MEM addr [-/b/B/1]=back or [+/f/F/2]=forward (0x80, 0x1_0000, 0x10_0000, 0x100_0000)\n"
            "- display as [d]ouble, [h]ex, [i]nteger\n"
            "- display [s]tack\n- do [n]ext instr. (on break) or [q]uit to syndrom address\n"
            "- [r]eset, power [o]ff, [p]s or next [t]hread switch.\n"        
        );
        VESA::me().setpos(0, 12, 0);
        interacting = true;
        while (interacting) {
            OS::serial->loadOutputFifo();
            if (OS::serial->readReady()) {
                unsigned char ch = OS::serial->read();
                if (ch == 'h') {
                    OS::serial->write("hex\n\r");
                    base = 16;
                    interacting = false;
                } else if (ch == 'i') {
                    OS::serial->write("integer\n\r");
                    base = 10;
                    interacting = false;
                } else if (ch == 'd') {
                    OS::serial->write("double\n\r");
                    base = 0;
                    interacting = false;
                } else if (ch == '-') {
                    OS::serial->write("display memory -128 Byte\n\r");
                    addrOff -= 0x80;
                    interacting = false;
                } else if (ch == '+') {
                    OS::serial->write("display memory +128 Byte\n\r");
                    addrOff += 0x80;
                    interacting = false;
                } else if (ch == 'b') {
                    OS::serial->write("display memory -64k Byte\n\r");
                    addrOff -= 0x10000;
                    interacting = false;
                } else if (ch == 'f') {
                    OS::serial->write("display memory +64k Byte\n\r");
                    addrOff += 0x10000;
                    interacting = false;
                } else if (ch == 'B') {
                    OS::serial->write("display memory -1MB\n\r");
                    addrOff -= 0x100000;
                    interacting = false;
                } else if (ch == 'F') {
                    OS::serial->write("display memory +1MB\n\r");
                    addrOff += 0x100000;
                    interacting = false;
                } else if (ch == '1') {
                    OS::serial->write("display memory -16MB\n\r");
                    addrOff -= 0x1000000;
                    interacting = false;
                } else if (ch == '2') {
                    OS::serial->write("display memory +16MB\n\r");
                    addrOff += 0x1000000;
                    interacting = false;
                } else if (ch == 's') {
                    OS::serial->write("display memory of old SP\n\r");
                    addrOff = 0;
                    mem = sp;
                    interacting = false;
                } else if (ch == 'n') {
                    OS::serial->write("next: syndrom address +4Byte\n\r");
                    os_regs[32] += 4;
                    VESA::me().offset(-1 * VESA::me()._yres, false);
                    return;
                } else if (ch == 'q') {
                    OS::serial->write("jump to syndrom address\n\r");
                    VESA::me().offset(-1 * VESA::me()._yres, false);
                    return;
                    
                } else if (ch == 'r') {
                    OS::serial->write("reset\n\r");
                    VESA::me().print("reset\n");
                    Power::reset();
                    
                } else if (ch == 'o') {
                    OS::serial->write("power off\n\r");
                    VESA::me().print("power off\n");
                    Power::poweroff();
                    
                } else if (ch == 'p') {
                    OS::serial->write("process snapshot\n\r");
                    Scheduler::dump();
                    
                } else if (ch == 't') {
                    OS::serial->write("next and thread switch\n\r");
                    os_regs[32] += 4;
                    Scheduler::switchover();
                    VESA::me().offset(-1 * VESA::me()._yres, false);
                    return;
                }
            }
        }
    }
}
