#include "exceptions/PsDumpCall.hpp"
#include "threads/Scheduler.hpp"

void PsDumpCall::trigger () {
    Scheduler::dump();
}

