#ifndef __RisingEdge_HPP__
#define __RisingEdge_HPP__ 1


#include "exceptions/ServiceRoutine.hpp"
#include "devices/Peripherals.hpp"
#include "devices/Gpio.hpp"

/**
 * it should be reprogrammed, because this shared peripheral
 * interrupt happends for all Rising Edge Pins and not
 * only a single pin. Thus: This ServiceRoutine should have
 * its own dispatching system!
 */
class RisingEdge : public ServiceRoutine {
    
private:
    unsigned int _pin;
    std::function<void(int *)> _foo;
    int * _para;
    
public:

    enum CONFIG {
        /// 96(start of peripheral VideoCore interrupts) + 49(GPIO 0 interrupt)
        SPI = 145
    };

    RisingEdge (unsigned int pin, int * para, std::function<void(int *)> foo) {
        _pin = pin;
        _para = para;
        _foo = foo;
    }
    
    ~RisingEdge () {
        plugout();
    }

    /**
     * plugin.
     * this adds the ServiceRoutine to the Dispatcher
     */
    void plugin () {
        _id = Dispatcher::assign(RisingEdge::CONFIG::SPI, this);
        Gpio::setRising(_pin, true);
    }
    
    /// remove this routine from Dispatcher map
    void plugout () {
        Gpio::setRising(_pin, false);
        Dispatcher::unassign(_id);
    }

    /**
     * trigger.
     * The Dispatcher tries to trigger this ServiceRoutine.
     */
    void trigger () {
        if (Gpio::getStatus(_pin) == false) return;
        _foo(_para);
        Gpio::clearStatus(_pin);
    }
};

#endif
