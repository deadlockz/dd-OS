#ifndef __SYSINFOCALL_HPP__
#define __SYSINFOCALL_HPP__ 1

#include "exceptions/Calls.hpp"
#include "exceptions/ServiceRoutine.hpp"

class SysinfoCall : public ServiceRoutine {
    
private:
    
    /// it is private to avoid copying.
    SysinfoCall (const SysinfoCall &copy) = default;
    SysinfoCall () = default;
    
public:

    enum CONFIG {
        ID = Calls::ID::SYSINFO
    };

    /// Singelton: get the instance.
    static SysinfoCall * me() {
        static SysinfoCall me;
        return &me;
    }

    /**
     * plugin.
     * this adds the ServiceRoutine to the Calls
     */
    void plugin () {
        Calls::assign(SysinfoCall::CONFIG::ID, me());
    }
    
    /// remove this routine from Call map
    void plugout () {
        Calls::unassign(SysinfoCall::CONFIG::ID);
    }
    
    /**
     * trigger.
     * The Calls::report() tries to trigger this ServiceRoutine.
     */
    void trigger ();
};

#endif
