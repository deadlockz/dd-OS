#include "OS.hpp"
#include "exceptions/ServiceRoutine.hpp"
#include "exceptions/Calls.hpp"

/// bss: inital pointers in array should be all 0
Calls::Routine Calls::callmap[Calls::CONFIG::MAXCALLS];

void Calls::init () {
    for (unsigned n = 0; n < Calls::CONFIG::MAXCALLS; ++n) {
        Calls::callmap[n].isSet = false;
    }
}

void Calls::assign (unsigned int callid, ServiceRoutine * sr) {
    Calls::callmap[callid].isSet = true;
    Calls::callmap[callid].sr = sr;
}

void Calls::unassign (unsigned int callid) {
    Calls::callmap[callid].isSet = false;
}

bool Calls::report () {
    // [34] has Exception Syndrom Register
    unsigned long esr = os_regs[34];
    unsigned int callid = (esr & 0x0000FFFF);
    switch (esr>>26) {
        case 0b010101: //supervisor call
            if (callid >= Calls::CONFIG::MAXCALLS) return false;
            break;
        case 0b010110: //hypervisor call
            if (callid >= Calls::CONFIG::MAXCALLS) return false;
            break;
        default:
            return false;
    }
    
    Calls::Routine routine = Calls::callmap[callid];
    if (routine.isSet == false) return false;
    routine.sr->trigger();
    return true;
}

void Calls::calling (unsigned int callid) {
    switch (callid) {
        case Calls::ID::SYSINFO:
            asm volatile ("svc %[flag]" : : [flag] "I" (0));
            break;
        case Calls::ID::PAUSE:
            asm volatile ("svc %[flag]" : : [flag] "I" (1));
            break;
        case Calls::ID::YIELD:
            asm volatile ("svc %[flag]" : : [flag] "I" (2));
            break;
        case Calls::ID::OYIELD:
            asm volatile ("svc %[flag]" : : [flag] "I" (3));
            break;
        case Calls::ID::PS_DUMP:
            asm volatile ("svc %[flag]" : : [flag] "I" (4));
            break;
        case Calls::ID::DUMMY0:
            asm volatile ("svc %[flag]" : : [flag] "I" (5));
            break;
        case Calls::ID::DUMMY1:
            asm volatile ("svc %[flag]" : : [flag] "I" (6));
            break;
        
        default:
            ;
    }
}
