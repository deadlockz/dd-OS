#ifndef __ServiceRoutine_include__
#define __ServiceRoutine_include__ 1

/**
 * A Service Routine.
 * Every Device with an interrupt must be an ISR. It must implement
 * a trigger and a plugin method. 
 */
class ServiceRoutine {

public:
    int _id;

    ServiceRoutine () = default;
    ServiceRoutine (const ServiceRoutine &copy) = delete;
    ServiceRoutine &operator= (const ServiceRoutine &copy) = delete;
       
    /// @todo with destructor I get a undefined reference to `atexit' on linking!!!
    virtual ~ServiceRoutine() = default;

    /**
     * plug it in.
     * to add the Service Routine to a dispatching system.
     */
    virtual void plugin () = 0;

    /**
     * trigger.
     * this method is called from the dispatching system, if
     * the service routine is needed.
     */
    virtual void trigger () = 0;
};

#endif
