#include <cstdint> // the uintX_t stuff is cool
#include "OS.hpp"
#include "exceptions/Calls.hpp"
#include "exceptions/GIC.hpp"
#include "exceptions/ServiceRoutine.hpp"
#include "exceptions/Dispatcher.hpp"
#include "exceptions/Bluescreen.hpp"

bool Dispatcher::isDispatching = false;

void dispatch (unsigned long type) {
    Dispatcher::isDispatching = true;
    bool success = false;
    if (type == Dispatcher::TYPES::DEBUG) {
        success = Calls::report();
        if (success == false) {
            bluescreen(type, GIC::CONFIG::SPURIOUS);
        }
    } else {
        unsigned int spi = GIC::pending();
        /// we have to handle and clear all in a loop
        while (spi != GIC::CONFIG::SPURIOUS) {
            success = Dispatcher::report(spi);
            if (success == false) bluescreen(type, spi);
            GIC::clear(spi);
            spi = GIC::pending();
        }
    }
    Dispatcher::isDispatching = false;
}

/// bss: inital pointers in array should be all 0
Dispatcher::Routine Dispatcher::map[Dispatcher::CONFIG::SERVICES];

void Dispatcher::init () {
    for (unsigned n = 0; n < Dispatcher::CONFIG::SERVICES; ++n) {
        Dispatcher::map[n].isSet = false;
    }
}

int Dispatcher::assign (unsigned int spi, ServiceRoutine * sr) {
    for (unsigned n = 0; n < Dispatcher::CONFIG::SERVICES; ++n) {
        if (Dispatcher::map[n].isSet == false) {
            Dispatcher::map[n].isSet = true;
            Dispatcher::map[n].spi = spi;
            Dispatcher::map[n].sr = sr;
            GIC::allow(spi);
            return n;
        }
    }
    // todo: handle fail!!
    return 6666;
}

void Dispatcher::unassign (int id) {
    unsigned int spi = Dispatcher::map[id].spi;
    Dispatcher::map[id].isSet = false;
    
    for (unsigned n = 0; n < Dispatcher::CONFIG::SERVICES; ++n) {
        if (
            Dispatcher::map[n].isSet == true && 
            Dispatcher::map[n].spi == spi 
        ) {
            // an other service routine exists for this spi
            return;
        }
    }
    // no other service routine exists
    GIC::forbid(spi);
}

bool Dispatcher::report (unsigned int spi) {
    if (spi >= GIC::CONFIG::IRQS) return false;
    bool result = false;
    
    for (unsigned n = 0; n < Dispatcher::CONFIG::SERVICES; ++n) {
        if (
            Dispatcher::map[n].isSet == true &&
            Dispatcher::map[n].spi == spi
        ) {
            Dispatcher::Routine routine = Dispatcher::map[n];
            routine.sr->trigger();
            result = true;
        }
    }
    return result;
}
