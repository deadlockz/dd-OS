#ifndef __Calls_include__
#define __Calls_include__ 1

#define breakpoint(NUMBER) asm volatile ("brk %[flag]" : : [flag] "I" (NUMBER))

#include "exceptions/ServiceRoutine.hpp"

/** Calls (a kind of software interrupt dispatcher).
 *  It maps systemcall exceptions to spezific, assinged Service Routine
 */
namespace Calls {
    
    enum ID {
        SYSINFO = 0,    ///< Displays some system infos
        PAUSE   = 1,    ///< stop Scheduler
        YIELD   = 2,    ///< force Scheduler to switchover
        OYIELD  = 3,    ///< optional Yield -> do a switchover if a preempt was stopped by a pause
        PS_DUMP = 4,    ///< dump process list
        DUMMY0  = 5,
        DUMMY1  = 6
    };

    enum CONFIG {
        MAXCALLS = 7
    };
    
    struct Routine {
        bool isSet;
        ServiceRoutine * sr;
    };
    
    extern Routine callmap[];

    void init ();
    
    /**
     * assing.
     * Add a concret Service Routine to the callmap.
     * @param callid
     * @param sr Pointer to the concret Service Routine instance.
     */
    void assign (unsigned int callid, ServiceRoutine * sr);

    /**
     * unassing.
     * Remove a concret Service Routine from the callmap.
     * @param callid
     */
    void unassign (unsigned int callid);

    /**
     * report.
     * executes the trigger if exist.
     * @return false if there is no ServiceRoutine for the callid
     */
    bool report ();

    void calling (unsigned int callid);
}

#endif
