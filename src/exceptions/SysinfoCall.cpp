#include "exceptions/SysinfoCall.hpp"

#include "OS.hpp"
#include "devices/VESA.hpp"
#include "devices/Temperature.hpp"
#include "devices/Revision.hpp"
#include "devices/Clocks.hpp"

void SysinfoCall::trigger () {
    OS::mm->dump();
    VESA::me().dblOut(Temperature::get(), 3)
        .print(" ")
        .print((char)248)
        .print("C\n");
    VESA::me().print("ARM clock rate: ")
        .print((long)Clocks::getMessured(Clocks::Device::ARM)/1000000, 10)
        .print(" MHz\n");
    VESA::me().print("Pi revision code: ")
        .print((long)Revision::get(), 16)
        .print("\n");
}

