#ifndef __Dispatcher_include__
#define __Dispatcher_include__ 1

#include <cstdint> // the uintX_t stuff is cool

#include "exceptions/ServiceRoutine.hpp"
#include "exceptions/Bluescreen.hpp"

/** dispatch function.
 * 
 * This routine is part of the [startup.S](startup_S.html) and
 * is a part of in the exception/interrupt vectors.
 * 
 * @see [startup.S](startup_S.html)
 */
extern "C" void dispatch (unsigned long type);

/**
 * Dispatcher.
 * Here is the Interrupt Dispatcher (Exception handler) stuff.
 * Every ISR has to assign to it. The VBAR_ELn has
 * pointer to "exc_vector" in [startup.S](startup_S.html) with entries to dispatch(). It
 * executes report to trigger the concret ISR.
 * 
 * https://github.com/rust-embedded/rust-raspberrypi-OS-tutorials/tree/master/13_exceptions_part2_peripheral_IRQs
 * The GICv2 in the Raspberry Pi 4, on the other hand, uses a different scheme. 
 * IRQ numbers 0..31 are for private IRQs. Those are further subdivided into 
 * SW-generated (SGIs, 0..15) and HW-generated (PPIs, Private Peripheral 
 * Interrupts, 16..31). Numbers 32..1019 are for shared hardware-generated 
 * interrupts (SPI, Shared Peripheral Interrupts). SPI 180 e.g. is
 * a PCIE HOST MSI (Message Signaled Interrupt).
 * 
 * https://github.com/rsta2/circle/blob/master/lib/interruptgic.cpp
 * 
 * @see dispatch()
 */
namespace Dispatcher {
    extern bool isDispatching;
    
    enum CONFIG {
        SERVICES = 10   ///< max number of asignable service routines!
    };

    enum TYPES {
        FIQ     = 0,
        IRQ     = 1,
        SERROR  = 2,
        DEBUG   = 3
    };

    /**
     * todo: use a linked list instead of an array... or in each field a linked list.
     * todo: make more than a single service routine possible.
     * todo: affter assing give an id back to make unasign easier for more than a single service routine.
     */ 
    struct Routine {
        bool isSet;
        unsigned int spi;
        ServiceRoutine * sr;
    };
    
    extern Routine map[];

    void init ();
    
    /**
     * assing.
     * Add a concret ISR to the map.
     * 
     * @param spi
     * @param sr Pointer to the concret ServiceRoutine instance.
     * @return id
     */
    int assign (unsigned int spi, ServiceRoutine * sr);

    /**
     * unassing.
     * Remove a concret Service Routine and remove interrupt from controller.
     * @param id the id in the map
     */
    void unassign (int id);
    
    /**
     * report.
     * execute the Service Routine trigger if exist for spi.
     * @param spi
     * @return true if there is a Service Routine for that spi
     */
    bool report (unsigned int spi);

}

#endif
