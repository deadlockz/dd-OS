#include "exceptions/GIC.hpp"
#include "devices/Peripherals.hpp"

void GIC::init() {
    // disable Distributor and CPU interface
    Peripherals::write32(GIC::ADDR::CTLR, 0);
    Peripherals::write32(GIC::ADDR::GICC_CTLR, 0);

    // disable, acknowledge and deactivate all interrupts
    for (unsigned n = 0; n < GIC::CONFIG::IRQS/32; ++n) {
        Peripherals::write32(GIC::ADDR::DISABLE     + 4*n, ~0);
        Peripherals::write32(GIC::ADDR::PEND_CLR    + 4*n, ~0);
        Peripherals::write32(GIC::ADDR::ACTIVE_CLR  + 4*n, ~0);
    }

    // direct all interrupts to core 0 (=01) with default priority a0. (00 is highest)
    for (unsigned n = 0; n < GIC::CONFIG::IRQS/4; ++n) {
        Peripherals::write32(GIC::ADDR::TARGET + 4*n, 0x01010101);
        Peripherals::write32(GIC::ADDR::PRIO + 4*n, 0xa0a0a0a0);
    }

    // config all interrupts to level triggered
    for (unsigned n = 0; n < GIC::CONFIG::IRQS/16; ++n) {
        Peripherals::write32(GIC::ADDR::CFG + 4*n, 0);
    }

    // enable Distributor
    Peripherals::write32(GIC::ADDR::CTLR, 1);

    // set Core0 interrupt prio mask threshold prios to F0.
    // if priority mask is 00 (the highest prio), cpu will never react. 
    // A0 prio is higher than F0 -> core0 must react.
    Peripherals::write32(GIC::ADDR::GICC_PRIO, 0xF0);
    // enable cpu interface
    Peripherals::write32(GIC::ADDR::GICC_CTLR, 1);
}


void GIC::allow (unsigned int spi) {
    Peripherals::write32(GIC::ADDR::ENABLE + 4 * (spi / 32), 1 << (spi % 32));
}

void GIC::forbid (unsigned int spi) {
    Peripherals::write32(GIC::ADDR::DISABLE + 4 * (spi / 32), 1 << (spi % 32));
}

unsigned int GIC::pending () {
    /** 
     * Reading the IAR do a automatic: the pending SPI is automatic acknolaged.
     * Read it a 2nd time, you will get the NEXT pending SPI and ack this, too.
     */
    return Peripherals::read32(GIC::ADDR::GICC_ACK);
}

void GIC::clear (unsigned int spi) {
    Peripherals::write32(GIC::ADDR::GICC_EOI, spi);
}

