GCCPATH = /home/tux/bin/toolchain/gcc-arm-10.2-2020.11-x86_64-aarch64-none-elf/bin/

CCFILES = \
	$(wildcard ./*.cpp) \
	$(wildcard ./devices/*.cpp) \
	$(wildcard ./libs/*.cpp) \
	$(wildcard ./exceptions/*.cpp) \
	$(wildcard ./apps/*.cpp) \
	$(wildcard ./heap/*.cpp) \
	$(wildcard ./threads/*.cpp) \
	$(wildcard ./libs/fonts/*.cpp)
OFILES = $(CCFILES:.cpp=.o)

INCLUDES = \
	-I./ -I./devices/ -I./libs/ -I./exceptions/ \
	-I./apps/ -I./heap/ -I./threads/ -I./lib/fonts/

# a -fpermissive fix the "can not cast const char * to char *" in combination with -pedantic
GCCFLAGS = \
	-pedantic -Wall -Wextra -Wno-aligned-new -O0 -g --std=gnu++14 \
	$(INCLUDES) \
	-mcpu=cortex-a72 \
	-fpermissive \
	-nostartfiles \
	-fno-rtti \
	-nostdlib \
	-Wno-write-strings -fno-stack-protector -ffreestanding -fno-use-cxa-atexit \
	-Wno-non-virtual-dtor -fno-threadsafe-statics -fno-exceptions

LDFLAGS = \
	-nostdlib -nostdinc -nostartfiles $(INCLUDES)

DELETE = rm
ASM = aarch64-none-elf-gcc
CXX = aarch64-none-elf-g++
LINKER = aarch64-none-elf-ld
OBJCPY = aarch64-none-elf-objcopy

args = `arg="$(filter-out $@,$(MAKECMDGOALS))" && echo $${arg:-${1}}`

all: clean kernel8.img

startup.o: startup.S
	$(GCCPATH)$(ASM) $(GCCFLAGS) -c startup.S -o startup.o

%.o: %.cpp
	$(GCCPATH)$(CXX) $(GCCFLAGS) -c $< -o $@

kernel8.img: startup.o $(OFILES)
	$(GCCPATH)$(LINKER) $(LDFLAGS) startup.o $(OFILES) -T link.ld -o kernel8.elf
	$(GCCPATH)$(OBJCPY) -O binary kernel8.elf ../sdcard/kernel8.img

clean:
	$(DELETE) -rf ../sdcard/kernel8.img
	$(DELETE) -rf startup.o kernel8.elf $(OFILES)

# With -g and -O0 gdb is able to decompile code (to c++) on given instruction addresses.
# Use e.g. `make debug 0x93d00` to find c++ code for that instruction address.
# An other way (but asm code): `aarch64-none-elf-obj -d kernel8.elf | less`
debug: kernel8.elf
	@echo "list *$(call args,0x80000)" | gdb kernel8.elf
