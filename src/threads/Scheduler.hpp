#ifndef __Scheduler_include__
#define __Scheduler_include__ 1

#include "libs/Queue.hpp"
#include "threads/Thread.hpp"

extern void kickoff (Thread * t);

namespace Scheduler {

    /// The chain contains the scheduled threads
    extern Queue<Thread> chain;
    
    /// a counter to give threads an id
    extern unsigned long _nextid;
    /// makes it easy to access the actual running Thread
    extern Thread * _actual;

    /// if you run(), mode NON_INIT -> RUNNING is set to true and scheduling begins.
    /// if PAUSE, the actual running thread will not be replaced by an other.
    enum MODE {
        NON_INIT = 0,
        RUNNING = 1,
        PAUSE = 2,
        PREEMPT_BUT_PAUSE = 3
    };
    extern unsigned int _mode;
    
    /// add a Thread to Scheduler
    void add (Thread * t);
    
    /**
     * marks Thread to be killed and removes it on the next switchover.
     * It deletes the Thread (and Thread deletes its Stack) and the Queue node.
     */
    void kill (Thread * t);
    /// search Thread by id and kills it
    void kill (unsigned long tid);
    
    /// check, if a task exists in chain (_id compare)
    bool exists (Thread * t);
    
    /**
     * the switch-to-thread routine used by PIT::trigger() during interrupt.
     */
    void switchover ();
    
    /// some initial sets before adding Threads and run()
    void init ();
    
    /// this starts the scheduling to work.
    void run ();
    
    /// runs the dump() for each Thread in chain
    void dump ();

    /// it is a kind of looping delay, which let a thread wait for getting cpu time
    void snooze ();
}

#endif
