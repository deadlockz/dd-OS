#include <cstdint> // the uintX_t stuff is cool

#include "OS.hpp"
#include "heap/MM.hpp"
#include "devices/CPU.hpp"
#include "devices/PIT.hpp"
#include "devices/VESA.hpp"
#include "threads/Thread.hpp"
#include "threads/Scheduler.hpp"

const int Thread::MAXNAMELEN;

Thread::Thread (bool cooperative) {
    _id = Scheduler::_nextid++;
    _mode = (cooperative == true ? Thread::MODE::co_fresh : Thread::MODE::fresh);
    _stackPointer = new uint64_t[MM::APP_STACK];
    _stack = _stackPointer;
    _stackPointer += MM::APP_STACK;
    _time = 0;
}

Thread::~Thread () {
    delete[] _stack;
}

void Thread::start () {
    _time = OS::systime;
    main();
    
    if (this->_mode == Thread::MODE::cooperate) {
        // finally a "done" cooperative Thread has to wakeup the scheduler
        Scheduler::_mode = Scheduler::MODE::RUNNING;
    }
    // Thread suizid
    Scheduler::kill(this);
    
    // a powersave endless loop until Scheduler removes me
    while (true) CPU::me().wait();
}

void Thread::dump () {
    static char buff[MAXNAMELEN];
    PIT::me()->stampToTime(OS::systime - _time, buff);
    VESA::me()
        .print(" ").print(_id, 10).print(" ")
        .print(buff);
    switch (_mode) {
        case MODE::fresh:
            VESA::me().print(" fresh     ");
            break;
        case MODE::young:
            VESA::me().print(" young     ");
            break;
        case MODE::normal:
            VESA::me().print(" normal    ");
            break;
        case MODE::co_fresh:
            VESA::me().print(" co. fresh ");
            break;
        case MODE::co_young:
            VESA::me().print(" co. young ");
            break;
        case MODE::cooperate:
            VESA::me().print(" cooperate ");
            break;
        default:
            VESA::me().print(" KILLED    ");
    }
    VESA::me().print("'").print(name()).print("'\n");
}
