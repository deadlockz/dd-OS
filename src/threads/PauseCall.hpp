#ifndef __PAUSECALL_HPP__
#define __PAUSECALL_HPP__ 1

#include "exceptions/Calls.hpp"
#include "exceptions/ServiceRoutine.hpp"
#include "threads/Scheduler.hpp"

/**
 * CallRoutine to pause Scheduler to do a switchover().
 * 
 * A Calls::calling(Calls::ID::PAUSE) triggers this routine.
 */
class PauseCall : public ServiceRoutine {
    
private:
    
    /// it is private to avoid copying.
    PauseCall (const PauseCall &copy) = default;
    PauseCall () = default;
    
public:

    enum CONFIG {
        ID = Calls::ID::PAUSE
    };

    /// Singelton: get the instance.
    static PauseCall * me() {
        static PauseCall me;
        return &me;
    }

    /**
     * plugin.
     * this adds the ServiceRoutine to the Calls
     */
    void plugin () {
        Calls::assign(PauseCall::CONFIG::ID, me());
    }

    /**
     * trigger.
     * The Calls::report() tries to trigger this ServiceRoutine.
     */
    void trigger () {
        if (Scheduler::_mode != Scheduler::MODE::PREEMPT_BUT_PAUSE) {
            Scheduler::_mode = Scheduler::MODE::PAUSE;
        } // else: let it still in mode PREEMPT_BUT_PAUSE
    }
};

#endif
