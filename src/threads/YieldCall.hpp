#ifndef __YIELDCALL_HPP__
#define __YIELDCALL_HPP__ 1

#include "exceptions/Calls.hpp"
#include "exceptions/ServiceRoutine.hpp"
#include "threads/Scheduler.hpp"

/**
 * CallRoutine to force a switchover() by Scheduler.
 * attention: a YIELD Call is additionally and did not garanties a
 * full Thread timespan. Maybe a while to wait on a changing OS::systime
 * is a good alternative.
 * 
 * A Calls::calling(Calls::ID::YIELD) triggers this routine.
 */
class YieldCall : public ServiceRoutine {
    
private:
    
    /// it is private to avoid copying.
    YieldCall (const YieldCall &copy) = default;
    YieldCall () = default;
    
public:

    enum CONFIG {
        ID = Calls::ID::YIELD
    };

    /// Singelton: get the instance.
    static YieldCall * me() {
        static YieldCall me;
        return &me;
    }

    /**
     * plugin.
     * this adds the ServiceRoutine to the Calls
     */
    void plugin () {
        Calls::assign(YieldCall::CONFIG::ID, me());
    }

    /**
     * trigger.
     * The Calls::report() tries to trigger this ServiceRoutine.
     */
    void trigger () {
        Scheduler::_mode = Scheduler::MODE::RUNNING;
        Scheduler::switchover();
    }
};

#endif
