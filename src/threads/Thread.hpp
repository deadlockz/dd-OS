#ifndef __THREAD_HPP__
#define __THREAD_HPP__ 1

#include <cstdint> // the uintX_t stuff is cool

/**
 * Thread.
 * Every App in the Scheduler must be a Thread.
 */
class Thread {

private:
    /// we do not want copying instances
    Thread (const Thread &copy) = delete;
    Thread &operator=(const Thread &other) = delete;

public:
    enum MODE {
        fresh,      ///< Thread needs to be entered start() via kickoff()
        young,      ///< Thread enters start()
        normal,     ///< Thread has entered main()
        co_fresh,   ///< for a cooperating Thread
        co_young,
        cooperate,  ///< entering this type of Thread pauses the Scheduler
        /**
         * Thread is marked to be killed and halted. If Scheduler want
         * to switch to it, it will delete it.
         */
        killed
    };
    
    MODE _mode;
    
    /// Ident the thread with _id
    unsigned long   _id;
    /// Ident the thread/App via name
    char *          _name;
    /// marks the thread beginning
    unsigned long   _time;
    
    /**
     * Stack array pointed to the last+1 Entry.
     * 
     * - Entries MM::APP_STACK to 38 is Stack Area
     * - Entries 37..0 stores registers for Scheduling and Bluescreen
     */
    uint64_t * _stackPointer;
    /// Stack array
    uint64_t * _stack;

    static const int MAXNAMELEN = 100;

    /**
     * constructor.
     * an new stack is been created of size MM::APP_STACK. The Address pointer is been
     * modified (+ APP_STACK), because the stack has to point to the ende of the allocated Block.
     * 
     * @param cooperative if it is true, the scheduler makes a pause everytime it enters this thread
     */
    Thread (bool cooperative = false);

    /**
     * destructor.
     * Scheduler only the Thread/Base and not the derivates!
     */
    virtual ~Thread ();

    /**
     * initial start via kickoff() with a final kill() in it.
     */
    void start ();
    
    /// the main() is started from start()
    virtual void main () = 0;

    /// name. get a const char pointer to the name of the Thread. It is useful for the dump() method.
    virtual char * name () = 0;

    /**
     * dump.
     * print infos about the process.
     * @see Shell.hpp *ps* command
     */
    void dump ();

};

#endif
