#include <cstddef> // we need it for size_t and sizeof()

#include "OS.hpp"
#include "heap/MM.hpp"
#include "devices/CPU.hpp"
#include "threads/Scheduler.hpp"
#include "threads/Thread.hpp"
#include "exceptions/Calls.hpp" // breakpoint and "System" calls
#include "threads/YieldCall.hpp"
#include "threads/PauseCall.hpp"
#include "threads/OYieldCall.hpp"
#include "libs/Queue.hpp"

Queue<Thread> Scheduler::chain;
unsigned long Scheduler::_nextid;
Thread * Scheduler::_actual;
unsigned int Scheduler::_mode;

void kickoff (Thread * t) {
    if (t->_mode == Thread::MODE::co_fresh) {
        t->_mode = Thread::MODE::co_young;
        Scheduler::_mode = Scheduler::MODE::PAUSE;
    } else {
        t->_mode = Thread::MODE::young;
    }
    t->start();
}

void Scheduler::init () {
    Scheduler::_mode = Scheduler::MODE::NON_INIT;
    Scheduler::_nextid = 1;
    Scheduler::_actual = nullptr;
    YieldCall::me()->plugin();
    OYieldCall::me()->plugin();
    PauseCall::me()->plugin();
}

void Scheduler::run () {
    Scheduler::_mode = Scheduler::MODE::RUNNING;
    // a powersave endless loop
    while (true) CPU::me().wait();
}

void Scheduler::add (Thread * t) {
    Scheduler::snooze();
    chain.push(t);
}

void Scheduler::kill (Thread * t) {
    t->_mode = Thread::MODE::killed;
}

void Scheduler::kill (unsigned long tid) {
    CPU::me().disable_int();
    Thread * it = Scheduler::chain.warp();
    for (unsigned i=0; i < Scheduler::chain.size(); ++i) {
        if (it->_id == tid) {
            it->_mode = Thread::MODE::killed;
            CPU::me().enable_int();
            return;
        }
        it = Scheduler::chain.warp();
    }
    CPU::me().enable_int();
}

bool Scheduler::exists (Thread * t) {
    Scheduler::snooze();
    Thread * it = Scheduler::chain.warp();
    for (unsigned i=0; i < Scheduler::chain.size(); ++i) {
        if (it->_id == t->_id) return true;
        it = Scheduler::chain.warp();
    }
    return false;
}

void Scheduler::dump () {
    Thread * it = Scheduler::chain.warp();
    for (unsigned i=0; i < Scheduler::chain.size(); ++i) {
        it->dump();
        it = Scheduler::chain.warp();
    }
}

void Scheduler::switchover () {
    
    if (Scheduler::_mode == Scheduler::MODE::PREEMPT_BUT_PAUSE) return;
    
    if (Scheduler::_mode == Scheduler::MODE::PAUSE) {
        Scheduler::_mode = Scheduler::MODE::PREEMPT_BUT_PAUSE;
        return;
    }
    
    if (Scheduler::_mode == Scheduler::MODE::NON_INIT || Scheduler::chain.isEmpty()) {
        Scheduler::_actual = nullptr;
        return;
    }

    Thread * actual = Scheduler::_actual;
    
    // get the next running Thread from to head of the chain
    Thread * n = Scheduler::chain.first();
    
    // a marker to find a Thread, which is not killed or in an other "not runnable" mode
    bool unfineThread = true;
    
    while (unfineThread) {
        if (n->_mode == Thread::MODE::killed) {
            Scheduler::chain.pop();
            delete n;
            n = Scheduler::chain.first();
        } else {
            unfineThread = false;
        }
        // the Shell Thread should always be there and never killed
        //-> while is not endless
    }

    Thread * nextask = n;
    
    // this could happend in a very initial state or after exit/kill a task
    if (Scheduler::_actual == nullptr) actual = nextask;

    if (actual->_mode == Thread::MODE::young) {
        actual->_mode = Thread::MODE::normal;
    }
    
    if (actual->_mode == Thread::MODE::co_young) {
        actual->_mode = Thread::MODE::cooperate;
    }
    
    for (int i=0; i < 104; ++i) {
        // save actual regs to the END! of the actual Task Stack
        actual->_stack[i] = os_regs[i];
        // and restore regs coming from the next task
        os_regs[i] = nextask->_stack[i];
    }
    
    if (
        nextask->_mode == Thread::MODE::fresh ||
        nextask->_mode == Thread::MODE::co_fresh
    ) {
        os_regs[29] = (uint64_t)(nextask->_stackPointer); // frame pointer
        os_regs[31] = (uint64_t)(nextask->_stackPointer); // sp
        os_regs[32] = (uint64_t)kickoff;                  // elr_EL1
        os_regs[0] = (uint64_t)(nextask);                 // parameter for kickoff
    }
    
    Scheduler::_actual = nextask;
    chain.warp(); // the next running Thread is at the end of chain!

    if (nextask->_mode == Thread::MODE::cooperate) {
        Scheduler::_mode = Scheduler::MODE::PAUSE;
    }
}

void Scheduler::snooze () {
    // active waiting "a bit" on a new scheduling event
    unsigned long now = OS::systime;
    while (OS::systime == now);
}

