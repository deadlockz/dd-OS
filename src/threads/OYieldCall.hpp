#ifndef __OYIELDCALL_HPP__
#define __OYIELDCALL_HPP__ 1

#include "exceptions/Calls.hpp"
#include "exceptions/ServiceRoutine.hpp"
#include "threads/Scheduler.hpp"

/**
 * Optional Yield.
 * Like YieldCall, but it only triggers the Scheduler to work,
 * if Schdeuler was triggered by "preempt" timer during a pause.
 * It will never un-pause the Scheduler, if Scheduler is just "pause"
 * without a preempt fired before!
 */
class OYieldCall : public ServiceRoutine {
    
private:
    
    /// it is private to avoid copying.
    OYieldCall (const OYieldCall &copy) = default;
    OYieldCall () = default;
    
public:

    enum CONFIG {
        ID = Calls::ID::OYIELD
    };

    /// Singelton: get the instance.
    static OYieldCall * me() {
        static OYieldCall me;
        return &me;
    }

    /**
     * plugin.
     * this adds the ServiceRoutine to the Calls
     */
    void plugin () {
        Calls::assign(OYieldCall::CONFIG::ID, me());
    }

    /**
     * trigger.
     * The Calls::report() tries to trigger this ServiceRoutine.
     */
    void trigger () {
        if (Scheduler::_mode == Scheduler::MODE::PREEMPT_BUT_PAUSE) {
            Scheduler::_mode = Scheduler::MODE::RUNNING;
            Scheduler::switchover();
        }
    }
};

#endif
