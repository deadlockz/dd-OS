#ifndef __MAIN_include__
#define __MAIN_include__ 1

#include <cstdint> // the uintX_t stuff is cool

extern "C" int main (uint64_t startAddr, uint64_t endAddr);

#endif