#ifndef __MMBitmask_include__
#define __MMBitmask_include__ 1

#include <cstdint> // the uintX_t stuff is cool
#include "heap/MM.hpp"
#include "devices/VESA.hpp" // dump
#include "libs/Font.hpp" // dump

#include "OS.hpp" // debug
#include "libs/CharTool.hpp" // debug

/**
 * 4GB Heap Memory Management via bitmask.
 * A 500 kB array has a Bit=1, if a 1024Byte
 * memory area is used (Block). Additionaly a Bit=0 border
 * is used to mark an end. We call the bitmask DIR.
 * It is splitted into 32bit (=4Byte) areas. Each area
 * has a dirpos (=index). Each bit in a single area represents
 * a Block (=dirbit as subindex).
 */
class MMBitmask : public MM {

private:
    uint64_t _usedBlocks;
    int64_t _endPos;
    
    int64_t _holePos;      /// < start pos in dir for mem hole (framebuffer)
    int64_t _holeLength;

    enum CONFIG {
        Blocksize   = 1024,     ///< if 1024, then Addralign = 10bits
        Addralign   = 10,
        BYTES       = 4,
        DIRstart    = MM::MEM0_START,
        DIRsize     = (512*1024)/CONFIG::BYTES,
        DUMPLIMIT   = 16,       ///< does not dump more than DUMPLIMIT blocks on the screen and begins to scale
        DUMPLIMIT2  = 64       ///< does not dump more than DUMPLIMIT2 blocks on the screen and begins to scale
    };
    
    MMBitmask (const MMBitmask &) = delete;
    void operator= (MMBitmask const &) = delete;
    
    /// get the start bit in bitmask for a given address
    uint64_t addr2bit (void * addr) {
        uint64_t input = (uint64_t) addr;
        input -= CONFIG::DIRstart + CONFIG::DIRsize * CONFIG::BYTES;
        input = input >> CONFIG::Addralign; // input is the bit in the complete bitmask, now!
        return input;
    }

    /// get the address for a given bit in bitmask
    void * bit2addr (uint64_t input) {
        input = input << CONFIG::Addralign;
        input += CONFIG::DIRstart + CONFIG::DIRsize * CONFIG::BYTES;
        return (void *) input;
    }

    /// is singleton = constructor is private
    MMBitmask () {
        MM::areas[0].startAddr = MM::MEM0_START;
        MM::areas[0].endAddr = MM::MEM0_END;
        MM::areas[1].startAddr = MM::MEM_START;
        MM::areas[1].endAddr = MM::MEM_END;
        
        uint32_t * ptr = (uint32_t *) CONFIG::DIRstart;
        
        /// Hint: accessing the mem via int cast to a 8bit pointer makes alignment trouble!!!!!!
        for (int64_t i = 0; i<CONFIG::DIRsize; ++i) *(ptr+i) = 0x00000000;
        
        /// mark memory hole (framebuffer) as used
        _holePos = addr2bit((void *)MM::areas[0].endAddr)/(CONFIG::BYTES*8);
        _holeLength = (MM::areas[1].startAddr - MM::areas[0].endAddr) >> CONFIG::Addralign;
        _holeLength /= (CONFIG::BYTES*8);
        ptr += _holePos;
        for (int64_t i = 0; i<_holeLength; ++i) *(ptr+i) = 0xFFFFFFFF;
        
        _usedBlocks = 0;
        _endPos = 0;
    }
    
public:

    /// we need it for delete
    void free (void * addr) {
        // calculate position in DIR
        uint64_t blocks = 0;
        uint64_t input = addr2bit(addr);
    
        int64_t dirpos = input/(CONFIG::BYTES*8);
        int64_t dirbit = input%(CONFIG::BYTES*8);

        // debug out to serial ----------------------------------- start

        char str[20] = "                   ";
        int pos = CharTool::intToStr(input, str);
        str[pos] = '\0';
        OS::serial->write("delete: ");
        OS::serial->write(str);
        OS::serial->write(" pointer to ");
        pos = CharTool::bit64ToStr((unsigned long long) addr, str);
        str[pos] = '\0';
        OS::serial->write(str);
        OS::serial->write(". ");

        // debug out to serial ------------------------------------- end
        
        uint32_t * ptr = (uint32_t *) CONFIG::DIRstart;
        ptr += dirpos;
        
        while ( ((*ptr) & (1 << dirbit)) > 0 ) {
            blocks++;
            *ptr &= ~(1 << dirbit); // set bits to false/zero
            dirbit++;
            if (dirbit == (CONFIG::BYTES*8)) {
                dirbit=0;
                ptr++;
            }
        }
        _usedBlocks -= (blocks+1); // +1 for the end bit/block which is not needed anymore

        // debug out to serial ----------------------------------- start

        pos = CharTool::intToStr(blocks+1, str);
        str[pos] = '\0';
        OS::serial->write(str);
        OS::serial->write(" blocks\n");

        // debug out to serial ------------------------------------- end
    }
    
    /// we need it for Shell (optional)
    void freeById (uint64_t input) {
        free(bit2addr(input));
    }
    
    /** we need it for new.
     * @todo A "no free block found" is not implemented yet.
     */
    void * alloc (uint64_t reqSize) {
        void * back;
        uint32_t * ptr = (uint32_t *) CONFIG::DIRstart;
        
        int64_t needed = 1;
        if (reqSize > CONFIG::Blocksize) {
            needed = (reqSize >> CONFIG::Addralign); // reqSize/CONFIG::Blocksize;
        }
        _usedBlocks += needed +1; // we need a additional zero bit to mark the end
        needed += 2; // we need to find a additional zero bit at the front (=the end of an other block)
        
        int64_t dirpos = 0;
        int64_t dirbit = 0;
        int64_t countFree = 0;
        
        // search for a bit area with enough free bits
        for (dirpos = 0; dirpos < CONFIG::DIRsize; ++dirpos) {
            if (*ptr == 0xFFFFFFFF) {
                countFree=0;
            } else {
                for (dirbit = 0; dirbit < (CONFIG::BYTES*8); ++dirbit) {
                    if (((*ptr >> dirbit) & 0x01) > 0) {
                        countFree=0;
                    } else {
                        countFree++;
                    }
                    if (countFree == needed) break;
                }
                if (countFree == needed) break;
            }
            ptr++;
        }
        // remember highest place in mem for dump/debug use
        if (_endPos < dirpos) _endPos = dirpos;

        // dirpos and dirbit are the position of the last free bit, now.
        // go to the start of that free position:
        int64_t dirstartpos = dirpos - (needed/(CONFIG::BYTES*8));
        int64_t dirstartbit = (dirbit+1) - needed%(CONFIG::BYTES*8);
        if (dirstartbit == (CONFIG::BYTES*8)) {
            dirstartpos++;
            dirstartbit=0;
        }
        
        // calculate pointer addr of first setted bit
        back = bit2addr(dirstartpos*(CONFIG::BYTES*8) + dirstartbit+1);
        
        // debug out to serial ----------------------------------- start

        char str[20] = "                   ";
        int pos = CharTool::intToStr(
            dirstartpos * CONFIG::BYTES*8
            + dirstartbit +1,
            str
        );
        str[pos] = '\0';
        OS::serial->write("new: ");
        OS::serial->write(str);
        OS::serial->write(" pointer to ");
        pos = CharTool::bit64ToStr((unsigned long long) back, str);
        str[pos] = '\0';
        OS::serial->write(str);
        OS::serial->write(". Requested ");
        pos = CharTool::intToStr(reqSize, str);
        str[pos] = '\0';
        OS::serial->write(str);
        OS::serial->write(" bytes\n");

        // debug out to serial ------------------------------------- end
        
        // set needed bits to true (expect front and end bit)
        needed -=2;
        
        // front bit still 0
        ptr = (uint32_t *) CONFIG::DIRstart;
        ptr += dirstartpos;
        dirstartbit++;
        if (dirstartbit == (CONFIG::BYTES*8)) {
            dirstartbit=0;
            ptr++;
        }
        while (needed>0) {
            *ptr |= 1 << dirstartbit; // set needed bits to true
            needed--;
            dirstartbit++;
            if (dirstartbit == (CONFIG::BYTES*8)) {
                dirstartbit=0;
                ptr++;
            }
        }
        
        return back;
    }
    
    /// this dumps the bitmask... and this will take many seconds!
    void dump (void) {
        uint64_t totalmem = (MM::areas[0].endAddr - MM::areas[0].startAddr) 
            + (MM::areas[1].endAddr - MM::areas[1].startAddr);
        totalmem -= CONFIG::DIRsize * CONFIG::BYTES;
        uint64_t used = _usedBlocks * CONFIG::Blocksize;
        VESA::me().print("total: ").print(totalmem, 10).print(" byte\n");
        VESA::me().print("used:  ").print(used, 10).print(" byte\n");
        VESA::me().print("free:  ").print(totalmem-used, 10).print(" byte\n");

        Font * _fnt;
        uint32_t * ptr = (uint32_t *) CONFIG::DIRstart;
        int64_t dirpos = 0;
        int64_t dirbit = 0;
        int x = 0;
        int y = 0;
        VESA::me().getpos(x, y);
        _fnt = &(VESA::me().getFont());
        x *= _fnt->width();
        y *= _fnt->height();
        VESA::me().print("\n");
        int32_t subcount = 0; // more then 2 unused -> free
        int32_t length = 0;
        float ll = 0.0;
        bool inuse = false;
        while (dirpos<=_endPos) {
            if (((*ptr >> dirbit) & 0x00000001) > 0) {
                if (inuse == false) {
                    inuse = true;
                    if (dirpos >= _holePos && dirpos <= (_holePos + _holeLength)) {
                        VESA::me()
                            .print(dirpos*CONFIG::BYTES*8 +dirbit)
                            .print(" is reserved.\n");
                        
                    } else {
                        VESA::me()
                            .print(dirpos*CONFIG::BYTES*8 +dirbit)
                            .print(" in use.\n");
                    }
                }
                if (dirpos >= _holePos && dirpos <= (_holePos + _holeLength)) {
                    // reserved
                    VESA::me().drawPixel(x,y, VESA::RGB::gray);
                    subcount = 0;
                    //count/display every 1000th
                    ll += 0.001;
                    if (ll > 1.0) {
                        x++;
                        ll = 0.0;
                    }
                } else {
                    // used
                    VESA::me().drawPixel(x,y, VESA::RGB::red);
                    if (subcount>2) {
                        // mark begin of used block as black and not green
                        VESA::me().drawPixel(x-1,y, VESA::RGB::black);
                        length = 0;
                        ll = 0.0;
                    }
                    subcount = 0;
                    if (length > DUMPLIMIT2) {
                        //count/display every 1000th
                        ll += 0.001;
                        if (ll > 1.0) {
                            x++;
                            length++;
                            ll = 0.0;
                        }
                    } else if (length > DUMPLIMIT) {
                        //count/display every 100th
                        ll += 0.01;
                        if (ll > 1.0) {
                            x++;
                            length++;
                            ll = 0.0;
                        }
                    } else {
                        length++;
                        x++;
                    }
                }
            } else {
                inuse = false;
                subcount++;
                if (subcount>2) {
                    // free!
                    VESA::me().drawPixel(x-1,y, VESA::RGB::green);
                    VESA::me().drawPixel(x,y, VESA::RGB::green);
                    if (length > DUMPLIMIT2) {
                        //count/display every 1000th
                        ll += 0.001;
                        if (ll > 1.0) {
                            x++;
                            length++;
                            ll = 0.0;
                        }
                    } else if (length > DUMPLIMIT) {
                        //count/display every 100th
                        ll += 0.01;
                        if (ll > 1.0) {
                            x++;
                            length++;
                            ll = 0.0;
                        }
                    } else {
                        length++;
                        x++;
                    }
                } else {
                    // un-usable
                    VESA::me().drawPixel(x,y, VESA::RGB::black);
                    length = 0;
                    ll = 0.0;
                    x++;
                }
            }
            
            if (x > VESA::me()._xres) {
                x=0; y+=2;
            }
            dirbit++;
            if (dirbit == (CONFIG::BYTES*8)) {
                dirbit=0;
                dirpos++;
                ptr++;
            }
        }
    }
    
    uint64_t unused (void) {
        uint64_t totalmem = (MM::areas[0].endAddr - MM::areas[0].startAddr)
            + (MM::areas[1].endAddr - MM::areas[1].startAddr);
        totalmem -= CONFIG::DIRsize * CONFIG::BYTES;
        uint64_t used = _usedBlocks * CONFIG::Blocksize;
        return totalmem-used;
    }

    /// MM is singleton
    static MMBitmask & me () {
        static MMBitmask mm;
        return mm;
    }
};

#endif
