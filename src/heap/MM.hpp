#ifndef __MM_include__
#define __MM_include__ 1

#include <cstdint> // the uintX_t stuff is cool


/**
 * Memory Management.
 * 
 * this is a heap memory management. the c++ 'new' and 'delete' used
 * it = we overload/rewrite new versions of these functions.
 * 
 * <pre>
 *      Memory-Layout                                                            
 *                      0x0000: Kernel STACK END           (-524 KB)
 *                    0x7_fff0: Kernel STACK START      -> link.ld
 * .START             0x8_0000: startup.S                  ( 524 KB)
 *  |        Globale Variablen: + BSS ?
 *  |                           + kernel
 *  |                           + static globals
 *  |
 *  v                                     -> kernel8.img max +262kB
 * .END
 *                      C_0000: SP_EL1 STACK END          (  786 kB)
 *                              stack space for supervisor calls (kernel)
 *    (+524 KB)      0x13_fff0: SP_EL1 STACK START   -> link.ld
 *
 *                   0x14_0000: SP_EL2 STACK END
 *                              stack space for hypervisor calls (above kernel)
 *    (+524 KB)      0x1B_fff0: SP_EL2 STACK START   -> link.ld
 *                                                        ( 1.79 MB)
 * 
 *                   0x1C_0000: room for a ramdisk
 *                              (ca 6MB)
 *                             
 *                   0x80_0000:                            (<= 8MB)
 * 
 *     <---- RAM MEM0 ---->
 * 
 *                  This area is succeffuly tested for DMA1 transfer
 *                  to PWM1 (Audio). Thus we have to reserve a 64MB
 *                  buffer via "new" very early!
 * 
 * 
 *                 0x3800_0000: GPUmem END  = Graphics._lfb
 *                 ^                           
 *    (-128 MB)    |    -> size option in config.txt (do not touch)
 *                 |
 *                 0x4000_0000: GPUmem START              (1048 MB)
 * 
 *   <--- RAM MEM --->
 * 
 *                 0xBFFF_FFFF: end of safe to be DMA-able RAM (< 3GB)
 * 
 *   <--- RAM MEM --->
 * 
 *                 0xF800_0000: PCIe inbound MMIO map (?)
 *                  ^
 *                  |   67MB
 *                  v
 *                 0xFC00_0000                             (4.03 GB)
 *                 -- ??? --
 *                 0xFD50_0000: PCIe Registers
 *                 0xFE00_0000: MMIO-Aufrufe/Peripherie    ( 4.2 GB)
 * 
 * Virtual Addresses ?! Broadcom BCM2711 Doc -> is it a Linux Config?
 * ------------------------------------------------------------------
 * @todo Pi4 boot defaults? Maybe not accessable in EL2/EL3? MMU config?
 *                                    
 *          HEAP options?
 *          This maybe a GPU MMU feature or Pi4 Linux stuff(?):
 *               0x1_0000_0000: SRAM START                 ( 4.3 GB)
 *               0x4_0000_0000: SRAM L2 cache allocating   (17   GB)
 *               0x4_4000_0000: SRAM L2 alloc end          (+1   GB)
 * 
 *               0x6_0000_0000: PCIe outbound MMIO map (?)
 *                  ^
 *                  |   67MB
 *                  v
 *               0x6_0400_0000
 * 
 *               0x7_FFFF_FFFF:  ->end of 35Bit addresses BCM2711B0 (??)
 * </pre>
 */
class MM {

public:
    
    struct Area {
        uint64_t startAddr;
        uint64_t endAddr;
    };
    
    static Area areas[2];

    /// Stack size for each App (each entry 8bytes !!)
    static const uint64_t APP_STACK = 1024*32;
    
    /// lower heap start address
    static const uint64_t MEM0_START = 0x000800000;
    /// lower heap end address
    static const uint64_t MEM0_END   = 0x038000000;
    
    /// heap start address
    static const uint64_t MEM_START = 0x040000000;
    /// heap end address
    static const uint64_t MEM_END   = 0x0F8000000;
    
    /// we need it for delete
    virtual void free (void * ptr) = 0;
    
    /// we need it for new
    virtual void * alloc (uint64_t req_size) = 0;

    /// use blockid to get the ptr of a block and free it (optional for Shell)
    virtual void freeById (uint64_t id) = 0;

    /// Prints Data about the Memory usage.
    virtual void dump (void) = 0;
    
    /// get the size of free memory.
    virtual uint64_t unused (void) = 0;

    virtual ~MM () {}

protected:
    MM () {}

    /**
     * maybe it is a c++ (anti)feature, but MM only work, if void* is
     * allign to 8. Stange, because there exists a __aarch64__ define MALLOC_ALIGNMENT 16
     * someware in include/sys/config.h of the toolchain.
     */
    static const uint8_t PTR_ALLIGN = 16;
    
private:
    MM (const MM &) = delete;
    void operator= (MM const &) = delete;
    
};

#endif
