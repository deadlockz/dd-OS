#ifndef __MMLinkedList_include__
#define __MMLinkedList_include__ 1

#include <cstdint> // the uintX_t stuff is cool
#include "heap/MM.hpp"

/**
 * Heap Memory Management with a linked
 * list in Heap (Chunk Head before the return pointer)
 */
class MMLinkedList : public MM {

public:
    /// we need it for delete
    void free (void * ptr);
    /// we need it for new
    void * alloc (uint64_t req_size);
    /// use blockid to get the ptr of a block and free it (optional)
    void freeById (uint64_t id);
    /// Prints Data about the Memory usage.
    void dump (void);
    /// get the size of free memory.
    uint64_t unused (void);
    
    enum ChType {
        Free = 0,
        Used = 1,
        Rest = 2,   ///< is free, too
        Recyc = 3,  ///< is used, but was a rest before usage
        Joined = 4, ///< is free, but was build via a join process
        Reused = 5  ///< is used, but was not cutted into 2 parts during alloc
    };
    
    struct Chunk { 
        uint64_t _id;   ///< 8bytes
        uint64_t _size; ///< requested size
        uint64_t _dif;  ///< the difference between requested size and real size
        ChType   _type; ///< ?? bytes
        Chunk *  _prev; ///< pointer is a address = 8 bytes
        Chunk *  _next; ///< pointer is a address = 8 bytes
    };

    /// join prev and next Chunk of node, if they are free,rest,joined 
    void join (Chunk * node);
    
    /// MM is singleton
    static MMLinkedList & me () {
        static MMLinkedList mm;
        return mm;
    }

private:
    Chunk * CNULL;

    Chunk * _head;
    
    /// every Block gets a new Block Id
    uint64_t _ids;

    /// Bytes used+free
    uint64_t _total;
    /// number of used Bytes
    int64_t _used;
    /// number of requested-freed Bytes
    uint64_t _requested;

    /// dump to serial some node infos
    void dump (Chunk * node);

    /// MM is singleton = constructor is private
    MMLinkedList ();
    MMLinkedList (const MMLinkedList &) = delete;
    void operator= (MMLinkedList const &) = delete;
    
};

#endif
