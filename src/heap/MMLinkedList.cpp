#include <cstddef> // we need it for size_t and sizeof()
#include <cstdint> // the uintX_t stuff is cool

#include "heap/MM.hpp"
#include "heap/MMLinkedList.hpp"
#include "devices/CPU.hpp"
#include "exceptions/Calls.hpp" // breakpoint()
#include "devices/VESA.hpp"

#include "OS.hpp" // debug
#include "libs/CharTool.hpp" // debug

MMLinkedList::MMLinkedList () {
    MM::areas[0].startAddr = MM::MEM0_START;
    MM::areas[0].endAddr = MM::MEM0_END;
    MM::areas[1].startAddr = MM::MEM_START;
    MM::areas[1].endAddr = MM::MEM_END;

    CNULL  = (Chunk *) 0x0;
    _used  = sizeof(Chunk); ///< we count free Chunk headers
    _head  = (Chunk *) (MM::areas[0].startAddr);
    Chunk * head2 = (Chunk *) (MM::areas[1].startAddr);
    _ids   = 2;

    _total = (MM::areas[0].endAddr - MM::areas[0].startAddr) + (MM::areas[1].endAddr - MM::areas[1].startAddr);
    _requested = 0;

    /// the first inital free chunk
    _head->_id = 0;
    _head->_prev = _head;
    _head->_next = head2;
    _head->_size = (MM::areas[0].endAddr - MM::areas[0].startAddr) - sizeof(Chunk);
    _head->_dif  = 0;
    _head->_type = ChType::Free;

    /// the second inital free chunk
    head2->_id = 1;
    head2->_prev = _head;
    head2->_next = CNULL;
    head2->_size = (MM::areas[1].endAddr - MM::areas[1].startAddr) - sizeof(Chunk);
    head2->_dif  = 0;
    head2->_type = ChType::Free;
}

/** we need it for new.
 * @todo A "no free block found" is not implemented yet.
 */
void * MMLinkedList::alloc (uint64_t reqSize) {
    void * ptr = nullptr;
    Chunk * node = _head;
    
    /**
     * @todo doing a change of reqSize to allign the pointer is not so elegant.
     */
    if (reqSize % PTR_ALLIGN > 0) {
        reqSize += PTR_ALLIGN - (reqSize % PTR_ALLIGN);
    }
    
    /// @todo make a better exception handling here!!
    if ( (reqSize + sizeof(Chunk)) > (_total - _used) ) {
        breakpoint(3);
    }
    
    while (node != CNULL) {
        if (
            (
                (node->_type == ChType::Free) ||
                (node->_type == ChType::Rest) ||
                (node->_type == ChType::Joined)
            ) && (node->_size >= reqSize)
        ) {
            // node is free and big enough
            
            ptr = (void *) ((uint64_t)node + sizeof(Chunk));
            
            if (node->_size > (reqSize + sizeof(Chunk))) {
                // there is place for a rest Chunk
                
                Chunk * cutted = (Chunk *)((uint64_t)ptr + reqSize);
                
                cutted->_id = node->_id;
                cutted->_size = node->_size - (reqSize + sizeof(Chunk));
                cutted->_prev = node;
                cutted->_next = node->_next;
                cutted->_dif = 0;

                if (node->_type == ChType::Free) {
                    node->_type = ChType::Used;
                    // the rest of inital chunks are always type "free"
                    if (cutted->_id == 0 || cutted->_id == 1) {
                        cutted->_type = ChType::Free;
                    } else {
                        cutted->_type = ChType::Rest;
                    }
                } else if (node->_type == ChType::Rest) {
                    node->_type = ChType::Recyc;
                    cutted->_type = ChType::Rest;
                    
                } else if (node->_type == ChType::Joined) {
                    node->_type = ChType::Recyc;
                    cutted->_type = ChType::Joined;
                }
                node->_id = _ids;
                node->_size = reqSize;
                node->_next = cutted;
                node->_dif = 0;
                // an additional new free block needs a additional chunk space
                _used += node->_size + sizeof(Chunk);
                _ids++;
            } else {
                // we reuse the chunk completely
                node->_type = ChType::Reused;
                node->_dif = node->_size - reqSize;
                node->_size = reqSize;
                _used += node->_size + node->_dif;
            }
            
            break;
        }
        node = node->_next;
    }
    _requested += node->_size;
    return ptr;
}

void MMLinkedList::free (void * ptr) {
    Chunk * node = (Chunk *) ( (uint64_t)ptr - sizeof(Chunk) );
    
    node->_type = ChType::Free;
    _requested -= node->_size;
    node->_size += node->_dif;
    node->_dif = 0;
    _used -= node->_size;
    // freeing the Chunk is done.
    join(node);
}

void MMLinkedList::join (Chunk * node) {
    if (node->_prev != CNULL) {
        if (node->_prev->_id != 0) {
            // no join back through framebuffer memory hole!
            if (
                (node->_prev->_type == ChType::Free) ||
                (node->_prev->_type == ChType::Rest) ||
                (node->_prev->_type == ChType::Joined)
            ) {
                if (node->_prev->_id > node->_id) {
                    // samller id wins!
                    node->_prev->_id = node->_id;
                }
                node->_prev->_type = ChType::Joined;
                node->_prev->_size += node->_size + sizeof(Chunk);
                _used -= sizeof(Chunk);
                if (node->_next != CNULL) {
                    node->_next->_prev = node->_prev;
                }
                node->_prev->_next = node->_next;
                node = node->_prev;
            }
        }
    }
    
    if (node->_next != CNULL) {
        if (node->_id != 0) {
            // no join last chunk before framebuffer memory hole through framebuffer memory hole!
            if (
                (node->_next->_type == ChType::Free) ||
                (node->_next->_type == ChType::Rest) ||
                (node->_next->_type == ChType::Joined)
            ) {
                if (node->_id > node->_next->_id) {
                    // samller id wins!
                    node->_id = node->_next->_id;
                }
                node->_type = ChType::Joined;
                node->_size += node->_next->_size + sizeof(Chunk);
                _used -= sizeof(Chunk);
                if (node->_next->_next != CNULL) {
                    node->_next->_next->_prev = node;
                }
                node->_next = node->_next->_next;
            }
        }
    }
}

void MMLinkedList::dump (void) {
    Chunk * node = _head;
    VESA::me().setColor(VESA::RGB::lightgray, VESA::RGB::black);
    
    VESA::me()
        .print("total:     ").print(_total, 10).print("\n")
        .print("used:      ").print((long)_used, 10).print("\n")
        .print("requested: ").print(_requested, 10).print("\n");
    
    while (node != CNULL) {
        dump(node);
        switch (node->_type) {
            // primary red (=used)
            case ChType::Used :
                VESA::me().setColor(VESA::RGB::white, RGB_24(180,0,0));
                VESA::me().print("used\t").print(node->_id, 10).print("\tsize: ");
                break;
            case ChType::Recyc :
                VESA::me().setColor(VESA::RGB::white, RGB_24(250,0,0));
                VESA::me().print("recyc\t").print(node->_id, 10).print("\tsize: ");
                break;
            case ChType::Reused :
                VESA::me().setColor(VESA::RGB::white, RGB_24(250,131,0));
                VESA::me().print("reused\t").print(node->_id, 10).print("\tsize: ");
                break;
            // primary green (=free)
            case ChType::Rest :
                VESA::me().setColor(VESA::RGB::white, RGB_24(80,120,80));
                VESA::me().print("rest\t").print(node->_id, 10).print("\tsize: ");
                break;
            case ChType::Joined :
                VESA::me().setColor(VESA::RGB::white, RGB_24(0,170,150));
                VESA::me().print("joined\t").print(node->_id, 10).print("\tsize: ");
                break;
            default:
                VESA::me().setColor(VESA::RGB::white, RGB_24(0,180,0));
                VESA::me().print("free\t").print(node->_id, 10).print("\tsize: ");
        }
        if (node->_size > 1048576) {
            VESA::me().print(node->_size/1048576, 10).print("M");
        } else if (node->_size > 1024) {
            VESA::me().print(node->_size/1024, 10).print("k");
        } else {
            VESA::me().print(node->_size, 10);
        }
        VESA::me().print(" +").print(node->_dif, 10).print("\t ptr: ")
            .print((uint64_t)node + sizeof(Chunk), 16).print("\n");
        node = node->_next;
    }
    VESA::me().setColor(VESA::RGB::lightgray, VESA::RGB::black);
}

void MMLinkedList::freeById (uint64_t id) {
    Chunk * node = _head;
    void * ptr = nullptr;
    while (node != CNULL) {
        if (node->_id == id) {
            ptr = (void *) ((uint64_t)node + sizeof(Chunk));
            break;
        }
        node = node->_next;
    }
    if (ptr != nullptr) free(ptr);
}

uint64_t MMLinkedList::unused (void) {
    return _total - _used;
}

void MMLinkedList::dump (Chunk * node) {
    char str[20] = "                   ";
    int pos;
    
    if (node->_prev == CNULL) {
        OS::serial->write("CNULL     ");
    } else {
        pos = CharTool::bit64ToStr((unsigned long long) node->_prev, str);
        str[pos] = '\0';
        OS::serial->write(str);
    }
    OS::serial->write(" <- ");

    pos = CharTool::bit64ToStr((unsigned long long) node, str);
    str[pos] = '\0';
    OS::serial->write(str);
    OS::serial->write(" -> ");

    if (node->_next == CNULL) {
        OS::serial->write("CNULL");
    } else {
        pos = CharTool::bit64ToStr((unsigned long long) node->_next, str);
        str[pos] = '\0';
        OS::serial->write(str);
    }

    pos = CharTool::intToStr(node->_id, str);
    str[pos] = '\0';
    OS::serial->write("\n id ");
    OS::serial->write(str);
    OS::serial->write(": ");
    pos = CharTool::intToStr(node->_size, str);
    str[pos] = '\0';
    OS::serial->write(str);
    OS::serial->write(" + ");
    pos = CharTool::intToStr(node->_dif, str);
    str[pos] = '\0';
    OS::serial->write(str);
    OS::serial->write(" bytes ");

    switch (node->_type) {
        case ChType::Used :
            OS::serial->write("used.\n");
            break;
        case ChType::Recyc :
            OS::serial->write("recyc.\n");
            break;
        case ChType::Reused :
            OS::serial->write("reused.\n");
            break;
        case ChType::Rest :
            OS::serial->write("rest.\n");
            break;
        case ChType::Joined :
            OS::serial->write("joined.\n");
            break;
        default:
            OS::serial->write("free.\n");
    }
}
