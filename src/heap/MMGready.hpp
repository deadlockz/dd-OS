#ifndef __MMGready_include__
#define __MMGready_include__ 1

#include <cstdint> // the uintX_t stuff is cool
#include "heap/MM.hpp"
#include "devices/VESA.hpp"

#include "OS.hpp" // debug
#include "libs/CharTool.hpp" // debug

/**
 * Stupid Heap Memory Management without a free
 */
class MMGready : public MM {

public:

    /// we need it for delete
    void free (void * addr) {
        // debug out to serial ----------------------------------- start
        char str[20] = "                   ";
        OS::serial->write("gready unhandle delete to ");
        int pos = CharTool::bit64ToStr((unsigned long long) addr, str);
        str[pos] = '\0';
        OS::serial->write(str);
        OS::serial->write("\n");
        // debug out to serial ------------------------------------- end
    }
    
    /// we need it for Shell (optional)
    void freeById (uint64_t) {
        // dummy
    }
    
    /** we need it for new.
     * @todo A "no free block found" is not implemented yet.
     */
    void * alloc (uint64_t reqSize) {
        uint64_t reqSizeMod = reqSize;
        /**
         * @todo doing a change of reqSize to allign the pointer is not so elegant.
         */
        if (reqSizeMod % PTR_ALLIGN > 0) {
            reqSizeMod += PTR_ALLIGN - (reqSizeMod % PTR_ALLIGN);
        }
        
        if (
            (MM::areas[0].endAddr <= ((uint64_t) _next + reqSizeMod)) &&
            ((uint64_t) _next) <= MM::areas[0].endAddr
        ) {
            // we work on MEM0 addresses, but the next address does not fit.
            _next = (void *) (MM::areas[1].startAddr);
            // => thus we jump in the 2nd mem area.
        }
        void * back = _next;
        _next = (void *) ((uint64_t) _next + reqSizeMod);

        // debug out to serial ----------------------------------- start
        char str[20] = "                   ";
        OS::serial->write("gready new pointer to ");
        int pos = CharTool::bit64ToStr((unsigned long long) back, str);
        str[pos] = '\0';
        OS::serial->write(str);
        OS::serial->write(". Requested ");
        pos = CharTool::intToStr(reqSize, str);
        str[pos] = '\0';
        OS::serial->write(str);
        OS::serial->write(" bytes\n");
        // debug out to serial ------------------------------------- end

        return back;
    }
    
    void dump (void) {
        long totalmem = (MM::areas[0].endAddr - MM::areas[0].startAddr) + (MM::areas[1].endAddr - MM::areas[1].startAddr);
        long freemem = unused();
        VESA::me().print("total: ").print(totalmem, 10).print(" byte\n");
        VESA::me().print("used:  ").print(totalmem - freemem, 10).print(" byte\n");
        VESA::me().print("free:  ").print(freemem, 10).print(" byte\n");
    }
    
    uint64_t unused (void) {
        if (((uint64_t) _next) <= MM::areas[0].endAddr) {
            return (MM::areas[0].endAddr - (uint64_t) _next) + (MM::areas[1].endAddr - MM::areas[1].startAddr);
        } else {
            return MM::areas[1].endAddr - (uint64_t) _next;
        }
    }

    /// MM is singleton
    static MMGready & me () {
        static MMGready mm;
        return mm;
    }

private:

    void * _next;
    
    /// MM is singleton = constructor is private
    MMGready () {
        MM::areas[0].startAddr = MM::MEM0_START;
        MM::areas[0].endAddr = MM::MEM0_END;
        MM::areas[1].startAddr = MM::MEM_START;
        MM::areas[1].endAddr = MM::MEM_END;
        
        _next  = (void *) (MM::areas[0].startAddr);
    }
    MMGready (const MMGready &) = delete;
    void operator= (MMGready const &) = delete;
};

#endif
