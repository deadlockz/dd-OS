#ifndef __OS_include__
#define __OS_include__ 1

#include <cstddef> // we need it for size_t and sizeof()
#include <cstdint> // the uintX_t stuff is cool
#include "apps/Shell.hpp"
#include "heap/MM.hpp"
#include "devices/Serial.hpp"
#include "devices/DMA.hpp"

extern uint64_t os_regs[104];

extern "C" void __cxa_atexit (void *pThis, void (*pFunc)(void *pThis), void *pHandle);
extern "C" void __cxa_pure_virtual (void);
extern "C" int atexit (void (*pFunc)(void));

/**
 * OS namespace.
 * It collects some very important objects and values of the system.
 */
namespace OS {
    /// systime counts the PIT ticks
    extern unsigned long systime;
    extern Shell * shell;
    extern MM * mm;
    extern uint64_t ramdiskAddr;
    extern Serial * serial;
    extern DMA * dma1;
    extern DMA * dma5;
    //extern DMA * dma14;
}

void * operator new (size_t s);
void * operator new[] (size_t c);
void operator delete (void * ptr);
void operator delete[] (void * ptr);
void operator delete[] (void * ptr, long unsigned int);
void operator delete (void * ptr, long unsigned int);

#endif
