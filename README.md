<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# hhuOS for Pi4 (AArch64)

![logo hhuOS - ARMv8](logo.png)

 -  Repository: [ARMv8 Branch](https://gitlab.com/deadlockz/dd-OS/tree/ARMv8)
 -  Code Documentation: [hhuOS Pi4 Website](https://deadlockz.gitlab.io/dd-OS/)
 -  Slides: [german](https://gitlab.com/deadlockz/dd-OS/-/blob/ARMv8/docs/hhuOS_auf_Pi4/Slides.pdf)
 -  Master thesis: [german](https://gitlab.com/deadlockz/dd-OS/-/blob/ARMv8/docs/thesis/Masterarbeit_Peters.pdf)
 -  Master presentation: [german](https://gitlab.com/deadlockz/dd-OS/-/blob/ARMv8/docs/presis/MasterBeamer.pdf) / [first-try video](https://gitlab.com/deadlockz/dd-OS/-/raw/ARMv8/docs/video1.webm)
 -  Video: [running hhuOS-for-Pi4](https://gitlab.com/deadlockz/dd-OS/-/raw/ARMv8/docs/screencast.mp4)

Take a look to some minimal experiments:

 - [just uart and irq c](https://github.com/no-go/pi4-irq-SystemTimer)
 - [just uart, graphics and irq c](https://github.com/no-go/pi4-irq-SystemTimer/tree/someGraphics)
 - [a pi4 serial bootloader](https://github.com/no-go/raspbootin64-Pi4)

This branch compared to [Master Branch](https://gitlab.com/deadlockz/dd-OS/-/tree/master):

 -  x86 -> ARMv8.0-A
 -  Personal Computer -> SoC (Pi4)
 -  32bit code -> 64bit code
 -  BIOS -> EEPROM (?), MMIO and MessageBus to VideoCore
 -  PS/2 keyboard -> Serial (GPIO 14, GPIO15)
 -  Ring0 -> EL1

# Code Structure

![hhuOS-for-Pi4, Code Structure](docs/hhuOS_auf_Pi4/images/structure.png)

# How to use it as User

 -  you need a Pi4 B with min. 4GB RAM (>4GB not useable, because hard coded and not tested)
 -  a micro SD card (min. 16MB) formated as FAT32 (first partiton)
 -  HDMI monitor/TV, 3.5mm audio connector (sometimes headset is ok, but the Pi4 connector is A/V)
 -  as keyboard replacement:
    - a Serial Terminal application (e.g. putty with 115200 baud, 8 data bits, 1stop bit, none parity+flow)
    - a UART to serial USB converter [e.g. this one](https://www.exp-tech.de/module/usb-uart/9030/pl2303-usb-uart-board-micro?c=1464)
    - wire connector to Pi: TX to RX (GPIO15), RX to TX (GPIO14), GND to Ground, NO +5V or +3.3V wire!!)
    - later: test TX to TX and RX to RX (sometimes labeling is wired)
 -  remove all files from micro SD
 -  copy new files (sdcard - folder on github) to micro SD
    - `kernel8.img` = the hhuOS Pi4 (ARMv8.0-A code)
    - `bcm2711-rpi-4-b.dtb` a device tree binary for...
    - `start4.elf` the BCM2711 CPU 64bit firmware
    - `config.txt` additional config file
    - `fixup4.dat` additional data to split RAM into ARM and GPU areas
    - `logo.bmp` or `sample.wav` for a file access in hhuOS without filesystem 
 - take a look into the `config.txt` file:
   - many parts are commented via hashtags
   - the `[all]` part:
     - `core_freq_min=500` to disallow CPU to be slower (to slow for some code)
     - `hdmi_group=2` is a "good" config for me in any case
     - `dtparam=audio=on` a strange device tree parameter. This wires the A/V conntector internaly to GPIO 41/41.
     - `ramfsfile=...` the `sample.wav` or `logo.bmp` will be copied to ...
     - `ramfsaddr=0x1C0000`. This makes the file accessable for hhuOS Pi4.
 - plug the SD card into the Pi4
 - plug in USB converter to your PC (and wire it to the Pi4)
 - plug in headphones to the Pi4 (and not to your ears, maybe to loud)
 - plug in monitor/TV to micro hdmi 0 (next to Pi4 USB-C power hole, maybe hdmi 1 works too)
 - start the serial terminal application (and configure it)
 - power on the Pi4 (plug in usb-c)
 - wait a bit (copy the ramfsfile to ram takes ca 4sec)
 - monitor/TV shows "hhuOS Pi4" and a digital clock
 - serial terminal gets "hhuOS Pi4"
 - type `help` in the terminal and press enter: You got a list commands and applications!

The Pi4 has an EEPROM Bootloader. The Version and Revision of that
one may be important! Make an empty FAT32-SD Card with the content of
the `sdcard/rpi4-boot-eeprom-recovery-2021-04-29-vl805-000138a1.zip` file,
and follow the readme-file in this zip to update the Pi4 EEPROM.
This update makes the Pi4 able to boot from USB-Stick and add the
actual USB3.0 firmware to the EEPROM (and VL805 chip). We may need it
for the future to support USB-Keyboard access in *hhuOS-for-Pi4*.
The file is comming from [here](https://github.com/raspberrypi/rpi-eeprom/releases/tag/v2021.04.29-138a1).

Some hints:
 - it is good to use a cooling solution on Pi4 !
 - type `temp` to get the cpu temperature. 40 is ok, 60 is not nice, 80 is critical!
 - you can use the cursor keys in the serial terminal
 - sometimes you got debug informations in the serial terminal, e.g. on bluescreen
 - for the `rotenc` application (Daemon) you have to add a rotation encoder:
   - pinA to GPIO 21
   - pinB to GPIO 20
   - Press button to GPIO16
   - connect the other encoder wires to ground
 - PS/2 Keyboard improvement: +5V and ground, GPIO 2 (Data), GPIO 3 (IRQ-Pin)
   - You can test a improved scroll with page-up and page-down keys.
 - ADDITIONALLY: Never connect +5V from the UART to serial USB converter
   to the Pi4! The Pi4 is extrem hungry and running the Pi4 with less than 3A
   may demage Pi4, SD card, your USB port or the converter!


# How to use it as Developer

[german](https://gitlab.com/deadlockz/dd-OS/-/blob/ARMv8/docs/thesis/Masterarbeit_Peters.pdf)

# Features (2021-11-15)

 -  Serial (UART)
 -  VESA print, set Font, Graphics, set resolution
 -  a CharTool to read and print values (double, too!)
 -  Timer (PIT) and Interrupts (ServiceRoutine)
 -  exceptions (GIC, Dispatcher) and Bluescreen
 -  Revision, Power and Temperature: (power)off, (re)boot, get tempetarure/revision
 -  simple MM (Memory Management, heap) for new in c++: MMGready
 -  Scheduler with preemptive Thread
 -  Shell as idle app
 -  Bluescreen with Serial interaction (stop on breakpoint, next, display stack as double, ...)
 -  lambda function for a SimpleThread
 -  get and set Clocks of the CPU
 -  handling alpha values (like aquarell colors)
 -  normal ClockApp and a vector VClockApp
 -  a 2D animation Graphics Apps: GlowFontApp, AntApp, WormApp, TurmiteApp
 -  supervisor EL1 system Calls (similar to Software Interrupt)
 -  optional cooperative Threads
 -  Test a cooperative Multithread Counting (CoCountApp)
 -  Improved counting Semaphore based on loop and scheduling detection (SemCountApp) 
 -  Create and example for BoundedBuffer (based on improved Semaphore): BufferedFactoryApp
 -  save/restore float registers on context switch
 -  use and test float registers
 -  make a 2nd Heap area behind kernel and before framebuffer (>800MB !!)
 -  DMA for sound (load, play, pause, stop, progess in %, duration)
 -  Audio via pwm (a tone generator)
    - actual Pi4B should work with PWM1 connected to audio jack
    - some minimal boot files does not route sound to HDMI, too
 -  play wav file (ramdisk) unsigned 8bit, 44.1kHz, PCM, stereo
 -  Shell beeps if execute is successfull (new: AudioControlApp)
 -  using cursor keys via Serial (InteractApp) and backspace
 -  tabstop `\t` in print
 -  MMBitmask is a simple Memory Management. In a 500kB big bitmask 
    every bit represents 1024 bytes. Additional 2 bits are not set
    during an alloc to mark start and end position. No storing of
    additional size or free regions is needed. BUT if you need ONE
    single byte, then you waste 2x 1024 bytes.
 -  MMBitmask makes a lot of serial debug messages!
 -  MMBitmask: mark reserved area in the mem middle (framebuffer area)
 -  MMLinkedList is the 3rd type of memory management (LinkedList in memory)
 -  concept for detecting interrupt SPI 145 on different GPIOs to
    handle rotationEncoder and PS/2 keyboard (and merge the branch)
    
Improvements:

 -  simple cursor blinking (part of InteractApp)
 -  using ramdisk `config.txt` option for additional data (BmpImage or WAV)
 -  set ramdisk address by shell command (as integer, not hex)
 -  RotationEncoderApp (+ press button) based on Interrupt RisingEdge
 -  PS/2 Keyboard (actally many lost bits)
 -  use page up and down scroll keys (overlaps with bluescreen)

# The progress

## phase 1 (implement essentials)

Optional (if time is available in Oct/Nov 2021):

 -  a kind of fake filesystem to store bmp AND wav file
 -  Keyboard with Bluetooth 4.0 (?) or PCIe-USB or USB-OTG (problem upload firmware?)

## phase 2 (write master thesis; start mid/end Aug 2021)

 -  compare hhuOS x86 hardware and chip concepts with ARMv8
 -  oostubs, xv6, hhuOS comparison
 -  MA introduction and structure
    - porting or renew everything?
    - development, IDEs, debug
    - Pi4, BCM2711, A72, ARMv8.0-A, 64bit mode
    - boot process
    - mmio, peripherals
    - VESA, VPU, message bus
    - address alignment
    - MMU... semaphores
    - pcie and xhci (usb keyboard?)
 -  start master thesis (MA)


# Possible next milestones

 -  the shell command `ps` should show the used size of each thread stack!
 -  DMA image scrolling like [pigfx](https://github.com/fbergama/pigfx/commit/2ada25d6)
 -  delay microseconds
 -  Fat32 (using ramdisk and/or EXTSTORE to SD Card)
 -  better Shell/App splitting
 -  multicore
 -  ethernet
 -  I2C Bus
 -  canceled stuff (PCIe, USB3, Spinlock/MMU)
 -  USB mouse
 -  Pi1 migration (2% done)
 -  virtual FS
 -  kernel "modules"
 -  kernel/user mode (EL1, EL0)
 -  c/c++ libs to compile links2, mc or vi (newlib0 based on `dwelch` baremetal hints and example?)
 -  WLAN
 -  Bluescreen with debugger shell like [minidbg](https://gitlab.com/bztsrc/minidbg)
    see: [Commit ee1e6308](https://gitlab.com/deadlockz/dd-OS/-/commit/ee1e6308f1e3c710e47c31f00e0c90671658cde8)
 -  JTAG like [here](https://www.suse.com/c/debugging-raspberry-pi-3-with-jtag/)
    or [here](https://sysprogs.com/VisualKernel/tutorials/raspberry/jtagsetup/) with openOCD
 -  Virtualisation with [ARMv8-A Fixed Virtual Platform](http://www.arm.com/fvp) because 
    qemu has no pi4 or gicV2
 -  Pi3 and qemu compatibility (May/June 2021: qemu without pi4 support)
 -  work with the virtual frambuffer resolution to scroll text (partly done)

## debug and comment

 -  alpha value with non(?)-black combination makes trouble (print the sprite-logo sometimes)
 -  Doc, Doxygen (done: basics exists!)
 -  better Bluescreen/breakpoint handling (e.g. ENUM because some breakpoint numbers are fix)
 -  monkey key testing, extrem jam in screen, UART massive input

## canceled Spinlock Mutex (and MMU) 2021-07-21, 14:00, after 7 weeks

 - MMU 1:1 VA:PA mapping for a minimal usage of global exclusive monitors (need for "atomic")
 - cache, paging, virtual addr, sharability, buffering
 - Spinlock based Mutex and counting Semaphores (and all Semaphores testing via BoundedBuffer, factory, mutexTest)
 - elf, loading "programs", flat-memory model

## canceled USB (PCIe) 2021-08-06, 11:40, after 1 week

### PCIe tests

 -  PCIe init and setup
 -  PCIe Bridge Interrupt handling
 -  handle MSI interrupts (PCIe-slots) for added devices
 -  xHCI USB Controller
    - load VL805 firmware from EEPROM(?) after PCIe bridge reset via GPU-message
    - add to MSI (?)

### PCIe todo

 -  Xhci USB Controller
    - find ports and MMIO area
    - detect/add USB devices
    - MSI handler
 -  try OTG with USB2 stuff on USB-C connector(?)
 -  USB keyboard key detection
 -  maybe test `config.txt` with [arm_peri_high=1](https://github.com/isometimes/rpi4-osdev/issues/13)



