all: clean kernel

doc: Doxyfile README.md
	rm -rf public
	mkdir public
	cp README.md src/
	doctoc src/README.md
	doxygen Doxyfile
	rm -rf src/README.md
	cp *.png public/
	cp --parents docs/hhuOS_auf_Pi4/images/*.png public/

kernel: 
	$(MAKE) -C src/

emu:
	# -d in_asm, ... = enable logging of specified items (use '-d help' for a list of log items)
	# see: https://github.com/bztsrc/raspi3-tutorial#emulation
	qemu-system-aarch64 \
	 -M raspi3 \
	 -kernel sdcard/kernel8.img \
	  -serial null -serial stdio

emu_notes:
	# -d in_asm, ... = enable logging of specified items (use '-d help' for a list of log items)
	# see: https://github.com/bztsrc/raspi3-tutorial#emulation
	qemu-system-aarch64 \
	 -M raspi3 \
	 -cpu cortex-a57 \
	 -dtb sdcard/bcm2711-rpi-4-b.dtb \
	 -kernel sdcard/kernel8.img \
	 -d in_asm \
	 -m 4G \
	 -serial mon:stdio

clean:
	$(MAKE) -C src/ clean
