Beim Informatik-Studium an der Heinrich-Heine-Universität in Düsseldorf
können Studierende ihr eigenes Betriebssystem in einer Veranstaltungs-Reihe
schreiben.
In Übungsaufgaben werden dabei mit Hilfe eines
Grundsystems wichtige Bestandteile entwickelt und zu einem lauffähigen,
individuellen Betriebssystem für die 3'86er Architektur ausgebaut.
Seit Mitte 2018 ist unter Anderem daraus das hhuOS-Projekt entstanden, was
von Studierenden auch bis heute weiter gepflegt wird \cite{hhuOS}.
Auf der Basis der Übungsaufgaben und dem Begleitmaterial ist
im Rahmen dieser Masterarbeit ein neues Grundsystem für hhuOS entstanden,
das für eine neuere System-Architektur (ARMv8.0-A) ausgelegt ist.
Als Hardware wurde das Ein-Chip-System (SoC) des Raspberry Pi4
genommen, aufgrund der ausreichenden Dokumentation der Peripherie
und der weiten Verbreitung dieses Kleinst-Rechners. Nachfolgend
wird dieses neuen Grundsystem hhuOS-for-Pi4 genannt \cite{hhuos4pi4}.
