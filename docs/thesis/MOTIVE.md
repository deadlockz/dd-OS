Die Idee einer Portierung auf eine neuere Architektur stammt
aus den Erfahrungen mit den ersten Übungsaufgaben zum Thema PC-Speaker
und PS/2-Tastatur. Im Winter 2016 waren unter den Studierenden im Kurs
nur wenige, die eine entsprechend alte Hardware mit diesen Bauteilen
besaßen. Aus dem Grund wurden ein paar ältere Laptops zur Verfügung gestellt.
Jedoch bei der Entwicklung im Kursverlauf gewann der Emulator
QEMU immer mehr Bedeutung, denn das Erstellen eines Bootmediums nach
jeder Code-Änderung und das Warten, bis der Laptop startet,
verlangsamte das Debuggen und Testen sehr. Auch wiesen die Geräte kleine
Hardware-Änderungen auf, die Systemabstürze schwer
nachvollziehbar machten. Selbst im Code des Emulators waren damals Anpassungen
beim Programmable Interval Timer (PIT) nötig, um das Grundsystem laufen
lassen zu können. 

Ebenso wies das Grundsystem über mehrere Dateien verteilt
Assembler-Code auf, was von der Idee abwich, ein Betriebssystem
in C++ zu schreiben und so die Bindung an eine Architektur zu lockern.
Die Datei `BIOS.cpp`[^aktuellBIOS] stellte hier
ein besonderes Extrem dar und ist bis heute auch im Code von hhuOS zu finden:
Dort wird 16-Bit Maschinensprache-Code
direkt via Pointer in einen Speicherbereich geschrieben, um 16-Bit
BIOS-Calls im Real-Mode (0'86er) machen zu können. Dies ist
nötig, um in VESA Grafik-Modi wechseln zu können. Verglichen
mit heutiger Hardware, die vorrangig den Nachfolger des BIOS, das
Unified Extensible Firmware Interface (UEFI) von 2006 nutzen und
auf 64-Bit Architekturen setzen, ist hier ein großer technologischer
Sprung zu sehen. Auch wenn diese alte Architektur inzwischen gut dokumentiert
ist und einen umfangreichen Einblick in die hardwarenahe Programmierung
gewährt, so sind die praktischen Probleme bei der Abwärtskompatibilität
heutiger Rechner mit der 3'86er Architektur nicht von der Hand zu
weisen. Sollte man sich vom BIOS und der
alten Architektur trennen wollen, ist dies zwangsläufig mit der
Entwicklung von Treibern zu konkreter Peripherie verbunden. Allerdings
existieren auf dem Markt viele Grafik-Chips, USB-Host-Chips, und Ethernet-Chips.
Für eine zukünftige Weiterentwicklung an
moderne Architekturen gibt es daher folgende Möglichkeiten:

1.  nur wenige Treiber entwickeln, für Hardware, die emuliert werden kann
2.  Entwicklung vieler Treiber bzw. Einbindung von Treibern anderer Betriebssysteme
3.  Fokus auf eine Zielplattform legen, deren Hardware sich kaum verändert

Der aktuelle Development-Branch von hhuOS verfolgt die erste
Möglichkeit \cite{hhuOSdev}. In dem Projekt wurde 2019 das Booten
im Emulator mit einem EFI-BIOS ermöglicht und GRUB als Bootloader genutzt, da das blockweise
Laden mit einem eigenen Bootloader sehr unkomfortabel ist. Ebenso
ist der Zugriff auf PCI-Bus und USB möglich, was die Entwicklung
von Treibern für USB-Tastatur und unterschiedliche Grafikkarten
vorbereitet. Für die PCI-Ethernet Karte Intel/PRO-1000
von 2004 \cite{intelPro1000}, die emuliert werden kann, ist für hhuOS
bereits ein Treiber entwickelt worden. Der Codeumfang ist entsprechend
gewachsen. Nach Löschen von Schriften, LibC und Anwendungen in `src/` bleiben
noch **27753 Codezeilen**. Paging, Dateizugriff und ein Konzept zur Einbindung
von Kernel-Modulen ist seit Mitte 2018 dazugekommen \cite{hhuosSlides}.
Dieser Code-Status stellt das Ende der Veranstaltungs-Reihe über 2 Semester
zur Entwicklung von hhuOS dar sowie ein weiteres Engagement der
vier Entwickler bis heute.

Die zweite Möglichkeit aus der o.g. Liste setzt Schnittstellen zu anderen Betriebssystemen wie
Linux oder Minix voraus. Auch wenn zusammen mit Newlib[^newlib] und
LibC eine Codebasis gefunden werden kann, wäre man gezwungen
viel bzgl. des Hardwarezugriffs der anderen Betriebssysteme zu übernehmen.

Mit dieser Masterarbeit wurde die dritte Möglichkeit verfolgt und das
ursprüngliche Grundsystem von \nohyphens{hhuOS} auf den Raspberry Pi4 portiert.
Als Vorlage für das Grundsystem aus der Veranstaltungsreihe
diente der Code von \cite{ddosbasic}, dessen Codezeilen sich aufteilen
lässt in ca. **620 Zeilen Assembler- und ca. 4440 Zeilen C++-Code**, wenn
man Binärdaten wie Schriftarten und den Code für Anwendungen nicht
mit zählt.

Es wurden folgende Ziele für diese Masterarbeit definiert, die durch
die Portierung erfüllen werden sollten:

- Reduzierung der Codezeilen: speziell Assembler-Code
- Vereinfachung des Bootvorgangs (der größte Teil des Assembler-Codes)
- durch Hardwarevorgabe eine Lösung von Kompatibilitätsproblemen erreichen
- Dokumentation des Grundsystems vervollständigen
- leichter Einstieg auch für Menschen, die allgemein an
  Raspberry-Pi-Projekten Interesse haben

Nach der Portierung sollen diese Funktionen des Grundsystems weiterhin
vorhanden sein:

- Speicherallokierung (new/delete)
- präemptives Scheduling mit Threads
- Audio-Ausgabe
- ein Interface zur Interrupt-Behandlung und Systemcalls
- Text- und Grafikausgabe
- Möglichkeit zur Markierung von atomaren Code-Abschnitten
- Eingabe-Shell
- Fehlerbehandlung via Bluescreen

Multicore und Paging sollten nach der Portierung auch
weiterhin im Grundsystem nicht enthalten sein. Dateizugriff und
die Verwendung einer USB-Tastatur waren dafür als zusätzliche Features geplant.

Ferner sollte durch die Portierung auf den Pi
das Betriebssystem hhuOS unter den Studierenden populärer werden. Der Raspberry Pi
wird bereits in der Lehre z.B. an der University of Cambridge[^piinderlehre1]
und University of Sheffield[^piinderlehre2] eingesetzt und
unter den Kleinst-Rechnern ist der Pi einer der Günstigsten[^price]. 
Außerdem werden in dieser Masterarbeit die Bestandteile der ARMv8-A-Architektur
des Pi4 so verdeutlicht, dass auch anderen Menschen eine
vergleichbare 3'86er Portierung hin zum Pi4 gelingen kann.

[^piinderlehre1]: \url{https://www.cl.cam.ac.uk/projects/raspberrypi/}
[^piinderlehre2]: \url{https://pi.gate.ac.uk/}
[^price]: Preise: \url{https://www.berrybase.de/}
[^newlib]: Einen Einstieg in das Thema bietet der `newlib0`-Ordner von \cite{dwelch}.
[^aktuellBIOS]: `src/device/misc/Bios.cpp` in \cite{hhuOSdev}




