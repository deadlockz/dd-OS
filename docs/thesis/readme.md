# Portierung des Betriebssystems hhuOS von x86 auf das 64bit Ein-Chip-System des Raspberry Pi4 (Cortex-A72)

Dies ist eine (schlechte) Alternative zur [PDF](Masterarbeit_Peters.pdf),
da nicht alle Referenzierungen und Fußnoten klappen werden.

[Abstract](ABSTRACT.md)

**Inhaltsverzeichnis**

- [Einleitung](INTRO.md)
  - [Motivation](MOTIVE.md)
  - [Aufbau der Arbeit](OVERVIEW.md)
- [Grundlagen](BASICS.md)
- [Verwandte Arbeiten](COMPARE_TO.md)
- [Architektur des Raspberry Pi4](MAIN.md)
- [Implementierung](IMPLEMENT.md)
- [Fazit und Ausblick](FAZIT.md)

