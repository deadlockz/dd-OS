Das Betriebssystem hhuOS, dessen Funktionalität auf der Basis von C++ Übungsaufgaben
von Studierenden entwickelt wurde, baut auf der Architektur
des 3'86ers von 1985 auf (32Bit). Aufgrund einer Abwärtskompatibilität
ist hhuOS daher zum Teil noch auf heutigen Rechnern lauffähig. Allerdings
ist diese Abwärtskompatibilität auch Grund für vielerlei Probleme:
Zum Einen wird die alte Peripherie von heutigen Herstellern nicht
mehr zu 100% abgebildet bzw. verbaut, und zum Anderen durchläuft der
Bootvorgang eines 3'86ers alle vorangegangenen Intel-Prozessor-Generationen
bis einschließlich des 0'86ers von 1978. Die Portierung des Grundsystems
von hhuOS auf den Raspberry Pi4 ist aus der Überlegung entstanden, dass
durch einen Sprung auf ein aktuelles
Ein-Chip-System (SoC) viele dieser Altlasten wegfallen und so der Zugang zur
Betriebssystem-Entwicklung für Studierende erleichtern kann. Die Wahl
des SoC fiel auf den Raspberry Pi4, da sich um diesen Kleinst-Rechner
bereits eine große Community gebildet hat mit Beispiel-Code
und Hardware-Dokumenta\"-tionen. Entsprechend beschreibt
diese Masterarbeit die Portierung des Grundsystems von hhuOS
auf die ARMv8.0-A Architektur des Pi4 und dessen 64-Bit Ausführungs-Modus.
Hierzu werden die dafür notwendigen ARM Architektur-Konzepte beschrieben.
Ebenso werden neue Möglichkeiten beschrieben, die sich durch
die Wahl dieser Hardware ergeben, wie z.B. das Abspielen von WAV-Dateien
oder den Anschluss eines Rotation-Encoders. Allerdings gestalten sich durch
die moderne Architektur andere Bereiche als schwieriger, wie z.B. der
Spinlock oder der Zugriff auf eine Tastatur (USB statt PS/2).
