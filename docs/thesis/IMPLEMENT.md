
Bei der Implementierung von hhuOS-for-Pi4 \cite{hhuos4pi4} wurde als Grundsystem die
Version genommen, die zum Ende der ersten
Veranstaltungs-Reihe ,,Betriebssystem Entwicklung''  zur Verfügung stand \cite{ddosbasic}.
In dieser speziellen Version[^yourownos] ist auf String- und ScreenStream verzichtet
worden, um den Bereich der Textausgabe auf ein Minimum an Code
und Dateien zu reduzieren. Stattdessen stehen in `VESA` mehrere
Print-Methoden zur Verfügung, die durch das Fluent-Interface miteinander
verkettet werden können[^fluent].
Dieses Konzept zur Textausgabe wurde in hhuOS-for-Pi4 beibehalten.
Außerdem wurde statt einer Datei mit mehreren `static` Instanzen
das Singelton-Muster verwendet. In hhuOS-for-Pi4 ist dies an sinnvollen
Stellen durch ein Namespace ersetzt worden. Daraus ergibt sich z.B.
folgende Struktur:

- Ein Namespace `OS` in dem ein `Serial * serial;` ist.
- Ein Singelton `UART0`, der von `Serial` erbt.
- Ein Kernel `main.cpp` in dem `OS::serial` die Adresse von `UART0::me()` übergeben wird.

Um den Rahmen dieser Masterarbeit nicht zu sprengen, wurde empfohlen,
auf ein Paging zu verzichten. Paging ist jedoch bei der ARMv8-A Architektur
das einzige Konzept zur virtuellen Adressierung, die für einen ARM-Spinlock
und Semaphoren optimal wären. Da sich die MMU nach vielen Versuchen
nicht einschalten ließ, konnte in hhuOS-for-Pi4 kein Spinlock implementiert
werden. Auch alternative Konzepte schlugen durch die OoO-Pipeline
z.B. im Promille-Bereich (Lost-Update) fehl.
Zur Synchronisation von Threads steht daher nur die Option zur Verfügung,
einen Thread als `cooperative` zu markieren, der nur dann vom Scheduler
durch einen anderen Thread ausgetauscht wird, wenn der kooperative Thread
es explizit zulässt.

Ebenso ist kein `scrollup` für die Grafikausgabe migriert worden
und der Cursor springt stattdessen vom unteren Rand an den oberen Rand. In `VESA`
existiert zwar eine `offset()`-Methode, die die Funktion des VideoCore-Framebuffers
für eine virtuelle Auflösung nutzt, aber wenn dieser virtuelle Bereich
voll ist, muss trotzdem der sichtbare
Bildinhalt an eine andere Stelle kopiert werden. Ein solcher Refresh kann
z.B. passieren, wenn die CPU
nicht ausgelastet ist. Bislang wurde auf eine entsprechende Implementierung
verzichtet und das Areal einer doppelten virtuellen Bildschirmhöhe
wird in hhuOS-for-Pi4 für den Bluescreen genutzt, statt für ein `scrollup`
oder `scrolldown`. Im Commit \href{https://github.com/fbergama/pigfx/commit/2ada25d6}{2ada25d6}
von \cite{pigfxterm} wird für die serielle Terminalanwendung PIGFX auf dem Pi4
ein DMA-Transfer für das Bildschirmscrollen eingebaut, den man übernehmen
könnte.

Ein Dateizugriff und die Nutzung einer USB-Tastatur
sind ebenfalls in hhuOS-for-Pi4 nicht implementiert. Mit einer
Code-Adoption zum Dateizugriff auf die SD-Karte ist begonnen worden,
dies wird aber vermutlich nicht bis Mitte November
2021 fertig sein. Da die USB-Ports hinter
einem PCIe Bus liegen und es dazu nur Code-Beispiele gibt, die
eine virtuelle Adressierung nutzen, konnte eine entsprechende
Code-Adoption für den Zugriff auf eine USB-Tastatur noch nicht
umgesetzt werden. Die Klassen `PCIe` und `Xhci` stellen die
ersten Versuche eines Zugriffs auf Register des USB-Chips dar,
die bislang erfolglos waren. Der Namespace `USB` stellt
Adressen für Register zur Verfügung, die Prinzipiell auf
die USB-Schnittstelle des BCM2711 ohne PCIe zugreifen und
nur für den USB-C Power-Anschluss dienen. Dieser Anschluss des Pi4
ist jedoch noch kaum dokumentiert, was eine Implementierung
erschwert.

Bei hhuOS-for-Pi4 wurden alle Assembler-Dateien, die vor allem
das Booten, die Interrupt-Behandlung und den Thread/Task-Wechsel
betreffen, auf ein Minimum reduziert und zusammengeführt. Der Code
des Schedulers und der Threads wurde zu diesem Zweck überarbeitet
und auch der Code des Interrupt-Dispatchers entsprechend angepasst.
Bis auf VESA, Grafik und ein paar ,,Apps''  wurde der Code 
von hhuOS-for-Pi4 neu
geschrieben anstatt 1:1 aus dem Grundsystem von 2017 übernommen.
Im Bereich der Schriften wurde eine
etwas flachere Struktur genommen, bei der eine `Font` vom 
Prinzip her eine Informations-Struktur ist, die mit den Daten
einer speziellen Schrift gefüllt wird. Die Verwendung von Alpha-Werten
bei der Farbdarstellung ist ebenfalls modifiziert. Sie wurde durch
ein additives Misch-Konzept statt ein multiplikatives ausgetauscht.
Dies lag in einer schlechten Dokumentation des Framebuffers bzgl. der
Nutzung des Alpha-Werts und kann trotz Software-seitiger Umsetzung
weiterhin zu unerwarteten Darstellungen durch den Framebuffer führen.

Auf einen Teil an Code konnte in hhuOS-for-Pi4
verzichtet werden, da der Pi4 bereits einen Bootloader besitzt
und somit das Erzeugen unterschiedlicher Bootmedien entfällt.
Makefile und Linker-Skript beschränken sich nur noch auf
das Erzeugen einer `kernel8.img` Datei, die zusammen mit weiteren
Pi4-Standard-Dateien auf die SD-Karte kopiert werden muss.
Mit dem EEPROM-Bootloader des Pi4 war es außerdem möglich, neben
der Kernel-Datei eine andere Datei auf der
SD-Karte als `Ramdisk` in den Arbeitsspeicher zu laden. So
lässt sich eine BMP-Datei mit 32-Bit Farbtiefe oder eine
WAV-Datei (Stereo, 8-bit, unsigned, 44,1 kHz) ohne ein
Dateisystem für den hhuOS-for-Pi4 Kernel erreichbar machen.
Entsprechend wurde ein Audio-Player und ein BMP-Viewer
implementiert, obwohl dies der Grundidee eines Minimal-Systems
widersprach.

Der Aufbau von hhuOS-for-Pi4 entspricht
im weitesten Sinne einem Exokernel, in dem ein Kernel-Code zwar eine
grobe Abstraktion zur Hardware zur Verfügung stellt, aber
Anwendungs-Threads -- kurz: Apps -- auch direkt auf die Hardware
zugreifen können. Über *Calls* ließe sich eine stärkere Trennung
von Kernel-Space und User-Space erreichen.
In hhuOS-for-Pi4 sind Calls hinterlegt, die
beispielhaft aus einer App heraus Einfluss auf den Scheduler
nehmen können oder eine Prozess-Liste ausgeben. Dieses
Konzept der Trennung von verwaltendem Kernel-Code zu
User-Code stand jedoch bei der Implementierung nicht im Vordergrund.

In den folgenden Abschnitten werden die o.g. Besonderheiten, 
Abweichungen, und Design-Entscheidungen auf der Software-Ebene 
näher beschrieben, die bei der Portierung des Grundsystems auf
den Pi4 eine Rolle spielten.

![fig:structure:Skizze des hhuOS-for-Pi4](fig/structure.png)

## Notwendige Boot-Dateien

Im ersten Schritt ist ein Update des Pi4 EEPROMs
(Version vom 3.9.2020), in dem der Bootloader
enthalten ist, empfehlenswert.
Hierzu muss der Inhalt einer zip-Datei auf eine SD-Karte
kopiert werden, die man vorher mit FAT32 formatiert hat.
Bootet der Pi4 von dieser SD-Karte, wird das Update durchgeführt
und ein angeschlossener Bildschirm wird bei Erfolg grün.
Nach dem Update sollte man alle Dateien auf der SD-Karte
löschen und durch die Dateien im Ordner `sdcard/` des
Git-Repositorys \cite{hhuos4pi4} austauschen. Es werden mindestens
folgende Dateien benötigt:

config.txt
: Der Bootloader des EEPROMs wertet diese Datei aus. Darin
  sollte für hhuOS-for-Pi4 mindestens im Abschnitt `[all]`
  der Eintrag `core_freq_min=500` und `dtparam=audio=on`
  enthalten, damit eine flüssige Audio-Ausgabe möglich ist.
  Zusätzlich sollte mit `ramfsfile=...` eine der
  Dateien `sample.wav` oder `logo.bmp` angegeben werden,
  deren Inhalt mit der Einstellung `ramfsaddr=0x1C0000`
  an die Stelle 0x1C0000 im RAM kopiert wird (Ramdisk).
  Diese zwei Dateien sind eher optional, da sie nur dann
  von hhuOS-for-Pi4 nötig sind, wenn die Funktionen
  zur BMP-Darstellung oder WAV-Audio-Wiedergabe genutzt werden.
  Die nachfolgenden Dateinamen könnten alle
  abweichende Dateinamen haben, wenn man dies in der `config.txt`
  entsprechend vermerkt.

bcm2711-rpi-4-b.dtb
: Diese Devicetree-Blob Datei wird vom EEPROM als Default
  zum Register-Adress-Mapping und anderen Default-Einstellungen
  der Hardware genutzt. Einstellungen aus `config.txt` beziehen
  sich oft auf diese Datei. Die Datei wird auch von der Closed-Source
  Firmware des VideoCore VI (CV6) genutzt sowie von Linux-Versionen,
  die für den Pi entwickelt wurden.

start4.elf und fixup4.dat
: Dies ist die Closed-Source Firmware des VC6, die den weiteren
  Bootvorgang steuert und die Cortex-A72 Kerne startet.
  Sollten diese Dateien in einer Version ohne die *4* im Namen
  auf der SD-Karte (oder USB-Stick) sein, dann werden die
  A72 Kerne des Pi4 in einem 32-Bit Ausführungs-Modus gestartet. Mit
  der *4* im Namen startet das System im 64-Bit Ausführungs-Modus (AArch64).

kernel8.img
: Der VC6 sucht beim Booten via `start4.elf` nach einem Kernel,
  der den 64-Bit Modus der ARMv8 Architektur unterstützt, was
  durch die Ziffer *8* in `kernel8.img` markiert ist. In dieser
  Datei ist hhuOS-for-Pi4 enthalten und
  sie wird an die Stelle 0x80000 im RAM kopiert.

Diese Dateien sind auch in der Abbildung \ref{fig:structure}
in ockergelb dargestellt. Die Datei `kernel8.img` beinhaltet
das Betriebssystem mit allen (User-) Anwendungen.

Die Dateien `start4db.elf` und `fixup4db.dat` sind im
Ordner `sdcard/` hinterlegt, und können über `config.txt` einen
VC6 Debugging-Modus aktivieren. Dabei gibt der
VC6 seriell auf den GPIOs 14/15 Informationen aus. Dies
steht allerdings im Konflikt mit hhuOS-for-Pi4, was 
in `src/main.cpp` dafür erst auf UART3 (GPIOs 4/5) gestellt
werden sollte.

Außerdem gibt es in `sdcard/` noch zwei alternative Kernel8-Dateien, die den
Bootloader Raspbootin in einer Pi4-64Bit-Version enthalten \cite{rasbootgit}.
Eine Datei ist für die Nutzung der seriellen Schnittstelle auf den
Default-GPIOs 14/15 und eine für die GPIOs 4/5. Mit diesem
Miniatur-Kernel kann man zusammen mit dem Programm Rasbootcom
einen Kernel seriell einspielen. Auf diese Weise muss man nicht
den USB-Stick oder die SD-Karte nach jedem Kernel-Update
neu beschreiben, sondern ein Reset des Pis genügt.

## Aufbau des Codes

Neben dem Ordner `sdcard/` gibt es den Ordner `src/`, in dem 
der gesamten Quellcode des Kernels ist. In dem Ordner sind außerdem
ein `Makefile` und ein Linker-Skript `link.ld`, die auf der
Basis des Cross-Compilers[^crossgcc] die
Datei `kernel8.elf` und daraus die Datei `sdcard/kernel8.img` erzeugen.
Im Linker-Skript sind die Start-Adresse des Kernel-Codes und
zwei Adress-Konstanten für die Stackpointer der Exception-Handler
EL1 und EL2 hinterlegt. Da Hypervisor-Calls zum Springen von EL1 nach EL2
nicht genutzt werden, ist der Speicher zwischen diesen beiden Adressen
0x140000 und 0x1C0000 für den EL2 Stack ungenutzt. Eine Kollision
mit den Daten der Ramdisk an der selben Adresse ist nicht möglich,
da das Byte an der Adresse 0x1C0000 vom Stackpointer nicht genutzt
wird, sondern alle Bytes vor dieser Adresse. Ein Stack-Overflow Schutz
ist in hhuOS-for-Pi4 nicht implementiert.

Der Code in `src/` ist wie folgt aufgeteilt:

startup.S
: In dieser Datei ist der Code, der an Adresse 0x80000 steht und
  nach dem Default-Boot-Vorgang des Pi4 als erstes ausgeführt wird.
  Da nach dem Booten grundlegende Funktionen des Cortex-A72 Prozessor
  über Register konfiguriert werden wie z.B. der Stackpointer, der
  für C/C++ wichtig ist, macht es Sinn, dies in Assembler zu programmieren.
  Auch die Exception-Behandlung des Prozessors, in der direkt auf
  Register zugegriffen werden muss, ist in Assembler deutlich einfacher
  zu realisieren als in einer Hochsprache. Daher sind auch diese
  Bestandteile der Exception-Behandlung in `startup.S` enthalten.
  Des weiteren sind spezielle A64 Befehle des Cortex-A72
  außer `SVC` und `BRK` in diese Datei ausgelagert. Weiteren Assembler-Code
  gibt es nicht.

main.cpp
: Nachdem die `startup.S` den Stackpointer hinterlegt, die
  Float-Einheiten aktiviert und den Adressbereich für Variablen (BSS)
  auf Null gesetzt hat, wird `main()` aufgerufen. In `main()` werden
  die Betriebssystem-Komponenten initialisiert, verknüpft und zum
  Schluss der Scheduler gestartet mit drei Threads: Der Shell (Datei: `apps/Shell.cpp`),
  einem Thread zur Annahme von PS/2- und seriellen-Tastatur\"-eingaben (`apps/Interact.cpp`) sowie
  einer Uhr, die rechts oben auf dem Bildschirm erscheint. Da auch
  das Memory-Management (Ordner `heap/`) in der `main()` initialisiert wird,
  findet man hier auch das erste Mal ein `new` im Code.

OS.cpp
: Der `OS` Namespace enthält Pointer und Variablen, auf die von vielen
  Komponenten zugegriffen wird. Dazu gehören unter anderem
  der Wert des Systemcounters, der bei jedem Timer-Interrupt
  inkrementiert wird, die serielle Schnittstelle, die als Ersatz
  für eine Tastatur dient sowie ein Memory-Management (MM), was
  in den New- und Delete-Funktionen verwendet wird. Diese
  Funktionen stehen ebenfalls in der `OS.cpp` Datei zusammen mit
  ein paar leeren C/C++ Codehülsen zum Abfangen von Software-Fehlern,
  die C++ beim Linken verlangt. Wichtig ist das `os_regs`-Array,
  in dem bei einer Exception wichtige Register zum Debuggen via
  Bluescreen und zum Thread-Wechsel gesichert werden.

Die unter *kernel* markierten Komponenten in Abbildung \ref{fig:structure}
befinden sich in den o.g. Dateien sowie in den Ordnern `threads/`
und `exceptions/`. Folgende Komponenten sind im Ordner `devices/`
zu finden aber Aufgrund ihrer Hardware-Nähe in der Abbildung dem *kernel*
zugeteilt. Wegen ihrer grundlegenden Aufgaben ist darin weder
ein `new` noch ein `delete` enthalten:

devices/Peripherals.hpp
: In dieser Datei sind alle Startadressen der Geräte-Register hinterlegt.
  Der gleichnamige Namespace stellt Methoden zur Verfügung, die den
  Zugriff auf diese Adressen erlauben. Hier könnte man z.B. über
  Schutzmaßnahmen des Kernels nachdenken, wenn man bestimmen Zugriff
  nicht erlauben will.

devices/GpuMbox.hpp
: In dieser Datei sind wichtige Konstanten hinterlegt, die zur
  Konstruktion einer Massage an den VideoCore (GPU/VC6) notwendig
  sind. Neben `Peripherals` ist dies der zweite wichtige Namespace
  zum Zugriff auf die Hardware. Abgesehen von `VESA::init()` benutzt
  der Rest von hhuOS-for-Pi4 zur Kommunikation mit dem VC6 und
  dessen Schnittstelle zu anderen Geräten den Befehl `GpuMbox::call()`.

devices/CPU.hpp
: Die Klasse `CPU` ist für mögliche zukünftige Multicore-Überlegungen
  gedacht. Aktuell verweisen die Methoden `wait()`, `idle()`, `halt()`
  sowie `enable_int()` und `disable_int()` auf Code-Abschnitte
  in der `startup.S`.

devices/Power.cpp, Gpio.cpp und Clocks.cpp
: Diese Geräte verwenden bereits `Peripherals` und `GpuMbox`. Abgesehen
  von `Power`, womit sich der Pi4 Abschalten oder Neustarten lässt,
  dienen die Methoden in `Gpio` und `Clocks` als Hilfsfunktionen für
  weitere Geräte.

Da man den Arbeitsspeicher auch als Geräte verstehen kann, ist das
Memory-Management (MM) in der Abbildung den *devices* zugeordnet. Aufgrund
der Wichtigkeit dieser Komponente zur Verwaltung des Heaps, ist
der Code in einem eigenen Ordner `heap/` abgelegt. Darin enthalten
sind auch unterschiedliche Implementierungen mit sehr unterschiedlichen
Funktionen zum Loggen, Debuggen und Betrachten des Heaps.

Im Ordner `libs/` befinden sich sehr allgemeine Hilfsfunktionen und Klassen,
die vor allem zur Text- und Grafik-Ausgabe dienen und im Ordner `apps/`
sind die *user applications* (alles Threads, bis auf `LOGO`) abgelegt.

Die in Abbildung \ref{fig:structure} ausgegrauten Komponenten
bestehen aus improvisiertem Code[^uglyarmlock] oder
sind noch nicht vollständig implementiert. Darunter gehört auch eine
in Software implementierte PS/2-Schnittstelle für eine Tastatur.
Die Pufferung der empfangenen Bytes ist jedoch noch nicht
komplett umgesetzt und passiert im *Interact*-Thread. Da je
Tastendruck bis zu 4x 11Bit in 2,6ms übertragen werden und jedes
Bit einen Interrupt auslöst, werden manche Eingaben nicht rechtzeitig
durch den Thread verarbeitet[^dateps2sucks].


## Speicherlayout und Heap Verwaltung

Das Speicherlayout von hhuOS-for-Pi4 nach dem Booten wird in
Abbildung \ref{fig:memhhuos} gezeigt. In der Pi4 Version mit 4GB
wird der hintere Teil des physikalischen Speichers für die Peripherie
reserviert und der Speicher für den Framebuffer liegt unterhalb
von 1GB. Der Adressbereich, der
vom `ARMstub8`-Code beim Booten genutzt wird (unterhalb 0x8_0000)
nutzt der Kernel als Stack. C++-Code der außerhalb von
Exception-Handler und Threads läuft, greift auf diesen Stack zu.
Für die Exception-Handler EL1 und EL2 sind eigene Stacks
definiert, auch wenn für EL2 (z.B. Hypervisor Calls) in hhuOS-for-Pi4
noch kein Code hinterlegt ist. Für den Kernel (derzeit 152kB)
sind 262kB ab der Adresse 0x8_0000 vorgesehen. Ab der Adresse 0x1C_0000
sind 6MB für die Ramdisk eingeplant und zwischen 0x80_0000 und
0x3800_0000 ist leerer Speicherbereich für den Heap.

![fig:memhhuos:Das ist das Speicherlayout von hhuOS-for-Pi4 nach dem Booten.](fig/memoryhhuospi4.png)

Einer der ersten benutzten Bereiche des Heaps ist 64MB groß und wird
beim Instanzieren von `Audio` reserviert, um einen DMA-Buffer unterhalb
von 1GB zu haben[^strangehint].
Beim DMA-Buffer ist zu bemerken, dass die Datenstruktur der DMA-Einheit
des Pi4 nur 32-Bit Geräteadressen akzeptiert. Sollte also beim Kompilieren
des Kernels Warnungen erscheinen, betrifft dies exakt diese Codestelle,
bei der eine 64-Bit Adresse auf einen 32-Bit Wert herunter gebrochen wird.
Der zweite leere Speicherbereich liegt zwischen 0x4000_0000 und 0xF800_0000,
was oberhalb des Framebuffers und unter den ersten möglichen Geräte-Register
liegt. Die Bereiche für PCIe im Speicherlayout sind mit einem Fragenzeichen
markiert, da zwar die Register des PCIe-Geräts ab 0xFD50_0000 beginnen,
aber für die Register der gefundenen PCIe-Geräte 64MB Inbound-Adressbereich
und ein ebenso großer Outbound-Adressbereich zu reservieren ist. Die
Konfiguration dieser Adressbereiche im Zusammenhang mit virtuellen Adressen
bei diesem Mapping der PCIe-Geräte-Register konnte im Rahmen dieser
Masterarbeit nicht weiter vertieft werden.

Im Ganzen stehen hhuOS-for-Pi4 für den Heap 3712 MB zur Verfügung, die
sich auf zwei Adressbereiche aufteilen. Bei dem verwendeten Pi4
sind nur 4GB verbaut und somit gibt es keinen physikalischer Speicher
für einen dritten Adressbereich ab 0x01_0000_0000. Die Möglichkeit
den Pi4 so zu konfigurieren, dass die Peripherie automatisch in einen
hohen, virtuellen Speicherbereich liegt um vom physischen Speicher mehr zu
nutzen, ist leider kaum dokumentiert.

In den nächsten Abschnitten werden die drei Arten der Speicherallokierung
vorgestellt, und welche Funktionen implementiert sind. Grundsätzlich
sind im Memory-Management (Datei: `heap/MM.hpp`) die Speicherbereiche des Heaps
hinterlegt, die Größe des Stacks der einem Thread zugeteilt wird (`APP_STACK`)
und das Pointer-Alignment. Letzteres dient dazu, um beim Allokieren
nur Adressen als Pointer zu erzeugen, die auf 0x00 oder 0x0000 enden,
was die gcc-Toolchain verlangt. In MM sind auch die virtuellen Methoden,
die jeder Abkömmling von MM zu implementieren hat.

Um in der Shell-App ein Löschen eines belegten Speicherbereichs zu
ermöglichen, werden in der `dump()`-Methode von *Linked-List* und *Bitmask* eine
Id oder Position der belegten Bereiche ausgegeben, die mit `freeById(uint64_t)` verarbeitet
werden kann.

**Gready**

Die Klasse in `heap/MMGready.hpp` implementiert ein Memory-Management, in dem
weder `free(void * addr)` noch `freeById(uint64_t)` Speicher frei gibt.
Das Allokieren mit `alloc(uint64_t reqSize)` gibt einen Pointer `_next` zurück
und setzt diesen entsprechend zur angefragten Größe und dem
gewünschtem Alignment ein Stück weiter. Allerdings wird im Code zuvor
darauf geachtet, ob bei der angefragten Größe der untere Speicherbereich
des Heaps überschritten wird. In dem Fall springt der Pointer zuvor in
den zweiten Bereich ab 0x4000_0000.

Gready gibt Seriell viele Zusatz-Informationen aus, wie viel Speicher
allokiert wurde, welche Adresse zurückgegeben wurde und welche Adresse
hätte mit `delete` oder einem automatisch aufgerufenen Destruktor
gelöscht werden sollen.

Die Methode `dump()` beschränkt sich auf die Informationen, wie viel
Speicher existiert, belegt und noch frei ist. Details zu einzelnen
Blöcken existieren in dieser Art des Memory-Managements nicht.

**Linked-List**

Das Memory-Management in `heap/MMLinkedList.hpp` ist deutlich umfangreicher
als die *Gready*-Version. Es arbeitet mit einer Chunk-Struktur, die
vor der Adresse des Pointers steht, der mit `alloc()` zurückgegeben wird.
Initial stehen für die beiden freien Speicherbereiche des Heaps entsprechend
zwei Chunks an dessen Start-Adressen. In der Chunk-Struktur sind sowohl
Pointer zum nächsten und vorherigen Chunk hinterlegt, Informationen
zu dessen Größe, ob er belegt ist oder nicht sowie eine Id. In dieser
doppelt-verketteten Liste von Chunks wird bei einem `alloc()` nach einem
freien Chunk gesucht und ein entsprechendes Stück herausgeschnitten. Umgekehrt
wird bei einem `free()` in den benachbarten Chunks geschaut, ob diese
frei sind und mit dem freigegebenen Chunk vereint. Bei der Vereinigung
wird immer die kleiner Id für den neuen Chunk übernommen und darauf geachtet,
dass nie die beiden initialen Chunks miteinander vereint werden.

Neben `Free` und `Used` kann ein Chunk noch 4 weitere Typen haben,
um das Memory-Management der Linked-List besser verstehen und debuggen
zu können:

Rest
: Ein Chunk war vorher `Free` und dieser neue freie Chunk entstand,
  weil das Rest-Stück groß genug war, um einen eigenen freien Chunk
  zu bilden.

Reused
: Dieser belegte Chunk wurde nicht zerschnitten, als er noch frei war.
  Entweder hatte er vorher genau die gewünschte Größe oder ein mögliches
  freies Rest-Stück ist nicht groß genug, um einen eigenen Chunk
  zu enthalten.

Recyc
: Ein recycelter Chunk ist belegt, war aber zuvor ein Rest-Chunk.

Joined
: Dies ist ein freier Chunk. Sobald zwei Chunks (Typ: `Free`, `Rest`, `Joined`)
  Nachbarn sind und vereint werden können, wird der neue Chunk zum Typ `Joined`.

Die Methode `dump()` gibt die Größe des Speichers, die Menge
des belegten Speichers und die Größe des angefragten Speichers aus.
Durch die Chunk-Strukturen, die zusätzlich im Speicher hinterlegt sind,
kann der Speicher nicht komplett für den Heap genutzt werden und die
Menge des belegten Speichers ist immer größer als der mit `alloc()` angefragte.
Zusätzlich gibt `dump()` Informationen je Chunk aus (auch seriell)
und markiert die Chunks farblich je Typ.

**Bitmask**

In `heap/MMBitmask.hpp` ist ein Memory-Management implementiert, dass
mit einem Array von 32-Bit Werten arbeitet und am Anfang des unteren
Heaps abgelegt ist (im Code: `DIRstart`).
Jedes Bit in einem der 32-Bit-Werte steht für 1kB des Heaps. Der verfügbare
Speicher wird so durch eine Bitmask in 1kB Blöcke unterteilt,
wobei ein gesetztes Bit (1-Bit) für einen belegten Block steht.
Initial wird der Bereich, der den Heap wegen des Framebuffers
unterbricht, mit 1-Bits gefüllt bzw. mit 0xFFFFFFFF.

Bei einem `alloc()` wird die Bitmask nach einem entsprechend großem
Bereich aus 1kB Blöcken durchsucht, wobei unabhängig von der
geforderten Größe, 2 weitere Blöcke frei sein müssen. Diese
zusätzlichen 0-Bits markieren das Ende eines
vorherigen belegten Bereichs sowie das Ende des neu anzulegenden
Bereichs. Bei der Freigabe des Speichers ist dies wichtig, da zwar
aus der freizugebenen Adresse der erste Block in der Bitmaske
identifiziert werden kann, aber nicht gespeichert ist, wie groß dieser
Block ist. Ein `free()` löscht dann solange in der Bitmaske die 1-Bits,
bis das erste 0-Bit erscheint.

Durch die Bitmaske entfällt eine Logik, um benachbarte freie Bereiche
miteinander zu verbinden. Allerdings werden -- durch die Segmentierung
in 1kB Blöck und die End-Markierung eines belegten Bereichs -- mindestens
2048 Bytes belegt, auch wenn viel weniger mit `alloc()`
angefragt wurde. Die ca. 500kB für die Bitmask, um 4GB Speicher zu
verwalten, ist noch recht überschaubar. Allerdings muss bei der
Suche nach freiem Speicher diese Bitmask durchsucht werden. Dies
könnte man jedoch mit zusätzlichen Datenstrukturen oder
die Nutzung der anderen 3 Kerne, beschleunigen.

Zum Debuggen gibt sowohl `alloc()` als auch `free()` seriell Informationen
zu den Adressen und angefragten Speichergrößen aus. Die Methode `dump()`
gibt seriell keine Informationen aus, dafür stellt sie die Bitmaske in
Form einer Farblinie auf dem Bildschirm dar. Dies kann mehrere Sekunden
lang dauern, obwohl größere Bereiche verkürzt dargestellt werden.
Mit dem Dump werden belegte Bereiche nummeriert und ihre Größe
auf dem Bildschirm ausgegeben. Der Bereich des Framebuffers wird
bei der Ausgabe als ,,reserviert''  dargestellt.

## Thread und Scheduler

Der Scheduler in hhuOS-for-Pi4 enthält eine Queue mit Threads (`chain`).
Da es sich bei dem Scheduler um einen Namespace und kein Singelton
handelt, ist eine Initialisierung als Ersatz für einen Konstruktor
implementiert (`Scheduler::init()`). Der Scheduler kann in folgende
Modi versetzt werden:

NON\_INIT
: Der Scheduler enthält noch keine Threads und wird in diesem Modus
  keinen Thread-Wechsel versuchen.

RUNNING
: Der Scheduler enthält Threads und würde durch
  ein `Scheduler::switchover()` einen Wechsel des Threads versuchen.
  Das Switchover darf nur **innerhalb eines Exception-Handlers** passieren,
  da nur in dem Fall die Register für einen Wechsel vorbereitet wurden.
  Der Scheduler wird mit `Scheduler::run()` am Ende der `main.cpp`
  inital in den RUNNING-Modus versetzt.

PAUSE
: Durch den Aufruf eines Pause-Calls mit `Calls::calling(Calls::ID::PAUSE)`
  wird ein Exception-Handler aufgerufen, der den Scheduler
  in PAUSE versetzt. Der Thread, der diesen Call aufruft,
  muss später mit einem Yield-Call den Scheduler in
  RUNNING versetzen, damit ein Threadwechsel wieder möglich ist.

PREEMPT\_BUT\_PAUSE
: Dieser Modus ist der selbe wie PAUSE, aber er markiert, dass durch
  den Handler des Timer-Interrupts `PIT::me()->trigger()` ein
  Switchover aufgerufen wurde, während der Scheduler in PAUSE war.

In der Initialisierung des Schedulers wird neben dem PAUSE- und YIELD-Call
ein optionaler Yield-Call (OYIELD) einem
Call-Dispatcher `exceptions/Calls.hpp` hinzugefügt. Dieser dritte
Call-Typ ist in cooperativen Threads nützlich, da er nicht sofort
ein \nohyphens{Switchover} auslöst, sondern nur, falls der Scheduler
im Modus PREEMPT\_BUT\_PAUSE ist. Der \nohyphens{OYIELD} versucht also nur
dann diesen kooperativen Thread zu unterbrechen, wenn dieser
ursprünglich durch das präemptive Scheduling unterbrochen worden
wäre. Das besondere an kooperativen Threads ist, dass sie durch
ihre Modus-Markierung `cooperate` beim Thread-Wechseln
den Scheduler auf PAUSE setzen, damit sie bei ihrer Ausführung
nicht durch ein Switchover ausgetauscht werden.

Im Scheduler und in der improvisierten Semaphore `libs/Semaphore.hpp`
wird eine Art Schlummer-Funktion verwendet.
Damit können wenige Befehle, die nach dieser Funktion folgen,
innerhalb der 10ms Zeitspanne die einem Thread zur Verfügung stehen
ohne Unterbrechung des Schedulers ausgeführt werden. Dies ist wichtig,
wenn z.B. ein Thread einen neuen Thread mit `Scheduler::add()`
dem Scheduler hinzufügen will. Da bei jedem Switchover via
Timer-Interrupt die Variable `OS::systime` inkrementiert wird, kann durch
eine Schleife im Thread aktiv auf ein Wiedereintritt nach einer
Unterbrechung gewartet werden. Dies ist in `Scheduler::snooze()` implementiert.

**Thread Mode, Add, Switchover und Kill**

Ein Thread enthält in hhuOS-for-Pi4 folgende Verwaltungs-Informationen:

- eine dem Scheduler entnommene neue Id
- den Zeitpunkt, ab dem der Thread durch den Scheduler gestartet wurde
- einen Namen
- einen eigenen Stackpointer
- einen Thread-Modus

Der Thread-Modus markiert einen Thread als *normal* (präemptiv
unterbrechbar), *cooperate* (nicht unterbrechbar)
und *killed* (bei der nächsten Unterbrechung wird der Thread
aus der Queue entfernt). Dann gibt es noch für normale
und kooperative Threads je zwei weitere Modi: *fresh* und *young*.
Diese Modi teilen dem Scheduler mit, in welcher Vorstufe
des Schedulings sich der Thread befindet.

Bei der Instanzierung des Threads ist auch ein
Name, ein eigener Stackpointer und Id dem Thread beigefügt worden.
Ein `add()` fügt dann den Thread mit dem Modus *fresh* der Queue hinzu. 
Sobald nun ein `Scheduler::switchover()` ausgeführt wird
und der nächste Thread den Modus *fresh* hat, wird in dem
Array `os_regs[]` eine initiale Modifikation vorgenommen.
Dieses Array wird am Ende einer Exception wieder zurück in die
Register des Kerns geschrieben[^regssave].
Vier Werte werden angepasst:

os_regs\[29\] und os_regs\[31\]
: Der Index 29 steht für Register X29 und ist der Framepointer. Der Wert von Index 31
  steht für das Systemregister `SP_EL0`. Dieses Register ist
  der Stack-Pointer des User-Modes vor der Exception. Der Stackpointer wird automatisch
  abgespeichert, da während einer Exception ein eigener Stackpointer
  aktiviert wird. Auch wenn hhuOS-for-Pi4 nur im EL1 (Kernel-Mode) arbeitet,
  so wird der Stackpointer des User-Modes für Code außerhalb eines
  Exception-Handlers genutzt. In 29 und 31 wird die Adresse des
  Stackpointers des neuen Threads hinterlegt, um dies nach Verlassen des
  Handlers ausgetauscht zu haben.

os_regs\[32\]
: Der Wert von Index 32 wird nach Verlassen von Switchover in das
  Systemregister `ELR_EL1` geschrieben. Dieses Register ist
  wie `SP_EL0` eine Sicherung vor der Exception. Der Wert
  in `ELR_EL1` wird als Link für `ERET`-Befehl genutzt, was das Ende
  der Exception-Behandlung bedeutet. Genau wie in hhuOS \cite{hhuOSdev} und
  dem ursprünglichen Grundsystem \cite{ddosbasic} wird hier die Adresse
  der Funktion `kickoff(Thread *)` hinterlegt.

os_regs\[0\]
: Der Wert von Index 0 wird nach Switchover und vor `ERET` in das
  Register X0 geschrieben. Der Wert wird in Kombination mit `kickoff(Thread *)`
  als Parameter interpretiert. Deswegen wird in Switchover bei
  einem *fresh*-Thread dessen Pointer in os_regs\[0\] hinterlegt.

Nach Ablauf des Switchover und der Exception-Behandlung des Timer-Interrupts
springt der Programmcounter des Kerns zur Funktion `kickoff()` und
hat seinen Stackpointer auf der Adresse stehen, die dem frischen Thread
zugeteilt wurde. In `kickoff()` wird der Thread dann in den *young*-Modus
versetzt und dessen `start()`-Methode aufgerufen.

In `Thread->start()` wird im nächsten Schritt der Zeitpunkt hinterlegt,
ab dem er durch den Scheduler gestartet wurde. Danach erfolgt
der Aufruf der `main()`-Methode, die bei jeder ,,User-App''  -- also
jeder individuellen Implementierung eines Threads -- anders ist.

Beim nächsten Switchover werden zuerst alle `os_regs[]` des Threads
am Ende seines eigenen Stacks gesichert und die Werte des nächsten Threads
ins Array `os_regs[]` geladen. Ein Switchover, was dann
einen *young*-Thread als nächstes ausführen will, wird diesen
in den *normal* bzw. *cooperate* Modus abändern. Eine Anpassung
der `os_regs[]` Werte wie bei *fresh* ist nicht mehr nötig.

Sobald eine ,,User-App''  ihre `main()` beendet hat, wird zurück
in die allgemeine `start()`-Methode von `Thread.cpp` gesprungen.
Mit `Scheduler::kill(this);` versetzt sich dann die User-App selbst
in den Modus *killed* und verweilt in einer Endlos-Schleife. In
dieser Schleife wird ein `CPU::me().wait()` aufgerufen, was
zum A64-Befehl `WFE` (Wait-for-Event) führt. Das ist ein
Befehl, der den Kern entlastet. Es gibt noch `WFI` (Wait-for-Interrupt),
der nur dann den Kern weiter Ausführen lässt, wenn ein Interrupt
passiert. In der ARMv8-A Doku wird `WFE` als weitere,
leichtgewichtige Möglichkeit vorgeschlagen, um einen Mutex
zu realisieren. Dies macht aber nur in sehr speziellen Fällen
ohne eine C++ Scheduler/Thread-Implementierung sinn, da
bereits `RET` ein Event im Kern auslöst.

Wenn bei der Ausführung des Switchovers der nächste Thread mit dem
Modus *killed* markiert ist, wird er aus der Queue entfernt und
mit `delete` wird dessen Speicher freigegeben. Das ist ungewöhnlich,
da das `new` zur Erzeugung des Threads nicht im Scheduler ist.
In Zukunft könnte man das Factory-Pattern für den Scheduler nutzen,
der dann Imstanzen vom Typ `Thread` erzeugt.
Nach dem `delete` wird der nächste ausführbare Thread in der
Queue gesucht[^upsqueue].

**Klasse: lib/SimpleThread.hpp**

Die SimpleThread-Anwendung verwendet die C++ Sprachelemente *Template*
und *std::function*, um im Konstruktor eine Funktion und einen
Parameter zu übergeben. Die Shell-App nutzt dies,
um sehr simple Funktionsaufrufe in eigene Threads zu übergeben.
Ansonsten werden jedoch viele Funktionen des Betriebssystems in der
Shell-App direkt ausgeführt. Nachfolgend wird ein
Beispiel zur Nutzung von SimpleThread gezeigt, um
eine eingegebene Zahl im Char-Array `_line[]="double 33.125"` zu verdoppeln:

\pagebreak

~~~cpp
SimpleThread<char *> * simple;
simple = new SimpleThread<char *>(
    &(_line[7]),
    [](char * para) {
        double dbl = CharTool::strToDouble(para);
        dbl *= 2.0;
        VESA::me().dblOut(dbl, 8).print("\n");
    }
);
Scheduler::add(simple);
~~~

Der erste Parameter des Konstruktors ist ein Char-Pointer, der auf
den Anfang des Char-Array's zeigt, wo die Ziffern beginnen. Der zweite
Parameter ist eine C++-lambda-Funktion, die einen Char-Pointer erwartet.
Der Aufruf von `dblOut(dbl, 8)` ist eine spezielle Print-Methode von
VESA, um Double-Werte (in diesem Fall mit 8 Nachkommastellen) auszugeben.

**Bewertung**

Vier Punkte sind in der Thread/Scheduler-Implementierung untypisch:

- Anstelle der Queue mit Threads, sollte es eine Queue mit einer Struktur
  sein, in der ein Pointer auf den Thread und die Verwaltungs-Informationen
  des Threads hinterlegt sind.
- Die Modi der Threads sind nur schlecht vergleichbar mit den Zuständen
  von Tasks in anderen Betriebssystemen. Hier könnte man mit Ready, Running,
  Waiting und Zombie einen besseren Vergleich schaffen. Die
  Markierung *cooperate* sollte anders erfolgen.
- Dass bei der Initialisierung des Schedulers der Modus auf NON\_INIT
  gesetzt wird, ist unelegant. Hier sollten andere Mechanismen greifen,
  die evtl. den Scheduler automatisch Initialisieren beim ersten Switchover.
- Ein `delete` des Threads, der mit `kill()` aus dem Scheduler entfernt wird,
  ist zwar ein guter Mittelweg um dessen Verwaltungs-Informationen
  erst zu verlieren, wenn er nicht mehr gebraucht wird, aber hier sollte
  eine andere Möglichkeit geschaffen werden.

## PCIe und USB 3.0

Zum jetzigen Zeitpunkt (29.10.2021) ist in hhuOS-for-Pi4 nur der Zugriff
auf die Register der PCIe-Schnittstelle gelungen sowie das Auflisten
der PCIe Geräte und dem Finden des VL805 USB-3.0-Chips. Der Code der
Circle-Bibliothek wurde dafür teilweise migriert. Es existiert zwar
ein Befehl im Interface des GPU-Messagebus, um nach einem PCIe-Bus-Reset
die Firmware des USB-Chips einzuspielen, aber dies hat bisher keinen
Erfolg gezeigt. Auch ein tieferes Verständnis des Adress-Mappings der
PCIe-Schnittstelle konnte nicht mehr entwickelt werden, um hier
Fehlerquellen auszuschließen. Ein Möglichkeit wäre, die `start4db.elf`
zum Debuggen zu nutzen, um ein mögliches Loggen beim Einspielen der Firmware oder
Zugriffe auf die PCIe Schnittstelle zu beobachten.

Die Einbindung der PCIe-Klasse erfolgt in hhuOS-for-Pi4 durch ein `plugin()`
in den allgemeinen Dispatcher für Exceptions und dem *Shared Peripheral Interrupt* 180 (SPI). PCIe sollte
bei einem der 32 Slots für ein *Message Signaled Interrupt* (MSI) der angeschlossenen
PCI-Geräte einen ,,Interrupt 180''  auslösen. Dann dient PCIe selbst
als MSI-Dispatcher für die angeschlossenen Geräte. Das angeschlossene
USB-Host-Gerät (XHCI) hat sich vorher ebenfalls mit einem `plugin()`
an einen MSI-Slot des PCIe-Geräts einzubinden. Dies war bisher der Plan,
wie die Verbindung zwischen hhuOS-for-Pi4 mit dem USB3.0 in Verbindung
treten sollte, sobald dort ein neues USB-Gerät erkannt wird.

## Tastatur-Ersatz

Als Ersatz für eine Tastatur dient eine serielle Schnittstelle an den
Pinnen 6 (Ground), 8 (GPIO 14) für TXD und dem Pin 10 (GPIO 15) für RXD.
Auf keinen Fall sollte man das +5V Kabel eines USB-UART Konverters an
den Pi4 anschließen, da sonst der Pi4 versucht diesen Anschluss als
Spannungsquelle zu nutzen, was der USB-Anschluss des Rechners i.d.R. nicht
leisten kann! Mit dem Anschuss von 3 Kabeln an den Pi, einem für
max. 6,- EUR teuren USB-UART-Konverter und einer seriellen
Terminal-Software wie PuTTY hat man alles für ein hhuOS-for-Pi4
Text-Eingabe. Nutzt man den *raspbootin*-Kernel und die Raspbootcom-Software
zum Einspielen eines Kernels über die serielle Schnittstelle, kann man
auch Raspbootcom als seriellen-Terminal verwenden \cite{serialboot}.
Die Pinne 2 (+5V), 9 (Ground), 3 (GPIO 2, Data) und 5 (GPIO 3, Interrupt)
sind für den Anschluss einer PS/2-Tastatur vorgesehen. Der Code
ist in `devices/PS2Keyboard.cpp` und `apps/Interact.cpp` eingepflegt
hat aber z.Z. noch keinen brauchbaren Puffer für die Eingabe und
verliert gelegentlich Zeichen.

**Zusammenspiel Shell- und Interact-Thread**

Ein Pointer des Shell-Threads ist im OS-Namespace abgelegt.
Mit `OS::shell->addChar()` kann der Shell ein Zeichen übergeben werden oder
mit `OS::shell->del()` ein Zeichen gelöscht werden. In der `main()` der
Shell wird durch eine Endlos-Schleife versucht, mit der Methode `tryExecute()`
die übergebenen Zeichen auszuwerten.

Der Interact-Thread nutzt `OS::serial` um durch Polling neue Eingaben
über die serielle Schnittstelle zu erkennen. Ein Interrupt ist hierfür
noch nicht implementiert. Interact kann auch Escape-Sequenzen erkennen,
um einen Cursor mit entsprechenden VESA-Methoden auf dem Bildschirm
zu bewegen. Die Darstellung eines Cursors ist in Interact sehr
improvisiert und es kann zu Situationen kommen, wo er auf dem Bildschirm
nicht gelöscht wurde, nachdem er sich durch einen anderen Thread oder
einem Call bewegt hat. Interact befüllt die Shell mit Zeichen aus der
seriellen Schnittstelle und ist für deren Darstellung auf dem Bildschirm
sowie einem ,,echo''  für das Terminal-Programm des angeschlossenen
Rechners verantwortlich.

## Rotation-Encoder

Ein Rotation-Encoder ist ein Bauteil, was beim Drehen eines Knopfes
zwei Kontakte (Pin A/B) öffnet und schließt. Allerdings geschieht dies versetzt,
so dass z.B. bei einer Drehung nach rechts zuerst Pin A geöffnet wird,
während Pin B noch geschlossen ist. Dies geschieht dann abwechselnd beim
Weiterdrehen: Pin B wird geöffnet während A noch offen ist, A wird geschossen,
wenn noch B geöffnet ist, usw. Aus diesem Versatz ist es möglich, die
Drehrichtung zu erkennen. Der Einfachheit halber kann man auf eine
steigende Flanke (rising edge) warten, der z.B. bei Pin A eintritt, wenn
dessen Kontakt geöffnet wird. Ist dann an Pin B der Kontakt geschlossen,
wurde rechtsherum gedreht, ansonsten linksherum. Wird nicht gedreht, entsteht
auch keine Flanke. Zusätzlich haben viele Rotation-Encoder noch eine
Funktion als Taster, die beim Drücken einen Kontakt schließt.

Der Code `exceptions/RisingEdge.hpp` ist implementiert worden,
um ein *rising edge* als externen Interrupt verarbeiten zu können.
Vergleichbar mit `lib/SimpleThread.hpp` muss im Konstrukt eine Funktion übergeben
werden, die dann beim Interrupt als Exception-Handler arbeitet. Zusätzlich
wird die GPIO Nummer (`_pin`) übergeben, um bei der GPIO-Schnittstelle die
passenden Bits zu setzen, die das Erzeugen eines Interrupts steuern.
Da sich zeigte, dass auch eine fallende Flanke an einem anderen Pin
den selben Interrupt auslöst, wird zu Anfang in `trigger()` abgefragt,
ob auch der richtige Pin der Auslöser war. Am Ende
ist in `trigger()` der Aufruf `Gpio::clearStatus(_pin);` eingebaut, um
der GPIO-Schnittstelle das Ende dieser GPIO-Interrupt-Behandlung zu
signalisieren. Damit mehrere Service-Routinen auf dem selben
Interrupt reagieren können, wurde der Dispatcher von hhuOS-for-Pi4
gegenüber dem x86-Grundsystem entsprechend angepasst.

Die Nutzung von RisingEdge wird in der Anwendung `apps/RotEncoderApp.hpp`
beispielhaft gezeigt. Bei der Instanzierung der Threads werden die
GPIOs 20, 21 und 16 als Eingang mit einem Pullup konfiguriert. Dass heißt,
sollte ein Kontakt offen sein, wird dieser als 1/true gewertet. Entsprechend
sind die Anschlüsse des Bauteils so gewählt, dass diese sich bei Kontakt
mit *Ground* verbinden (= 0/false). In der `main()` wird dann eine
RisingEdge-Instanz erzeugt, der ein Pin des Encoders, ein Pointer
als Rückgabewert (`_event`) aus dem Handler sowie eine lambda-Funktion
als Handler übergeben wird. Nach einem `plugin()` des `RisingEdge`
folgt eine Endlosschleife. Diese nutzt Polling, um das Ergebnis
in der `_event` Variable abzufragen und entsprechend einen Hinweis
auf dem Bildschirm auszugeben. Der GPIO 16 für ein *button press*
wird ebenfalls durch Polling erfragt.

Im Destruktor von `RotEncoderApp` ist ein `delete` für `RisingEdge` enthalten.
Im Destruktor von `RisingEdge` ist zur Sicherheit ein `plugout()`
hinterlegt, damit nach der Nutzung der Instanz nicht mehr auf dem
SPI 145 reagiert wird. Ein `kill()` durch den Scheduler wird also
automatisch alles rückgängig machen.

## Bluescreen

Der Bluescreen in hhuOS-for-Pi4 ist eine Funktion, die im Laufe der
Implementierung immer umfangreicher wurde. Jede Exception
des EL1, der kein Handler zugeordnet ist, erzeugt den Bluescreen.
Dazu ist folgende Funktion in `startup.S` je Situation und je Exception-Typ
als Makro hinterlegt:

~~~gas
stp  x29, x30, [sp, #-16]!  // push x29, x30.
str  x0, [sp, #-16]!        // push original x0 value.
bl   os_regs_save_el1       // after that: the SP is at
mov  x0, \para1             // the same place than before.
mov  x1, \para2
bl   bluescreen
b    os_regs_load_el1
~~~

Dem Makro wird mit dem ersten Parameter der Exception-Typ übergeben und
mit dem Zweiten eine Interrupt-Nummer. Durch `mov x0, \para1` und dem
zweiten `mov` werden die Parameter der Bluescreen-Funktion übergeben.
Innerhalb der `startup.S` wird als Interrupt immer der 1023 übergeben,
da an dieser Stelle der Exception-Behandlung noch keine Auswertung
sattfindet, welche Interrupt-Nummer zu verarbeiten ist. In der
`dispatch()` Funktion[^dispatchfile] wird dieser
Parameter für den Bluescreen genutzt, um erzeugte Interrupts ohne
einer implementierten Service-Routine behandeln zu können. An der
selben Stelle geschieht vergleichbares mit nicht implementierten
Service-Routinen von Calls. Sowohl einem Befehl zum Call `svc` als auch einem
Breakpoint `brk` wird ein 16-Bit-Wert übergeben, der durch das Auslesen
des *Exception Syndrome Register* ESR_EL1 im Code des Bluescreen
verarbeitet werden kann. Genau wie bei einem Interrupt für einen
Thread-Wechsel werden die wichtigsten Register zu Anfang in `os_regs[]`
gesichert und nach dem Sprung in die C++ Funktion wiederhergestellt.
Der Zugriff auf diese Register und einer möglichen Modifikation
geschieht daher über die Inhalte in `os_regs[]`. Ein gutes Beispiel
dafür ist `os_regs[32] += 4;` was den Wert des ELR_EL1 um 4 Byte
erhöht und nach dem Ende der Exception-Behandlung den `brk` Befehl
überspringt, der die Exception auslöste.

Der Bluescreen gibt für alle auftretenden Fehler sprechende Namen
aus sowie den Inhalt der wichtigen Register zur Exception-Behandlung.
Speziell der Inhalt von ELR_EL1 ist interessant, da es sich um die
Programm-Adresse handelt, in der ein möglicher Fehler auftrat. Im
`src/Makefile` von hhuOS-for-Pi4 ist eine Art ,,Shortcut''  hinterlegt,
mit dem man sich den C++ Code zu der betroffenen Stelle durch `gdb`
anschauen kann. Zum Beispiel mit `make debug 0x93d00` wird
die `kernel8.elf`-Datei ausgewertet, die vor der `kernel8.img` Erzeugung
erstellt wird und die angegebene Programm-Adresse mit `gdb` sucht.
Der Befehl `aarch64-none-elf-objdump` `-d kernel8.elf` ist für eine Darstellung
des Assembler-Codes zu verwenden. Dieser `objdump` ist Teil der Toolchain.

Zusätzlich zu den Exception-Registern gibt der Bluescreen den Inhalt der X0-X30 Register aus
sowie den Speicherinhalt in der Nähe des Stackpointers, der zum Zeitpunkt
der Exception genutzt wurde. Seriell steht eine Debug-Schnittstelle
zur Verfügung, um durch den Inhalt zu blättern, zwischen Integer-, Hex-
und Double-Darstellung umzuschalten, den Bluescreen zu verlassen oder
einen Reset/Poweroff auszulösen. Es kann auch die Prozess-Liste ausgegeben
werden oder auf den nächsten Thread gewechselt werden.

Damit der alte Bildschirminhalt nicht verloren geht, nutzt hhuOS-for-Pi4
einen doppelt so hohen Framebuffer-Bereich als der Bildschirm ausgeben
kann. Durch `VESA::me().offset()` am Anfang und am Ende
des Bluescreens wird entsprechend der sichtbare Bereich umgeschaltet.

## Audio und DMA

Der Code zur Audio-Ausgabe basiert auf einem C-Baremetal-Tutorial von
Adam Greenwood-Byrne[^audiopi4]
speziell für den Pi4. Allerdings ist in hhuOS-for-Pi4 zusätzlich das Laden
einer WAV-Datei aus der Ramdisk möglich, sowie die Funktionen
Play, Pause, Stop und dem Anzeigen, wie viel von der Datei bereits
abgespielt wurde. Da die Audio-Ausgabe in hhuOS-for-Pi4 erst sehr
spät hinzukam, mangelte es an Recherche, wieso speziell bei kleinen
Datenmengen der DMA-Transfer ein ,,Krächtzen''  erzeugt. Nutzt
man hingegen Polling, um einen leeren FIFO-Buffer beim Transfer
von 8-Bit Werten auf die PWM-Hardware zu erkennen, ist das Audiosignal
klar. Letzteres funktioniert aber nur, wenn es keine Unterbrechungen
durch den Scheduler gibt.

Um einen Ton einer bestimmten Frequenz zu erzeugen, generiert `devices/Audio.cpp`
aus einer idealen 8-Bit Sinuswelle Daten, die im DMA-Puffer abgelegt
werden. Ein ,,Krächtzen''  bleibt aus, wenn hier mindestens 2MB an
Daten enthalten sind. Daher muss man für den ersten Shell-Beep ca. 2 Sekunden
in hhuOS-for-Pi4 warten, bis dieser Ton initial hinterlegt ist. Mit
dem Shell-Befehl `load` werden jedoch die Daten im Puffer durch
die Daten der WAV-Datei (Ramdisk) ausgetauscht und der Shell-Beep
abgeschaltet.

Durch die Einstellung `dtparam=audio=on` in der Boot-Konfigurations-Datei `config.txt`
werden die GPIOs 40/41 auf den 3,5mm Audio/Video-Anschluss des Pi4 geroutet.
Beide GPIOs gehören zum PWM1 Gerät, dessen Register an der Adresse 0xFE00c800
stehen. PWM1 hat zwei Kanäle (Stereo) die entsprechend auf die beiden
GPIOs gelegt werden müssen. Im Konstruktor von `Audio` wird dies konfiguriert.
In der WAV-Datei sind die Audio-Daten in 8-Bit Werten bzw. zwei 8-Bit Werte
je Kanal abgespeichert. Bei einer Signal-Abtastung von 44,1 kHz sind das
44100 Werte für eine Mono-Kanal-Sekunde. Im Code von `Audio::wav()` kann man das Füllen der
FIFO-Buffer je PWM-Kanal im Detail betrachten -- ohne Code zum DMA-Transfer.

Damit das Leeren des FIFO-Buffers mit 44,1 kHz erfolgt, muss die Takt-Quelle
für PWM-Signale konfiguriert werden (27MHz). Zusätzlich muss PWM1 so konfiguriert
werden, dass es diese Quelle zur Taktung des FIFO-Buffers nutzt. Zur
Feinabstimmung dient ein `RANGE` Wert je Channel der angibt, ab wie vielen
Takt-Signalen der nächste Wert genommen werden soll. Mit einem Wert
von 612 kommt man in etwa auf die 44,1 kHz der WAV-Datei. Auch diese
Konfiguration ist im Konstruktor von `Audio` hinterlegt.

**DMA**

Der Code zum *Direct Memory Access* (DMA) des Pi4 wird an dieser Stelle nur grob
beschrieben. Mit DMA lässt sich ohne zwingend einen Interrupt auszulösen ein Signal
an eine DMA-Einheit senden, um Quelldaten nach Zieldaten zu übertragen, sobald
diese nötig sind. PWM1 signalisiert die DMA1-Einheit, wenn der FIFO leer ist.
Beim Pi4 ist hingegen PWM0 mit der DMA5-Einheit verbunden. Diese spezielle
DMA-Zuordnung ist beim Pi4 fest vergeben.
In `Audio::load()` werden nicht nur die Daten der Ramdisk in den Puffer
geladen, der in hhuOS-for-Pi4 für den DMA1-Datentransfer vorgesehen ist,
sondern es wird eine Konfigurations-Struktur (DMA-Kontroll-Block) mit wichtigen Informationen
für den Transfer gefüllt. Darunter fallen Informationen wie die Datenmenge
und die Adresse der nächsten Struktur, sobald die Daten übertragen wurden.
Die DMA-Einheit des Pi4 verwendet alte 32-Bit Geräte-Adressen, die mit 0x7E
beginnen, so dass die Zieladresse des DMA-Transfers angepasst werden muss,
um den FIFO von PWM1 als Ziel angeben zu können. In `Audio::play()`
wird nun nur noch die Adresse des DMA-Kontroll-Blocks in das passende
Register von DMA1 geschrieben, PWM1 wird für ein Arbeiten mit der
DMA-Einheit-Konfiguriert und DMA1 wird gestartet. Wie bereits in *[Speicherlayout und Heap Verwaltung]*
erwähnt, wird beim Kompilieren an der Stelle eine Warnung ausgegeben,
weil eine 64-Bit Adresse für einen 32-Bit Adresswert im DMA-Kontroll-Block
hinterlegt werden muss.

[^dateps2sucks]: Stand: 16.11.2021
[^crossgcc]: im Ordner `gcc-arm-10.2-2020.11-x86_64-aarch64-none-elf/`
[^yourownos]: Die Studierenden konnte ihr eigenes Betriebssystem entwickeln.
[^uglyarmlock]: Problematik: siehe \protect\hyperlink{arm-spinlock}{ARM-Spinlock}
[^fluent]: Beispiel: `VESA::me().print("free: ").print(diff/1024, 10).print("kB\n");`
[^regssave]: In `startup.S` ist das der Sprungpunkt `os_regs_load_el1`.
[^upsqueue]: Stand 28.10.2021: Ein Erkennen, ob die Queue leer ist, findet nicht statt.
[^dispatchfile]: Datei: `exceptions/Dispatcher.cpp`
[^audiopi4]: Ordner `part9-sound/` in \cite{pi4tut}
[^strangehint]: In mehreren Code-Hinweisen wurde das unbegründet empfohlen.
