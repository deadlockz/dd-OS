In diesem Kapitel wird das grundlegende Architekturkonzept
des Raspberry Pis, dessen Broadcom Chip und vor allem die
ARM-Architektur der integrierten vier Cortex-A72 Kerne erläutert. Hierbei wird historisch
von innen nach außen beschrieben: Zuerst wird die Entstehungsgeschichte
der ARM-Architektur und deren Weiterentwicklung bis zur ARMv8.0-A
Architekturversion skizziert und wie diese in der Micro-Architektur
des Cortex-A72 umgesetzt ist. In einem weiteren Abschnitt wird
die Einbindung der ARM-Architekturen in den Raspberry Pi 1 bis 4
beschrieben sowie deren unterschiedliche Peripherie (speziell Pi4).
Zum Schluss werden zwei Alternativen zur ARM-Architektur
und dem Broadcom-Chip skizziert.

## Die ARM-Architektur

Die Architektur der Advanced RISC Machine (ARM)[^acorn]
kommt aus der Zeit der ersten Entwicklung \cite{patterson1980case} von RISC
Prozessoren und deren Kritik \cite{clark1980comments} bei
der Gegenüberstellung von RISC- und CISC.
Hierbei ist die Polarisierung bei der Sichtweise, was man unter einem
komplexen Befehlssatz versteht, nicht unerheblich. Man kann
den Unterschied u.a. bei der Verwendung eines
Microcodes festmachen sowie an der strikten Trennung bei RISC von
Befehlen, die auf den Arbeitsspeicher zugreifen[^lsarch] und
denen, die die Daten verarbeiten.
Die ursprüngliche Idee bei RISC war, auch keine unnötig
redundanten Befehle zu haben. Dieser minimalistische Ansatz
bezieht sich sowohl auf die vielen Adressierungsarten, die bei CISC
üblich sind und in vielen Befehlen verwendet werden können, als auch
auf mögliche Erweiterungen im Prozessor, die genauso gut in Software
implementiert werden können. IBM hatte in den 1970er bis 80er Jahren am
801-Minicomputer \cite{ibm801} gearbeitet (ein RISC-Prozessor), der vorrangig für
schnelle, sehr hardwarenahe Aufgaben (z.B. Telefon-Vermittlungsstelle)
gedacht war. Um einen Befehl
je Takt zu erreichen[^microcodexxx], beschränkte man sich auf nur wenige Befehle,
die durch eine Optimierung des Compilers auch in eine optimale
Reihenfolge gebracht werden konnten, um die Zeit beim Speicherzugriff
ideal zu nutzen. Später wurden diese Out-of-Order Optimierungen
zusammen mit einem Pipeline-Konzept in Hardware implementiert,
um im Schnitt aufgrund der deutlich höheren Geschwindigkeit bei
der Verarbeitung im Vergleich zum Speicherzugriff weiterhin einen Befehl
pro Takt zu verarbeiten.

In der ARM-Architektur finden sich in Design und Befehlssatz viele
Ähnlichkeiten der CISC-Architektur. So ist der Befehlssatz nicht
minimal: In vielen Befehlen lassen sich mehrere Operationen gleichzeitig
ausführen, was den Befehlssatz vergrößert. Ebenso existieren spezielle
Register für Fließkomma-Operationen und andere Prozessor-Erweiterungen (SIMD/NEON).
Allerdings bietet die Firma *ARM Limited* Erweiterungen
optional an. Sowohl die Erweiterungen als auch die Micro-Architektur
eines ARM-Chips werden gesondert an Chip-Hersteller über ein Lizenzmodell
vergeben. *ARM Limited* stellt selbst keine Prozessoren her und
vergibt lediglich Lizenzen für ihre Architektur. Neben den
Erweiterungen, die vergleichbar sind mit Prozessor-Features anderer
Hersteller wie AMD und Intel, wird bei ARM das Pipeline-Konzept
ständig überarbeitet und verbessert \cite[grau hinterlegter Text]{jaggar1997arm}.
In der ARM-Architektur v6[^arm11] finden sich
bereits einzelne Pipeline-Stufen \cite[S.~5]{cormie2002arm11}, die jeweils **mehrere**
Instruktionen **je Takt** verarbeiten können. In der Micro-Architektur
des Cortex-A72 (ARMv8-A), der im Raspberry Pi4
verbaut ist, wird dies auch bereits als *Super-Pipelining* bezeichnet \cite{A72microarch}.
Das ursprüngliche MIPS-Design-Konzept, welches keine verschränkten
Pipeline-Stufen vorsieht, ist beim A72 auf der Super-Pipeline-Stufe
trotzdem verschränkt: Die
Instruktionen werden nicht zwingend[^ioforce]
in der Reihenfolge verarbeitet, in der sie im Programm stehen[^ooopipe] und
in den Pipeline-Stufen *Rename/Dispatch* und *Issue* wird auf
Instruktionen reagiert, die einander bedingen. In der Abbildung \ref{fig:a72internal}
sind in zwei Grafiken die Pipeline und weitere Komponenten des A72
dargestellt. Aus der rechten Grafik ist zu entnehmen, dass in 5 Prozessor-Zyklen
15 Pipeline-Stufen verarbeitet werden. Diese Grafik wurde aus der Quelle \cite{A72microarch} entnommen
und zur besseren Darstellung neu gezeichnet.

![fig:a72internal:Die Abbildungen zeigen den funktionellen Aufbau des Cortex-A72 (links) aus \cite[S.~25]{cortexa72doc} sowie Details der 15 Pipeline-Stufen aus \cite{A72microarch}.](fig/a72internal.png)

Der ARM-Befehlssatz folgt
der Load/Store-Architektur: Vor der Verarbeitung von Daten, müssen diese zuvor
in Register geladen werden. Diese Befehle brauchen je nach
Vorhandensein der Daten im Cache unterschiedlich lange und können im
ARM-Befehlssatz auch ähnlich komplex sein wie bei CISC. In einem
Befehl können z.B. zwei Register geladen werden inklusive einer
Inkrementierung der Adressen in diesen Registern.

Eine weitere Besonderheit bei ARM: RISC-Prozessoren folgen im
Bereich von **Mikrocontroller** einer Harvard Architektur, also einer
strengen Trennung von Befehlsbus und Datenbus. Ein Schutz zur Trennung
durch einen virtuellen Adressraum ist in dem Fall nicht notwendig.
ARM ist allerdings eine modifizierte Harvard-Architektur. Die
Trennung zwischen Daten- und Befehlen beim Speicherzugriff
über einen einzelnen Bus wird durch einen getrennten Cache, sowie eine
Memory Management Unit (MMU) mit Paging als Speicherschutz gelöst.
Um ARM-Prozessoren auch für den Bereich der Mikrocontroller
interessant zu machen, wo ein virtueller Adressraum unüblich ist,
gibt es seit der ARM-Cortex-Prozessor-Familie auch Architektur-Versionen
ohne MMU. In dem Fall wird ein *M* anstelle eines *A*s beim Prozessor
und der Architektur-Version angehangen. Hierbei steht das *M* für
*Microcontroller profile*. Je nach Anwendungsfall bietet ARM die Cortex-M Chips
in einer reinen *Harvard-* (Trennung) oder einer reinen *Von-Neumann-*Architektur
an. Die Cortex-A Chips entsprechen dem *Application profile*,
was die Ausführung von unabhängig kompilierten Anwendungen mit
einem Flat-Memory-Modell innerhalb eines Betriebssystems ermöglicht.

## Hardware-Historie des Raspberry Pi

Die Ein-Chip-Systeme (SoC[^whatsoc]) erweitern
Micro-Architekturen um Schnittstellen, so dass z.B. eine Grafik-Ausgabe
wie beim PC, der Anschluss von USB-Geräten und der Zugriff auf gängige
Datenträger möglich ist. Durch diese Implementierung in einem Chip
entfallen die Leitungen zu anderen Chips sowie Absprachen und Standards zu anderen
Herstellern.

Für den ersten Raspberry Pi, dem Pi1, wurde 2013
die Micro-Architektur des \nohyphens{ARM1176JZF-S} von 2003 verwendet und zusammen
mit einem Closed-Source VideoCore IV vom Hersteller Broadcom sowie
einem USB Anschluss im BCM2835-Chip zusammengeführt. Besonders
deutlich wird das SoC-Konzept beim ,,5 Dollar Computer''  Pi Zero, der
ein vereinfachter Pi1 ist. Bei ihm ist kein USB-Hub-Chip und kein
USB-Ethernet Chip verbaut. Bei den üblichen Pi1, Pi2 und Pi3 sind diese Chips vorhanden,
um mehr als einen USB-Port und den Ethernet-Anschluss auf der
Platine zu verbauen. Das Default-Boot-Medium aller
Raspberry Pis ist eine SD-Karte, die direkt am Chip angeschlossen ist.

2015 bekam der neue Pi2 den BCM2836-Chip mit einem Quadcore Cortex-A7
und statt zwei sind nun vier USB Ports Standard. Vor allem durch die vier USB-Ports
wird statt einer 700mA Stromversorgung nun eine 2000mA Versorgung empfohlen.
2018 folgte im Pi3 durch die neue ARMv8-A Architektur des Cortex-A53 ein
64-Bit Modus (AArch64) mit einem neuen A64-Befehlssatz
und ein WiFi Chip on Board. Durch diese weitere Peripherie stieg die
Empfehlung auf eine 2500mA Versorgung. Im aktuelle Pi4 sind zwei der vier USB-Ports
3.0-tauglich und die Ethernet-Schnittstelle kann Gigabit-Ethernet. Dies ist
beim Pi4 möglich, da ein PCIe-Bus aus dem Chip
heraus führt anstelle eines USB2-Ports. Auch in diesem Fall stieg die
Empfehlung bei der Versorgung auf nun 3000mA.

![fig:bcm2711:Details zur Anbindung von Bluetooth, Ethernet und USB-Ports am BCM2711 sowie des internen VideoCore VI](fig/bcm2711.png)

Beim Gigabit-Ethernet handelt es sich um einen eigener Chip von Broadcom (BCM54213PE),
der genau wie der USB-3.0-Chip (VIA VL805) nicht im BCM2711 integriert ist.
Dies gilt auch für den WiFi-Chip (CYW43455). Der Bluetooth-Part des WiFi-Chips ist
seriell ansprechbar (siehe Abbildung \ref{fig:bcm2711}). Die Firmware
des Bluetooth- und des USB-3.0-Chips \cite{usbpciefirmware} ist nicht
dauerhaft im Chip vorhanden und
muss eingespielt werden (siehe \cite{bluetoothfirmware1}
und \href{https://github.com/isometimes/rpi4-osdev/tree/5c5fb149/part7-bluetooth}{Bluetooth-Part}
in \cite{pi4tut}).
Beim USB-3.0-Chip geht dies über ein EEPROM, welches
beim Pi4 verbaut ist und auch dessen Bootloader enthält. Seit 
September 2020 kann dieser Pi4-Bootloader direkt von USB booten \cite{pi4usbboot}.

Neben den Datenblättern der Architektur ARMv8.0-A sowie des
Prozessors Cortex-A72, der im Pi4 verwendet wurde, veröffentlicht *ARM Limited* auch
die Informationen für mögliche ARM Interrupt-Controller. Seit dem Pi4 ist
dies der GIC-400[^thegic]. Dieser
Interrupt-Controller ist zusammen mit den vier Cortex-A72 Kernen
im BCM2711 von Broadcom verbaut. Im BCM2711
ist außerdem eine neuere GPU von Broadcom verbaut (VideoCore VI), deren
Closed-Source Firmware beim Booten vom Bootmedium geladen wird.
Diese Firmware steuert den weiteren Bootprozess inkl. Einblenden der
Peripherie im Speicher. Die GPU stellt über einen Messagebus
eine weitere Schnittstelle zur Peripherie zur Verfügung. Obwohl
diese Schnittstelle bei allen Pi Versionen ähnlich ist, sind jedoch
die Unterschiede im Fall des Pi4 kaum dokumentiert. Ein
Beispiel sind die beiden LEDs für *Power* und *Active*, die beim Pi4
vollkommen anders angesteuert werden.

## Offene ARM- und Broadcom-Alternativen

Der Dokumentationsumfang des Raspberry Pi4 ist aufgrund des Closed-Source
VideoCore VI nicht sehr umfangreich. Im Handbuch des Pi4 ist nicht einmal
die API des VideoCore-Messagebus beschrieben. In dem 164 Seiten starken
Handbuch finden sich auch keine Details zum Bootvorgang, keine Beschreibung
der Pinne zur SD-Karte für einen Dateizugriff und auch keine Register-Beschreibung
des neuen Interrupt-Controllers. Komplett offene Architekturen
verbieten solche Dokumentationslücken.
Durch FPGAs ist Architektur-Design für
Hobby-Entwickler`*`innen möglich und es existieren inzwischen
zahlreiche[^exampleriscv] offene
Architekturen \cite{examplecores}.
Vergleichbar mit der ARM-Architektur ist hier RISC-V, deren 
Micro-Architektur ohne Lizenzgebühren publiziert
wird \cite{riscvlist}.
Mitglieder der RISC-V International Organisation können für ihre Chips
ein RISC-V-konformitäts-Logo bekommen. Die Mitgliedschaft ist
für Privatpersonen kostenlos \cite{riscvmember}.
Vergleicht man Preise, Taktraten und Arbeitsspeicher von SoC Boards auf der Basis
von RISC-V Chips, so sind diese Boards aber weder leistungsstärker
noch günstiger als der Raspberry Pi.
Ein Kompromiss zwischen Dokumentationsumfang, Leistungsstärke
und Offenheit der Architektur bietet der Chip AM335x von
Texas Instruments. In diesem ist
zwar auch ein ARM Cortex-A8 (von 2005) integriert, jedoch ist der
komplette AM335x Chip mit 5118 Seiten Handbuch ausführlicher und
übersichtlicher beschrieben als der Raspberry Pi \cite{beaglebone}.
Dieser Chip ist unter anderem im *BeagleBone Black* verbaut
und eine etwas leistungsstärkere, doppelt so teure Alternative zum **Pi1**.
Dafür ist das
Hardware-Design[^harddesign] des
gesamten *BeagleBone Black*
neben der umfänglichen Anleitung frei verfügbar. Bei jeder Pi
Version hingegen gibt es nur reduzierte, vereinfachte Pläne, bei denen
z.B. GPIO-Pinne aus der Anleitung fehlen, die dort mit *INTERNAL* markiert
sind. Genauso lückenhaft ist die Beschreibung der Einstellungen
in der `config.txt` Datei. So kann
hhuOS-for-Pi4 aktuell nur mit `dtparam=audio=on` auf einem
Audio/Video-Connector Sound ausgeben statt auf HDMI. Wie eine HDMI-Sound-Ausgabe
ohne Linux möglich ist, fehlt im Handbuch und der Online Dokumentation des Pi.
Der *BeagleBone Black*
ist daher deutlich verständlicher für Menschen, die keine Linux-Version
als Basis jeder Anwendung nutzen wollen.
So kann man im Hardware-Design des BeagleBone z.B. erkennen, wie der HDMI-Anschluss
bei diesem Board möglich ist, denn der AM335x unterstützt
nur eine hochauflösende Farb-LCD-Ausgabe. Der HDMI-Anschluss
dieses Boards ist durch einen TDA19988 Chip umgesetzt \cite[S.~10]{lcdhdmi}. Dieser
Chip wandelt das LCD-Signal in ein HDMI-Signal um.

[^microcodexxx]: CISC-Microcode brauchte für einen Befehl immer mehrere Takte, als die
  CISC-Prozessoren noch ohne Pipeline und Hyperthreading arbeiteten. Diese Techniken
  kamen ab dem Intel Pentium Pro (Nov. 1995) hinzu, der im Kern eine Art RISC-Prozessor
  nutzt.
[^acorn]: von 1983-1989: Acorn RISC Machine
[^lsarch]: Load/Store Architektur
[^arm11]: ARMv6, eingeführt mit der ARM11 Prozessor Familie
[^ioforce]: es existieren Befehle im Bereich des Speicherzugriffs, die In-Order forcieren
[^ooopipe]: Out-of-Order-Pipeline; kurz: OoO-Pipeline. Es existieren auch Cortex-A Prozessoren, 
  die eine In-Order Pipeline haben, wie z.B. der A7 und der A53 des Pi2 und Pi3.
[^whatsoc]: System-On-a-Chip
[^thegic]: GIC: Generic Interrupt Controller
[^exampleriscv]: Beispiel für ein solches Hobby-Projekt: \url{https://github.com/darklife/darkriscv}
[^harddesign]: Schaltpläne und PCB-Dateien (Printed Circuit Board)
