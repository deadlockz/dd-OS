# Motivation

## Motivation

- Studierenden die Konzepte eines OS klar machen
- ein schlichtes OS entwickeln als Beispiel
- Umfangreiche Dokumentation des Systems
- Ist hhuOS durch Entfernung "alten" Codes zu vereinfachen?
- Focus auf konkrete Hardware: = Vereinfachung?
- **Modernes Architektur Konzept vorstellen/gegenüberstellen**
- Popularität von hhuOS erhöhen

# hhuOS

## hhuOS bisher

- aktueller Stand ist sehr komplex geworden
- Dokumentations-Stau (?)
- Focus auf 386'er (CISC) mit BIOS sowie Emulation mit Bochs
- soll auf jedem echten PC laufen (tat es aber nicht)
- Ethernet bisher nur ein Treiber vorhanden
- Singlecore

## Eine alte Idee

> Warum machen wir nicht hhuOS für den Raspberry Pi? Da ist immer
> die selbe Hardware verbaut also z.B. immer die selbe Netzwerk-Karte?

Aber:

- keine 16bit BIOS calls und nicht x86 (damaliger Fokus der Veranstaltung)
- inzwischen gibt es unterschiedliche PIs

## Die neue Idee

> Lässt sich hhuOS vereinfachen, indem man es auf konkrete System-on-a-Chip
> Hardware migriert? Wäre Multicore & Ethernet leichter implementierbar?

## Aufbau

![Aufbau](images/structure.png)

# System-on-a-Chip

## System-on-a-Chip (SoC)

Wahl einer konkreten Hardware, die Populär ist:

- Arduino oder High-End Microcontroler (z.B. ATSAMD21 Cortex M0)
  - Kaum mit PC vergleichbar (8Bit Chips)
  - High-End Microcontroler: zu teuer und trotzdem kein ganzer Rechner
- Mit x86 Chip:
  - Hardware variiert stark
  - recht teuer, da Kleinst-Serien
  - komplexe Entwicklungstools (z.B. bei Intel Galileo)
- BeagleBoard, BananaPi, Olimex Boards
  - Raspberry Pi mit besserer Hardware haben oft selben Preis
  - noch zu unpopulär
  - aber "offener" als Pi (closed-source VideoCore!)

# Raspberry Pi

## Raspberry Pi - Historie

- 2013 Pi1: Singelcore ARM, 32bit
  - Chip Layout ARM1176JZF-S (veröffentlicht 2003)
  - Chip Spezifikation ARMv6, Umgesetz durch Broadcom in BCM2835
- 2015 Pi2: Quadcore
  - Cortex-A7 (2013)
  - ARMv7-A, BCM2836
- 2018 Pi3: 64bit, WLAN + Bluetooth
  - ARM Cortex-A53 (2014)
  - ARMv8-A, BCM2837
- 2019 Pi4: Gigabit Ethernet, USB3.0, 4K HDMI
  - Cortex-A72 (2016)
  - ARMv8-A, BCM2711

## Raspberry Pi4

- populärer Kleinstcomputer
- guter Kompromis zwischen Preis, Leistung und Aktualität
  - pi3 1GB Version: EUR 34,-
  - pi4 2GB Version: EUR 40,-
- relativ gut dokumentiert (viel basiert auf Pi3 und 32Bit)


## Pi4 vs Pi3

Der Pi4 hat ...

- anderer Interrupt Controller (GICv2 = GIC-400)
- Flash EEPROM mit Bootloader
- USB und Ethernet via PCIe (deswegen USB3 und Gigabit)
- leistungsstärkere GPU

ältere PIs: die BCM283x Chips haben einen USB2 Port, der mit einem
USB-HUB und einem USB-Ethernet Chip bestückt ist.

# Pi4 Dokumentation
## Pi4 Dokumentation

- Allgemein: ARMv8 (Acorn/Advanced RISC Machines) ist nur eine
  Spezifikation, und *ARM Limited* vergibt Lizenzen an
  Hersteller (BCM = Broadcom)
- Pi4 Doku ist BCM2711 Doku, die der BCM283x (Pi2/3) ähnelt
- oft wird auf allgemeine ARMv8 Doku referenziert
- Genric-Interrupt-Controller: ebenfalls Verweis zu öffentlicher ARM Doku
- VideoCore (steuert Booten und "orchestriert" ARM-Cores und Peripherie) ist **kaum dokumentiert**

## Beispielcode

- Linux Kernel (c, Assembler)
- BareMetal von David Welch (c, Assembler, Fokus auf Pi3 und 32bit) 
- Open RISC OS (BBC Basic, Assembler)
- osdev Pi3/4 Tutorials (c, Assembler, Pi4 bzgl. Interrupt unvollständig)
- Circle Lib (c++, Assembler; unübersichtlich, da alle Pis und auch 32bit)
- free RTOS (c, Assembler, unklar für welchen Chip)
- ARM Doku: Assemblercode zum Teil fehlerhaft
- RUST embedded (RUST, Assembler)
- [ARM Trusted Firmware-A](https://github.com/ARM-software/arm-trusted-firmware)

# Meilensteine
## Meilensteine 1/3 (erledigt)

- Zielhardware wählen
- C-BareMetal Tutorials nutzen und c++ Code in hhuOS anpassen:
  - booten
  - Heap
  - shell
  - UART (statt Tastatur)
  - Text und Grafik
- ARM Dokus und andere Codes lesen
- Abgrenzung: Must-Have und Nice-to-Have

## Meilensteine 2/3

- x SystemTimer
- x Exception/Bluescreen und Interrupt
- x preemptive Scheduling und Threads
- x optionale cooperative Threads
- . USB Tastatur (Abbruch nach ca 1 Wochen wegen Problemen PCIe Memory Mapping)
- x Test der Floating-Point Register
- x Test/Bugfix join von leeren Speicherblöcken
- . Dokumentation des neuen hhuOS
- Masterarbeit

x = erledigt / . teilweise erledigt

## Meilensteine 3/3 (optional)

- x Sound
- . Multicore Nutzung
- docker Image zur Entwicklung 
- Datei Zugriff
- Ethernet
- . Vertiefung ARMv8 Exception Level
- x (Mutex-)Semaphoren (MMU Problematik)
- . Vertiefung ARMv8 Virtueller Speicher und Cache (Sections -> Paging)

x = erledigt / . eingelesen

# Zeitplan
## Zeitplan

- x Interrupt: Ende April 2021
- x Scheduler und preemptive Multitasking: Mitte Mai 2021
- x MA anmelden und Klärung der Ziele/Thema: Ende Mai 2021
- x Semaphoren, Spinlock, MMU: Abbruch Mitte Juli 2021
- Dreiteilung
  - Implementierung: bis Mitte August 2021
  - Masterarbeit: bis Ende Oktober 2021
  - Dokumentation (+Bluetooth Keyboard): falls Zeit November 2021

## Aufbau Masterarbeit (grob)

Kurz-Thema: "Migration von hhuOS IA32/x86 auf AArch64/BCM2711"

- Grundlagen
  - Vergleich diese hhuOS mit anderen Lern OS
    - oostubs, [www4.cs.fau.de/Lehre/WS19/V_BS/](https://www4.cs.fau.de/Lehre/WS19/V_BS/)
    - xv6, [pdos.csail.mit.edu/6.828/2012/xv6.html](https://pdos.csail.mit.edu/6.828/2012/xv6.html)
  - Entscheidungskriterium: warum hhuOS/Pi4 und nicht Minix/QEMU ?
- neue Hardware = neue Herausforderungen
- Kern: Vergleich aller Unterschiede x86 und Pi4 in hhuOS
- Debugging (?) / Anhang für DIY

# Fazit
## Fazit nach der Implementierung

- Pi4 zu neu: Linux, Beispiele und Boot-Dateien ändern sich ständig
- Pi4 Dokus: lückenhaft dokumentiert, Fokus im Prinzip auf Linux.
- USB & Bluetooth: Closed-Source Firmware. Ob per Hand
  geladen werden kann oder via Boot-Dateien und GPU Calls unklar.
  Zum Teil nur via Suche in closed Issues herausfindbar.
- Spinlock (und vermutlich auch PCIe): setzt aktive MMU voraus, wo Beispiel
  speziell Pi4 nur in Code-Monstern zu finden sind.
- hhuOS Pi4: erster Beispiel Baremetal C++ Code mit Thread-Scheduler
  via Interrupt und mit new/delete.
- CPU BCM2711 ist nicht 1:1 Cortex A72
- Lüfter auch bei niedriger Tacktung nötig sowie hoher Stromverbrauch (nicht USB3 konform).
- Micro A/V und Micro HDMI Connector unpraktisch.

## Fazit nach der Implementierung

Vereinfachungen durch Pi4 im Vergleich zu x86

- WAV Dateien abspielen via PWM ist recht einfach
- Toolchain
- wichtige Register der Peripherie ist zentral und übersichtlich dokumentiert
- Code zum Booten
- Zugriff auf hohe Speicheradressen
- kein "läuft bei mir nicht", weil Hardware fest vorgegeben ist
- Bluescreen: Framebuffer kann virtuell größeren Bildschirm haben, so
  dass alter Bildschirminhalt erhalten bleibt
- Zugriff auf digitale Pinne für LEDs, Buttons, externe Interrupts
- Multicore vermutlich einfach implementierbar, solange kein Cache aktiviert wird.

# Quellen
## Quellen (Links)

- [ARMv8](https://developer.arm.com/documentation/den0024/a)
- [BCM2711](https://www.raspberrypi.org/documentation/hardware/raspberrypi/bcm2711/README.md)
- [Pi3 C Tutorial](https://github.com/bztsrc/raspi3-tutorial/)
- [Pi4 C Tutorial](https://github.com/isometimes/rpi4-osdev)
- [BareMetal C Programme auf Pi](https://github.com/dwelch67/raspberrypi/)
- [Linux auf Pi3 Tutorial](https://github.com/s-matyukevich/raspberry-pi-os/)
- [Circle Lib](https://github.com/rsta2/circle/)
- [GPU VideoCore Mailbox](https://github.com/raspberrypi/firmware/wiki/Mailbox-property-interface)
- [Pi Preise](https://www.berrybase.de/)
- [Aktueller Stand meines Codes](https://gitlab.com/deadlockz/dd-OS/-/tree/ARMv8/)
