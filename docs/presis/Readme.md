# Motivation

Es gibt ein Grundsystem für die Lehre, aus dem später hhuOS entstand.

\vspace{5mm}

**Leider:**

- 3'86er: Altlasten (1979-1985)
- Booten
  - real Mode umständlich
  - viel Assembler-Code!
- PC Speaker Sound
- 16bit BIOS Calls
- keine *state-of-the-art* Techniken

=> viele Hacks notwendig

## Idee: ARM/Pi4

- vergleichbar umfassend wie 3'86er dokumentiert
- neue Technik (2013-2019)
- nur ein Hersteller
  - weniger Ärger mit Treibern (?)
  - Peripherie-Register: bekannte RAM-Position


# Pi4 und AArch64

Vorteile und Herausforderungen

## Pi4 und AArch64

**Raspberry Pi4 Vorteile** 

- Bootloader in Hardware
- ein Chip
  - für Audio
  - für Grafik
  - serielle Schnittstelle
  - Zugriff Bootmedium
- leider nur via PCIe ein USB-Zugang (extra Chip)

## Pi - System-On-a-Chip

![alter Pi1 ohne USB Hub/Ethernet Chip; RAM im BCM Chip](fig/pi1.png)

## Pi - System-On-a-Chip

![Pi4 SoC mit PCIe statt USB; PCIe Hardware-Hacks (links)](fig/pi4.png)


## Pi4 und AArch64

**64Bit-Modus Vorteile**

- neu und ohne Altlasten
- Interrupt/Exception
  - Handler haben eigenen Stack
  - sichert SP, LR und PSTATE selbst
  - maskiert selbst

## Pi4: neue Herausforderungen

Baremetal

- Dokus zielen auf Linux
- eher Multicore statt Scheduler/Tasks
- Framebuffer/Grafik
  - gut beschrieben
  - alle Pi-Versionen gleich

Pi4

- bisher wenige Baremetal-Beispiele
- nur ein C++ Beispiel: ,,Code-Monster''  Circle
- neuer Interrupt-Controller (GIC-400)
- Out-of-Order-Pipeline (OoO)
- PICe statt nur USB

## Pi4: Baremetal Eingabe

- Serielle-Schnittstelle
- Firmware für USB3-Chip (einspielen unklar)
- PS/2 Tastatur-Hack aus 8-Bit Arduino Code

![Seriell und PS/2](fig/ps2.png)

## Interrupt Controller

- Distributor-Register
  - Ziel-Kern(e) je Interrupt
  - (De)Maskierung
  - Identifizierung des IRQ-erzeugende CPU
- CPU-Interface (je Kern)
  - EOI Markierung
  - Identifizierung des IRQs

## AArch64 Exception-Level

- vier Level
  - hhuOS-for-Pi4 nutzt EL1 (Kernel)
  - alle Level haben eigenen SP
  - SP0: User-Level ,,normal'' 
- vier Exception-Typen
  - Debug, SError
  - Interrupt, Fast-Interrupt

## hhuOS-for-Pi4: Bluescreen

nur zwei EL1 Exception-Typen gehen zu `dispatch()`

- Debug-Typ (Call, Break)
- Interrupt-Typ

restliche Exception-Typen: `bluescreen()`

- Fehleranalyse
- Register Ausgabe
- Hex Stack Dump
- serielle Interaktion

## hhuOS-for-Pi4: Dispatcher

- Kernel Calls
  - Scheduler/Coroutinen Steuerung
  - Prozess-Liste
  - Systeminfo
  - ... it is your turn!
- Interrupt
  - durch unbehandelte IRQs des GIC-400 ,,blättern'' 
  - passende Service-Routinen aufrufen
- unbekannter Call/IRQ: Bluescreen!

# Cortex-A72

ARM-Spinlock & OoO-Pipeline

## Cortex-A72: ARM-Spinlock

2 Probleme

- RISC bzw. ARMv8.0-A: atomarer Compare-And-Set unmöglich!
- OoO-Pipeline: Verarbeitungsreihenfolge unsicher

**Trick**

- Pipeline-Reoder verhindern
  - Load-Aquire `ldXr` und Store-Release `stlXr`
  - umklammert Code-Abschnitt, der sich auf konkrete Adresse bezieht
  - e**X**clusive Flag
- Loop in Assembler
  - solange bis 1x eXclusive ausgeführt
  - schreibt erst in RAM/Cache, wenn erfolgreich

Ergebnis: Code nur 1x ausgeführt und nur 1x in RAM geschrieben


## Cortex-A72: ARM-Spinlock

- hhuOS-for-Pi4
  - ohne Cache
  - ohne Paging bzw. virtuelle Adressen
  - daher ARM-Spinlock **nicht möglich**

Befehle Load-Aquire `ldXr` und Store-Release `stlXr` brauchen das!

=> Details zur Adresse wie z.B. Cache-Verhalten, Periphere-Register
und Sharing zwischen CPUs sind u.a. **in Page-Table-Entries**.


# Einstieg in hhuOS-for-Pi4

Booten und Threads


## Booten

- GPU
  - findet SD-Karte
  - lädt Bootloader aus EEPROM
  - `config.txt` auswerten
  - GPU 2nd Stage Firmware `start4.elf`
  - platziert `ARMstub8.S`, `kernel8.img` und `ramdisk`
- ARM CPU0-3
  - startet `ARMstub8.S` an Adresse 0x0
  - CPU0 springt nach Adresse 0x80000
  - Code `startup.S` springt nach `main()`

## Booten bis `BL main`

![Pi4 Einstieg in den Kernel](fig/structure-boot2.png)

## Threads

- Handler sichert in `os_regs[]`
- Thread durchwandert 3 Modi
  - *fresh*: `kickoff()` nötig
  - *young*: `start()` bzw. `main()` erreicht
  - *normal*: bereits 1x unterbrochen
- Scheduler ändert `os_regs` ab
  - Rücksprung ggf. `kickoff()`
  - eigener SP für Thread
  - bei Wechsel: Speicherung `os_regs` an Thread-Stack-Ende
- Handler stellt `os_regs` her

# Fazit und Ausblick


Fazit und Ausblick


## Fazit

- Pi4
  - zu neu: kaum Baremetal-Beispiele
  - unerwartet erschwerte Entwicklung
    - Closed-Source Bootloader
    - Closed-Source Board-Layout
  - Linux booten undokumentiert & an GPU Firmware gebunden
- AArch64
  - angenehmes Exception-/SP-Konzept
  - tatsächlich kaum Altlasten
  - Multicore sieht einfach realisierbar aus
- ARM / Cortex-A72
  - OoO-Pipeline herausfordernd
  - wichtige Befehle brauchen Cache und Page-Table-Entries

## Fazit

Ziele erfüllt?

- ja
  - weniger Assembler
  - einfaches Booten
  - keine Altlasten
  - moderne Architektur
- nein
  - USB-Tastatur
  - Spinlock-Semaphore
  - MMU/Cache
- grundsätzlich mehr Überblick
  - Kleinst-Rechner
  - Pi4, ARMv8.0-A, AArch64, Cortex-A72
  - Kleinst-Betriebsysteme (in der Lehre)

## Ausblick

- Scheduler
  - Thread-Infos sollten nicht im Thread sein
  - Thread-Modi besser: ready, waiting, running, killed
- Multicore Konzept
- User-Mode
- stärkere Kernel/User-Trennung via Calls
- SD-Karte: FAT32-Zugriff
- virtuelle Adressen
  - ARM-Spinlock
  - PCIe/USB (?)

# Video

## Video

[Video Datei im Web](https://gitlab.com/deadlockz/dd-OS/-/raw/ARMv8/docs/screencast.mp4)

https://gitlab.com/deadlockz/dd-OS/-/raw/ARMv8/docs/screencast.mp4

\appendix

## Quellen/Links

- [Programmer's Guide for ARMv8-A](https://developer.arm.com/documentation/den0024/a/)
- [Bild PCIe Hack 1](https://domipheus.com/blog/raspberry-pi-4-pci-express-it-actually-works-usb-sata-gpu/)
- [Bild PCIe Hack 2](https://www.hackster.io/news/pci-express-on-the-raspberry-pi-4-9b03c59f7a04)
- [Circle: A C++ bare metal environment for Raspberry Pi with USB (32 and 64 bit)](https://github.com/rsta2/circle)
- [hhuOS for Pi4 (AArch64): Quellcode, Masterthesis, etc.](https://gitlab.com/deadlockz/dd-OS/-/tree/ARMv8/)
- [Pi4 C Tutorial](https://github.com/isometimes/rpi4-osdev)
- [BCM2711 Handbuch](https://www.raspberrypi.org/documentation/hardware/raspberrypi/bcm2711/README.md)
- [Cortex A72 Pipeline](https://www.anandtech.com/show/10347/arm-cortex-a73-artemis-unveiled/2)
- [ARM Cortex-A72 MPCore Processor](https://developer.arm.com/documentation/100095/0003/)
- Raspi-Kernschau, Das Prozessor-Innenleben des Raspberry Pi 4 im Detail, c't 20/2019, S. 164, Bild von Maik Merten

## weitere Grafiken

![hhuOS-for-Pi4 Speicherlayout](fig/memoryhhuospi4.png)

## weitere Grafiken

![hhuOS-for-Pi4 Struktur (vereinfacht)](fig/structure.png)

## weitere Grafiken

![Memory Model ARMv8-A, Prog. Guide S.202, 211](fig/cci.png)

## weitere Grafiken

![Broadcom Chip BCM2711](fig/bcm2711.png)

## weitere Grafiken

![Cortex-A72 intern und Pipeline](fig/pipeline.png)

